using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Render.Map;
using AncientEmpires.Test.TestEngine;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Jobs;

namespace AncientEmpires.Test.Benchmark.Engine.Map
{
    [SimpleJob(RuntimeMoniker.Net472, baseline: true)]
    public class GameMapRendererBenchmarks
    {
        private readonly TestGameEngine _engine;
        private readonly GameMapRenderer _mapRenderer;
        private readonly GameMap _map;

        public GameMapRendererBenchmarks()
        {
            _engine = new TestGameEngine();
            _engine.Register();
            _mapRenderer = _engine.GetComponent<GameMapRenderer>();
            _map = _engine.GetComponent<GameMap>();
        }

        [Benchmark]
        public void BuildMap()
        {
            var mapTiles = _map.Tiles;
            //_mapRenderer.BuildMap(ref mapTiles);
        }
    }
}