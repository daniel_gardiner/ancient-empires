using AncientEmpires.Engine.Interface;
using AncientEmpires.Test.TestEngine;

namespace AncientEmpires.Test.Benchmark.Engine.Layout
{
    public class GameInterfaceBenchmarks
    {
        private readonly TestGameEngine _engine;
        private readonly GameInterfaceRenderer _interface;

        public GameInterfaceBenchmarks()
        {
            _engine = new TestGameEngine();
            _engine.Register();
            _interface = _engine.GetComponent<GameInterfaceRenderer>();
        }
        
        public void ManagementPanel()
        {
            
        }
    }
}