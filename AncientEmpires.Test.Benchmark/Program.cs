﻿using AncientEmpires.Test.Benchmark.Engine.Map;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Jobs;
using BenchmarkDotNet.Running;

namespace AncientEmpires.Test.Benchmark
{
    public static class Program
    {
        static void Main(string[] args)
        {
            var config = GetGlobalConfig();
            BenchmarkRunner.Run<MeshGroupsBenchmarks>(config);
            BenchmarkRunner.Run<GameMapRendererBenchmarks>(config);
        }

        static IConfig GetGlobalConfig()
            => DefaultConfig.Instance
                .WithOption(ConfigOptions.DisableOptimizationsValidator, true)
                .AddJob(Job.Default
                    .WithWarmupCount(1)
                    .AsDefault());
    }
}