using System;
using System.Collections.Generic;
using AncientEmpires.Engine;
using AncientEmpires.Engine.Entities;
using AncientEmpires.Engine.Entities.Pathing;
using AncientEmpires.Engine.Entities.Units;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Map.Picker;
using AncientEmpires.Engine.Map.Rendering;
using AncientEmpires.Engine.Render;
using AncientEmpires.Engine.Render.Groups;
using AncientEmpires.Engine.Render.Map;
using AncientEmpires.Engine.Render.Text;
using AncientEmpires.Test.TestEngine;
using BenchmarkDotNet.Attributes;

namespace AncientEmpires.Test.Benchmark
{
    public class MeshGroupsBenchmarks
    {
        private TestGameEngine _engine;
        private GamePathSolver _gamePathSolver;
        private Unit _entity1;
        private Unit _entity2;
        private IsometricPositionStrategy _isometricPositionStrategy;
        private MapRenderer _mapRenderer;

        [GlobalSetup]
        public void Setup()
        {
            _engine = new TestGameEngine();
//            _engine.Map.Mesh.OnInitialize();
            _gamePathSolver = new GamePathSolver(_engine, null);
            _isometricPositionStrategy = new IsometricPositionStrategy(_engine, _engine.Map.Size, _engine.Map.TileSize);
            _mapRenderer = new MapRenderer(_engine, null);

            _entity1 = _engine.GetComponent<MapManager>().EntitySpawner.Spawn(new GameVector2(50, 50), Entity.Create(_engine, EntityType.RomanLegion)) as Unit;
            _entity2 = _engine.GetComponent<MapManager>().EntitySpawner.Spawn(new GameVector2(60, 50), Entity.Create(_engine, EntityType.GreekLegion)) as Unit;
        }
        
        public void Benchmark()
        {
            
        }

        [Benchmark]
        public void CalculatePathBenchmarks()
        {
            for (int i = 0; i < 100; i++)
            {
                _gamePathSolver.Solve(_entity1, _entity1.Coordinates, _entity2.Coordinates);
            }
        }

        public void CalculatePath()
        {
            var x = 51;
            var y = 51;
            var path = _gamePathSolver.Solve(_entity1, _entity1.Coordinates, new GameVector4(x, y, 0, 0));
        }

        // 28.6s
        // 20.8
        // 10.2
        // 9.1
        // 8.1
        // 6.4
        [Benchmark]
        public void CalculatePathMaximumIterations()
        {
            for (int i = 0; i < 100; i++)
            {
                _gamePathSolver.Solve(_entity1, _entity1.Coordinates, _entity2.Coordinates);
            }
        }

        // 2m
        // 1:30m
        // 1:05m
        // 46s
        // 6.4
        // 5
        [Benchmark]
        public void ShowPathingScores()
        {
            var result = _gamePathSolver.Solve(_entity1, _entity1.Coordinates, _entity2.Coordinates);
            for (var i = 0; i < 3; i++)
            {
                _engine.GetComponent<GameDebug>().ShowPathingScores(_entity1, result.Path, result.Scores);
            }
        }

        // 2m
        // 1:30m
        // 1:05m
        [Benchmark]
        public void Dictionary()
        {
            var dictionary = new Dictionary<string, MeshFragment>();
            for (int i = 0; i < 4; i++)
            {
                for (var x = 0; x < 200; x++)
                {
                    for (var y = 0; y < 200; y++)
                    {
                        CheckDictionary(x, y, dictionary);
                    }
                }
                
                dictionary.Clear();
            }
        }

        private void CheckDictionary(int x, int y, Dictionary<string, MeshFragment> dictionary)
        {
            var key = $"some-prefix-{x}-{y}";
            if (dictionary.ContainsKey(key))
            {
                Console.WriteLine("{0} - hit", key);
                return;
            }
            
            dictionary.Add(key, MapMeshFragment.Create(_engine, _mapRenderer, new MeshKey(KeyType.Coordinates), 3, GameVector4.Zero, new MapTile(GameVector3.Down.ToVector4())));
        }
    }
}