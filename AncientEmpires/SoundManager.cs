using System.Media;

namespace AncientEmpires;

internal static class SoundManager
{
    private static readonly SoundPlayer[] Sounds = new SoundPlayer[11];

    public static void Init_Sounds()
    {
        /*
        if (Main.Instance == null)
            return;
            
        // SOUND
        var mainWindowHandle = Main.Instance.Handle;

        Globals.DirectSound = new DirectSound();
        Globals.DirectSound.SetCooperativeLevel(mainWindowHandle, CooperativeLevel.Normal);

        var primaryBufferDesc = new SoundBufferDescription();
        primaryBufferDesc.Flags = BufferFlags.PrimaryBuffer;
        primaryBufferDesc.AlgorithmFor3D = Guid.Empty;

        var primarySoundBuffer = new PrimarySoundBuffer(Globals.DirectSound, primaryBufferDesc);


        // Default WaveFormat Stereo 44100 16 bit
            
        FileSystem.ChDir(Application.StartupPath + @"\Resources\Sound");
            
        Sounds[1] = AddWave($@"{Application.StartupPath}\Resources\Sound\Click.wav");
        Sounds[2] = AddWave($@"{Application.StartupPath}\Resources\Sound\Battle.wav");
        Sounds[3] = AddWave($@"{Application.StartupPath}\Resources\Sound\Walk.wav");
    */
    }

    public static SoundPlayer AddWave(string fileName)
    {
        var soundPlayer = new SoundPlayer(fileName);
        soundPlayer.LoadAsync();
        return soundPlayer;
    }

    public static void Play(int index)
    {
        //dSounds[index].Play();
    }
}