﻿using System.Collections.Concurrent;
using System.Runtime.InteropServices;
using Microsoft.VisualBasic;

namespace AncientEmpires;

/*internal static class Globals
{
    public static bool IsFullScreen = false;
    public static ConcurrentDictionary<string, int> CurrentErrors { get; set; } = new();

    [DllImport("user32")]
    public static extern int SetCursorPos(float x, float y);

    [DllImport("gdi32")]
    public static extern int SetBkColor(int hdc, int crColor);

    [DllImport("gdi32")]
    public static extern int SetTextColor(int hdc, int crColor);

    [DllImport("gdi32", EntryPoint = "TextOutA")]
    public static extern int TextOut(int hdc, float x, float y, string lpString, int nCount);

    // Public dsbdesc As New PhantomDirectSound.VBDSBUFFERDESC
    public static Rectangle TempRect;
    public static int ExtraX = 60, ExtraY = 30;
    public static bool DrawMiniMap;
        
    public static int CurrentProvince;
    public static bool DisableInput;
    public static bool MenuOpen;
    public static decimal TheSpeed;
    public static bool Collision;
    public static bool XCollision, Active, YCollision;
    public static bool AllDead;
    public static string[] MonthDB = new string[14];
    public static string UnitsPath;
    public static bool DeploySucceded;
    public static decimal Speed;
    public static Rectangle[,] Letters = new Rectangle[3, 27];
    public static int CurrentOptions;
    public static decimal ArmySize;
    public static bool FoundRoad;
    public static Rectangle Interface;
    public static Rectangle[] WallSprite = new Rectangle[17];
    public static string TBMsg;
    public static RectangleF[] Labels = new RectangleF[11];
    public static ButtonType[,] OtherButtons = new ButtonType[3, 21];
    public static ButtonType[,] ScrollButtons = new ButtonType[3, 4];
    public static int PDx, PDy;
    public static RectangleF MiniMapFrame;
    public static Rectangle[] MiniMapFocusBox = new Rectangle[4];
    public static RectangleF TextBox;
    public static Rectangle[] MiniMapUnit = new Rectangle[11];
    public static int cc, ee;
    public static Rectangle Bar;
    public static int TempFrame, TempDirection;
    public static int Upper1, XDis, YDis, Lower1;
    public static bool LoadIcon;
    public static int Upper2, Lower2;
    public static int ExtraDamage, TempDamage, DamageDone;
    public static int TempHealth, ExtraArmour, TempRace;
    public static int TempTeam, TempTroop;
    public static bool Draging;
    public static bool ShiftDown;
    public static decimal DumbieVar;
    public static decimal DumbieVar2;
    public static int TrueX, TrueY;
    public static int NP1, NP2;
    public static int NumberPressed;
    public static int xx, yy;
    public static int Counter3;
    public static UnitGroupType[] UnitGroup = new UnitGroupType[10];
    public static MapType[,] BattleMap = new MapType[75, 75];
    public static MapType[,,] ProvincalMap = new MapType[10, 77, 77];
    public static int Start;
    public static bool BuildingCollision, UnitCollision;
    public static int MouseX, MouseY;

    public static GameDataType GeneralData;
    public static int TempX, TempY;
    public static int hdc;
    public static int LengthOfBar;
    public static decimal RegulatedSpeed;
    public static int NextTroop, RowLength;
    public static decimal XPos, YPos;
    public static RoadType RoadDeploy;
    public static int XPos2, YPos2;
    public static bool TextureCollision;

    public static int TempVar1, TempVar2;
    // TYPES

    public struct LeigionType
    {
        public bool MovingToGarrison;
        public bool Garrisoned;
        public bool FollowingRoad;
        public int Counter;
        public int Waiting;
        public bool Moving;
        [VBFixedArray(10)] public int[] DestX;
        [VBFixedArray(10)] public int[] DestY;
        public int CurrentPoint;
        public int ControlPoints;
        public decimal wCurX;
        public decimal wCurY;
        public int Province;
        public int NumberInLeigion;
        [VBFixedArray(20)] public int[] UnitType;
        public int Direction;
        public int Frame;
        public int Aggressiveness;
        public bool ReRouting;
        public int RoadX;
        public int RoadY;
        public int TempX;
        public int TempY;
        public bool Dead;
        public int TeamTarget;
        public int Target;
        public bool Attacking;
        public decimal Speed;
        public bool wLeft;
        public bool wRight;
        public bool wUp;
        public bool wDown;

        public void Initialize()
        {
            if (DestX != null)
                return;

            DestX = new int[11];
            DestY = new int[11];
            UnitType = new int[21];
        }
    }

    public struct RoadType
    {
        public int Ix;
        public int Iy;
        public int Fx;
        public int Fy;
    }

    public struct BuildingDatabaseType
    {
        public int Yeild;
        public int ObjectWidth;
        public int ObjectHeight;
        public int Cost;
        public int ObjectStrength;
        public string Message;
        public int Period;
    }

    public struct BuildingType
    {
        public int Period;
        public int Strength;
        public int NumOfType;
        public BuildingInstance[] Instances;
        public static BuildingType Empty = new();
    }

    public struct BuildingInstance
    {
        public static BuildingInstance Empty = new();

        public decimal Population;
        public int Yeild;
        public int Phase;
        public int Counter;
        public int Top;
        public int Left;
        public int ObjectWidth;
        public int ObjectHeight;
        public bool Dead;
    }


    public struct UnitForGame
    {
        public int Agressiveness;
        public bool ReRouting;
        public int TempX;
        public int TempY;
        public bool Dead;
        public int Armour;
        public int Damage;
        public int TeamTarget;
        public int Target;
        public bool Attacking;
        public int MaxHealth;
        public decimal Speed;
        public bool wLeft;
        public bool wRight;
        public bool wUp;
        public bool wDown;
        public int UnitType;
        public int Race;
        public decimal wCurX;
        public decimal wCurY;
        public int Direction;
        public int Frame;
        public bool Moving;
        public int DestX;
        public int DestY;
        public int Health;
        public int Skill;
        public bool InBattle;
        public int Waiting;
        public int Counter;
        public int Period;
    }

    public struct GameDataType
    {
        [VBFixedArray(10)] public int[] TeamSelected;
        [VBFixedArray(10)] public int[] TroopSelected;
        public int NumberSelected;
        [VBFixedArray(3)] public int[] PlayerRace;
        [VBFixedArray(3)] public int[] NumOfTroops;
        public int MapX;
        public int MapY;
        public int MapWidth;
        public int MapHeight;
        public int IconType;
        public int ScrollSpeed;
        public int GroupSelected;
        public bool Formation;

        public void Initialize()
        {
            TeamSelected = new int[11];
            TroopSelected = new int[11];
            PlayerRace = new int[4];
            NumOfTroops = new int[4];
        }
    }

    public struct MapType
    {
        public EntityType StructureType;
        public int StructureNumber;
        public int Texture;
        public int Team;
        public int Troop;
    }

    public struct UnitGroupType
    {
        [VBFixedArray(20)] public int[] GroupTroop;
        public int NumberInGroup;
        public bool Formation;

        public void Initialize()
        {
            GroupTroop = new int[21];
        }
    }

    public struct CPType
    {
        public int x;
        public int y;
    }

    // CONSTS

    public const int mWidth = 20;
    public const int mHeight = 20;
    public const int MiniX = 470;
    public const int MiniY = 390;
    public const int SquareWidth = 35;
    public const int SquareHeight = 35;
    public const int mMoveIcon = 1;
    public const int mAttackIcon = 2;
    public const int mMiniMapIcon = 3;
    public const int mStandardIcon = 4;
    public const int mScrollIcon = 5;
    public const int mBulldozeIcon = 6;
    public const int mDown = 1;
    public const int mRight = 2;
    public const int mLeft = 3;
    public const int mUp = 4;
    public const int mScrollDown = 7;
    public const int mScrollRight = 8;
    public const int mScrollLeft = 9;
    public const int mScrollUp = 10;
    public const int mAttackDown = 5;
    public const int mAttackRight = 6;
    public const int mAttackLeft = 7;
    public const int mAttackUp = 8;
    public const int mDead = 9;
    public const int mStandard = 1;
    public const int mGameOptions = 2;
    public const int mUnitOptions = 3;
    public const int mGameStats = 4;
    public const int mBuild = 3;
    public const int mBuildingData = 5;
    public const int mUnitData = 6;
    public const int mAttackRequest = 7;
    public const int mGarrisonRequest = 8;
    public const int mGlobal = 1;
    public const int mProvincal = 2;
    public const int mBattle = 3;
    public const int mSelect = 20;

    public const int mUnit = 3;
    public const int PopUpX = 230;
    public const int PopUpY = 102;
    public const int TextBoxX = 160;
    public const int TextBoxY = 447;
    public const int mJanuary = 1;
    public const int mFebuary = 2;
    public const int mMarch = 3;
    public const int mAprilv = 4;
    public const int mMay = 5;
    public const int mJune = 6;
    public const int mJuly = 7;
    public const int mAugust = 8;
    public const int mSeptember = 9;
    public const int mOctober = 10;
    public const int mNovember = 11;
    public const int mDecember = 12;
    public const int mLengthOfYear = 1000;
    public const int mFull = 1;
    public const int mHalf = 2;
    public const int mQuarter = 3;
    public const int mRoman = 1;
    public const int mGreek = 2;
    public const int mButtonClick = 1;
    public const int mBattleSound = 2;
    public const int mArmyMarching = 3;
}*/