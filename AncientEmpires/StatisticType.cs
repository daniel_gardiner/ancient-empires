using Microsoft.VisualBasic;

namespace AncientEmpires;

public class StatisticType
{
    public int BuildingTeam;
    public EntityType BuildingType;
    public int BuildingNumber;
    public int FoodRation;
    public int YourLegion;
    public int ComputerLegion;
    [VBFixedArray(3)] public int[] NumberOfLegion;
    public int Income;
    public int Housing;
    public decimal Population;
    public int Tax;
    public int Month = 1;
    public int Year;
    public decimal Grain;
    public decimal Money;

    public void Initialize()
    {
        NumberOfLegion = new int[4];
    }
}