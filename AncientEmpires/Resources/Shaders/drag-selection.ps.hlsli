float4 material_drag_selection_action(PixelShaderInput input) : SV_TARGET
{
    if (drag_selection_active.x > 0.5)
    {
        return draw_bordered_rectangle(input, drag_selection_box, float4(1, 1, 1, 1), float2(2, 2));
    }
    else
    {
        //return float4(1, 0, 0, 0.1);
    }

    discard;
    return float4(0, 0, 0, 0);
}
