#include "global.hlsli"

float4 invert_color(in float4 color)
{
    return float4(1 - color.r, 1 - color.g, 1 - color.b, color.a);
}

float4 draw_selection_circle(in float2 uv, float squish, float2 center)
{
    uv.y *= squish;
    float4 base_color = float4(0, 0, 0, 0);
    const float dist1 = 1 - distance(center, uv);
    const float dist2 = distance(center, uv);
    //float color1 = smoothstep(sin(timing.x / 300) * 0.80, sin(timing.x / 300) * 0.90, dist1);
    float color1 = smoothstep(0.75, 0.77, dist1);
    //float color2 = smoothstep(sin(timing.x / 300) * 0.10, sin(timing.x / 300) * 0.20, dist2);
    float color2 = smoothstep(0.20, 0.22, dist2);
    const float4 color1_4 = float4(color1, color1, color1, 1);
    const float4 color2_4 = float4(color2, color2, color2, 1);
    float4 color = lerp(color1_4, color2_4, 0.5);

    if (color.r < 0.6)
        return base_color;

    color.a = 0.6;
    return float4(color.rgb, 1);
}

float4 draw_collision_bounds(in float2 uv, float2 tile_size, float2 collision_center, float radius)
{
    //const float2 offset = (collision_center - tile_size / 2) / tile_size;
    const float2 offset = collision_center;
    const float ratio = tile_size.y / tile_size.x;
    float x = (0.5 - uv.x) * ratio - offset.x;
    float y = (0.5 - uv.y) - offset.y;
    radius /= tile_size.x;

    float dist1 = sqrt(x * x + y * y);
    float smooth_distance = smoothstep(0, radius, dist1);
    float color1 = float4(smooth_distance, smooth_distance, smooth_distance, smooth_distance);

    if (color1 >= 1)
        return float4(0, 0, 0, 0);

    if (color1 > 0.95)
    {
        float4 color = smoothstep(0.97, 1, color1) * float4(0.6, 0.2, 0.2, 1);
        color.a = saturate(color.a + 0.5);
        return color;
    }

    return float4(0, 0, 0, 0);
}

float4 draw_entity_selection_highlight(PixelShaderInput input)
{
    const float4 selection1 = draw_selection_circle(input.uv1, 1.7, float2(0.5, 1.37));
    const float4 selection2 = invert_color(draw_selection_circle(input.uv1, 1.7, float2(0.5, 1.39)));
    return selection1 + (1 - selection1.a) * selection2;
}

float4 main(PixelShaderInput input) : SV_TARGET
{
    float4 tex0 = get_texture_from_atlas(textureMap, sample, input.uv0);
    tex0 = change_team_color(0, tex0);
    
    float4 selection = float4(0, 0, 0, 0);

    if (input.is_selected)
        selection = draw_entity_selection_highlight(input);

    /*for (uint i = 0; i < 128; i++)
    {
        if (any(selected_entities[i] == input.entity_id)/* && any(input.entity_id > 0)#1#)
        {
            selection = draw_entity_selection_highlight(input);
        }
    }*/

    //return draw_absolute_circle(input.uv1, input.tile.zw, input.tile.xy, input.collision.xy, input.collision.w);
    const float4 collision_bounds = draw_collision_bounds(input.uv1, input.tile.wz, input.collision.xy, input.collision.w);

    float4 color = tex0;
    color = lerp(color, selection, saturate(selection.a - color.a));
    color = lerp(color, collision_bounds, saturate(1 - color.a));

    if (color.a <= 0.01)
        discard;

    return color;
}
