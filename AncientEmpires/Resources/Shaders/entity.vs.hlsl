#include "math.hlsli"
#include "global.hlsli"

float gold_noise(in VertexShaderInput input, in float2 xy, in float seed)
{
    return frac(tan(distance(xy * phi(), xy) * seed) * xy.x);
}

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
PixelShaderInput main(VertexShaderInput input)
{
    PixelShaderInput output = copy_standard_pixel_shader_input(input);
    
    if (input.entity_id.y > 0)
    {
        int entity_index = input.entity_id.y - 1;
        output.color = float4(0, 1, 0, 0.5);
        float4 position = mul(units_position[entity_index], world);
        position = mul(position, view);
        position = mul(position, projection);
        //output.collision += position;
        output.position.x += position.x;
        output.position.y += position.y;
        output.position.z = position.z;
        output.uv0.x += units_uv0[entity_index].x;
        output.uv0.y += units_uv0[entity_index].y;
        output.is_selected = units_details[entity_index].y > 0.5;;
    }

    return output;
}
