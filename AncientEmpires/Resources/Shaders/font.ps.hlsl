#include "global.hlsli"

//const float smoothing = 64.0/128.0;

float4 calculate_signed_distance(PixelShaderInput input, float4 tex0, float smoothing = 0.1)
{
    const float outlineWidth = 7.0 / 16.0;
    const float outerEdgeCenter = 0.5 - outlineWidth;
    const float distance = tex0.a;
    float alpha = smoothstep(outerEdgeCenter - smoothing, outerEdgeCenter + smoothing, distance);
    float border = smoothstep(0.5 - smoothing, 0.5 + smoothing, distance);
    alpha = smoothstep(0.3, 1, alpha);
    return float4(lerp((0, 0, 0), tex0.rgb, border), alpha);
}

float4 calculate_signed_distance_at(PixelShaderInput input, float2 offset, float smoothing = 0.1)
{
    offset = offset * texel_size.xy;
    const float4 color = get_texture_from_atlas(textureMap, sample, input.uv0 + float4(offset, 0, 0));
    return calculate_signed_distance(input, color, smoothing);
}

float4 main(PixelShaderInput input) : SV_TARGET
{
    float4 color = calculate_signed_distance_at(input, float2(0, 0), 0.3);

    if (color.a <= 0)
        discard;

    return lerp(input.color, color, 1 - (color.r + color.g + color.b) / 3);
}
