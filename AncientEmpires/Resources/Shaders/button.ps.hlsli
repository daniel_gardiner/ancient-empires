float4 material_button(PixelShaderInput input) : SV_TARGET
{
	return get_texture_from_atlas(textureMap, sample, input.uv0);
}
