﻿#include "math.hlsli"
#include "global.hlsli"

float4 main(PixelShaderInput input) : SV_TARGET
{
    //float4 textureColor = textureMap.Sample(sample, input.uv0);
    //return lerp(input.color, textureColor, 0.5);
    for (uint i = 0; i < 4; i++)
    {
        if (input.tile.x == 0 && input.tile.y == 0)
            continue;

        if (input.tile.x == selected_tiles[i].x &&
            input.tile.y == selected_tiles[i].y)
        {
            return float4(0, 1, 0, 1);
        }
    }

    return input.color;
}
