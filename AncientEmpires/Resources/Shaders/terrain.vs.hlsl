#include "math.hlsli"
#include "global.hlsli"

float gold_noise(in VertexShaderInput input, in float2 xy, in float seed)
{
    return lerp(0, 1, input.tile.x / map_size.x);
    return lerp(0, 1, (float)1000 / timing.y);

    return frac(tan(distance(xy * phi(), xy) * seed) * xy.x);
}

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
PixelShaderInput main(VertexShaderInput input)
{
    PixelShaderInput output = copy_standard_pixel_shader_input(input);
    output.color = float4(
        lerp(input.tile.x / map_size.x, 1 - input.tile.y / map_size.y, sin(timing.y)),
        lerp(input.tile.x / map_size.x, 1 - input.tile.y / map_size.y, cos(timing.y)),
        lerp(input.tile.x / map_size.x, 1 - input.tile.x / map_size.x, sin(timing.y)),
        1);

    for (uint i = 0; i < 4; i++)
    {
        if (input.tile.x == hover_tiles[i].x &&
            input.tile.y == hover_tiles[i].y)
        {
            output.position.z -= 0.1;
        }
    }

    return output;
}
