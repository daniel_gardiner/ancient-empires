#include "global.hlsli"

float4 main(PixelShaderInput input) : SV_TARGET
{
    float4 tex0 = get_texture_from_atlas(textureMap, sample, input.uv0);

    input.uv0.z = map_tile_mask_index.x;
    float4 tex1 = get_texture_from_atlas(textureMap, sample, input.uv0);

    if (debug_settings.x == 1)
    {
        for (uint i = 0; i < 4; i++)
        {
            if (input.tile.x == hover_tiles[i].x &&
                input.tile.y == hover_tiles[i].y)
            {
                if (tex1.r < 1)
                    discard;

                tex0 = lerp(tex0, float4(0, 1, 0, 1), 0.2) * tex1.r;
            }
        }

        if (tex1.r < 0.01 || tex1.a < 0.01)
            discard;

        return lerp(tex0, draw_tile_grid(input, tex0, tex1), 0.5);
    }

    if (tex0.a == 0)
        discard;

    return tex0;
}
