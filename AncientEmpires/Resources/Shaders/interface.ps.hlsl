#include "global.hlsli"
#include "button.ps.hlsli"
#include "panel.ps.hlsli"
#include "drag-selection.ps.hlsli"

float4 invert_color(in float4 color)
{
    return float4(1 - color.r, 1 - color.g, 1 - color.b, color.a);
}

float4 material_interface(PixelShaderInput input)
{
    return get_texture_from_atlas(textureMap, sample, input.uv0);
}

float4 main(PixelShaderInput input) : SV_TARGET
{
    if (input.material.x == shader_material_transparent)
        return float4(0, 0, 0, 0);
    if (input.material.x == shader_material_interface)
        return material_interface(input);
    if (input.material.x == shader_material_interface_button)
        return material_button(input);
    if (input.material.x == shader_material_interface_panel)
        return material_panel(input);
    if (input.material.x == shader_material_action_drag_selection)
        return material_drag_selection_action(input);
    
    float4 tex0 = get_texture_from_atlas(textureMap, sample, input.uv0);
    tex0 = tex0 + (1 - tex0.a) * input.color;

    /*
    if (tex0.a <= 0)
        discard;
        */

    return tex0;
}