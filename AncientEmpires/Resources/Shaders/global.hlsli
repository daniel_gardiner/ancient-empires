#include "math.hlsli"

/////////////
// GLOBALS //
/////////////
Texture2D textureMap[16];
SamplerState sample;

static const int shader_material_default = 0;
static const int shader_material_transparent = 10;
static const int shader_material_interface = 70;
static const int shader_material_interface_button = 71;
static const int shader_material_interface_panel = 72;
static const int shader_material_action_drag_selection = 80;
static const int shader_material_entity = 100;
static const int shader_material_texture = 300;

cbuffer WorldConstant: register(b0)
{
matrix world_view_projection;
matrix world;
matrix view;
matrix projection;
vector timing;
vector map_size;
vector debug_settings;
vector tile_size;
vector viewport;
}

cbuffer TextureConstant : register(b1)
{
float4 pixel_size;
float4 texel_size;
int4 map_tile_mask_index;
}

cbuffer SelectionInfo : register(b2)
{
float4 cursor;
float4 viewport_size;
float4 hover_tiles[4];
float4 selected_tiles[4];
int4 selected_entities[128];
}

cbuffer DragSelectionInfo : register(b3)
{
float4 drag_selection_active;
float4 drag_selection_box;
}

cbuffer UnitInfo : register(b4)
{
float4 units_position[1024];      // (position.x, position.y, position.z, ??)
float4 units_next_position[1024]; // (next_position.x, next_position.y, next_position.z, ?)
float4 units_uv0[1024];           // (uv0.x, uv0.y, uv1.x, uv1.y)   
float4 units_details[1024];       // (entity_id, is_selected, speed, ???)
}

////////////// 
// TYPEDEFS //
//////////////

struct VertexShaderInput
{
    float4 position : SV_POSITION;
    float4 tile : TILEPOSITION;
    float4 color : COLOR0;
    float4 overlay_color : COLOR1;
    int4 entity_id: ENTITY_IDS;
    float4 uv0 : TEXCOORD0;
    float4 uv1 : TEXCOORD1;
    float4 collision : TEXCOORD2;
    float4 bounds : TEXCOORD3;
    int4 material : SHADERMATERIAL0;
};

struct PixelShaderInput
{
    float4 position : SV_POSITION;
    float4 tile : TILEPOSITION;
    float4 color : COLOR0;
    float4 overlay_color : COLOR1;
    int4 entity_id: ENTITY_IDS;
    float4 uv0 : TEXCOORD0;
    float4 uv1 : TEXCOORD1;
    float4 collision : TEXCOORD2;
    float4 bounds : TEXCOORD3;
    int4 material : SHADERMATERIAL0;
    bool is_selected : IS_SELECTED;
};

float4 get_texture_from_atlas(Texture2D textures[16], SamplerState sample, float4 tex)
{
    if (tex.z == 0) return textures[0].Sample(sample, tex.xy);
    if (tex.z == 1) return textures[1].Sample(sample, tex.xy);
    if (tex.z == 2) return textures[2].Sample(sample, tex.xy);
    if (tex.z == 3) return textures[3].Sample(sample, tex.xy);
    if (tex.z == 4) return textures[4].Sample(sample, tex.xy);
    if (tex.z == 5) return textures[5].Sample(sample, tex.xy);
    if (tex.z == 6) return textures[6].Sample(sample, tex.xy);
    if (tex.z == 7) return textures[7].Sample(sample, tex.xy);
    if (tex.z == 8) return textures[8].Sample(sample, tex.xy);
    if (tex.z == 9) return textures[9].Sample(sample, tex.xy);
    if (tex.z == 10) return textures[10].Sample(sample, tex.xy);
    if (tex.z == 11) return textures[11].Sample(sample, tex.xy);
    if (tex.z == 12) return textures[12].Sample(sample, tex.xy);
    if (tex.z == 13) return textures[13].Sample(sample, tex.xy);
    if (tex.z == 14) return textures[14].Sample(sample, tex.xy);
    if (tex.z == 15) return textures[15].Sample(sample, tex.xy);

    return float4(0, 0, 0, 0.1);
}


float4 draw_bordered_rectangle(const PixelShaderInput input, const float4 rect, const float4 color, const float2 line_width)
{
    float4 box = float4(
        min(rect.x, rect.z),
        min(rect.y, rect.w),
        max(rect.x, rect.z),
        max(rect.y, rect.w));

    const float2 current_xy = (input.uv1.xy * viewport.zw) + viewport.xy;
    const float2 step_xy1 = step(box.xy, current_xy) * step(current_xy, box.zw);
    const float2 step_xy2 = step(box.xy + line_width, current_xy) * step(current_xy, box.zw - line_width);
    const float on_off1 = step_xy1.x * step_xy1.y;
    const float on_off2 = step_xy2.x * step_xy2.y;
    const float on_off = on_off1 && !on_off2;

    return lerp(float4(0, 0, 0, 0), color, on_off);
}

PixelShaderInput copy_standard_pixel_shader_input(VertexShaderInput input)
{
    PixelShaderInput output;
    input.position.w = 1.0f;
    output.position = mul(input.position, world);
    output.position = mul(output.position, view);
    output.position = mul(output.position, projection);
    output.color = input.color;
    output.overlay_color = input.overlay_color;
    output.tile = input.tile;
    output.uv0 = input.uv0;
    output.uv1 = input.uv1;
    output.collision = input.collision;
    output.bounds = input.bounds;
    output.entity_id = input.entity_id;
    output.material = input.material;
    return output;
}

float4 draw_tile_grid(PixelShaderInput input, float4 tex0, float4 tex1)
{
    const float4 overlay_color = float4(lerp(tex0.rgb, input.overlay_color.a * input.overlay_color.rgb, 0.5), 1);
    float4 color = lerp(tex0, overlay_color, debug_settings.x);
    float4 t1 = get_texture_from_atlas(textureMap, sample, input.uv0 + float4(texel_size * float2(-1.0f, -1.0f), 0, 0));
    float4 t3 = get_texture_from_atlas(textureMap, sample, input.uv0 + float4(texel_size * float2(1.0f, -1.0f), 0, 0));
    float4 t7 = get_texture_from_atlas(textureMap, sample, input.uv0 + float4(texel_size * float2(-1.0f, 1.0f), 0, 0));
    float4 t9 = get_texture_from_atlas(textureMap, sample, input.uv0 + float4(texel_size * float2(1.0f, 1.0f), 0, 0));

    float4 color_border = color;
    if (tex1.r > t1.r) color_border = float4(0.05, 0.05, 0.05, 1);
    if (tex1.r > t3.r) color_border = float4(0.05, 0.05, 0.05, 1);
    if (tex1.r > t7.r) color_border = float4(0.05, 0.05, 0.05, 1);
    if (tex1.r > t9.r) color_border = float4(0.05, 0.05, 0.05, 1);
    return color_border;
}

float4 change_team_color(int team, float4 tex0)
{
    if (team == 0)
    {
        if (tex0.r > 0.1 && tex0.r - tex0.g - tex0.b > (tex0.r + tex0.g + tex0.b) / 4)
        {
            const float3 original = float3(tex0.rgb);
            tex0.b = original.r;
            tex0.r = original.b;
        }
    }
    if (team == 1)
    {
        if (tex0.r > 0.1 && tex0.r - tex0.g - tex0.b > (tex0.r + tex0.g + tex0.b) / 4)
        {
            const float3 original = float3(tex0.rgb);
            tex0.g = original.r;
            tex0.r = original.g;
        }
    }

    return tex0;
}
