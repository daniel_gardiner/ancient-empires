#include "global.hlsli"

float4 main(PixelShaderInput input) : SV_TARGET
{
    if (input.material.x == shader_material_entity)
    {
        float4 tex0 = get_texture_from_atlas(textureMap, sample, input.uv0);
        input.uv0.z = map_tile_mask_index.x;
        float4 tex1 = get_texture_from_atlas(textureMap, sample, input.uv0);

        if (tex0.a < 0.5 || tex1.a < 0.5)
            discard;
        
        float4 color = lerp(tex0, input.overlay_color, 0.2);
        return color;
    }
    
    float4 tex0 = get_texture_from_atlas(textureMap, sample, input.uv0);
    input.uv0.z = map_tile_mask_index.x;
    float4 tex1 = get_texture_from_atlas(textureMap, sample, input.uv0);

    if (tex1.r < 1)
        discard;

    float4 color = lerp(tex0, input.overlay_color, 0.4);

    if (debug_settings.x == 1)
    {
        if (tex1.r < 1 || tex1.a < 1)
            discard;

        color = lerp(color, draw_tile_grid(input, tex0, tex1), 0.5);
        return color;
    }
    return tex0;
}