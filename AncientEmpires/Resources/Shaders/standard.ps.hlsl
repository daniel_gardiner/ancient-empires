#include "global.hlsli"

float4 main(PixelShaderInput input) : SV_TARGET
{
    float4 color = get_texture_from_atlas(textureMap, sample, input.uv0);

    if (color.a < 0.01)
        discard;
    
    return color;
}
