﻿/*using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using AncientEmpires.Engine;
using Windows;

namespace AncientEmpires;

public partial class Main : RenderForm
{
    [DllImport("kernel32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    static extern bool AllocConsole();

    const int WM_SYSCOMMAND = 0x0112;
    const int SC_MAXIMIZE = 0xF030;
    const int SC_RESTORE = 0xF120;
    const int WM_NCLBUTTONDOWN = 0x00A1;
    const int WM_NCMBUTTONDBLCLK = 0x00A3;

    public static Main Instance { get; set; }

    private void Main_Load(object sender, EventArgs e)
    {
        //DotTrace.EnsurePrerequisite();

        AllocConsole();

        //Cursor = Cursors.Cross;
        var workingArea = Screen.PrimaryScreen.WorkingArea;
        Top = 100;
        Left = 100;
        Instance = this;
        Visible = true;
        WindowState = FormWindowState.Maximized;
        FormBorderStyle = FormBorderStyle.Sizable;
        ResizeRedraw = false;
        OnResizeEnd(e);
        Application.DoEvents();

        Focus();
        GameManager.Instance = new GameManager(this);
        GameManager.Instance.StartGame();
    }

    private static bool HasFocus
    {
        get
        {
            var hasFocus = ActiveForm != null;
            return hasFocus;
        }
    }

    protected override void WndProc(ref Message m)
    {
        base.WndProc(ref m);

        if (m.Msg == WM_NCMBUTTONDBLCLK)
            OnResizeEnd(EventArgs.Empty);

        if (m.Msg != WM_SYSCOMMAND)
            return;

        if (m.WParam == (IntPtr)SC_MAXIMIZE || m.WParam == (IntPtr)SC_RESTORE)
            OnResizeEnd(EventArgs.Empty);
    }

    private void Main_Enter(object sender, EventArgs e)
    {
        //throw new System.NotImplementedException();
    }

    private void Main_Activated(object eventSender, EventArgs eventArgs)
    {
    }

    public void RunMain()
    {
        /*
        switch (_game.GamePhase)
        {
            case mGlobal:
            {
                break;
            }

            case mProvincal:
            {
                _game.Run();
                break;
            }

            case mBattle:
            {
                Module4.Run_Battle();
                break;
            }
        }
    #1#
    }

    private void Main_MouseMove(object eventSender, MouseEventArgs eventArgs)
    {
        /*
        if (_game == null)
            return;

        switch (_game.GamePhase)
        {
            case mBattle:
            {
                Battle.ChangeBattleIcon();
                if (Drag.MousePressed)
                {
                    XPos = Conversion.Int((MouseX - ExtraX) / (decimal) SquareWidth) + GeneralData.MapX;
                    YPos = Conversion.Int((MouseY - ExtraY) / (decimal) SquareHeight) + GeneralData.MapY;
                    if ((XPos == PDx) & (YPos == PDy))
                        return;
                    Battle.Drag_Select();
                }

                break;
            }

            case mProvincal:
            {
                /*
                Provincal.ChangeProvincalIcon();
                if (Deploying & MouseY < 380 & _game.Input.ButtonState.LeftDown)
                {
                    switch (DeployingEntityType)
                    {
                        case EntityType.Road:
                            Provincal2.Deploy_Road();
                            return;

                        case EntityType.Wall:
                            Provincal2.Deploy_Wall();
                            return;

                        case EntityType.Bulldozer:
                            Provincal.BulldozeObject();
                            return;
                    }
                }
                #2#

                break;
            }

            case mGlobal:
            {
                break;
            }
        }
    #1#
    }

    private void Main_EndResize(object sender, EventArgs e)
    {
        if (GameContext.Instance.Engine == null)
            return;

        ConstrainResize();
        ResizeRedrawFix();

        GameManager.Instance.TriggerResize();
    }

    private void ResizeRedrawFix()
    {
        Location = new Point(Location.X + 1, Location.Y);
        Location = new Point(Location.X - 1, Location.Y);
    }

    private void ConstrainResize()
    {
        var width = ClientSize.Width < 600
            ? 600
            : ClientSize.Width;
        var height = ClientSize.Height < 500
            ? 500
            : ClientSize.Height;
        ClientSize = new Size(width, height);
    }

    protected override void OnLayout(LayoutEventArgs levent)
    {
        base.OnLayout(levent);
    }

    protected override void OnMove(EventArgs e)
    {
        base.OnMove(e);

        if (GameContext.Instance.Engine == null)
            return;

        ConstrainResize();

        GameManager.Instance.TriggerResize();
    }

    protected override void OnResize(EventArgs e)
    {
        base.OnResize(e);
    }

    protected override void OnResizeBegin(EventArgs e)
    {
        base.OnResizeBegin(e);
    }

    private void Main_KeyDown(object sender, KeyEventArgs e)
    {
            switch (e.KeyCode)
            {
                case Keys.F10:
                    e.SuppressKeyPress = true;
                    return;
            }
    }
}*/