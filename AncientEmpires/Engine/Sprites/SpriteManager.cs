using AncientEmpires.Engine.Entities;
using AncientEmpires.Engine.Entities.Buildings;
using AncientEmpires.Engine.Render;

namespace AncientEmpires.Engine.Sprites;

public class SpriteManager
{
    private readonly List<SpriteCycle> _cycles = new();
    private readonly Entity _entity;

    public SpriteManager(GameEngine engine, Entity entity, TextureType textureType)
    {
        Engine = engine;
        TextureType = textureType;
        _entity = entity;
        IsRunning = true;
    }

    public GameEngine Engine { get; }
    public TextureType TextureType { get; }
    public Queue<SpriteCycle> CycleQueue { get; } = new();
    public Direction Direction { get; private set; }
    public float LastSpriteShown { get; private set; }
    public int CurrentCycleIndex { get; private set; }
    public Sprite CurrentSprite { get; set; }
    public bool IsRunning { get; set; }
    public bool IsTransitioning { get; set; }
    public float CycleDelay { get; set; }


    public void AddCycle(SpriteCycle cycle)
    {
        if (cycle == null)
            throw new ArgumentNullException(nameof(cycle));

        cycle.SpriteManager = this;
        _cycles.Add(cycle);
    }

    public SpriteCycle GetCycle<T>()
    {
        return _cycles.FirstOrDefault(cycle => cycle is T);
    }

    public void QueueCycle<T>()
    {
        var cycle = GetCycle<T>();

        if (cycle == null || cycle == CurrentCycle || (CycleQueue.Any() && CycleQueue.Peek() == cycle))
            return;

        //ConsoleWriter.WriteLineIf(_entity.Engine.Actions.GetComponent<EntityControl>().SelectedEntities.Contains(_entity), $"---------> {_entity.Name} queue {cycle.GetType().Name}");
        CycleQueue.Enqueue(cycle);

        if (CurrentCycle == null)
            Update(_entity.Engine);
    }
        
    public void ReplaceCycle<T>()
    {
        CycleQueue.Clear();
        CurrentCycle = null;
            
        QueueCycle<T>();
    }

    public SpriteCycle CurrentCycle { get; set; }


    public void SetDirection(Direction direction)
    {
        Direction = direction;
        Update(Engine);
    }

    public bool Update(GameEngine engine)
    {
        if (!IsRunning)
            return false;

        if (CurrentCycle == null || CycleQueue.Any())
            if (TryDequeue())
                return true;

        if (CurrentSprite == null)
        {
            CurrentCycleIndex = 0;
            CurrentSprite = CurrentCycle.Directions[Direction].First();
            return true;
        }

        if (!CanShowNextFrame(engine))
            return false;

        var nextSprite = GetNextSprite(Direction);
        var changed = nextSprite != CurrentSprite;
        CurrentSprite = nextSprite;

        CycleDelay = CurrentCycleIndex == 0
            ? CurrentCycle.GetCycleDelay()
            : 0;

        /*
        if (changed)
            ConsoleWriter.WriteLineIf(_entity.Engine.Actions.GetComponent<EntityControl>().SelectedEntities.Contains(_entity), $"---------> {Direction} {CurrentCycleIndex} (IsTransitioning: {IsTransitioning}, CycleDelay: {CycleDelay}, FrameDuration: {CurrentSprite.FrameDuration} | {CurrentCycle.FrameDuration})");
            */

        return changed;
    }

    private bool CanShowNextFrame(GameEngine engine)
    {
        var multiplier = 1;
        var currentFrame = engine.Timing.Frame;

        if (IsTransitioning)
            multiplier = 2;

        var frameDuration = CurrentSprite.FrameDuration / multiplier;
        var cycleDelayDuration = CycleDelay / multiplier;
        var elapsed = currentFrame - LastSpriteShown;

        switch (frameDuration + cycleDelayDuration < elapsed)
        {
            case false:
                return false;
            default:
                LastSpriteShown += frameDuration + cycleDelayDuration;
                return true;
        }
    }

    private bool TryDequeue()
    {
        if (!CycleQueue.Any())
            return false;

        if (CurrentCycle?.TransitionFrames.Any() == true && !CurrentCycle.TransitionFrames.Contains(CurrentCycleIndex))
        {
            IsTransitioning = true;
            return false;
        }

        CurrentCycle = CycleQueue.Dequeue();
        CurrentCycleIndex = 0;
        IsTransitioning = false;
        LastSpriteShown = Engine.Timing.Frame;
        CycleDelay = 0;
            
        //ConsoleWriter.WriteLineIf(_entity.Engine.Actions.GetComponent<EntityControl>().SelectedEntities.Contains(_entity), $"---------> {_entity.Name} DEQUEUING {CurrentCycle.GetType().Name}");
        return true;
    }

    private Sprite GetNextSprite(Direction direction)
    {
        CurrentCycleIndex++;
        if (CurrentCycleIndex >= CurrentCycle.Frames)
            CurrentCycleIndex = 0;

        return CurrentCycle.Directions[direction][CurrentCycleIndex];
    }

    public void Stop()
    {
        IsRunning = false;
        CurrentSprite = CurrentCycle.Directions[Direction].First();
        LastSpriteShown = _entity.Engine.Timing.Frame;
    }
}