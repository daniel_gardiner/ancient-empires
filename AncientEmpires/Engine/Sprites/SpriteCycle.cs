using System.Numerics;
using AncientEmpires.Engine.Entities;
using AncientEmpires.Engine.Entities.Buildings;
using AncientEmpires.Engine.Render;

namespace AncientEmpires.Engine.Sprites;

public class SpriteCycle : TimedDirectionalSet
{
    public SpriteCycle(
        GameEngine engine, 
        TextureType textureType,
        Vector2 spriteSize,
        Vector2 pixelSetOffset,
        Vector2 spriteSetOffset,
        int frames,
        int frameDuration,
        int cycleDelay,
        int cycleDelayVariation,
        List<int> transitionFrames)
    {
        SpriteSize = spriteSize;
        TileSize = new Vector2(128, 64);
        PixelSetOffset = pixelSetOffset;
        SpriteSetOffset = spriteSetOffset;
        Frames = frames;
        FrameDuration = frameDuration;
        CycleDelay = cycleDelay;
        CycleDelayVariation = cycleDelayVariation;
        TransitionFrames = transitionFrames;

        AddDirectionalSprites(engine, frames, frameDuration, textureType);
    }

    protected Vector2 SpriteSize { get; }
    protected Vector2 TileSize { get; }
    public Vector2 SpriteSetOffset { get; set; }
    public int Frames { get; set; }
    public Vector2 PixelSetOffset { get; set; }
    public int FrameDuration { get; set; }
    public int CycleDelay { get; }
    public int CycleDelayVariation { get; }
    public List<int> TransitionFrames { get; set; }
    public SpriteManager SpriteManager { get; set; }

    protected void AddDirectionalSprites(GameEngine engine, int frames, int frameDuration, TextureType textureType)
    {
        AddSprites(engine, Direction.NorthEast, textureType, new Vector2(0, 0), frames, frameDuration);
        AddSprites(engine, Direction.North, textureType, new Vector2(0, 1), frames, frameDuration);
        AddSprites(engine, Direction.NorthWest, textureType, new Vector2(0, 2), frames, frameDuration);
        AddSprites(engine, Direction.West, textureType, new Vector2(0, 3), frames, frameDuration);
        AddSprites(engine, Direction.SouthWest, textureType, new Vector2(0, 4), frames, frameDuration);
        AddSprites(engine, Direction.South, textureType, new Vector2(0, 5), frames, frameDuration);
        AddSprites(engine, Direction.SouthEast, textureType, new Vector2(0, 6), frames, frameDuration);
        AddSprites(engine, Direction.East, textureType, new Vector2(0, 7), frames, frameDuration);
    }

    public void AddSprites(GameEngine engine, Direction direction, TextureType textureType, Vector2 directionOffset, int frames, int frameDuration)
    {
        var textureIndexes = engine.GetComponent<GameTextures>().FindTextureIndexes($"{textureType}.dds");

        var spritePixelSize = SpriteSize * TileSize;
        for (var i = 0; i < frames; i++)
        {
            var spriteOffset = new Vector2(i, 0);
            var pixelOffset = (SpriteSetOffset + directionOffset + spriteOffset) * spritePixelSize;
            AddSprite(direction, new Sprite(new RectangleF(pixelOffset.X, pixelOffset.Y, spritePixelSize.X, spritePixelSize.Y), textureIndexes)
            {
                FrameDuration = frameDuration
            });
        }
    }

    public float GetCycleDelay()
    {
        var engineRandom = SpriteManager.Engine.Random;
        return CycleDelay + engineRandom.Next(0, CycleDelayVariation);
    }
}