using System.Numerics;

namespace AncientEmpires.Engine.Sprites;

public class WalkCycle : SpriteCycle
{
    public WalkCycle(GameEngine engine, TextureType textureType, Vector2 spriteSize, Vector2 pixelSetOffset, Vector2 spriteSetOffset, int frames, int frameDuration, int cycleDelay, int cycleDelayVariation, List<int> transitionFrame)
        : base(engine, textureType, spriteSize, pixelSetOffset, spriteSetOffset, frames, frameDuration, cycleDelay, cycleDelayVariation, transitionFrame)
    {
    }
}