using System.Numerics;

namespace AncientEmpires.Engine.Sprites;

public class IdleCycle : SpriteCycle
{
    public IdleCycle(GameEngine engine, TextureType textureType, Vector2 spriteSize, Vector2 pixelSetOffset, Vector2 spriteSetOffset, int frames, int frameDuration, int cycleDelay, int cycleDelayVariation, List<int> transitionFrames)
        : base(engine, textureType, spriteSize, pixelSetOffset, spriteSetOffset, frames, frameDuration, cycleDelay, cycleDelayVariation, transitionFrames)
    {
    }
}