using System.Numerics;

namespace AncientEmpires.Engine.Entities;

public class EntityMetaData
{
    public EntityType Type { get; set; }
    public Vector4 Size { get; set; } = Vector4.One;
    public Vector4 SpriteSize { get; set; } = Vector4.One;
    public int ObjectStrength { get; set; }
    public int Cost { get; set; }
    public int Yeild { get; set; }
    public int Period { get; set; }
    public string Message { get; set; }
    public float Speed { get; set; }
    public bool Passable { get; set; }
    public int ZIndex { get; set; } = 10;
}