using AncientEmpires.Engine.Render;

namespace AncientEmpires.Engine.Entities;

public abstract class SpriteSet
{
    protected float StartMs { get; set; }
    protected float ElapsedFrames { get; set; }
    public abstract Sprite Current { get;}
        
    public virtual void Initialize(float elapsedMs)
    {
        StartMs = elapsedMs;
    }

    /*
    public virtual bool Update(GameEngine engine, float tickFraction, float elapsedTicks)
    {
        ElapsedFrames = engine.Timing.Frame;
        return false;
    }
*/
}