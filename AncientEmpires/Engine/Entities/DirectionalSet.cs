using AncientEmpires.Engine.Entities.Buildings;
using AncientEmpires.Engine.Render;

namespace AncientEmpires.Engine.Entities;

public class DirectionalSet : SpriteSet
{
    private readonly SortedList<PathDirection, Sprite> _sprites = new();
    private PathDirection _pathDirection;
    private Sprite _current;
    public override Sprite Current => _current ?? _sprites.Values.First() ?? Sprite.Empty;

    public void AddSprite(PathDirection direction, Sprite sprite)
    {
        _sprites.Add(direction, sprite);
    }

    public void SetDirection(PathDirection direction)
    {
        _pathDirection = direction;
        _current = _sprites[_pathDirection];
    }

    public bool Update(GameEngine engine, float tickFraction, float elapsedTicks)
    {
        var nextSprite = _sprites[_pathDirection];
        var changed = nextSprite != _current;
        _current = nextSprite;
        return changed;
    }
}