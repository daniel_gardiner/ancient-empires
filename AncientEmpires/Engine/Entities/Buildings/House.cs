using AncientEmpires.Engine.Render;

namespace AncientEmpires.Engine.Entities.Buildings;

public class House : Building
{
    private readonly Sprite Small = new(new RectangleF(60, 0, 100, 40), new List<int>(1));
    private readonly Sprite Medium = new(new RectangleF(60, 40, 100, 40), new List<int>(1));
    private readonly Sprite Large = new(new RectangleF(60, 80, 100, 40), new List<int>(1));

    public House(GameEngine engine, int id) : base(engine, id, EntityType.House)
    {
        Sprite = Large;
    }
}