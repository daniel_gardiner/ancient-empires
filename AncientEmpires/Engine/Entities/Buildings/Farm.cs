using AncientEmpires.Engine.Render;

namespace AncientEmpires.Engine.Entities.Buildings;

public class Farm : Building
{
    private readonly Sprite Harvested = new(new RectangleF(0, 0, 60, 60), new List<int>(1));
    private readonly Sprite Growing1 = new(new RectangleF(0, 60, 60, 128 - 60), new List<int>(1));
    private readonly Sprite Growing2 = new(new RectangleF(0, 128, 60, 180 - 128), new List<int>(1));
    private readonly Sprite Growing3 = new(new RectangleF(0, 180, 60, 240 - 180), new List<int>(1));

    public Farm(GameEngine engine, int id) : base(engine, id, EntityType.Farm)                 
    {
        Sprite = Harvested;
    }
}