namespace AncientEmpires.Engine.Entities.Buildings;

public enum PathDirection
{
    EastWest,
    NorthSouth,
    NorthWest,
    NorthEast,
    SouthWest,
    SouthEast,
    NorthSouthEastWest,
    NorthSouthWest,
    NorthSouthEast,
    NorthEastWest,
    SouthEastWest,
    FilledNorthSouthEastWest,
    FilledNorthEastWest,
    FilledNorthSouthWest,
    FilledNorthSouthEast,
    FilledSouthEastWest,
    FilledSouthWest,
    FilledSouthEast,
    FilledNorthWest,
    FilledNorthEast,
    ConnectedEastFilledNorthSouthWest,
    ConnectedWestFilledNorthSouthEast,
    ConnectedSouthFilledNorthEastWest,
    ConnectedNorthFilledSouthEastWest,
}