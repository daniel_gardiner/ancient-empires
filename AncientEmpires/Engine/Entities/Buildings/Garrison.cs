using AncientEmpires.Engine.Render;

namespace AncientEmpires.Engine.Entities.Buildings;

public class Garrison : Building
{
    private readonly Sprite Default = new(new RectangleF(100, 0, 60, 80), new List<int>(1));

    public Garrison(GameEngine engine, int id) : base(engine, id, EntityType.Garrison)
    {
        Sprite = Default;
    }
}