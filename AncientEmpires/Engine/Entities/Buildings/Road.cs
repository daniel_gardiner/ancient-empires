using AncientEmpires.Engine.Render;
using static AncientEmpires.Engine.Entities.Buildings.PathDirection;

namespace AncientEmpires.Engine.Entities.Buildings;

public class Road : Path<Road>
{
    protected static List<int> TextureIndexes;

    public Road(GameEngine engine, int id) : base(engine, id, EntityType.Road)
    {
        DepthOffset = 0.1f;
        SortingDepthOffset = -0.1f;

        var tileWidth = TileWidth;
        const int row = 0;
        const int rowHeight = 1;

        TextureIndexes ??= Engine.GetComponent<GameTextures>().FindTextureIndexes($"{TextureType.Paths}.dds");
        Sprites.AddSprite(NorthSouth, new Sprite(Rect(0, row, rowHeight), TextureIndexes));
        Sprites.AddSprite(EastWest, new Sprite(Rect(tileWidth, row, rowHeight), TextureIndexes));
        Sprites.AddSprite(NorthEast, new Sprite(Rect(tileWidth * 2, row, rowHeight), TextureIndexes));
        Sprites.AddSprite(NorthWest, new Sprite(Rect(tileWidth * 3, row, rowHeight), TextureIndexes));
        Sprites.AddSprite(SouthWest, new Sprite(Rect(tileWidth * 4, row, rowHeight), TextureIndexes));
        Sprites.AddSprite(SouthEast, new Sprite(Rect(tileWidth * 5, row, rowHeight), TextureIndexes));
        Sprites.AddSprite(SouthEastWest, new Sprite(Rect(tileWidth * 6, row, rowHeight), TextureIndexes));
        Sprites.AddSprite(NorthSouthEast, new Sprite(Rect(tileWidth * 7, row, rowHeight), TextureIndexes));
        Sprites.AddSprite(NorthEastWest, new Sprite(Rect(tileWidth * 8, row, rowHeight), TextureIndexes));
        Sprites.AddSprite(NorthSouthWest, new Sprite(Rect(tileWidth * 9, row, rowHeight), TextureIndexes));
        Sprites.AddSprite(NorthSouthEastWest, new Sprite(Rect(tileWidth * 10, row, rowHeight), TextureIndexes));

        /*
        Sprites.AddSprite(FilledNorthSouthEastWest, new Sprite(Rect(row, 240), TextureType.Paths));
        Sprites.AddSprite(FilledNorthSouthWest, new Sprite(Rect (128, 240), TextureType.Paths));
        Sprites.AddSprite(FilledNorthSouthEast, new Sprite(Rect(240, 240), TextureType.Paths));
        Sprites.AddSprite(FilledNorthEastWest, new Sprite(Rect(360, 240), TextureType.Paths));
        Sprites.AddSprite(FilledSouthEastWest, new Sprite(Rect(480, 240), TextureType.Paths));
        Sprites.AddSprite(ConnectedEastFilledNorthSouthWest, new Sprite(Rect(600, 240), TextureType.Paths));
        Sprites.AddSprite(ConnectedWestFilledNorthSouthEast, new Sprite(Rect(720, 240), TextureType.Paths));
        Sprites.AddSprite(ConnectedSouthFilledNorthEastWest, new Sprite(Rect(840, 240), TextureType.Paths));
        Sprites.AddSprite(ConnectedNorthFilledSouthEastWest, new Sprite(Rect(960, 240), TextureType.Paths));
        Sprites.AddSprite(FilledSouthWest, new Sprite(Rect(row, 360), TextureType.Paths));
        Sprites.AddSprite(FilledSouthEast, new Sprite(Rect (128, 360), TextureType.Paths));
        Sprites.AddSprite(FilledNorthWest, new Sprite(Rect(240, 360), TextureType.Paths)); 
        Sprites.AddSprite(FilledNorthEast, new Sprite(Rect(360, 360), TextureType.Paths));
    */
    }

    private static RectangleF Rect(int left, int top, int rowHeight = 1) => new(left, top, TileWidth, TileHeight);

    public override void OnNextFrame(float timeDelta)
    {
        base.OnNextFrame(timeDelta);
    }
}