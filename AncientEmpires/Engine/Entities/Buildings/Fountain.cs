using AncientEmpires.Engine.Render;

namespace AncientEmpires.Engine.Entities.Buildings;

public class Fountain : Building
{
    private readonly TimedSet _sprites = new();

    public Fountain(GameEngine engine, int id, EntityType entityType) : base(engine, id, entityType)
    {
        Sprite = _sprites.Current;
    }

    public Fountain(GameEngine engine, int id) : base(engine, id, EntityType.Fountain)
    {
        _sprites.AddSprite(new Sprite(new RectangleF(0, 280, 30, 20), new List<int>(1)) { FrameDuration = 2 });
        _sprites.AddSprite(new Sprite(new RectangleF(30, 280, 30, 20), new List<int>(1)) { FrameDuration = 2 });
        _sprites.AddSprite(new Sprite(new RectangleF(60, 280, 30, 20), new List<int>(1)) { FrameDuration = 2 });
        _sprites.AddSprite(new Sprite(new RectangleF(90, 280, 30, 20), new List<int>(1)) { FrameDuration = 2 });
        _sprites.Start();
    }

    public override void OnNextFrame(float timeDelta)
    {
        base.OnNextFrame(timeDelta);

        if (_sprites.Update(Engine))
            MarkDirty();
    }
}