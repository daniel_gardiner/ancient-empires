using AncientEmpires.Engine.Entities.Units.Movement;
using AncientEmpires.Engine.Entities.Units.Physics;

namespace AncientEmpires.Engine.Entities.Buildings;

public abstract class Building : Entity
{
    protected Building(GameEngine engine, int id, EntityType entityType) : base(id, entityType)
    {
        Movement = new StationaryMovement(engine, this);
        Collision = new BuildingCollision(engine, this);
    }
}