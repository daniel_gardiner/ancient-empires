namespace AncientEmpires.Engine.Entities.Buildings;

public enum Direction
{
    North,
    South,
    West,
    East,
    SouthEast,
    SouthWest,
    NorthEast,
    NorthWest,
}