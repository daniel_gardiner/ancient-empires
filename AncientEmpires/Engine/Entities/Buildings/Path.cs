using System.Numerics;
using AncientEmpires.Engine.Entities.Units.Movement;
using AncientEmpires.Engine.Entities.Units.Physics;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Render;
using AncientEmpires.Engine.Sprites;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Entities.Buildings;

public abstract class Path : Entity
{
    private PathDirection _direction = PathDirection.EastWest;
    private BoundingBox? _boundingBox;

    public new PathDirection Direction
    {
        get => _direction;
        set
        {
            Sprites.SetDirection(value);
            _direction = value;
        }
    }

    public DirectionalSet Sprites { get; set; }

    protected Path(GameEngine engine, int id, EntityType entityType) : base(id, entityType)
    {
        Sprites = new DirectionalSet();
        SpriteManager = new SpriteManager(engine, this, TextureType.Paths);
        Movement = new StationaryMovement(engine, this);
        Collision = new PathCollision(engine, this);
    }

    public BoundingBox GetBoundingBox()
    {
        _boundingBox ??= CreateBoundingBox();
        return _boundingBox.Value;
    }

    private BoundingBox CreateBoundingBox()
    {
        var position = Position.MapSpace.ToVector3() + new Vector3(0.01f, 0.01f, 0);
        var size = Size.ToVector3() - new Vector3(0.02f, 0.02f, 0);
        var boundingBox = new BoundingBox(position, position + size);
        return boundingBox;
    }

    public BoundingBox GetBoundingBox(Vector4 position)
    {
        _boundingBox ??= new BoundingBox(position.ToVector3(), position.ToVector3() + Size.ToVector3());

        return _boundingBox.Value;
    }

    public void AlignPath<TPathEntity>() where TPathEntity : Path
    {
        var mapManager = Engine.GetComponent<MapManager>();
        var north = mapManager.GetTile(mapManager.North(Position)).Has<TPathEntity>();
        var south = mapManager.GetTile(mapManager.South(Position)).Has<TPathEntity>();
        var east = mapManager.GetTile(mapManager.East(Position)).Has<TPathEntity>();
        var west = mapManager.GetTile(mapManager.West(Position)).Has<TPathEntity>();
        var northEast = mapManager.GetTile(mapManager.NorthEast(Position)).Has<TPathEntity>();
        var northWest = mapManager.GetTile(mapManager.NorthWest(Position)).Has<TPathEntity>();
        var southEast = mapManager.GetTile(mapManager.SouthEast(Position)).Has<TPathEntity>();
        var southWest = mapManager.GetTile(mapManager.SouthWest(Position)).Has<TPathEntity>();

        var diagonals = new[] { northEast, northWest, southWest, southEast }.Where(o => o);
        var lastDirection = Direction;

        Direction = true switch
        {
            /*
            true when north && south && east && west && diagonals.Count() > 3 => PathDirection.FilledNorthSouthEastWest,
            true when north && south && west && northWest && southWest => PathDirection.FilledNorthSouthWest,
            true when north && south && east && northEast && southEast => PathDirection.FilledNorthSouthEast,
            true when north && south && east && southEast => PathDirection.FilledNorthSouthEast,
            true when north && east && west && northEast && northWest => PathDirection.FilledNorthEastWest,
            true when south && east && west && southEast && southWest => PathDirection.FilledSouthEastWest,
            true when south && east && west && northWest && southWest => PathDirection.ConnectedEastFilledNorthSouthWest,
            true when south && east && west && northEast && southEast => PathDirection.ConnectedWestFilledNorthSouthEast,
            true when south && east && west && northEast && northWest => PathDirection.ConnectedSouthFilledNorthEastWest,
            true when south && east && west && southEast && southWest => PathDirection.ConnectedSouthFilledNorthEastWest,
            true when north && west && northWest => PathDirection.FilledNorthWest,
            true when north && east && northEast => PathDirection.FilledNorthEast,
            true when south && west && southWest => PathDirection.FilledSouthWest,
            true when south && east && southEast => PathDirection.FilledSouthEast,
            */

            true when north && south && east && west => PathDirection.NorthSouthEastWest,
            true when north && south && east => PathDirection.NorthSouthEast,
            true when north && south && west => PathDirection.NorthSouthWest,
            true when north && east && west => PathDirection.NorthEastWest,
            true when south && east && west => PathDirection.SouthEastWest,
            true when north && south => PathDirection.NorthSouth,
            true when north && west => PathDirection.NorthWest,
            true when north && east => PathDirection.NorthEast,
            true when south && west => PathDirection.SouthWest,
            true when south && east => PathDirection.SouthEast,
            true when south || north => PathDirection.NorthSouth,
            true when east || west => PathDirection.EastWest,
            _ => PathDirection.EastWest
        };

        if (lastDirection != Direction)
        {
            MarkDirty();
        }
    }

    public override Sprite GetSprite() => Sprites.Current;

    public virtual void Despawn()
    {
    }
}

public abstract class Path<TPathEntity> : Path where TPathEntity : Path
{
    protected Path(GameEngine engine, int id, EntityType entityType) : base(engine, id, entityType)
    {
    }

    public override void OnInitialize()
    {
        base.OnInitialize();

        AlignSurrounding(Position);
    }

    public override void Despawn()
    {
        base.Despawn();

        AlignSurrounding(Position);
    }

    private void AlignSurrounding(GamePosition position)
    {
        var mapManager = Engine.GetComponent<MapManager>();
        foreach (var surrounding in mapManager.Neighbours(position, true))
        {
            mapManager.GetTile(surrounding).Get<TPathEntity>().ForEach(o => o.AlignPath<TPathEntity>());
        }
    }

    protected override void OnRefresh()
    {
        base.OnRefresh();
        AlignSurrounding(Position);
    }
}