using AncientEmpires.Engine.Render;
using static AncientEmpires.Engine.Entities.Buildings.PathDirection;

namespace AncientEmpires.Engine.Entities.Buildings;

public class Wall : Path<Wall>
{
    protected static List<int> TextureIndexes;

    public Wall(GameEngine engine, int id) : base(engine, id, EntityType.Wall)
    {
        const int row = 1280;
        const int rowHeight = TileHeight * 2;

        TextureIndexes ??= engine.GetComponent<GameTextures>().FindTextureIndexes($"{TextureType.Paths}.dds");
        Sprites.AddSprite(NorthSouth, new Sprite(new RectangleF(TileWidth * 1.5f * 0, row, TileWidth, rowHeight), TextureIndexes));
        Sprites.AddSprite(EastWest, new Sprite(new RectangleF(TileWidth * 1.5f * 1, row, TileWidth, rowHeight), TextureIndexes));
        Sprites.AddSprite(NorthEast, new Sprite(new RectangleF(TileWidth * 1.5f * 2, row, TileWidth, rowHeight), TextureIndexes));
        Sprites.AddSprite(NorthWest, new Sprite(new RectangleF(TileWidth * 1.5f * 3, row, TileWidth, rowHeight), TextureIndexes));
        Sprites.AddSprite(SouthWest, new Sprite(new RectangleF(TileWidth * 1.5f * 4, row, TileWidth, rowHeight), TextureIndexes));
        Sprites.AddSprite(SouthEast, new Sprite(new RectangleF(TileWidth * 1.5f * 5, row, TileWidth, rowHeight), TextureIndexes));
        Sprites.AddSprite(SouthEastWest, new Sprite(new RectangleF(TileWidth * 1.5f * 6, row, TileWidth, rowHeight), TextureIndexes));
        Sprites.AddSprite(NorthSouthEast, new Sprite(new RectangleF(TileWidth * 1.5f * 7, row, TileWidth, rowHeight), TextureIndexes));
        Sprites.AddSprite(NorthEastWest, new Sprite(new RectangleF(TileWidth * 1.5f * 8, row, TileWidth, rowHeight), TextureIndexes));
        Sprites.AddSprite(NorthSouthWest, new Sprite(new RectangleF(TileWidth * 1.5f * 9, row, TileWidth, rowHeight), TextureIndexes));
        Sprites.AddSprite(NorthSouthEastWest, new Sprite(new RectangleF(TileWidth * 1.5f * 10, row, TileWidth, rowHeight), TextureIndexes));
    }
}