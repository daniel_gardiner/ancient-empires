using AncientEmpires.Engine.Entities.Buildings;
using AncientEmpires.Engine.Render;

namespace AncientEmpires.Engine.Entities;

public class TimedDirectionalSet : SpriteSet
{
    public SortedList<Direction, List<Sprite>> Directions { get; } = new();

    public void AddSprite(Direction direction, Sprite sprite)
    {
        if (!Directions.ContainsKey(direction))
            Directions.Add(direction, new List<Sprite>());

        Directions[direction].Add(sprite);
    }

    public override Sprite Current { get; }
}