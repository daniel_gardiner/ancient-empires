using AncientEmpires.Engine.Render;

namespace AncientEmpires.Engine.Entities;

public class TimedSet : SpriteSet
{
    private readonly List<Sprite> _sprites = new();
    private float _lastSpriteFrame;
    private Sprite _current;

    public bool Running { get; private set; }

    public void AddSprite(Sprite sprite)
    {
        _sprites.Add(sprite);
    }

    public override Sprite Current => _current ?? _sprites.First();

    public bool Update(GameEngine engine)
    {
        if (!Running)
            return false;

        if (Current.FrameDuration >= engine.Timing.Frame - _lastSpriteFrame)
            return false;

        _lastSpriteFrame += Current.FrameDuration;
        var nextSprite = GetNextSprite();
        var changed = nextSprite != _current;
        _current = nextSprite;

        return changed;
    }

    private Sprite GetNextSprite()
    {
        var next = _sprites.IndexOf(Current) + 1;
        if (next > _sprites.Count - 1)
            next = 0;

        return _sprites[next];
    }

    public void Start()
    {
        Running = true;
        _lastSpriteFrame = ElapsedFrames;
        _current = _sprites.First();
    }

    public void Stop()
    {
        Running = false;
        _current = _sprites.First();
        _lastSpriteFrame = ElapsedFrames;
    }
}