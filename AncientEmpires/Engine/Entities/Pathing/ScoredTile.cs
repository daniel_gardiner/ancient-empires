namespace AncientEmpires.Engine.Entities.Pathing;

public class ScoredTile : IEqualityComparer<ScoredTile>
{
    public ScoredTile Parent { get; init; }
    public GamePosition Tile { get; init; }
    public GamePosition Position { get; init; }
    public float Steps { get; init; }
    public float Distance { get; init; }
    public float Score => Steps + Distance;

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != GetType()) return false;
        var other = (ScoredTile)obj;
        return Equals(this, other);
    }

    public bool Equals(ScoredTile x, ScoredTile y)
    {
        if (ReferenceEquals(x, y)) return true;
        if (ReferenceEquals(x, null)) return false;
        if (ReferenceEquals(y, null)) return false;
        return x.Position.Equals(y.Position);
    }

    public override int GetHashCode() => Position.GetHashCode();
    public int GetHashCode(ScoredTile obj) => GetHashCode();
}