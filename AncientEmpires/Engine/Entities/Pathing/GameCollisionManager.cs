using System.Collections.Concurrent;
using System.Numerics;
using AncientEmpires.Engine.Data;
using AncientEmpires.Engine.Entities.Units.Physics;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Map.Rendering;
using AncientEmpires.Engine.Render.Components;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Entities.Pathing;

public class GameCollisionManager : EngineComponent
{
    private readonly ConcurrentDictionary<BoundingBox, IEnumerable<Entity>> _entityCache = new();
    protected ComponentLookup<EntityLookup> EntityLookup;
    protected ComponentLookup<IsometricPositionStrategy> PositionStrategy;
    private static ICollisionResult NoCollision = new NoCollision();

    public override void OnInitialize()
    {
        base.OnInitialize();
        
        EntityLookup = new ComponentLookup<EntityLookup>();
        PositionStrategy = new ComponentLookup<IsometricPositionStrategy>();
    }

    public override void OnNextFrame(float timeDelta)
    {
        base.OnNextFrame(timeDelta);

        _entityCache.Clear();
    }

    public ICollisionResult EntityHasCollisionAt(Entity source, in GamePosition position, bool ignoreMovingUnits = false)
    {
        var entities = GetNearbyEntities(position);

        foreach (var entity in entities)
        {
            var result = EntityHasCollisionAtWith(source, in position, entity, ignoreMovingUnits);

            if (result is CollisionResult)
                return result;
        }

        return NoCollision;
    }

    public virtual ICollisionResult EntityHasCollisionAtWith(Entity entity, in GamePosition unitPosition, Entity collider, bool ignoreMovingUnits = false)
    {
        if (collider.Id == entity.Id)
            return NoCollision;

        /*
        if (ignoreMovingUnits && collider.Movement.State is WalkingState)
            return NoCollision;
            */

        return entity.Collision.At(unitPosition).CollidesWith(collider);
    }

    public ICollisionResult HasCollisionAt(in BoundingBox boundingBox)
    {
        var entities = GetEntitiesNearBounds(boundingBox);
        foreach (var entity in entities)
        {
            var result = entity.Collision.CollidesWith(BoxCollisionShape.FromBoundingBox(boundingBox));
            if (result is CollisionResult)
                return result;
        }

        return NoCollision;
    }

    private bool TryGetFromEntityCache(in BoundingBox collisionCheckBounds, out IEnumerable<Entity> cacheValue)
    {
        foreach (var cache in _entityCache)
        {
            var cacheBounds = cache.Key;

            if (cacheBounds.Contains(in collisionCheckBounds) == ContainmentType.Contains)
            {
                cacheValue = cache.Value;
                return true;
            }
        }

        cacheValue = null;
        return false;
    }

    private BoundingBox GetCollisionCheckBounds(GamePosition position)
    {
        var minPositionClose = PositionStrategy.Value.ConstrainPosition(position.MapSpace - new Vector4(2, 2, 0, 0)).ToVector3();
        var maxPositionClose = PositionStrategy.Value.ConstrainPosition(position.MapSpace + new Vector4(2, 2, 0, 0)).ToVector3();
        return new BoundingBox(minPositionClose, maxPositionClose);
    }

    private IEnumerable<Entity> GetNearbyEntities(GamePosition position)
    {
        var collisionCheckBounds = GetCollisionCheckBounds(position);
        var entities = GetEntitiesNearBounds(collisionCheckBounds);
        return entities;
    }

    private IEnumerable<Entity> GetEntitiesNearBounds(BoundingBox collisionCheckBounds)
    {
        if (TryGetFromEntityCache(in collisionCheckBounds, out var cacheValue))
            return cacheValue;

        var entities =  EntityLookup.Value.MapTree.FindObjects(collisionCheckBounds)
            .ToList();

        _entityCache.TryAdd(collisionCheckBounds, entities);
        return entities;
    }
}