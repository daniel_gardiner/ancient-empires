namespace AncientEmpires.Engine.Entities.Pathing;

public class GamePathResult
{
    public bool WasSolved;
    public Stack<GamePosition> Path;
    public List<ScoredTile> Scores;
    public float StartMs;
    public float TimeTakenMs;
}