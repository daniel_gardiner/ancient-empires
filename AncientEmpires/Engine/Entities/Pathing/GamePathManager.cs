using AncientEmpires.Engine.Render.Components;

namespace AncientEmpires.Engine.Entities.Pathing;

public class GamePathManager : EngineComponent
{
    private static readonly object _solverLockObj = new();
    private const int MaxIterations = 1000;

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();

        AddComponent<GameCollisionManager>();
    }

    public GamePathSolver CreatePathSolver()
    {
        lock (_solverLockObj)
        {
            return CreateNewPathSolver();
        }
    }

    private GamePathSolver CreateNewPathSolver()
    {
        var pathSolver = new GamePathSolver(Engine, this);
        pathSolver.MaxIterations = MaxIterations;
        return pathSolver;
    }

    public override void OnNextFrame(float timeDelta)
    {
        base.OnNextFrame(timeDelta);
    }
}