using System.Numerics;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Metrics;
using AncientEmpires.Engine.Render.Components;
using App.Metrics;
using Unit = AncientEmpires.Engine.Entities.Units.Unit;

namespace AncientEmpires.Engine.Entities.Pathing;

public class GamePathSolver : EngineComponent
{
    public bool IsActive;
    public int MaxIterations;
    private readonly SortedSet<ScoredTile> _openByScore = new(Comparer<ScoredTile>.Create((a, b) => (int)(a.Score * 100 - b.Score * 100)));
    public GamePathManager PathManager;
    private readonly HashSet<ScoredTile> _open = new();
    private readonly HashSet<GamePosition> _closed = new();
    private readonly Dictionary<ScoredTile, ScoredTile> _scores = new();
    private static MetricTags? _pathFindingTags;
    private readonly Vector4 _halfTile = new(0.5f, 0.5f, 0, 0);
    private MapManager MapManager;
    private int _iterations;
    private GameCollisionManager CollisionManager;

    public GamePathSolver(GameEngine engine, GamePathManager parent)
    {
        _pathFindingTags ??= Metrics.CreateTags("pathing:find");
        PathManager = parent;
        MapManager = Engine.GetComponent<MapManager>();
        CollisionManager = Engine.GetComponent<GameCollisionManager>();
    }

    public GamePathResult Solve(Unit unit, GamePosition start, Vector4 destination)
    {
        IsActive = true;
        _iterations = 0;
        _openByScore.Clear();
        _open.Clear();
        _closed.Clear();
        _scores.Clear();

        ConsoleWriter.WriteLine($"Path finding started for {unit}");

        using (Metrics.TrackTimeAlways(KnownMetrics.Timer, _pathFindingTags.Value))
        {
            var result = FindPath(unit, start, destination);
            IsActive = false;
            return result;
        }
    }

    private GamePathResult FindPath(Unit unit, GamePosition start, GamePosition destination)
    {
        var current = AddToOpen(ScoreTile(start, null, destination, unit));

        while (_iterations++ < MaxIterations)
        {
            var best = TakeBestOpenTile();

            if (best == null)
                return CreateUnsolvedResult(current);

            if (best.Position == destination)
                return CreateResult(best, current);

            if (DistanceMethods.EuclideanDistance(best.Position, destination) < 1)
                return CreateResult(best, current, new ScoredTile { Position = destination });

            foreach (var neighbour in GetNeighbours(best))
            {
                ScoreTiles(neighbour, best, destination, unit);
            }
        }

        return CreateResult(TakeBest(_open), null);
    }

    private void ScoreTiles(GamePosition tile, ScoredTile best, GamePosition destination, Unit unit)
    {
        if (_closed.Contains(tile))
            return;
        
        if (CollisionManager.EntityHasCollisionAt(unit, in tile) is CollisionResult)
        {
            _closed.Add(tile);
            return;
        }

        //if (CollisionManager.HasCollisionFrom(unit, best.Position))
        //return;

        var scored = ScoreTile(tile, best, destination, unit);

        if (_open.Contains(scored) && scored.Steps >= _scores[scored].Steps)
            return;

        AddToOpen(scored);
    }

    private IEnumerable<GamePosition> GetNeighbours(ScoredTile best)
    {
        return MapManager.Neighbours(best.Position);
    }

    private GamePathResult CreateUnsolvedResult(ScoredTile current)
    {
        return CreateResult(null, current);
    }

    private GamePathResult CreateResult(ScoredTile current, ScoredTile previous, ScoredTile partialTile)
    {
        return new GamePathResult
        {
            WasSolved = true,
            Path = CollectPath(current ?? previous, partialTile),
            Scores = _scores.Values.ToList()
        };
    }

    private GamePathResult CreateResult(ScoredTile current, ScoredTile previous)
    {
        return new GamePathResult
        {
            WasSolved = true,
            Path = CollectPath(current ?? previous),
            Scores = _scores.Values.ToList()
        };
    }

    private ScoredTile AddToOpen(ScoredTile scored)
    {
        if (_open.Contains(scored))
        {
            _open.Remove(scored);
            _openByScore.Remove(scored);
            _scores.Remove(scored);
        }

        _openByScore.Add(scored);
        _open.Add(scored);
        if (!_scores.ContainsKey(scored))
            _scores.Add(scored, scored);

        return scored;
    }

    public ScoredTile ScoreTile(GamePosition position, ScoredTile current, GamePosition destination, Unit unit)
    {
        var stepCost = unit.Movement.Cost(position);
        var centerPosition = position; // + _halfTile;

        return new ScoredTile
        {
            Parent = current,
            Tile = position,
            Position = centerPosition,
            Distance = DistanceMethods.EuclideanDistance(centerPosition, destination),
            Steps = current?.Steps + stepCost ?? 0,
        };
    }

    private Stack<GamePosition> CollectPath(ScoredTile end1, ScoredTile end2 = null)
    {
        var path = new List<GamePosition>();

        if (end2 != null)
            path.Add(end2.Position);

        var next = end1;
        while (next != null)
        {
            path.Add(next.Position);
            next = next.Parent;
        }

        return new Stack<GamePosition>(path);
    }

    private ScoredTile TakeBest(HashSet<ScoredTile> open)
    {
        return open.OrderBy(o => o.Score).First();
    }

    private ScoredTile TakeBestOpenTile()
    {
        if (!_open.Any() || !_openByScore.Any())
            return null;

        var best = _openByScore.First();
        _openByScore.Remove(best);
        _open.Remove(best);
        _closed.Add(best.Position);
        return _scores[best];
    }
}