using AncientEmpires.Engine.Entities.Buildings;
using AncientEmpires.Engine.Entities.Units;
using AncientEmpires.Engine.Entities.Units.Movement;
using AncientEmpires.Engine.Entities.Units.Physics;
using Vortice.Mathematics;
using Path = AncientEmpires.Engine.Entities.Buildings.Path;

namespace AncientEmpires.Engine.Entities.Pathing;

public class CollisionResult : ICollisionResult
{
    public IMovementState PreviousState { get; }
    public static NoCollision NoCollision = new();

    public readonly Entity Entity;
    public readonly Entity CollidedWith;
    public readonly BoxCollisionShape CollidedWithShape;
    public CollisionShape SourceShape;
    public CollisionShape CollisionShape;
    private BoundingBox BoundingBox;

    public CollisionResult(Entity entity, Entity collidedWith, IMovementState previousState)
    {
        PreviousState = previousState;
        Entity = entity;
        CollidedWith = collidedWith;

        ConsoleWriter.WriteLine(ToDescription());
        //ConsoleWriter.WriteLineIf(collidedWith.Engine.Timing.Slice % 100 == 0, ToDescription());
    }

    public CollisionResult(Entity entity, BoxCollisionShape collisionShape)
    {
        Entity = entity;
        CollidedWithShape = collisionShape;

        ConsoleWriter.WriteLine(ToDescription());
        //ConsoleWriter.WriteLineIf(entity.Engine.Timing.Slice % 100 == 0, ToDescription());
    }

    public ICollisionResult AppendCollisionShapes(CollisionShape sourceShape, CollisionShape collisionShape)
    {
        SourceShape = sourceShape;
        CollisionShape = collisionShape;
        return this;
    }

    public ICollisionResult AppendCollisionShapes(CollisionShape sourceShape, BoundingBox boundingBox)
    {
        SourceShape = sourceShape;
        BoundingBox = boundingBox;
        return this;
    }

    public string ToDescription()
    {
        if (PreviousState is CollisionState)
            return null;
        
        switch (CollidedWith)
        {
            case Unit unit:
                return $"Unit {Entity} has collided with unit {unit}";
            case Path path:
                return $"Unit {Entity} has collided with path {path}";
            case Building building:
                return $"Unit {Entity} has collided with building {building}";
            default:
                return "Unknown collision";
        }
    }
}