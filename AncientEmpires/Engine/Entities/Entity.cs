using System.Numerics;
using AncientEmpires.Engine.Entities.Buildings;
using AncientEmpires.Engine.Entities.Pathing;
using AncientEmpires.Engine.Entities.Units;
using AncientEmpires.Engine.Entities.Units.Movement;
using AncientEmpires.Engine.Entities.Units.Physics;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Render;
using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Entities;
using AncientEmpires.Engine.Sprites;
using Newtonsoft.Json;

namespace AncientEmpires.Engine.Entities;

public abstract class Entity : RenderableComponent, IEquatable<Entity>, IComparable<Entity>
{
    protected ComponentLookup<GameMap> Map;
    protected ComponentLookup<MapViewport> MapViewport;
    protected ComponentLookup<EntityLayer> EntityRenderer;

    public const int TileWidth = 128;
    public const int TileHeight = 64;

    public Direction Direction;
    public EntityMovement Movement;
    public EntityCollision Collision;
    public int LastUpdateMs;
    public SpriteManager SpriteManager;
    public Sprite Sprite;
    public int Id;
    public int Team;
    public EntityType Type;
    public Vector4 DebugColor = Vector4.One;
    public GamePosition Position;
    public GamePosition LastQuadTreePosition;
    public Vector4 Size;
    public readonly Vector4 SpriteSize;
    public readonly Vector4 SpritePixelSize;
    public float DepthOffset;
    public float SortingDepthOffset;
    public EntityMeshFragment Fragment;
    public float Speed;

    protected Entity(int id, EntityType entityType)
    {
        Position = GamePosition.FromMapSpace(new(99, 99, 99, 99));
        Map = new ComponentLookup<GameMap>();
        MapViewport = new ComponentLookup<MapViewport>();
        EntityRenderer = new ComponentLookup<EntityLayer>();

        var entityMetaData = EntityMetadataManager.Instance.Get(entityType);
        Id = id;
        Engine = GameContext.Instance.Engine;
        Type = entityMetaData.Type;
        SpriteManager = new SpriteManager(Engine, this, TextureType.Units);
        Size = entityMetaData.Size;
        SpriteSize = entityMetaData.SpriteSize;
        SpritePixelSize = entityMetaData.SpriteSize * Engine.Configuration.TilePixelSize;
    }

    public virtual bool CanSpawn(GamePosition position)
    {
        var collisionManager = Engine.GetComponent<GameCollisionManager>();
        if (collisionManager.EntityHasCollisionAt(this, in position) is CollisionResult result)
        {
            return false;
        }

        return true;
    }

    public virtual void JumpTo(GamePosition position)
    {
        Position = position;
    }

    public override string ToString() => $"{Type}#{Id}";

    public static Entity Create(EntityType entityType)
    {
        var engine = GameContext.Instance.Engine;
        var nextEntityId = engine.GetComponent<EntityManager>().GetNextEntityId();
        return Create(engine, nextEntityId, entityType);
    }

    public static Entity Create(GameEngine engine, int id, EntityType entityType)
    {
        return entityType switch
        {
            EntityType.Bulldozer => new Bulldozer(engine, id),
            EntityType.Wall => new Wall(engine, id),
            EntityType.Road => new Road(engine, id),
            EntityType.Fountain => new Fountain(engine, id),
            EntityType.Garrison => new Garrison(engine, id),
            EntityType.Farm => new Farm(engine, id),
            EntityType.House => new House(engine, id),
            EntityType.RomanLegion => new RomanLegion(engine, id),
            EntityType.GreekLegion => new GreekLegion(engine, id),
            EntityType.TestEntity1 => new TestEntity1(engine, id),
            _ => throw new ArgumentOutOfRangeException(nameof(entityType), entityType, null)
        };
    }

    public override void OnInitialize()
    {
        base.OnInitialize();

        Fragment = EntityRenderer.Value.CreateFragment(this);
        OnNextFrame(0);
    }

    public override void OnNextSlice(float timeDelta)
    {
        base.OnNextSlice(timeDelta);

        if (!Changed)
            return;

        Fragment.Refresh(timeDelta);
        MarkClean();
    }

    public void MarkDirty()
    {
        if (Changed)
            return;

        Changed = true;
        Fragment?.MarkDirty();
    }

    public void MarkClean()
    {
        Changed = false;
    }

    public void UpdatePosition(GamePosition newPosition)
    {
        Position = newPosition;
    }

    public bool AllowsSpawn(EntityType entityType) => false;

    public DehydratedEntity Dehydrate()
    {
        var properties = new Dictionary<string, string>();
        properties["Team"] = Team.ToString();
        properties = DehydrateProperties(properties);

        return new DehydratedEntity
        {
            Id = Id,
            Type = Type,
            Location = $"({Position.MapSpace.X},{Position.MapSpace.Y})",
            Size = $"({Size.X},{Size.Y})",
            Properties = properties
        };
    }

    protected virtual Dictionary<string, string> DehydrateProperties(Dictionary<string, string> properties)
    {
        return properties;
    }

    protected virtual void Hydrate(DehydratedEntity dehydratedEntity)
    {
        Team = Convert.ToInt32(dehydratedEntity.Properties["team"]);
    }

    public static Entity Hydrate(EntityManager entityManager, DehydratedEntity dehydratedEntity)
    {
        var location = HydrateLocation();
        var entity = Create(entityManager.Engine, dehydratedEntity.Id, dehydratedEntity.Type);
        entityManager.Engine.GetComponent<MapManager>().EntitySpawner.Spawn(location, entity);
        entity.Hydrate(dehydratedEntity);

        return entity;

        Vector4 HydrateLocation()
        {
            var coords = dehydratedEntity.Location.Trim('(', ')').Split(',');
            return new Vector4(float.Parse(coords[0]), float.Parse(coords[1]), 0, 0);
        }
    }

    public virtual void RenderSelection(GameEngine engine)
    {
    }

    public int CompareTo(Entity other)
    {
        if (ReferenceEquals(this, other)) return 0;
        if (ReferenceEquals(null, other)) return 1;
        return Id.CompareTo(other.Id);
    }

    public bool Equals(Entity other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return Id == other.Id;
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != GetType()) return false;
        return Equals((Entity)obj);
    }

    public override int GetHashCode() => Id.GetHashCode();

    public void Refresh()
    {
        MarkDirty();
        OnRefresh();
    }

    protected virtual void OnRefresh()
    {
        Fragment = EntityRenderer.Value.CreateFragment(this);
    }

    public virtual Sprite GetSprite()
    {
        return Sprite ?? Sprite.Empty;
    }

    public virtual int GetEntityTypeId()
    {
        return 0;
    }
}

public class DehydratedEntity
{
    [JsonProperty("I")]
    public int Id { get; set; }

    [JsonProperty("T")]
    public EntityType Type { get; set; }

    [JsonProperty("L")]
    public string Location { get; set; }

    [JsonProperty("S")]
    public string Size { get; set; }

    [JsonProperty("P")]
    public Dictionary<string, string> Properties { get; set; }
}