using System.Numerics;
using AncientEmpires.Engine.Sprites;

namespace AncientEmpires.Engine.Entities.Units;

public class GreekLegion : Legion
{
    public GreekLegion(GameEngine engine, int id) : base(engine, id, EntityType.GreekLegion)
    {
        SpriteManager = new SpriteManager(engine, this, TextureType.Romans2x);
        SpriteManager.AddCycle(new WalkCycle(engine, TextureType.Romans2x, new Vector2(2f, 2f), new Vector2(0, 0), new Vector2(0, 0), 11, 3, 0, 0, new List<int> { 5, 15 }));
        SpriteManager.QueueCycle<WalkCycle>();
    }
}