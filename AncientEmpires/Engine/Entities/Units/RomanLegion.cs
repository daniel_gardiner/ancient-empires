using System.Numerics;
using AncientEmpires.Engine.Sprites;

namespace AncientEmpires.Engine.Entities.Units;

public class RomanLegion : Legion
{
    public RomanLegion(GameEngine engine, int id) : base(engine, id, EntityType.RomanLegion)
    {
        SpriteManager = new SpriteManager(engine, this, TextureType.Romans);
        SpriteManager.AddCycle(new WalkCycle(engine, TextureType.Romans, new Vector2(1f, 1.5f), new Vector2(0, 0), new Vector2(0, 0), 20, 6, 0, 0, new List<int> { 5, 15 }));
        SpriteManager.AddCycle(new IdleCycle(engine, TextureType.Romans, new Vector2(1f, 1.5f), new Vector2(0, 0), new Vector2(21, 0), 10, 12, 100, 300, new List<int>()));
        SpriteManager.QueueCycle<IdleCycle>();
    }
}