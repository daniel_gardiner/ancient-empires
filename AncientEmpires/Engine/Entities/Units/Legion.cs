namespace AncientEmpires.Engine.Entities.Units;

public abstract class Legion : Unit
{
    public Legion(GameEngine engine, int id, EntityType entityType) : base(engine, id, entityType)
    {
    }

    /*
    protected override bool CheckBuildingCollision(IEnumerable<Entity> entities)
    {
        foreach (var building in entities)
        {
            switch (building.Type)
            {
                case EntityType.Garrison:
                    CurrentOptions = Globals.mGarrisonRequest;
                    MovingToGarrison = true;
                    break;
            }
        }

        return base.CheckBuildingCollision(entities);
    }

    public override bool CheckUnitCollision(IEnumerable<Unit> units)
    {
        foreach (var unit in units)
        {
            if (unit.Team != Team)
            {
                CurrentOptions = Globals.mAttackRequest;
                Attacking = true;
                TeamTarget = unit.Team;
                Target = unit;
                //DsBuffer[mBattleSound].Play(0, BufferPlayFlags.Default); // battle sound
                return true;
            }
        }

        return base.CheckUnitCollision(units);
    }
*/
}