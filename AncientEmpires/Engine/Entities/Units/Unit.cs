using System.Numerics;
using AncientEmpires.Engine.Entities.Units.Movement;
using AncientEmpires.Engine.Entities.Units.Physics;
using AncientEmpires.Engine.Render;

namespace AncientEmpires.Engine.Entities.Units;

public abstract class Unit : Entity
{
    public int UnitId;
    
    protected Unit(GameEngine engine, int id, EntityType entityType) : base(id, entityType)
    {
        DebugColor = new Vector4(0, 0, 1, 1);

        var entityMetaData = EntityMetadataManager.Instance.Get(entityType);
        Speed = entityMetaData.Speed;
        Movement = new EntityMovement(engine, this);
        Collision = new UnitCollision(engine,  this);
        //Offset = new(0, Engine.Map.TileSize.Y / 4, 0);
    }

    public override void OnInitialize()
    {
        base.OnInitialize();
    }
    
    public override void OnNextSlice(float timeDelta)
    {
        SpriteManager.SetDirection(Direction);

        if (SpriteManager.Update(Engine))
            MarkDirty();

        base.OnNextSlice(timeDelta);
    }

    public override void OnNextFrame(float timeDelta)
    {
        if (Movement.Update(timeDelta))
            MarkDirty();

        base.OnNextFrame(timeDelta);
    }

    public static void Provincal_ChangeFrame(int aa, int bb)
    {
        /*
        Leigion[bb, aa].Counter = Leigion[bb, aa].Counter + 1;
        if (Leigion[bb, aa].Counter >= 5)
        {
            Leigion[bb, aa].Counter = 0;
            Leigion[bb, aa].Frame = Leigion[bb, aa].Frame + 1;
            if (Leigion[bb, aa].Frame == 4)
            {
                Leigion[bb, aa].Frame = 1;
            }
        }
    */
    }

    public void Move(Stack<GamePosition> path)
    {
        Movement.Move(path);
    }

    public override Sprite GetSprite() => SpriteManager.CurrentSprite ?? Sprite.Empty;

    public override int GetEntityTypeId() => UnitId;
}