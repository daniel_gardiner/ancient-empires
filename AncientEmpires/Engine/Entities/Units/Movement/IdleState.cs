using AncientEmpires.Engine.Sprites;

namespace AncientEmpires.Engine.Entities.Units.Movement;

public class IdleState : MovementStateBase
{
    public IdleState(EntityMovement movement) : base(movement)
    {
        Movement.CollisionCount = 0;
        Movement.Entity.SpriteManager.QueueCycle<IdleCycle>();
    }
}