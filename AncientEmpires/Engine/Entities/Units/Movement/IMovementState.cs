namespace AncientEmpires.Engine.Entities.Units.Movement;

public interface IMovementState
{
    string Name { get; }
    bool Update(float timeDelta);
    string GetDescription();
}