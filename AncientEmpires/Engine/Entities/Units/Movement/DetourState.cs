using System.Numerics;

namespace AncientEmpires.Engine.Entities.Units.Movement;

public class DetourState : WalkingState
{
    private readonly Vector4 _detourPosition;

    public DetourState(EntityMovement movement, GamePosition currentPathNode, Vector4 detourPosition) : base(movement)
    {
        _detourPosition = detourPosition;
        Movement.NextPosition = _detourPosition;
        
        if (Movement.Path.Any() && Movement.Path.Peek() == currentPathNode)
            Movement.Path.Pop();
    }

    public override bool Update(float timeDelta)
    {
        if (Movement.Path.Any() && Movement.Path.Peek() != Movement.NextPosition)
            return base.Update(timeDelta);

        Movement.ChangeState(new WalkingState(Movement));
        return false;
    }
}