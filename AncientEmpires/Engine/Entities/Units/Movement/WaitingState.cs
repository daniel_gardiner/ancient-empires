namespace AncientEmpires.Engine.Entities.Units.Movement;

public class WaitingState : MovementStateBase
{
    private readonly int SliceWait;
    private int StartSlice;

    public WaitingState(EntityMovement movement, int sliceWait) : base(movement)
    {
        SliceWait = sliceWait;
        StartSlice = Engine.Timing.Slice;
    }
    
    public override bool Update(float timeDelta)
    {
        if (Engine.Timing.Slice - StartSlice < SliceWait) 
            return false;
        
        ConsoleWriter.WriteLine("Waiting state over");
        Movement.ResetCollisionTracking();
        Movement.ChangeState(new WalkingState(Movement));
        return true;

    }
}