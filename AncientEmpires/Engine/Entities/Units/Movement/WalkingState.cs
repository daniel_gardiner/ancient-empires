using AncientEmpires.Engine.Entities.Pathing;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Sprites;

namespace AncientEmpires.Engine.Entities.Units.Movement;

public class WalkingState : MovementStateBase
{
    public WalkingState(EntityMovement movement) : base(movement)
    {
        Movement.Entity.SpriteManager.ReplaceCycle<WalkCycle>();
    }

    public override bool Update(float timeDelta)
    {
        if (!Path.Any())
        {
            Movement.ChangeState(new IdleState(Movement));
            return false;
        }

        var movement = Entity.Speed * timeDelta;
        var newPosition = Entity.Position.MoveTowards(Movement.NextPosition, movement);

        if (Movement.HasCollisionAt(in newPosition) is CollisionResult result)
        {
            Movement.ChangeState(new CollisionState(Movement, result));
            return false;
        }

        Entity.UpdatePosition(newPosition);
        UpdateNextPosition(newPosition);
        Movement.UpdateDirection(Movement.NextPosition);
        Movement.OnNoCollision();
        return true;
    }

    private void UpdateNextPosition(GamePosition newPosition)
    {
        if (IsAtPathNode(newPosition))
            Path.Pop();

        if (!Path.Any())
        {
            Movement.ChangeState(new IdleState(Movement));
            return;
        }

        Movement.NextPosition = Path.Peek();
    }

    private bool IsAtPathNode(GamePosition newPosition)
    {
        if (!Path.Any()) 
            return false;

        if (newPosition != Movement.NextPosition) 
            return false;
        
        return Path.Peek() == Movement.NextPosition;
    }
}