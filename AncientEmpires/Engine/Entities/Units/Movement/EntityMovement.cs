using System.Numerics;
using AncientEmpires.Engine.Entities.Buildings;
using AncientEmpires.Engine.Entities.Pathing;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Map.Picker;

namespace AncientEmpires.Engine.Entities.Units.Movement;

public class EntityMovement
{
    public GameCollisionManager CollisionManager;
    protected readonly EntityManager EntityManager;
    private readonly GameEngine Engine;
    public int CollisionCount;
    public float CollisionDetourDistance;
    public bool CollisionHasWaited;
    public Entity Entity;
    public IMovementState State;
    public GamePosition Destination;
    public GamePosition NextPosition;
    public Stack<GamePosition> Path;

    public EntityMovement(GameEngine engine, Entity entity)
    {
        Path = new Stack<GamePosition>();
        Entity = entity;
        Engine = engine;
        ChangeState(new IdleState(this));
        EntityManager = Entity.Engine.GetComponent<EntityManager>();
        CollisionManager = Entity.Engine.GetComponent<GameCollisionManager>();
    }
    
    public virtual void Move(Stack<GamePosition> path)
    {
        Path = path;
        Destination = path.Last();
        NextPosition = path.Peek();
        ChangeState(new WalkingState(this));
    }

    public void Stop()
    {
        Path = new Stack<GamePosition>();
        Destination = Entity.Position;
        NextPosition = Entity.Position;
        ChangeState(new IdleState(this));
    }

    public bool Update(float timeDelta)
    {
        return State.Update(timeDelta);
    }

    public virtual ICollisionResult HasCollisionAt(in GamePosition position, bool silent = false)
    {
        return CollisionManager.EntityHasCollisionAt(Entity, in position);
    }

    public void UpdateDirection(GamePosition destination)
    {
        Entity.Direction = CalculateDirection(Entity.Position, destination);
    }

    public Direction CalculateDirection(GamePosition current, GamePosition destination)
    {
        var direction = destination - current;
        direction = Vector4.Normalize(direction.MapSpace);

        var angle = Math.Atan2(direction.MapSpace.Y, direction.MapSpace.X);
        var octant = Math.Round(8 * angle / (2 * Math.PI) + 8) % 8;

        return octant switch
        {
            0 => Direction.SouthEast,
            1 => Direction.South,
            2 => Direction.SouthWest,
            3 => Direction.West,
            4 => Direction.NorthWest,
            5 => Direction.North,
            6 => Direction.NorthEast,
            7 => Direction.East,
            _ => Entity.Direction
        };
    }

    public float Cost(GamePosition coordinates)
    {
        var tile = Entity.Engine.GetComponent<MapManager>().GetTile(coordinates);
        return MovementCost(tile);
    }

    private float MovementCost(MapTile tile)
    {
        // TODO: check for roads
        return 1;
    }

    public void ChangeState(IMovementState state)
    {
        ConsoleWriter.WriteLine($"{Entity} state changed from {State?.Name} to {state.Name} {state.GetDescription()}");
        State = state;
    }

    public void OnCollision()
    {
        CollisionCount += 3;
    }

    public void OnNoCollision()
    {
        CollisionCount = Math.Max(0, CollisionCount - 1);

        if (CollisionCount == 0)
        {
            CollisionDetourDistance = 0;
            CollisionHasWaited = false;
        }
    }

    public void ResetCollisionTracking()
    {
        CollisionCount = 0;
        CollisionHasWaited = false;
    }
}