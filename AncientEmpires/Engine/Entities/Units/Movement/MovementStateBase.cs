namespace AncientEmpires.Engine.Entities.Units.Movement;

public class MovementStateBase : IMovementState
{
    private string _name;

    public EntityMovement Movement { get; }
    public GameEngine Engine { get; }
    public Entity Entity { get; }
    public Stack<GamePosition> Path { get; }
    public string Name => _name ??= GetType().Name;

    public MovementStateBase(EntityMovement movement)
    {
        Movement = movement;
        Entity = movement.Entity;
        Path = movement.Path;
        Engine = movement.Entity.Engine;
    }

    public virtual bool Update(float timeDelta)
    {
        return false;
    }

    public string GetDescription() => string.Empty;
}