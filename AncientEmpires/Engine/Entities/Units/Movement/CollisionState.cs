using System.Numerics;
using AncientEmpires.Engine.Entities.Pathing;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Sprites;

namespace AncientEmpires.Engine.Entities.Units.Movement;

public class CollisionState : MovementStateBase
{
    private readonly ICollisionResult _collision;
    private int CollisionLimit = 20;

    public CollisionState(EntityMovement movement, ICollisionResult collision) : base(movement)
    {
        _collision = collision;
        Movement.Entity.SpriteManager.QueueCycle<IdleCycle>();
        Movement.OnCollision();

        ConsoleWriter.WriteLine("Collision Count : " + Movement.CollisionCount);
    }

    public override bool Update(float timeDelta)
    {
        if (Movement.CollisionCount > CollisionLimit && !Movement.CollisionHasWaited)
            return ChangeToWaiting(10);

        var nextTile = Path.Peek();
        var movementDistance = Entity.Speed * timeDelta;

        if (CheckCollisionAt(nextTile, movementDistance) is NoCollision)
            return ChangeToWalking();

        Movement.OnCollision();

        if (CanDetour(nextTile, 0.1f))
            return false;
        if (CanDetour(nextTile, 0.2f))
            return false;
        if (CanDetour(nextTile, 0.3f))
            return false;
        if (CanDetour(nextTile, 0.4f))
            return false;
        if (CanDetour(nextTile, 0.5f))
            return false;
        if (CanDetour(nextTile, 0.6f))
            return false;
        if (CanDetour(nextTile, 0.7f))
            return false;

        if (CanDetour(nextTile, 1f))
            return false;

        if (CanDetour(nextTile, 5f))
            return false;

        return false;
    }

    private bool ChangeToWaiting(int sliceWait)
    {
        Movement.ChangeState(new WaitingState(Movement, sliceWait));
        Movement.CollisionHasWaited = true;
        return true;
    }

    private bool ChangeToWalking()
    {
        Movement.ChangeState(new WalkingState(Movement));
        return true;
    }

    private bool CanDetour(GamePosition currentPathNode, float detourDistance)
    {
        if (detourDistance <= Movement.CollisionDetourDistance)
            return false;

        var detourPositions = new[]
        {
            new Vector4(currentPathNode.MapSpace.X + detourDistance, currentPathNode.MapSpace.Y, currentPathNode.MapSpace.Z, currentPathNode.MapSpace.W),
            new Vector4(currentPathNode.MapSpace.X + detourDistance, currentPathNode.MapSpace.Y - detourDistance, currentPathNode.MapSpace.Z, currentPathNode.MapSpace.W),
            new Vector4(currentPathNode.MapSpace.X + detourDistance, currentPathNode.MapSpace.Y + detourDistance, currentPathNode.MapSpace.Z, currentPathNode.MapSpace.W),
            new Vector4(currentPathNode.MapSpace.X - detourDistance, currentPathNode.MapSpace.Y, currentPathNode.MapSpace.Z, currentPathNode.MapSpace.W),
            new Vector4(currentPathNode.MapSpace.X - detourDistance, currentPathNode.MapSpace.Y - detourDistance, currentPathNode.MapSpace.Z, currentPathNode.MapSpace.W),
            new Vector4(currentPathNode.MapSpace.X - detourDistance, currentPathNode.MapSpace.Y + detourDistance, currentPathNode.MapSpace.Z, currentPathNode.MapSpace.W),
            new Vector4(currentPathNode.MapSpace.X, currentPathNode.MapSpace.Y + detourDistance, currentPathNode.MapSpace.Z, currentPathNode.MapSpace.W),
            new Vector4(currentPathNode.MapSpace.X, currentPathNode.MapSpace.Y - detourDistance, currentPathNode.MapSpace.Z, currentPathNode.MapSpace.W),
        };

        foreach (var detourPosition in detourPositions)
        {
            if (CheckCollisionAt(detourPosition, 0) is NoCollision)
            {
                Movement.CollisionDetourDistance = detourDistance;
                Movement.ChangeState(new DetourState(Movement, currentPathNode, detourPosition));
                return false;
            }
        }

        return false;
    }

    private ICollisionResult CheckCollisionAt(GamePosition nextTile, float distance)
    {
        var newPosition = Entity.Position.MoveTowards(nextTile, distance);
        return Movement.HasCollisionAt(in newPosition);
    }
}