using AncientEmpires.Engine.Interface;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Entities.Units.Physics;

public class SphereCollisionShape : CollisionShape
{
    protected BoundingSphere Sphere;
    protected readonly BoundingSphere OriginalSphere;

    public SphereCollisionShape(CollisionShapes parent, BoundingSphere sphere) : base(parent)
    {
        Sphere = sphere;
        OriginalSphere = sphere;
    }

    public override bool Intersects(CollisionShape shape, GamePosition collisionPosition)
    {
        var worldSpaceSphere = GetWorldSpaceSphere(Parent.Position);
        switch (shape)
        {
            case SphereCollisionShape sphere:
                var worldSpaceIntersectionSphere = sphere.GetWorldSpaceSphere(collisionPosition);
                return worldSpaceSphere.Contains(worldSpaceIntersectionSphere) != ContainmentType.Disjoint;
            case BoxCollisionShape box:
                var worldSpaceBox = box.GetWorldSpaceBox();
                return worldSpaceSphere.Contains(worldSpaceBox) != ContainmentType.Disjoint;
            default:
                throw new ArgumentOutOfRangeException(nameof(shape), shape, null);
        }
    }

    public override bool Intersects(CollisionShape shape)
    {
        var worldSpaceSphere = GetWorldSpaceSphere(Parent.Position);
        switch (shape)
        {
            case SphereCollisionShape sphere:
                var worldSpaceIntersectionSphere = sphere.GetWorldSpaceSphere();
                return worldSpaceSphere.Contains(worldSpaceIntersectionSphere) != ContainmentType.Disjoint;
            case BoxCollisionShape box:
                var worldSpaceBox = box.GetWorldSpaceBox();
                return worldSpaceSphere.Contains(worldSpaceBox) != ContainmentType.Disjoint;
            default:
                throw new ArgumentOutOfRangeException(nameof(shape), shape, null);
        }
    }

    public override bool Contains(GamePosition position)
    {
        var worldSpaceSphere = GetWorldSpaceSphere(Parent.Position);
        return worldSpaceSphere.Contains(position.WorldSpace.ToVector3()) != ContainmentType.Disjoint;
    }

    public override bool ContainedBy(BoxCollisionShape boxCollision)
    {
        var worldSpaceSphere = GetWorldSpaceSphere(Parent.Position);
        var worldSpaceBox = boxCollision.GetWorldSpaceBox();
        return worldSpaceBox.Contains(worldSpaceSphere) != ContainmentType.Disjoint;
    }

    public BoundingSphere GetMapSpaceSphere()
    {
        var worldSpace = Parent.Position.MapSpace.ToVector3();
        var worldSpaceSphere = new BoundingSphere(
            OriginalSphere.Center * TilePixelSize + worldSpace,
            OriginalSphere.Radius * TilePixelSize.X);
        return worldSpaceSphere;
    }

    public BoundingSphere GetWorldSpaceSphere()
    {
        var worldSpace = Parent.Position.WorldSpace.ToVector3();
        var worldSpaceSphere = new BoundingSphere(
            OriginalSphere.Center * TilePixelSize + worldSpace,
            OriginalSphere.Radius * TilePixelSize.X);
        return worldSpaceSphere;
    }

    public BoundingSphere GetWorldSpaceSphere(GamePosition entityPosition)
    {
        var worldSpace = entityPosition.WorldSpace.ToVector3();
        var worldSpaceSphere = new BoundingSphere(
            OriginalSphere.Center * TilePixelSize + worldSpace,
            OriginalSphere.Radius * TilePixelSize.X);
        return worldSpaceSphere;
    }
}