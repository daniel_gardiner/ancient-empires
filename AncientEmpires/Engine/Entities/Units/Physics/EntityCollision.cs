using AncientEmpires.Engine.Entities.Pathing;
using AncientEmpires.Engine.Map.Rendering;

namespace AncientEmpires.Engine.Entities.Units.Physics;

public abstract class EntityCollision
{
    protected GameEngine Engine;
    protected Entity Entity;
    protected EntityCollision BackingEntityCollision;
    protected readonly CollisionShapes CollisionShapes;
    protected readonly CollisionShapes SelectionShapes;
    protected IsometricPositionStrategy PositionStrategy;
    public virtual GamePosition Position => Entity.Position;

    protected EntityCollision(GameEngine engine, Entity entity)
    {
        Engine = engine;
        Entity = entity;
        CollisionShapes = new CollisionShapes(engine, entity, this);
        SelectionShapes = new CollisionShapes(engine, entity, this);
        PositionStrategy = Engine.GetComponent<IsometricPositionStrategy>();
    }

    protected EntityCollision()
    {
    }

    public virtual ICollisionResult CollidesWith(Entity entity) => GetCollisionShapes().CollidesWith(entity);

    public virtual ICollisionResult CollidesWith(BoxCollisionShape collisionShape) => GetCollisionShapes().CollidesWith(collisionShape);

    public virtual bool SelectedBy(GamePosition position) => GetSelectionShapes().Contains(position);

    public virtual bool SelectedBy(BoxCollisionShape boxCollision) => GetSelectionShapes().ContainedBy(boxCollision);

    public virtual CollisionShapes GetCollisionShapes() => CollisionShapes;

    public virtual CollisionShapes GetSelectionShapes() => SelectionShapes;

    public abstract EntityCollision At(GamePosition entityPosition);
}