namespace AncientEmpires.Engine.Entities.Units.Physics;

public class UnitCollisionAt : EntityCollisionAt
{
    public UnitCollisionAt(GamePosition entityPosition, EntityCollision entityCollision) : base(entityPosition, entityCollision)
    {
    }

    public override EntityCollision At(GamePosition entityPosition) => new UnitCollisionAt(entityPosition, this);
}