using System.Numerics;
using AncientEmpires.Engine.Entities.Pathing;
using Vortice.Mathematics;
using Wintellect.PowerCollections;

namespace AncientEmpires.Engine.Entities.Units.Physics;

public class CollisionShapes : ListBase<CollisionShape>
{
    private readonly List<CollisionShape> _shapes = new();
    public GamePosition Position => Collision.Position;
    public GameEngine Engine { get; }
    public Entity Entity { get; }
    public EntityCollision Collision { get; }


    public CollisionShapes(GameEngine engine, Entity entity, EntityCollision collision)
    {
        Engine = engine;
        Entity = entity;
        Collision = collision;
    }

    public ICollisionResult CollidesWith(Entity collider)
    {
        foreach (var sourceShape in this)
        foreach (var collisionShape in collider.Collision.GetCollisionShapes())
        {
            if (sourceShape.Intersects(collisionShape, collider.Position)) 
                return new CollisionResult(Entity, collider, Entity.Movement.State).AppendCollisionShapes(sourceShape, collisionShape);
        }

        return CollisionResult.NoCollision;
    }

    public ICollisionResult CollidesWith(BoxCollisionShape collisionShape)
    {
        foreach (var sourceShape in this)
            if (sourceShape.Intersects(collisionShape))
                return new CollisionResult(Entity, collisionShape).AppendCollisionShapes(sourceShape, collisionShape);

        return CollisionResult.NoCollision;
    }

    public bool Contains(GamePosition position)
    {
        foreach (var sourceShape in this)
        {
            if (sourceShape.Contains(position))
                return true;
        }

        return false;
    }

    public bool ContainedBy(BoxCollisionShape collisionShape)
    {
        foreach (var sourceShape in this)
            if (sourceShape.ContainedBy(collisionShape))
                return true;

        return false;
    }

    public override void Clear() => _shapes.Clear();

    public override void Insert(int index, CollisionShape item) => _shapes.Insert(index, item);

    public override void RemoveAt(int index) => _shapes.RemoveAt(index);


    public override CollisionShape this[int index]
    {
        get => _shapes[index];
        set => _shapes[index] = value;
    }

    public override int Count => _shapes.Count;

    public void AddBox(Vector3 minimum, Vector3 maximum)
    {
        Add(new BoxCollisionShape(this, new BoundingBox(minimum, maximum)));
    }

    public void AddSphere(Vector3 center, float radius)
    {
        Add(new SphereCollisionShape(this, new BoundingSphere(center, radius)));
    }
}