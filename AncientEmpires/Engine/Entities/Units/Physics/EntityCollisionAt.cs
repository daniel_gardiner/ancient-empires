namespace AncientEmpires.Engine.Entities.Units.Physics;

public abstract class EntityCollisionAt : EntityCollision
{
    public override GamePosition Position { get; }
    public EntityCollision EntityCollision { get; }

    public EntityCollisionAt(GamePosition entityPosition, EntityCollision entityCollision)
    {
        Position = entityPosition;
        EntityCollision = entityCollision;
    }

    public override CollisionShapes GetCollisionShapes() => EntityCollision.GetCollisionShapes();
    public override CollisionShapes GetSelectionShapes() => EntityCollision.GetSelectionShapes();
}