using System.Numerics;
using AncientEmpires.Engine.Entities.Buildings;

namespace AncientEmpires.Engine.Entities.Units.Physics;

public class PathCollision : EntityCollision
{
    protected float CollisionRadius = 0.5f;
    protected Vector3 SelectionBox = new(0.25f, 0.25f, 0);

    public PathCollision(GameEngine engine, Path path) : base(engine, path)
    {
        Entity = path;
        var minimum = SelectionBox * -1;
        var maximum = SelectionBox;
        SelectionShapes.AddBox(minimum, maximum);
        CollisionShapes.AddSphere(new Vector3(0, 0, 0), CollisionRadius);
    }

    public override EntityCollision At(GamePosition entityPosition) => new PathCollisionAt(entityPosition, this);
}