using System.Numerics;

namespace AncientEmpires.Engine.Entities.Units.Physics;

public class UnitCollision : EntityCollision
{
    protected float CollisionRadius = 0.1f;
    protected Vector3 SelectionBox = new(0.25f, 0.75f, 0);

    public UnitCollision(GameEngine engine, Unit unit) : base(engine, unit)
    {
        var minimum = SelectionBox * -1;
        var maximum = SelectionBox;
        SelectionShapes.AddBox(minimum, maximum);
        CollisionShapes.AddSphere(new Vector3(0f, -(unit.Size.Y / 2 - CollisionRadius * 2), 0), CollisionRadius);
    }

    public override EntityCollision At(GamePosition entityPosition) => new UnitCollisionAt(entityPosition, this);
}