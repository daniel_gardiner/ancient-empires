using System.Numerics;
using AncientEmpires.Engine.Entities.Buildings;

namespace AncientEmpires.Engine.Entities.Units.Physics;

public class BuildingCollision : EntityCollision
{
    protected readonly Building Building;
    protected float CollisionRadius = 0.5f;
    protected Vector3 SelectionBox = new(0.25f, 0.75f, 0);

    public BuildingCollision(GameEngine engine, Building building) : base(engine, building)
    {
        Building = building;
        var minimum = SelectionBox * -1;
        var maximum = SelectionBox;
        SelectionShapes.AddBox(minimum, maximum);
        CollisionShapes.AddSphere(new Vector3(0, 0, 0), CollisionRadius);
    }

    public override EntityCollision At(GamePosition entityPosition) => new BuildingCollisionAt(entityPosition, this);
}