namespace AncientEmpires.Engine.Entities.Units.Physics;

public class BuildingCollisionAt : EntityCollisionAt
{
    public BuildingCollisionAt(GamePosition entityPosition, EntityCollision entityCollision) : base(entityPosition, entityCollision)
    {
    }

    public override EntityCollision At(GamePosition entityPosition) => new BuildingCollisionAt(entityPosition, this);
}