using AncientEmpires.Engine.Interface;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Entities.Units.Physics;

public class BoxCollisionShape : CollisionShape
{
    protected readonly BoundingBox OriginalBox;

    private BoxCollisionShape(BoundingBox box) : this(null, box)
    {
    }

    public BoxCollisionShape(CollisionShapes parent, BoundingBox box) : base(parent)
    {
        OriginalBox = box;
    }

    public override bool Intersects(CollisionShape shape, GamePosition position)
    {
        var worldSpaceBox = GetWorldSpaceBox();
        switch (shape)
        {
            case SphereCollisionShape sphere:
                var worldSpaceIntersectionSphere = sphere.GetWorldSpaceSphere(position);
                return worldSpaceBox.Contains(worldSpaceIntersectionSphere) != ContainmentType.Disjoint;
            case BoxCollisionShape box:
                var worldSpaceIntersectionBox = box.GetWorldSpaceBox();
                return worldSpaceBox.Contains(worldSpaceIntersectionBox) != ContainmentType.Disjoint;
            default:
                throw new ArgumentOutOfRangeException(nameof(shape), shape, null);
        }
    }

    public override bool Intersects(CollisionShape shape)
    {
        var worldSpaceBox = GetWorldSpaceBox();
        switch (shape)
        {
            case SphereCollisionShape sphere:
                var worldSpaceIntersectionSphere = sphere.GetWorldSpaceSphere();
                return worldSpaceBox.Contains(worldSpaceIntersectionSphere) != ContainmentType.Disjoint;
            case BoxCollisionShape box:
                var worldSpaceIntersectionBox = box.GetWorldSpaceBox();
                return worldSpaceBox.Contains(worldSpaceIntersectionBox) != ContainmentType.Disjoint;
            default:
                throw new ArgumentOutOfRangeException(nameof(shape), shape, null);
        }
    }

    public override bool Contains(GamePosition position)
    {
        var worldSpaceBox = GetWorldSpaceBox();
        return worldSpaceBox.Contains(position.WorldSpace.ToVector3()) != ContainmentType.Disjoint;
    }

    public override bool ContainedBy(BoxCollisionShape boxCollision)
    {
        var worldSpaceBox = GetWorldSpaceBox();
        var intersectionBox = boxCollision.GetWorldSpaceBox();
        return intersectionBox.Contains(worldSpaceBox) != ContainmentType.Disjoint;
    }

    public BoundingBox GetWorldSpaceBox()
    {
        // TODO: create space independent bounding box
        if (Parent == null)
        {
            // TODO: without parent we assume this is world spaced until we have space independent collision shapes
            return OriginalBox;
        }
        
        var worldSpace = Parent.Position.WorldSpace.ToVector3();
        var minimum = OriginalBox.Minimum * TilePixelSize + worldSpace;
        var maximum = OriginalBox.Maximum * TilePixelSize + worldSpace;

        if (minimum.X > maximum.X) (minimum.X, maximum.X) = (maximum.X, minimum.X);
        if (minimum.Y > maximum.Y) (minimum.Y, maximum.Y) = (maximum.Y, minimum.Y);
        return new BoundingBox(minimum, maximum);
    }

    public static BoxCollisionShape FromBoundingBox(BoundingBox boundingBox)
    {
        return new BoxCollisionShape(boundingBox);
    }
}