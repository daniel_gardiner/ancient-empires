namespace AncientEmpires.Engine.Entities.Units.Physics;

public class PathCollisionAt : EntityCollisionAt
{
    public PathCollisionAt(GamePosition entityPosition, EntityCollision entityCollision) : base(entityPosition, entityCollision)
    {
    }

    public override EntityCollision At(GamePosition entityPosition) => new PathCollisionAt(entityPosition, this);
}