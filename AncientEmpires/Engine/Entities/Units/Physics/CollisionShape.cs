using System.Numerics;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Map.Rendering;

namespace AncientEmpires.Engine.Entities.Units.Physics;

public abstract class CollisionShape
{
    protected ComponentLookup<IsometricPositionStrategy> PositionStrategy { get; }
    protected CollisionShapes Parent { get; }
    protected GameEngine Engine => GameContext.Instance.Engine;
    protected Entity Entity => Parent.Entity;
    protected Vector3 TilePixelSize { get; }

    protected CollisionShape(CollisionShapes parent)
    {
        Parent = parent;
        PositionStrategy = Engine.ComponentLookup<IsometricPositionStrategy>();
        TilePixelSize = Engine.Configuration.TilePixelSize.ToVector3();
    }

    public abstract bool Intersects(CollisionShape collisionShape, GamePosition position);
    public abstract bool Intersects(CollisionShape collisionShape);
    public abstract bool Contains(GamePosition position);
    public abstract bool ContainedBy(BoxCollisionShape boxCollision);
    protected Vector3 GetWorldSpacePoint(GamePosition position) => position.WorldSpace.ToVector3();
}