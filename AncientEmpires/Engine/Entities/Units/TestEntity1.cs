using System.Numerics;
using AncientEmpires.Engine.Render;
using AncientEmpires.Engine.Sprites;
using MoreLinq;

namespace AncientEmpires.Engine.Entities.Units;

public class TestEntity1 : Legion
{
    public DirectionalSet Sprites { get; set; }

    public TestEntity1(GameEngine engine, int id) : base(engine, id, EntityType.RomanLegion)
    {
        var walkCycle = new WalkCycle(engine, TextureType.Units, new Vector2(20, 20), new Vector2(0, 0), new Vector2(0, 160), 1, 10, 0, 0, new List<int>());
        walkCycle.Directions.ForEach(o => o.Value.Clear());
        walkCycle.Directions.ForEach(o => o.Value.Add(new Sprite(new RectangleF(0, 3200, 20, 3220), new List<int>(1))));
        var idleCycle = new IdleCycle(engine, TextureType.Units, new Vector2(20, 20), new Vector2(0, 0), new Vector2(0, 160), 1, 10, 0, 0, new List<int>());
        idleCycle.Directions.ForEach(o => o.Value.Clear());
        idleCycle.Directions.ForEach(o => o.Value.Add(new Sprite(new RectangleF(0, 3200, 20, 3220), new List<int>(1))));
        SpriteManager.AddCycle(walkCycle);
        SpriteManager.AddCycle(idleCycle);
        SpriteManager.QueueCycle<IdleCycle>();

        Size = new Vector4(2, 2, 0, 0); //,  new Vector4a(20, 20, 0) / Engine.Map.TileSize;
    }
}