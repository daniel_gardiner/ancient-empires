using System.Numerics;

namespace AncientEmpires.Engine.Entities;

public class EntityMetadataManager
{
    public static readonly EntityMetadataManager Instance = new();
    private readonly Dictionary<EntityType, EntityMetaData> _entities = new();
    private decimal LengthOfYear = 1200;

    public EntityMetadataManager()
    {
        _entities[EntityType.TestEntity1] = new EntityMetaData
        {
            Type = EntityType.TestEntity1,
            Speed = 2f,
            Message = "Test Entity 1",
        };
            
        _entities[EntityType.RomanLegion] = new EntityMetaData
        {
            Type = EntityType.RomanLegion,
            Cost = 15000,
            Size = new Vector4(1, 1.5f, 0, 0),
            SpriteSize = new Vector4(1, 1.5f, 0, 0),
            Speed = 1f,
            Message = "Roman Legion",
        };

        _entities[EntityType.GreekLegion] = new EntityMetaData
        {
            Type = EntityType.RomanLegion,
            Cost = 15000,
            Size = new Vector4(1f, 1.5f, 0, 0),
            SpriteSize = new Vector4(1, 1.5f, 0, 0),
            Speed = 1f,
            Message = "Greek Legion",
        };
            
        _entities[EntityType.Bulldozer] = new EntityMetaData
        {
            Type = EntityType.Bulldozer,
            Cost = 1000,
            Size = new Vector4(1, 1, 0, 0),
            Message = "Bulldozer",
        };
                
        _entities[EntityType.Fountain] = new EntityMetaData
        {
            Type = EntityType.Fountain,
            Period = 10,
            Cost = 5000,
            Size = new Vector4(1, 1, 0, 0),
            ObjectStrength = 200,
            Message = "Fountain",
        };
            
        _entities[EntityType.Road] = new EntityMetaData
        {
            Type = EntityType.Road,
            Period = 1,
            Cost = 1000,
            Size = new Vector4(1, 1, 0, 0),
            ObjectStrength = 200,
            Message = "Paved Road",
            Passable = true,
            ZIndex = 9
        };

        _entities[EntityType.Wall] = new EntityMetaData
        {
            Type = EntityType.Wall,
            Period = 1,
            Cost = 5000,
            Size = new Vector4(1, 1f, 0, 0),
            SpriteSize = new Vector4(1, 2f, 0, 0),
            ObjectStrength = 200,
            Message = "Wall",
        };
            
        _entities[EntityType.Farm] = new EntityMetaData
        {
            Type = EntityType.Farm,
            Period = (int) Math.Round(LengthOfYear / 12m),
            Yeild = 10000,
            Cost = 50000,
            Size = new Vector4(3, 3, 0, 0),
            ObjectStrength = 200,
            Message = "Grain farm",
        };
            
        _entities[EntityType.House] = new EntityMetaData
        {
            Type = EntityType.House,
            Cost = 10000,
            Size = new Vector4(2, 2, 0, 0),
            ObjectStrength = 100,
            Message = "House",
        };
            
        _entities[EntityType.Garrison] = new EntityMetaData
        {
            Type = EntityType.Garrison,
            Cost = 100000,
            Size = new Vector4(3, 4, 0, 0),
            ObjectStrength = 500,
            Message = "Garrison",
        };
    }

    public EntityMetaData Get(EntityType type)
    {
        return _entities[type];
    }
}
    
/*
BuildingDatabase[(int) EntityType.Road].Period = 1;
BuildingDatabase[(int) EntityType.Road].Cost = 1000;
BuildingDatabase[(int) EntityType.Road].ObjectHeight = 1;
BuildingDatabase[(int) EntityType.Road].ObjectWidth = 1;
BuildingDatabase[(int) EntityType.Road].ObjectStrength = 200;
BuildingDatabase[(int) EntityType.Road].Message = "Paved Road";

BuildingDatabase[(int) EntityType.Wall].Period = 1;
BuildingDatabase[(int) EntityType.Wall].Cost = 5000;
BuildingDatabase[(int) EntityType.Wall].ObjectHeight = 1;
BuildingDatabase[(int) EntityType.Wall].ObjectWidth = 1;
BuildingDatabase[(int) EntityType.Wall].ObjectStrength = 200;
BuildingDatabase[(int) EntityType.Wall].Message = "Wall";

BuildingDatabase[(int) EntityType.Farm].Yeild = 10000;
BuildingDatabase[(int) EntityType.Farm].Period = (int) Math.Round(mLengthOfYear / 12m);
BuildingDatabase[(int) EntityType.Farm].Cost = 50000;
BuildingDatabase[(int) EntityType.Farm].ObjectHeight = 3;
BuildingDatabase[(int) EntityType.Farm].ObjectWidth = 3;
BuildingDatabase[(int) EntityType.Farm].ObjectStrength = 200;
BuildingDatabase[(int) EntityType.Farm].Message = "Grain farm";

BuildingDatabase[(int) EntityType.House].Cost = 10000;
BuildingDatabase[(int) EntityType.House].ObjectHeight = 2;
BuildingDatabase[(int) EntityType.House].ObjectWidth = 2;
BuildingDatabase[(int) EntityType.House].ObjectStrength = 100;
BuildingDatabase[(int) EntityType.House].Message = "Housing";

BuildingDatabase[(int) EntityType.Garrison].Cost = 100000;
BuildingDatabase[(int) EntityType.Garrison].ObjectHeight = 4;
BuildingDatabase[(int) EntityType.Garrison].ObjectWidth = 3;
BuildingDatabase[(int) EntityType.Garrison].ObjectStrength = 500;
BuildingDatabase[(int) EntityType.Garrison].Message = "Garrison";
*/