using AncientEmpires.Engine.Render;

namespace AncientEmpires.Engine.Entities;

public class Bulldozer : Entity
{
    public Bulldozer(GameEngine engine, int id) : base(id, EntityType.Bulldozer)
    {
        Sprite = Sprite.Empty;
    }

    public override bool CanSpawn(GamePosition position)
    {
        /*var entities = tiles.SelectMany(o => o.Entities).ToList();
        return entities.Any() && entities.All(o => o.Type != EntityType.Bulldozer);*/
        ConsoleWriter.WriteLine("Update to use CollisionManager Wall.CanSpawn");
        return true;
    }
}