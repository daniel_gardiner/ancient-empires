using AncientEmpires.Engine.Actions;
using AncientEmpires.Engine.Interface;

namespace AncientEmpires.Engine;

public static class ActionCondition
{
    public static readonly Func<GameEngine, InputState, bool> NoneActive = (engine, _) => !engine.CurrentActions.Any(o => o.IsActive);

    public static Func<GameEngine, InputState, bool> LeftDownFor(int duration) => (engine, state) => state.LeftDownDuration > duration;
    public static Func<GameEngine, InputState, bool> Active<TAction>() where TAction : GameAction => (engine, _) => engine.CurrentActions.OfType<TAction>().Any(o => o.IsActive);
    public static Func<GameEngine, InputState, bool> NotRunning<TAction>() where TAction : GameAction => (engine, state) => !engine.CurrentActions.OfType<TAction>().Any();

    public static Func<GameEngine, InputState, bool> NotRunning<TAction1, TAction2>()
        where TAction1 : GameAction
        where TAction2 : GameAction => (engine, state) => !engine.CurrentActions.OfType<TAction1>().Any() && !engine.CurrentActions.OfType<TAction2>().Any();

    public static Func<GameEngine, InputState, bool> NotActive<TAction>() where TAction : GameAction => (engine, _) => !engine.CurrentActions.OfType<TAction>().Any(o => o.IsActive);

    public static Func<GameEngine, InputState, bool> All(params Func<GameEngine, InputState, bool>[] conditions) => (engine, state) =>
    {
        foreach (var condition in conditions)
        {
            var result = condition.Invoke(engine, state);
            if (!result)
                return false;
        }

        return true;
    };

    public static Func<GameEngine, InputState, bool> NotActive<TAction1, TAction2>()
        where TAction1 : GameAction
        where TAction2 : GameAction => (engine, _) =>
        !engine.CurrentActions.OfType<TAction1>().Any(o => o.IsActive) &&
        !engine.CurrentActions.OfType<TAction2>().Any(o => o.IsActive);
}