﻿using System.Numerics;
using AncientEmpires.Engine.Interface;

namespace AncientEmpires.Engine;

public struct GamePosition : IEquatable<GamePosition>
{
    public static readonly GamePosition Empty = new();

    public Vector4 CursorPosition;
    public Vector4 WorldSpace;

    public Vector4 MapSpace;

    public GamePosition()
    {
        CursorPosition = default;
        WorldSpace = default;
        MapSpace = default;
    }

    public GamePosition(InputState inputState)
    {
        CursorPosition = inputState.CursorPosition;
        WorldSpace = GameContext.Instance.Engine.Camera.ScreenSpaceToWorldSpace(inputState.CursorPosition);
        MapSpace = GameContext.Instance.Engine.Camera.ScreenSpaceToMapSpace(CursorPosition);
    }

    public bool Equals(GamePosition other) => MapSpace.Equals(other.MapSpace);

    public override bool Equals(object obj) => obj is GamePosition other && Equals(other);

    public override int GetHashCode() => MapSpace.GetHashCode();

    public static GamePosition FromWorldSpace(Vector4 position)
    {
        return new GamePosition
        {
            MapSpace = GameContext.Instance.Engine.Camera.WorldSpaceToMapSpace(position),
            CursorPosition = GameContext.Instance.Engine.Camera.MapSpaceToScreenSpace(position),
            WorldSpace = position
        };
    }

    public static GamePosition FromMapSpace(Vector4 position)
    {
        return new GamePosition
        {
            MapSpace = position,
            CursorPosition = GameContext.Instance.Engine.Camera.MapSpaceToScreenSpace(position),
            WorldSpace = GameContext.Instance.Engine.Camera.MapSpaceToWorldSpace(position)
        };
    }

    public static bool operator ==(GamePosition a, GamePosition b) => a.Equals(b);
    public static bool operator !=(GamePosition a, GamePosition b) => !a.Equals(b);

    public static implicit operator GamePosition(Vector4 position)
    {
        return FromMapSpace(position);
    }
    
    public static GamePosition operator +(GamePosition a, GamePosition b)
    {
        var mapSpace = a.MapSpace + b.MapSpace;
        return new GamePosition
        {
            MapSpace = mapSpace,
            CursorPosition = GameContext.Instance.Engine.Camera.MapSpaceToScreenSpace(mapSpace),
            WorldSpace = GameContext.Instance.Engine.Camera.MapSpaceToWorldSpace(mapSpace)
        };
    }

    public static GamePosition operator -(GamePosition a, GamePosition b)
    {
        return new GamePosition
        {
            CursorPosition = a.CursorPosition - b.CursorPosition,
            WorldSpace = a.WorldSpace - b.WorldSpace,
            MapSpace = a.MapSpace - b.MapSpace
        };
    }
}