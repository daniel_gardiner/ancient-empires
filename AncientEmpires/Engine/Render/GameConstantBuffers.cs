﻿using System.Diagnostics;
using System.Numerics;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Helpers;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Render;

public abstract class GameConstantBuffers : RenderableComponent
{
    public static ConstantBuffer<WorldConstant> WorldConstantBuffer;
    public static ConstantBuffer<TextureConstant> TextureConstantBuffer;
    protected ComponentLookup<GameMap> Map;
    protected ComponentLookup<MapViewport> MapViewport;
    protected ComponentLookup<GameDebug> GameDebug;

    public abstract Matrix4x4 Projection { [DebuggerStepThrough] get; }
    public abstract Matrix4x4 View { [DebuggerStepThrough] get; }
    public abstract Matrix4x4 World { [DebuggerStepThrough] get; }

    public override void OnInitialize()
    {
        base.OnInitialize();

        Map = Engine.ComponentLookup<GameMap>();
        MapViewport = Engine.ComponentLookup<MapViewport>();
        GameDebug = Engine.ComponentLookup<GameDebug>();
    }

    public override void OnReleaseResources()
    {
        base.OnReleaseResources();

        WorldConstantBuffer = null;
        TextureConstantBuffer = null;
    }

    public override void OnBeforeDraw()
    {
        base.OnBeforeDraw();

        UpdateWorldConstant();
        UpdateTextureBuffer();
    }

    protected virtual void UpdateWorldConstant()
    {
        var viewportSize = Engine.ScreenSize;
        var viewportLeft = 0;
        var viewportTop = 0;

        var timing = new Vector4(
            Engine.Timing.TotalElapsedMs,
            Engine.Timing.TotalElapsedMs / 1000,
            Engine.Timing.TotalElapsedMs % 1000,
            Engine.Timing.TotalElapsedMs / 1000 % 1);

        var constant = new WorldConstant
        {
            WorldViewProjectProjection = Matrix4x4.Transpose(World * View * Projection),
            World = Matrix4x4.Transpose(World),
            View = Matrix4x4.Transpose(View),
            Projection = Matrix4x4.Transpose(Projection),
            Timing = timing,
            MapSize = Map.Value.Size,
            Debug = new Vector4(GameDebug.Value.Enabled
                ? 1
                : 0, 0, 0, 0),
            TileSize = new Vector4(128, 64, 0, 0),
            Viewport = new Vector4(
                viewportLeft,
                viewportTop,
                viewportSize.X,
                viewportSize.Y),
        };
        WorldConstantBuffer.UpdateValue(in constant);
    }

    protected virtual void UpdateTextureBuffer()
    {
        /*
        var texture = AllTextures[0];
        if (Math.Abs(TextureConstantBuffer.Value.PixelSize.X - texture.Size.Width) < 0.1 &&
            Math.Abs(TextureConstantBuffer.Value.PixelSize.Y - texture.Size.Height) < 0.1)
            return;
            */

        var constant = new TextureConstant
        {
            PixelSize = new Vector4(Engine.Configuration.TexturePixelSize, Engine.Configuration.TexturePixelSize, 0, 0),
            TexelSize = Engine.Configuration.TexelSize,
            MapTileMaskTextureIndex = new Int4(Engine.Configuration.TileMaskTextureIndex)
        };
        TextureConstantBuffer.UpdateValue(in constant);
    }
}