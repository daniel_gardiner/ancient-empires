using System.Diagnostics;
using System.Numerics;
using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Helpers;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Render;

public class DeferredWorldConstantBuffers : WorldConstantBuffers
{
    public override Matrix4x4 Projection
    {
        [DebuggerStepThrough] get => CreateOrthographicOffCenterLH();
    }

    public override Matrix4x4 View
    {
        [DebuggerStepThrough] get => Matrix4x4.Identity;
    }

    public override Matrix4x4 World
    {
        [DebuggerStepThrough] get => Matrix4x4.Identity;
    }

    protected override void UpdateTextureBuffer()
    {
        var constant = new TextureConstant
        {
            PixelSize = new Vector4(Engine.ScreenSize.X, Engine.ScreenSize.Y, 0, 0),
            TexelSize = new Vector4(1f / Engine.ScreenSize.X, 1f / Engine.ScreenSize.Y, 0, 0),
            MapTileMaskTextureIndex = new Int4(0)
        };
        TextureConstantBuffer.UpdateValue(in constant);
    }

    private Matrix4x4 CreateOrthographicOffCenterLH()
    {
        var matrix4X4 = Matrix4x4.CreateOrthographicOffCenter(0, Engine.ScreenSize.X, Engine.ScreenSize.Y, 0, 0.1f, 10f);
        matrix4X4.M33 *= -1.0f;
        return matrix4X4;
    }
}