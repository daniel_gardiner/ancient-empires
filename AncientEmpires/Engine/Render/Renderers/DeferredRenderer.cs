using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Interface;

namespace AncientEmpires.Engine.Render.Renderers;

public class DeferredRenderer : GameRenderer
{
    public override void OnCreateComponents()
    {
        base.OnCreateComponents();

        AddComponent<DeferredWorldLayer>();
        //AddComponent<DeferredInterfaceLayer>();
    }
    
    public override void OnDrawRenderable(GameRenderable renderable)
    {
        GraphicsDevice.Value.Draw(renderable);
    }
}