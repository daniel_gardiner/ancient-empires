using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Interface;

namespace AncientEmpires.Engine.Render.Renderers;

public class GameInterfaceRenderer : GameRenderer
{
    public override void OnCreateComponents()
    {
        base.OnCreateComponents();
        
        AddComponent<InterfaceLayer>();
        AddComponent<InterfaceTextLayer>();
    }

    public override void OnDraw()
    {
        base.OnDraw();
    }
}