using System.Diagnostics;
using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Groups;

namespace AncientEmpires.Engine.Render.Renderers;

public abstract class GameRenderer : RenderableComponent
{
    protected ComponentLookup<GraphicsDevice> GraphicsDevice;

    protected readonly IComparer<GameLayer> Comparer = Comparer<GameLayer>.Create((a, b) => (int)(b.ZIndex - a.ZIndex switch
    {
        > 0 => 1,
        < 0 => -1,
        _ => 0
    }));

    public List<GameLayer> Layers { get; set; } = new();

    public override void OnInitialize()
    {
        base.OnInitialize();

        GraphicsDevice = Engine.ComponentLookup<GraphicsDevice>();
    }

    public void RegisterLayer(GameLayer layer)
    {
        if (Layers.Contains(layer))
            return;

        Layers.Add(layer);
    }

    public override void OnDraw()
    {
        base.OnDraw();

        Layers.Sort(Comparer);
        foreach (var renderable in Layers)
        {
            Debug.Assert(renderable.Shader != null, "Shader != null");

            if (!renderable.CanDraw())
                continue;

            OnDrawRenderable(renderable);
        }
    }

    public virtual void OnDrawRenderable(GameRenderable renderable)
    {
        GraphicsDevice.Value.Draw(renderable);
        renderable.MarkClean();
    }

    public override void Clear(string reason)
    {
        ConsoleWriter.WriteLine($"Clear GameRenderables because {reason}");
        Layers.Clear();
    }
}