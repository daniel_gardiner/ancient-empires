using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Groups;
using AncientEmpires.Engine.Render.Interface;

namespace AncientEmpires.Engine.Render.Renderers;

public class GameRenderManager : RenderableComponent
{
    public GameWorldRenderer WorldRenderer;
    public GameInterfaceRenderer InterfaceRenderer;
    public DeferredRenderer DeferredRenderer;

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();

        WorldRenderer = AddComponent<GameWorldRenderer>();
        InterfaceRenderer = AddComponent<GameInterfaceRenderer>();
        DeferredRenderer = AddComponent<DeferredRenderer>();
    }

    public void RegisterLayer(GameLayer layer)
    {
        switch (layer)
        {
            case IDeferredRenderable:
                DeferredRenderer.RegisterLayer(layer);
                break;
            case IInterfaceRenderable:
                InterfaceRenderer.RegisterLayer(layer);
                break;
            default:
                WorldRenderer.RegisterLayer(layer);
                break;
        }
    }

    public override void Clear(string reason)
    {
        ConsoleWriter.WriteLine($"Clear GameRenderables because {reason}");
        WorldRenderer.Clear(reason);
        InterfaceRenderer.Clear(reason);
    }
}