﻿using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Entities;
using AncientEmpires.Engine.Render.Map;
using AncientEmpires.Engine.Render.Util;

namespace AncientEmpires.Engine.Render.Renderers;

public class GameWorldRenderer : GameRenderer
{
    public override void OnCreateComponents()
    {
        base.OnCreateComponents();

        AddComponent<WorldConstantBuffers>();
        AddComponent<MapLayer>();
        AddComponent<EntityLayer>();
        AddComponent<MapTextLayer>();
        AddComponent<CollisionLayer>();
    }

    public override void OnDrawRenderable(GameRenderable renderable)
    {
        base.OnDrawRenderable(renderable);
    }
}