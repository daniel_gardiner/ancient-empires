using Vortice.Mathematics;

namespace AncientEmpires.Engine.Render;

public static class ConversionExtensions
{
    public static Int4 ToInt4(this Guid guid)
    {
        var bytes = guid.ToByteArray();
        var ints = new int[bytes.Length / sizeof(int)];
        for (var i = 0; i < bytes.Length; i++)
        {
            ints[i / sizeof(int)] = ints[i / sizeof(int)] | (bytes[i] << 8 * ((sizeof(int) - 1) - (i % sizeof(int))));
        }

        return new Int4(ints);
    }

    public static Guid ToGuid(this Int4 int4)
    {
        var ints = new[] { int4.X, int4.Y, int4.Z, int4.W };
        byte[] bytes = new byte[4 * sizeof(int)];
        for (int i = 0; i < bytes.Length; i++)
        {
            bytes[i] = (byte)((ints[i / sizeof(int)] & (byte.MaxValue << 8 * ((sizeof(int) - 1) - (i % sizeof(int))))) >> 8 * ((sizeof(int) - 1) - (i % sizeof(int))));
        }

        return new Guid(bytes);
    }
}