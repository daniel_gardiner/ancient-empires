using System.Numerics;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Map.Picker;
using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Groups;
using AncientEmpires.Engine.Render.Shaders;

namespace AncientEmpires.Engine.Render.Map;

public class OverlayMapLayer : MapLayerBase
{
    protected ComponentLookup<GameMap> Map;

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();

        Shader = AddComponent<TerrainOverlayShader>();
    }

    public override void OnInitialize()
    {
        base.OnInitialize();
        
        Map = new ComponentLookup<GameMap>();
        RenderMode = BatchRenderMode.Dynamic;
        ZIndex = Engine.Configuration.GetRenderLayer(KnownRenderLayers.MapOverlay);
        BufferPool = new ComponentLookup<BufferPoolManager>();
    }

    public void AddTile(GamePosition tile, Vector4 overlayColor, MapTile mapTile)
    {
        Batches.AddFragment(OverlayMapMeshFragment.Create(Engine, this, tile, ZIndex, overlayColor, mapTile));
    }

    public void AddFragment(OverlayMapMeshFragment fragment)
    {
        Batches.AddFragment(fragment);
    }

    public override bool OnUpdateBuffer(List<GameMeshBatch> drawableBatches)
    {
        return base.OnUpdateBuffer(drawableBatches);
    }
}