﻿using System.Numerics;
using AncientEmpires.Engine.Map.Picker;

namespace AncientEmpires.Engine.Render.Map;

public class MapFragment : MapFragmentBase
{
    public MapFragment(Vector4 position) : base(position)
    {
        Key = OnRegisterFragment();
    }
    
    public static MapFragment Create(GameEngine engine, MapLayerBase parent, float zIndex, Vector4 offset, MapTile mapTile)
    {
        return new MapFragment(mapTile.Coordinates)
        {
            ZIndex = zIndex,
            Tile = mapTile,
            Sprite = new Sprite(mapTile.Sprite.Offset, engine.Configuration.TilePixelSize, TextureIndexes)
        };
    }
}