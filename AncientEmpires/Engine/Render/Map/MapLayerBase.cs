using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Groups;
using AncientEmpires.Engine.Render.Shaders;

namespace AncientEmpires.Engine.Render.Map;

public abstract class MapLayerBase : GameBatchLayer
{
    public MapLayerBase()
    {
        BatchStrategy = new TiledMeshBatchStrategy(Engine, true);
    }

    public override void OnInitialize()
    {
        base.OnInitialize();

        RenderMode = BatchRenderMode.Static;
    }

    public override void OnCreateShaders()
    {
        Shader = AddComponent<TerrainShader>();
        WireframeShader = AddComponent(new WireframeShader("terrain"));
    }

    public void AddTile(MapFragmentBase fragment)
    {
        Batches.AddFragment(fragment);
    }
}