using AncientEmpires.Engine.Render.Components;

namespace AncientEmpires.Engine.Render.Map;

public class MapLayer : MapLayerBase
{
    public Random Random = new(Environment.TickCount);

    public override void OnInitialize()
    {
        base.OnInitialize();

        ZIndex = Engine.Configuration.GetRenderLayer(KnownRenderLayers.Map);
    }

    public override void OnEngineReady()
    {
        base.OnEngineReady();
    }

    protected override void PrepareBatches()
    {
        base.PrepareBatches();
    }

    public override void OnBeforeDraw()
    {
        base.OnBeforeDraw();
    }

    public override void OnDraw()
    {
        base.OnDraw();
    }

    public void Clear()
    {
        Batches.Clear();
    }
}