using System.Numerics;
using AncientEmpires.Engine.Entities.Units;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Map.Picker;
using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Entities;
using AncientEmpires.Engine.Render.Groups;
using AncientEmpires.Engine.Render.Quads;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Render.Map;

public class OverlayMapMeshFragment : MapFragmentBase
{
    public EntityMeshFragment EntityFragment;
    public Vector4 OverlayColor;
    public GamePosition PreviousPosition;

    public OverlayMapMeshFragment() : base(Vector4.Zero)
    {
        Key = OnRegisterFragment();
    }

    protected override MeshKey OnRegisterFragment()
    {
        return FragmentManager.Instance.Register(this);
    }

    public override void OnUpdateFragment(float timeDelta)
    {
        if (EntityFragment == null)
        {
            base.OnUpdateFragment(timeDelta);
        }
        else
        {
            EntityFragment.OnUpdateFragment(timeDelta);

            var unit = (Unit)EntityFragment.Entity;
            var texelSize = Engine.Configuration.TexelSize;
            var quads = EntityFragment.Entity.Fragment.Quads.Select(o => new RawQuad(o)).ToList();
            var worldPosition = Map.Value.PositionStrategy.Project(Position.MapSpace);
            var renderLayer = Engine.Configuration.GetRenderLayer(KnownRenderLayers.MapOverlay);

            foreach (var quad in quads)
            {
                var direction = unit.Movement.CalculateDirection(PreviousPosition, Position);
                var sprite = unit.SpriteManager.CurrentCycle.Directions[direction].First();
                var spriteOffset = texelSize * sprite.Offset;

                for (var index = 0; index < quad.Length; index++)
                {
                    ref var vertex = ref quad[index];
                    vertex.Position += worldPosition + new Vector4(0, 0, 1, 0);
                    vertex.Material = new Int4((int)ShaderMaterial.Entity, vertex.Material.Y, vertex.Material.Z, vertex.Material.W);
                    vertex.UV0 += spriteOffset;
                }
            }

            Quads.Clear();
            Quads.AddRange(quads);
        }

        foreach (var quad in Quads)
        {
            for (var i = 0; i < 4; i++)
            {
                ref var vertex = ref quad[i];
                vertex.Color = OverlayColor;
                vertex.OverlayColor = OverlayColor;
            }
        }
    }

    public static OverlayMapMeshFragment Create(
        GameEngine engine,
        MapLayerBase parent,
        GamePosition position,
        float zIndex,
        Vector4 overlayColor,
        MapTile mapTile)
    {
        return new OverlayMapMeshFragment
        {
            Position = position,
            ZIndex = zIndex,
            Tile = mapTile,
            Sprite = new Sprite(mapTile.Sprite.Offset, engine.GetComponent<GameMap>().TileSize, TextureIndexes),
            OverlayColor = overlayColor,
        };
    }

    public static OverlayMapMeshFragment Create(EntityMeshFragment unitFragment, GamePosition position, GamePosition previousNode)
    {
        return new OverlayMapMeshFragment
        {
            EntityFragment = unitFragment,
            Position = position,
            PreviousPosition = previousNode
        };
    }
}