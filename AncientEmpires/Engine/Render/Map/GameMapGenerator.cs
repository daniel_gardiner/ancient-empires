using System.Numerics;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Map.Picker;
using AncientEmpires.Engine.Render.Components;
using DotnetNoise;

namespace AncientEmpires.Engine.Render.Map;

public class GameMapGenerator : EngineComponent
{
    public RectangleF MapWorldBounds;
    private static FastNoise _fastNoise;

    protected float ZIndex;
    protected ComponentLookup<MapLayer> MapLayer;
    protected ComponentLookup<GameMap> Map;

    public override void OnInitialize()
    {
        base.OnInitialize();

        MapLayer = new ComponentLookup<MapLayer>();
        Map = new ComponentLookup<GameMap>();
    }

    public MapTile[,] GenerateMap(MapTile[,] mapTiles)
    {
        var tileSize = Map.Value.TileSize;
        var mapSize = Map.Value.SizeInt;
        var textureTilePadding = new Vector4(30, 30, 0, 0);

        ZIndex = Engine.Configuration.GetRenderLayer(KnownRenderLayers.Map);

        for (var x = 0; x < mapSize.X; x++)
        {
            for (var y = 0; y < mapSize.Y; y++)
            {
                var terrainSprite = GetTextureTile(x, y);
                var pixelOffset = terrainSprite * textureTilePadding + (textureTilePadding / 2);
                var mapTile = mapTiles[x, y];
                mapTile.Sprite = new Sprite(terrainSprite * tileSize + pixelOffset, tileSize);
                MapLayer.Value.AddTile(MapFragment.Create(Engine, MapLayer.Value, ZIndex, Vector4.Zero, mapTile));
            }
        }

        var corner1 = Map.Value.PositionStrategy.Project(new Vector4(0, 0, 0, 0));
        var corner2 = Map.Value.PositionStrategy.Project(new Vector4(0, Map.Value.SizeInt.Y, 0, 0));
        var corner3 = Map.Value.PositionStrategy.Project(new Vector4(Map.Value.SizeInt.X, 0, 0, 0));
        var corner4 = Map.Value.PositionStrategy.Project(new Vector4(Map.Value.SizeInt.X, Map.Value.SizeInt.Y, 0, 0));
        var left = Math.Min(Math.Min(Math.Min(corner1.X, corner2.X), corner3.X), corner4.X);
        var top = Math.Min(Math.Min(Math.Min(corner1.Y, corner2.Y), corner3.Y), corner4.Y);
        var right = Math.Max(Math.Max(Math.Max(corner1.X, corner2.X), corner3.X), corner4.X);
        var bottom = Math.Max(Math.Max(Math.Max(corner1.Y, corner2.Y), corner3.Y), corner4.Y);

        MapWorldBounds = new RectangleF(
            left - (right - left) / 2,
            -(top - (bottom - top) / 2),
            right - left,
            bottom - top
        );

        return mapTiles;
    }

    private Vector4 GetTextureTile(int x, int y)
    {
        var spriteX = Engine.Random.Next(0, 4);
        var spriteY = Engine.Random.Next(0, 4);
        var textureTile = new Vector4(spriteX, spriteY, 0, 0);

        if (x == 0 || y == 0)
        {
            return new Vector4(2, 0, 0, 0);
        }

        return (y % 20, x % 20) switch
        {
            (0, _) => new Vector4(2, 0, 0, 0),
            (_, 0) => new Vector4(2, 1, 0, 0),
            _ => textureTile
        };
    }

    private static Func<float, float, float> PrepareSeed(int seed)
    {
        _fastNoise = new FastNoise(seed)
        {
            Frequency = 0.003f
        };
        return (float x, float y) => _fastNoise.GetCellular(x, y);
    }
}