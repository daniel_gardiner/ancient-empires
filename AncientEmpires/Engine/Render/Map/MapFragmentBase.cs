﻿using System.Numerics;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Map.Picker;
using AncientEmpires.Engine.Render.Entities;
using AncientEmpires.Engine.Render.Groups;
using AncientEmpires.Engine.Render.Quads;

namespace AncientEmpires.Engine.Render.Map;

public abstract  class MapFragmentBase : MeshFragment
{
    protected ComponentLookup<GameMap> Map;
    protected static List<int> TextureIndexes;
    public MapTile Tile;
    public Texture[] Textures;
    public Sprite Sprite;
    protected Vector4 TileSize;
    protected readonly Vector4 TilePadding;
    protected DepthCalculator DepthCalculator;
    protected GamePosition WorldPosition;

    public MapFragmentBase(Vector4 position)
    {
        Map = new ComponentLookup<GameMap>();
        DepthCalculator = new DepthCalculator(Map.Value);
        Position = position;
        TextureIndexes ??= Engine.GetComponent<GameTextures>().FindTextureIndexes("Terrain.dds", "TerrainTileMask.dds");
        TileSize = Engine.Configuration.TilePixelSize;
        TilePadding = Engine.Configuration.TilePadding;
        ZIndex = Engine.Configuration.GetRenderLayer(KnownRenderLayers.Map);
    }

    protected override MeshKey OnRegisterFragment()
    {
        return FragmentManager.Instance.Register(this, new MeshKey(KeyType.Map, Position));
    }
    
    public override void OnUpdateFragment(float timeDelta)
    {
        WorldPosition = Position.WorldSpace.AtDepth(Depth);
        Tile.Quad = new TexturedQuad(in Position, in WorldPosition, in TileSize, in Sprite, in TilePadding, ShaderMaterial);

        Quads.Clear();
        Quads.Add(Tile.Quad);
    }

    public override void OnUpdateDepth()
    {
        base.OnUpdateDepth();
        
        WorldPosition = Position.WorldSpace.AtDepth(Depth);
        var depth = DepthCalculator.CalculateDepth(Position);
        Depth = ZIndex - (1 - depth);
        SortingDepth = (1 - depth);
    }
}