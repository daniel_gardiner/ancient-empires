﻿namespace AncientEmpires.Engine.Render;

public enum ConstantBufferSlot
{
    World = 0,
    Texture = 1,
    Selection = 2,
    DragSelection = 3,
    Unit = 4
}