using System.Numerics;
using System.Runtime.InteropServices;

namespace AncientEmpires.Engine.Render.Helpers;

[StructLayout(LayoutKind.Sequential, Size = 16)]
public struct WorldConstant
{
    public Matrix4x4 WorldViewProjectProjection;
    public Matrix4x4 World;
    public Matrix4x4 View;
    public Matrix4x4 Projection;
    public Vector4 Timing;
    public Vector4 MapSize;
    public Vector4 Debug;
    public Vector4 TileSize;
    public Vector4 Viewport;
}