using System.Runtime.InteropServices;
using AncientEmpires.Engine.Render.Components;
using Vortice;
using Vortice.Direct3D11;

namespace AncientEmpires.Engine.Render.Helpers;

public abstract class ConstantBuffer : EngineComponent
{
    public ID3D11Buffer Buffer;
    protected ComponentLookup<GraphicsDevice> GraphicsDevice;
    protected ComponentLookup<D3DFactory> D3DFactory;

    public abstract void MapAndWrite();
}

public class ConstantBuffer<T> : ConstantBuffer where T : struct
{
    protected ConstantBufferSlot ConstantBufferSlot;
    public T Value;

    public ConstantBuffer(ConstantBufferSlot constantBufferSlot)
    {
        ConstantBufferSlot = constantBufferSlot;
    }
    
    public override void OnEngineReady()
    {
        base.OnEngineReady();

        GraphicsDevice = Engine.ComponentLookup<GraphicsDevice>();
        D3DFactory = Engine.ComponentLookup<D3DFactory>();
        Buffer = D3DFactory.Value.CreateConstantBuffer<T>(Name).Collect();
    }

    public void UpdateValue(in T value)
    {
        Value = value;
        GraphicsDevice.Value.SetConstantBuffer(ConstantBufferSlot, this);
    }

    public override void MapAndWrite()
    {
        var mappedSubresource = GraphicsDevice.Value.D3DContext.Map(Buffer, MapMode.WriteDiscard);
        var dataStream = new DataStream(mappedSubresource.DataPointer, Marshal.SizeOf<T>(), false, true);
        Marshal.StructureToPtr(Value, dataStream.BasePointer, false);
        GraphicsDevice.Value.D3DContext.Unmap(Buffer);
    }

    public override void Dispose()
    {
        base.Dispose();
        
        Buffer?.Dispose();
        Buffer = null;
    }
}