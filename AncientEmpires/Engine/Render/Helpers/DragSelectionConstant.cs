using System.Numerics;
using System.Runtime.InteropServices;

namespace AncientEmpires.Engine.Render.Helpers;

[StructLayout(LayoutKind.Sequential, Pack = 4)]
public struct DragSelectionConstant
{
    public Vector4 Active;
    public Vector4 Box;
}