using System.Numerics;
using System.Runtime.InteropServices;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Render.Helpers;

[StructLayout(LayoutKind.Sequential)]
public struct TextureConstant
{
    public Vector4 PixelSize;

    public Vector4 TexelSize;

    public Int4 MapTileMaskTextureIndex;
}