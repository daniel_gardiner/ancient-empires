using System.Numerics;
using System.Runtime.InteropServices;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Render.Helpers;

[StructLayout(LayoutKind.Sequential)]
public struct SelectionConstant
{
    public Vector4 Cursor;
    public Vector4 ViewportSize;

    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
    public Vector4[] HoverTiles;

    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
    public Vector4[] SelectedTiles;

    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)]
    public Int4[] SelectedEntities;
}