using AncientEmpires.Engine.Render.Components;
using Vortice.Direct3D11;
using Vortice.Mathematics;
using static Vortice.Direct3D11.Filter;
using static Vortice.Direct3D11.TextureAddressMode;

namespace AncientEmpires.Engine.Render.Helpers;

public class SamplerStateCollection
{
    /// <summary>
    /// Point filtering with texture coordinate wrapping.
    /// </summary>
    public readonly ID3D11SamplerState PointWrap;

    /// <summary>
    /// Point filtering with texture coordinate clamping.
    /// </summary>
    public readonly ID3D11SamplerState PointClamp;

    /// <summary>
    /// Point filtering with texture coordinate mirroring.
    /// </summary>
    public readonly ID3D11SamplerState PointMirror;

    /// <summary>
    /// Linear filtering with texture coordinate wrapping.
    /// </summary>
    public readonly ID3D11SamplerState LinearWrap;

    /// <summary>
    /// Linear filtering with texture coordinate clamping.
    /// </summary>
    public readonly ID3D11SamplerState LinearClamp;

    /// <summary>
    /// Linear filtering with texture coordinate mirroring.
    /// </summary>
    public readonly ID3D11SamplerState LinearMirror;

    /// <summary>
    /// Anisotropic filtering with texture coordinate wrapping.
    /// </summary>
    public readonly ID3D11SamplerState AnisotropicWrap;

    /// <summary>
    /// Anisotropic filtering with texture coordinate clamping.
    /// </summary>
    public readonly ID3D11SamplerState AnisotropicClamp;
    
    /// Initializes a new instance of the <see cref="SamplerStateCollection" /> class.
    /// </summary>
    /// <param name="device">The device.</param>
    public SamplerStateCollection(ID3D11Device1 device)
    {
        if (device == null) return;
        
        PointClamp = device.CreateSamplerState(new SamplerDescription { Filter = MinMagMipPoint, AddressU = Clamp, AddressV = Clamp, AddressW = Clamp, MipLODBias = -2, MinLOD = 0, MaxLOD = 2, BorderColor = Color4.Aqua }).Collect();
        AnisotropicClamp = device.CreateSamplerState(new SamplerDescription { Filter = Anisotropic, AddressU = Clamp, AddressV = Clamp, AddressW = Clamp, MipLODBias = -2, MinLOD = 0, MaxLOD = 2, MaxAnisotropy = 16 }).Collect();
    }
}