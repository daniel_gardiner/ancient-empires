using System.Numerics;
using System.Runtime.InteropServices;

namespace AncientEmpires.Engine.Render.Helpers;

[StructLayout(LayoutKind.Sequential, Size = 4)]
public readonly struct UnitConstant
{
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1024)]
    public readonly Vector4[] Position;

    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1024)]
    public readonly Vector4[] NextPosition;

    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1024)]
    public readonly Vector4[] UV;

    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1024)]
    public readonly Vector4[] Details;

    public UnitConstant()
    {
        Position = new Vector4[1024];
        NextPosition = new Vector4[1024];
        UV = new Vector4[1024];
        Details = new Vector4[1024];
    }
}