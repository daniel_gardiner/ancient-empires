using AncientEmpires.Engine.Render.Components;
using Vortice.Direct3D11;

namespace AncientEmpires.Engine.Render;

public class BackBufferRenderTarget : RenderTarget
{
    public override void OnEngineReady()
    {
        base.OnEngineReady();

        if (RenderTargetTexture != null)
            return;

        try
        {
            RenderTargetTexture = GetBackBufferTexture();
            RenderTargetView = CreateRenderTargetView(RenderTargetTexture.Description.Format);
            DepthStencilBuffer = CreateDepthBuffer();
            DepthStencilView = CreateDepthStencilView();
            ShaderResourceView = null;

            RenderTargetTexture.DebugName = $"BackBufferTexture";
            RenderTargetView.DebugName = $"BackBufferRTV";
            DepthStencilBuffer.DebugName = $"BackBufferDSB";
            DepthStencilView.DebugName = $"BackBufferDSV";

            BufferSize = new Size(RenderTargetTexture.Description.Width, RenderTargetTexture.Description.Height);
            ConsoleWriter.WriteLine($"{Path}#{ID} - BufferSize: {BufferSize}");
        }
        catch (Exception ex)
        {
            ConsoleWriter.WriteLine($"{ex.GetType().Name} - {ex.Message}");
        }
    }


    private ID3D11Texture2D1 GetBackBufferTexture() => GraphicsPresentation.Value.SwapChain.GetBuffer<ID3D11Texture2D1>(0);
}