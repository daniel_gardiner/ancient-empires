using AncientEmpires.Engine.Render.Components;
using DirectXTexNet;
using Vortice.Direct3D11;

namespace AncientEmpires.Engine.Render;

public class Texture : EngineComponent
{
    private int _width;
    private int _height;
    protected ID3D11ShaderResourceView1 _shaderResourceView;
    protected Func<ID3D11ShaderResourceView1> _textureResourceLoader;
    public string FilePath;
    public string FileName;

    public Texture(GameEngine engine, EngineComponent parent, string filePath)
    {
        FilePath = filePath;
        FileName = System.IO.Path.GetFileName(filePath);
    }

    public override void OnInitialize()
    {
        base.OnInitialize();
        
        _textureResourceLoader = () => LoadShaderResourceViewFromFile(FilePath);
    }

    public override void OnReleaseResources()
    {
        base.OnReleaseResources();

        Dispose();
    }

    public SizeF Size
    {
        get
        {
            _shaderResourceView ??= _textureResourceLoader();
            return new(_width, _height);
        }
    }

    public ID3D11ShaderResourceView1 ShaderResourceView => _shaderResourceView ??= _textureResourceLoader();

    private ID3D11ShaderResourceView1 LoadShaderResourceViewFromFile(string filePath)
    {
        using var ddsFile = TexHelper.Instance.LoadFromDDSFile(filePath, DDS_FLAGS.NONE);
        var metadata = ddsFile.GetMetadata();
        var shaderResourceViewPtr = ddsFile.CreateShaderResourceViewEx(Engine.GetComponent<GraphicsDevice>().D3DDevice.NativePointer, D3D11_USAGE.DEFAULT, D3D11_BIND_FLAG.SHADER_RESOURCE, 0, 0, false);
        var shaderResourceView = new ID3D11ShaderResourceView1(shaderResourceViewPtr).Collect();
        _width = metadata.Width;
        _height = metadata.Height;
        shaderResourceView.DebugName = System.IO.Path.GetFileName(filePath);
        return shaderResourceView;
    }

    public override void Dispose()
    {
        base.Dispose();
        
        GameManager.DisposeCollector.RemoveAndDispose(ref _shaderResourceView);
    }
}