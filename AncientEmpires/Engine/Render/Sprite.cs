using System.Numerics;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Render;

public class Sprite
{
    public Vector4 Offset;
    public Vector4 Size;
    public Vector4 Color;
    public int FrameDuration;
    public List<int> TextureIndexes;

    public Sprite(RectangleF bounds, List<int> textureIndexes)
        : this(
            new Vector4(bounds.Left, bounds.Top, 0, 0),
            new Vector4(bounds.Right - bounds.Left, bounds.Bottom - bounds.Top, 0, 0),
            new Vector4(1, 1, 1, 1),
            textureIndexes
        )
    {
    }

    public Sprite(Vector4 offset, Vector4 size)
        : this(offset, size, Vector4.Zero, new List<int> { 0 })
    {
    }

    public Sprite(Vector4 offset, Vector4 size, Color4 color)
        : this(offset, size, Vector4.Zero, new List<int> { 0 })
    {
    }

    public Sprite(float offsetX, float offsetY, float width, float height, List<int> textureIndexes)
        : this(new Vector4(offsetX, offsetY, 0, 0), new Vector4(width, height, 0, 0), Vector4.Zero, textureIndexes)
    {
    }

    public Sprite(Vector4 offset, Vector4 size, List<int> textureIndexes)
        : this(offset, size, Vector4.Zero, textureIndexes)
    {
    }

    public Sprite(Vector4 offset, Vector4 size, Vector4 color, List<int> textureIndexes)
    {
        TextureIndexes = textureIndexes;
        Size = size;
        Color = color;
        Offset = offset;
    }

    public static Sprite Empty { get; set; } = new(Vector4.Zero, Vector4.Zero);

    public static Sprite Create(Vector4 offset, Vector4 spriteSize, int textureIndex)
    {
        return new Sprite(
            offset, 
            spriteSize,
            new Vector4(0, 1, 0.5f, 0.5f),
            new List<int> { textureIndex });
    }
}