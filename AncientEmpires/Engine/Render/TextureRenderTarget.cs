using AncientEmpires.Engine.Render.Components;
using Vortice.Direct3D11;
using Vortice.DXGI;

namespace AncientEmpires.Engine.Render;

public class TextureRenderTarget : RenderTarget
{
    public override void OnEngineReady()
    {
        base.OnEngineReady();

        try
        {
            const Format format = Format.R32G32B32A32_Float;

            RenderTargetTexture = CreateTexture(format);
            RenderTargetView = CreateRenderTargetView(format);
            ShaderResourceView = CreateShaderResourceView(format);
            DepthStencilBuffer = CreateDepthStencilBuffer();
            DepthStencilView = CreateDepthStencilView();

            RenderTargetTexture.DebugName = $"{DebugName}Texture";
            RenderTargetView.DebugName = $"{DebugName}RTV";
            DepthStencilBuffer.DebugName = $"{DebugName}DSB";
            DepthStencilView.DebugName = $"{DebugName}DSV";
            ShaderResourceView.DebugName = $"{DebugName}SRV";

            BufferSize = new Size(RenderTargetTexture.Description.Width, RenderTargetTexture.Description.Height);
            ConsoleWriter.WriteLine($"{Path} - BufferSize: {BufferSize}");
        }
        catch (Exception ex)
        {
            ConsoleWriter.WriteLine($"{ex.GetType().Name} - {ex.Message}");
        }
    }

    public override void OnReleaseResources()
    {
        base.OnReleaseResources();

        Dispose();
    }
    
    public RenderTarget SetDebugName(string name)
    {
        DebugName = name;
        return this;
    }

    private ID3D11Texture2D CreateTexture(Format format)
    {
        return GraphicsDevice.Value.D3DDevice.CreateTexture2D(new Texture2DDescription
        {
            Width = Size.Width,
            Height = Size.Height,
            MipLevels = 1,
            ArraySize = 1,
            Format = format,
            SampleDescription = new SampleDescription(1, 0),
            Usage = ResourceUsage.Default,
            BindFlags = BindFlags.RenderTarget | BindFlags.ShaderResource,
            CpuAccessFlags = CpuAccessFlags.None,
            OptionFlags = ResourceOptionFlags.None
        });
    }

    protected override ID3D11DepthStencilView CreateDepthStencilView()
    {
        return GraphicsDevice.Value.D3DDevice.CreateDepthStencilView(DepthStencilBuffer, new DepthStencilViewDescription()
        {
            Format = Format.D24_UNorm_S8_UInt,
            ViewDimension = DepthStencilViewDimension.Texture2D,
            Texture2D = { MipSlice = 0 }
        });
    }

    public override void SetRenderTarget(ID3D11DeviceContext1 context)
    {
        base.SetRenderTarget(context);
        context.OMSetDepthStencilState(Engine.GetComponent<GraphicsPresentation>().RenderBuffers.DepthStencilState, 1);
    }

    public override void Dispose()
    {
        base.Dispose();
    }
}