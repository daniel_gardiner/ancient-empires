using System.Collections.Concurrent;
using System.IO;
using AncientEmpires.Engine.Render.Components;
using MoreLinq;
using Vortice.Direct3D11;

namespace AncientEmpires.Engine.Render;

public class GameTextures : EngineComponent
{
    protected static Texture[] AllTexturesSorted;
    protected static List<(Texture texture, int index)> AllTexturesIndexLookup;
    protected static ID3D11ShaderResourceView1[] TextureResources;
    protected static ConcurrentDictionary<string, Texture> TextureCache = new();
    protected static readonly Dictionary<TextureType, Lazy<Texture[]>> TexturesLazy = new();
    protected static readonly Dictionary<TextureType, Lazy<List<int>>> TextureIndexesLazy = new();

    public override void OnInitialize()
    {
        base.OnInitialize();
    }

    public override void OnEngineReady()
    {
        base.OnEngineReady();

        InitializeTextures();
    }

    public override void OnReleaseResources()
    {
        base.OnReleaseResources();

        Dispose();
    }

    protected virtual void InitializeTextures()
    {
        var searchPath = $"{Application.StartupPath}\\Resources\\Textures\\DDS\\";
        var textureFiles = Directory.GetFiles(searchPath, "*.dds");
        LoadTextures(textureFiles);
    }

    public List<int> FindTextureIndexes(params Texture[] textures)
    {
        var textureIndexLookup = GetAllTextureIndexLookup();
        return textures
            .Select(o => textureIndexLookup.First(tli => tli.texture.FileName == o.FileName).index)
            .ToList();
    }

    public List<int> FindTextureIndexes(params string[] textures)
    {
        var textureIndexLookup = GetAllTextureIndexLookup();
        return textures
            .Select(file => textureIndexLookup.First(tli => tli.texture.FileName == file).index)
            .ToList();
    }

    public virtual Texture[] GetTextures(params string[] fileNames)
    {
        return fileNames
            .Select(fileName => TextureCache.TryGetValue(fileName, out var texture)
                ? texture
                : throw new Exception($"Texture {fileName} not found"))
            .ToArray();
    }

    public virtual Texture[] GetAllTextures()
    {
        if (!IsInitialized)
        {
            InitializeTextures();
            IsInitialized = true;
        }

        return AllTexturesSorted ??= TextureCache.Values
            .OrderBy(o => o.FileName)
            .ToArray();
    }

    private List<(Texture texture, int index)> GetAllTextureIndexLookup()
    {
        return AllTexturesIndexLookup ??= GetAllTextures()
            .Select((texture, index) => (texture, index))
            .ToList();
    }

    public virtual ID3D11ShaderResourceView1[] GetAllTextureResources()
    {
        TextureResources ??= GetAllTextures().Select(o => o.ShaderResourceView).ToArray();
        return TextureResources;
    }

    protected virtual void LoadTextures(params string[] textureFiles)
    {
        textureFiles.ForEach(file =>
        {
            var fileName = System.IO.Path.GetFileName(file);
            TextureCache.GetOrAdd(fileName, _ => RegisterComponent(new Texture(Engine, this, file)));
        });

        var fontManager = Engine.GetComponent<FontManager>();
        foreach (var font in fontManager.Fonts.Values)
        {
            var fileName = font.Texture.FileName;
            TextureCache.AddOrUpdate(fileName, _ => font.Texture, (_, texture) => texture);
        }
    }

    public override void Dispose()
    {
        base.Dispose();
        
        TextureResources?.ForEach(o => o.Dispose());
        AllTexturesSorted?.ForEach(o => o.Dispose());
        TextureResources = null;
        AllTexturesSorted = null;
        TextureResources = null;
    }
}