﻿using System.Numerics;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Render.Groups;
using AncientEmpires.Engine.Render.Quads;

namespace AncientEmpires.Engine.Render;

public class RenderableMeshFragment : MeshFragment
{
    public int TextureIndex;
    public Vector2 ScreenSize => Engine.ScreenSize;
    public GameRenderable Renderable { get; }

    public RenderableMeshFragment(GameRenderable renderable)
    {
        Renderable = renderable;
        ZIndex = renderable.ZIndex;
        Key = OnRegisterFragment();
    }

    protected override MeshKey OnRegisterFragment()
    {
        return FragmentManager.Instance.Register(this, KeyType.Interface);
    }

    public override void OnUpdateDepth()
    {
        base.OnUpdateDepth();
        
        Depth = ZIndex;
        SortingDepth = Depth;
    }

    public override void OnUpdateFragment(float timeDelta)
    {
        UpdateQuads(new DeferredTextureQuad(
            new Vector4(0, 0, ZIndex, 0),
            new Vector4(ScreenSize, ZIndex, 0),
            Sprite.Create(Vector4.Zero, new Vector4(ScreenSize, 0, 0), TextureIndex),
            ShaderMaterial.Texture));
    }
}