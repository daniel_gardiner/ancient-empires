using AncientEmpires.Engine.Render.Components;

namespace AncientEmpires.Engine.Render;

public class LargeGameGeometryBuffer : GameGeometryBuffer
{
    public override int GpuQuadCapacity => 400_000;
}