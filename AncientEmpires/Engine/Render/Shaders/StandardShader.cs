using AncientEmpires.Engine.Render.Components;

namespace AncientEmpires.Engine.Render.Shaders;

public class StandardShader : GameShader
{
    public override void OnEngineReady()
    {
        base.OnEngineReady();

        InitializeShader(Engine.GetComponent<GraphicsDevice>().D3DDevice, "standard", "standard");
        Blend = BlendAlpha;
    }
}