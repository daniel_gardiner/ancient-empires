using AncientEmpires.Engine.Render.Components;

namespace AncientEmpires.Engine.Render.Shaders;

public class TerrainShader : GameShader
{
    public override void OnInitialize()
    {
        base.OnInitialize();
    }

    public override void OnEngineReady()
    {
        base.OnEngineReady();

        InitializeShader(Engine.GetComponent<GraphicsDevice>().D3DDevice, "terrain", "terrain");
        Blend = BlendStatePreMultiplied;
    }
}