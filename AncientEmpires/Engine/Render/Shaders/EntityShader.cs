using System.Numerics;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Helpers;

namespace AncientEmpires.Engine.Render.Shaders;

public class EntityShader : GameShader
{
    public static ConstantBuffer<UnitConstant> UnitConstantBuffer;
    public static UnitConstant UnitConstant = new();   
    protected ComponentLookup<GameMap> Map;
    protected ComponentLookup<EntityManager> EntityManager;
    protected ComponentLookup<GraphicsDevice> GraphicsDevice;

    public override void OnInitialize()
    {
        base.OnInitialize();
        
        GraphicsDevice = new ComponentLookup<GraphicsDevice>();
        EntityManager = new ComponentLookup<EntityManager>();
        Map = new ComponentLookup<GameMap>();
    }

    public override void OnEngineReady()
    {
        base.OnEngineReady();
        
        UnitConstantBuffer = AddComponent(new ConstantBuffer<UnitConstant>(ConstantBufferSlot.Unit));
        InitializeShader(GraphicsDevice.Value.D3DDevice, "entity", "entity");
        Blend = BlendAlpha;
    }

    public override void OnReleaseResources()
    {
        base.OnReleaseResources();
        Dispose();
    }

    protected override bool OnUpdateConstantBuffers(GameRenderable renderable)
    {
        base.OnUpdateConstantBuffers(renderable);

        //if (!Engine.Timing.IsNewFrame(this, out var delta))
        //   return false;

        var texelSize = Engine.Configuration.TexelSize;
        var units = EntityManager.Value.Units
            .Where(o => o.Fragment != null)
            .OrderBy(o => o.Id)
            .ToArray();

        var positionStrategy = Map.Value.PositionStrategy;
        for (var index = 0; index < units.Length; index++)
        {
            var unit = units[index];

            var worldPosition = unit.Position.WorldSpace;
            var nextWorldLocation = unit.Movement.NextPosition.WorldSpace;
            //var worldPosition = positionStrategy.ProjectUnit(unit.Position, unit.Fragment.Depth);
            //var nextWorldLocation = positionStrategy.ProjectUnit(unit.Movement.NextPosition, unit.Fragment.Depth);
            var isSelected = EntityManager.Value.SelectedUnits.Contains(unit)
                ? 1
                : 0;
            UnitConstant.Position[index] = worldPosition;
            UnitConstant.NextPosition[index] = nextWorldLocation;
            UnitConstant.UV[index] = texelSize * unit.GetSprite().Offset;
            UnitConstant.Details[index] = new Vector4(unit.Id, isSelected, unit.Speed, 0);
        }

        UnitConstantBuffer.UpdateValue(in UnitConstant);
        return true;
    }
    
    public override void Dispose()
    {
        base.Dispose();
        
        UnitConstantBuffer?.Dispose();
        UnitConstantBuffer = null;
    }
}