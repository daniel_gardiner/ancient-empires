namespace AncientEmpires.Engine.Render.Shaders;

public class WireframeShader : GameShader
{
    private readonly string _vertexShader = "standard";

    public WireframeShader()
    {
    }

    public WireframeShader(string vertexShader)
    {
        _vertexShader = vertexShader;
    }
    
    public override void OnEngineReady()
    {
        base.OnEngineReady();
        
        InitializeShader(Engine.GetComponent<GraphicsDevice>().D3DDevice, "wireframe", _vertexShader);
    }
}