using Vortice.Direct3D11;

namespace AncientEmpires.Engine.Render.Shaders;

public class GameVertexShader
{
    public ID3D11VertexShader VertexShader { get; }
    public ID3D11InputLayout InputLayout { get; }

    public GameVertexShader(ID3D11VertexShader vertexShader, ID3D11InputLayout inputLayout)
    {
        VertexShader = vertexShader;
        InputLayout = inputLayout;
    }
}