namespace AncientEmpires.Engine.Render.Shaders;

public class EntityWireframeShader : GameShader
{
    private readonly string _vertexShader = "entity";

    public EntityWireframeShader(string vertexShader)
    {
        _vertexShader = vertexShader;
    }

    public override void OnEngineReady()
    {
        base.OnEngineReady();
        
        InitializeShader(Engine.GetComponent<GraphicsDevice>().D3DDevice, "wireframe", _vertexShader);
    }
}