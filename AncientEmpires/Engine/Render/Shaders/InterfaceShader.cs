using System.Numerics;
using AncientEmpires.Engine.Actions;
using AncientEmpires.Engine.Map.Rendering;
using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Helpers;

namespace AncientEmpires.Engine.Render.Shaders;

public class InterfaceShader : GameShader
{
    private ComponentLookup<GraphicsDevice> GraphicsDevice;
    private ComponentLookup<IsometricPositionStrategy> PositionStrategy;
    public ConstantBuffer<DragSelectionConstant> DragSelectionBuffer { get; set; }

    public override void OnInitialize()
    {
        base.OnInitialize();

        GraphicsDevice = Engine.ComponentLookup<GraphicsDevice>();
        PositionStrategy = Engine.ComponentLookup<IsometricPositionStrategy>();
    }

    public override void OnEngineReady()
    {
        base.OnEngineReady();

        DragSelectionBuffer = AddComponent(new ConstantBuffer<DragSelectionConstant>(ConstantBufferSlot.DragSelection));
        Blend = BlendStatePreMultiplied;
        InitializeShader(GraphicsDevice.Value.D3DDevice, "interface", "standard");
    }

    public override void OnReleaseResources()
    {
        base.OnReleaseResources();

        Dispose();
    }

    protected override bool OnUpdateConstantBuffers(GameRenderable renderable)
    {
        base.OnUpdateConstantBuffers(renderable);

        if (!Engine.Actions.TryGetAction<DragSelectEntities>(out var action))
            return false;

        if (!action.IsActive)
        {
            DragSelectionBuffer.UpdateValue(new DragSelectionConstant
            {
                Active = new Vector4(0, 0, 0, 0)
            });
            return true;
        }

        var actionOrigin = Engine.Camera.WorldSpaceToScreenSpace(in action.Origin.WorldSpace);
        var actionCurrent = Engine.Camera.WorldSpaceToScreenSpace(in action.Current.WorldSpace);

        var active = action.IsActive && action.IsVisible
            ? 1
            : 0;

        var constant = new DragSelectionConstant
        {
            Active = new Vector4(active, 0, 0, 0),
            Box = new Vector4(actionOrigin.X, actionOrigin.Y, actionCurrent.X, actionCurrent.Y),
        };
        DragSelectionBuffer.UpdateValue(in constant);
        return true;
    }

    public override void Dispose()
    {
        base.Dispose();

        DragSelectionBuffer?.Dispose();
        DragSelectionBuffer = null;
    }
}