namespace AncientEmpires.Engine.Render.Shaders;

public class TextShader : GameShader
{
    public Font Font;

    public TextShader(Font font)
    {
        Font = font;
    }

    public override void OnEngineReady()
    {
        base.OnEngineReady();
        
        InitializeShader(Engine.GetComponent<GraphicsDevice>().D3DDevice, "font", "standard");
    }
}