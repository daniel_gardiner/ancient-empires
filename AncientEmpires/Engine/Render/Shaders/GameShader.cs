using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Helpers;
using AncientEmpires.Engine.Render.Interface;
using AncientEmpires.Engine.Render.Renderers;
using SharpDX.D3DCompiler;
using Vortice.Direct3D11;
using Vortice.DXGI;
using static Vortice.Direct3D11.Blend;
using static Vortice.Direct3D11.BlendOperation;

namespace AncientEmpires.Engine.Render.Shaders;

public abstract class GameShader : EngineComponent
{
    public static Dictionary<string, GameVertexShader> VertexShaderCache = new();
    public static Dictionary<string, ID3D11PixelShader> PixelShaderCache = new();

    public GameDebug GameDebug;
    public SamplerStateCollection SamplerCollection;
    public ID3D11VertexShader VertexShader;
    public ID3D11PixelShader PixelShader;
    public ID3D11SamplerState SampleState;
    public ID3D11BlendState1 BlendTemp;
    public ID3D11BlendState1 BlendAlpha;
    public ID3D11BlendState1 BlendStatePreMultiplied;
    public ID3D11BlendState1 Blend;
    private readonly FileIncludeHandler _fileIncludeHandler = new();
    private ComponentLookup<GameRenderManager> Renderer;
    private ComponentLookup<GraphicsDevice> GraphicsDevice;
    protected SamplerStateCollection SamplerStateCollection;


    public readonly InputElementDescription[] InputElements =
    {
        new("SV_POSITION", 0, Format.R32G32B32A32_Float, 0, 0, InputClassification.PerVertexData, 0),
        new("TILEPOSITION", 0, Format.R32G32B32A32_Float, 16, 0, InputClassification.PerVertexData, 0),
        new("COLOR", 0, Format.R32G32B32A32_Float, 32, 0, InputClassification.PerVertexData, 0),
        new("COLOR", 1, Format.R32G32B32A32_Float, 48, 0, InputClassification.PerVertexData, 0),
        new("ENTITY_IDS", 0, Format.R32G32B32A32_SInt, 64, 0, InputClassification.PerVertexData, 0),
        new("TEXCOORD", 0, Format.R32G32B32A32_Float, 80, 0, InputClassification.PerVertexData, 0),
        new("TEXCOORD", 1, Format.R32G32B32A32_Float, 96, 0, InputClassification.PerVertexData, 0),
        new("TEXCOORD", 2, Format.R32G32B32A32_Float, 112, 0, InputClassification.PerVertexData, 0),
        new("TEXCOORD", 3, Format.R32G32B32A32_Float, 128, 0, InputClassification.PerVertexData, 0),
        new("SHADERMATERIAL", 0, Format.R32G32B32A32_SInt, 144, 0, InputClassification.PerVertexData, 0),
    };

    public override void OnInitialize()
    {
        base.OnInitialize();

        Renderer = new ComponentLookup<GameRenderManager>();
        GameDebug = new ComponentLookup<GameDebug>();
        GraphicsDevice = new ComponentLookup<GraphicsDevice>();
    }

    public override void OnEngineReady()
    {
        base.OnEngineReady();

        BlendAlpha = CreateBlendStateAlpha();
        BlendStatePreMultiplied = CreatePreMultipliedBlendState();
        BlendTemp = CreateBlendTemp();
        Blend = BlendStatePreMultiplied;
        SamplerStateCollection = GraphicsDevice.Value.CreateSamplerCollection();
    }

    public override void OnReleaseResources()
    {
        base.OnReleaseResources();

        Dispose();
    }

    private ID3D11BlendState1 CreateBlendTemp()
    {
        var desc = new BlendDescription1();
        desc.AlphaToCoverageEnable = false;
        desc.IndependentBlendEnable = true;

        desc.RenderTarget[0].IsBlendEnabled = true;
        desc.RenderTarget[0].SourceBlend = SourceAlpha;
        desc.RenderTarget[0].DestinationBlend = InverseSourceAlpha;
        desc.RenderTarget[0].BlendOperation = Add;

        desc.RenderTarget[0].SourceBlendAlpha = One;
        desc.RenderTarget[0].DestinationBlendAlpha = Zero;
        desc.RenderTarget[0].BlendOperationAlpha = Add;
        return Engine.GetComponent<D3DFactory>().CreateBlendState(desc).Collect();
    }

    private ID3D11BlendState1 CreateBlendStateAlpha()
    {
        var desc = new BlendDescription1();
        desc.AlphaToCoverageEnable = false;
        desc.IndependentBlendEnable = true;
        desc.RenderTarget[0].IsBlendEnabled = true;
        desc.RenderTarget[0].RenderTargetWriteMask = ColorWriteEnable.All;

        desc.RenderTarget[0].SourceBlend = SourceAlpha;
        desc.RenderTarget[0].DestinationBlend = InverseSourceAlpha;
        desc.RenderTarget[0].BlendOperation = Add;

        desc.RenderTarget[0].SourceBlendAlpha = One;
        desc.RenderTarget[0].DestinationBlendAlpha = Zero;
        desc.RenderTarget[0].BlendOperationAlpha = Add;
        return Engine.GetComponent<D3DFactory>().CreateBlendState(desc);
    }


    private ID3D11BlendState1 CreatePreMultipliedBlendState()
    {
        var desc = new BlendDescription1();
        desc.AlphaToCoverageEnable = false;
        desc.IndependentBlendEnable = true;

        desc.RenderTarget[0].IsBlendEnabled = true;
        desc.RenderTarget[0].RenderTargetWriteMask = ColorWriteEnable.All;

        desc.RenderTarget[0].SourceBlend = SourceAlpha;
        desc.RenderTarget[0].DestinationBlend = InverseSourceAlpha;
        desc.RenderTarget[0].BlendOperation = Add;

        desc.RenderTarget[0].SourceBlendAlpha = One;
        desc.RenderTarget[0].DestinationBlendAlpha = Zero;
        desc.RenderTarget[0].BlendOperationAlpha = Add;
        return Engine.GetComponent<D3DFactory>().CreateBlendState(desc);
    }

    protected bool InitializeShader(ID3D11Device1 device, string psFileName, string vsFileName)
    {
        if (GameManager.Instance.IsTestMode)
            return true;

        try
        {
            PixelShader = CompilePixelShader(device, psFileName);

            var vertexShader = CompileVertexShader(device, vsFileName);
            GraphicsDevice.Value.D3DContext.IASetInputLayout(vertexShader.InputLayout);

            VertexShader = vertexShader.VertexShader;
            VertexShader.DebugName = $"{Name}";
            PixelShader.DebugName = $"{Name}";

            return true;
        }
        catch (Exception ex)
        {
            ConsoleWriter.WriteLine(ex.Message);
            return false;
        }
    }

    private ID3D11PixelShader CompilePixelShader(ID3D11Device1 device, string psFileName)
    {
        if (PixelShaderCache.ContainsKey(psFileName))
        {
            return PixelShaderCache[psFileName];
        }

        const ShaderFlags shaderFlags = ShaderFlags.Debug | ShaderFlags.SkipOptimization | ShaderFlags.PreferFlowControl;
        //const ShaderFlags shaderFlags = ShaderFlags.None;
        using var pixelShaderByteCode = ShaderBytecode.CompileFromFile(GetPixelShaderPath(psFileName), "main", "ps_4_0", shaderFlags, EffectFlags.None, null, _fileIncludeHandler);
        var pixelShader = device.CreatePixelShader(pixelShaderByteCode.Bytecode);
        pixelShader.Tag = $"{TypeName.Value}_PS";
        return PixelShaderCache[psFileName] = pixelShader;
    }

    private GameVertexShader CompileVertexShader(ID3D11Device1 device, string vsFileName)
    {
        if (VertexShaderCache.ContainsKey(vsFileName))
        {
            return VertexShaderCache[vsFileName];
        }

        const ShaderFlags shaderFlags = ShaderFlags.Debug | ShaderFlags.SkipOptimization | ShaderFlags.PreferFlowControl;
        //const ShaderFlags shaderFlags = ShaderFlags.None;

        using var vertexShaderByteCode = ShaderBytecode.CompileFromFile(GetVertexShaderPath(vsFileName), "main", "vs_4_0", shaderFlags, EffectFlags.None, null, _fileIncludeHandler);
        var vertexShader = device.CreateVertexShader(vertexShaderByteCode);
        vertexShader.Tag = $"{TypeName.Value}_VS";
        var inputLayout = device.CreateInputLayout(InputElements, ShaderSignature.GetInputSignature(vertexShaderByteCode)).Collect();
        return VertexShaderCache[vsFileName] = new GameVertexShader(vertexShader, inputLayout);
    }

    private string GetPixelShaderPath(string name) => $"{Application.StartupPath}\\Resources\\Shaders\\{name}.ps.hlsl";
    private string GetVertexShaderPath(string name) => $"{Application.StartupPath}\\Resources\\Shaders\\{name}.vs.hlsl";

    public void OnBeforeDraw(GameRenderable renderable)
    {
        if (OnUpdateConstantBuffers(renderable))
        {
        }

        SetRasterizerState(renderable);
    }

    protected virtual bool OnUpdateConstantBuffers(GameRenderable renderable)
    {
        return true;
    }

    private void SetRasterizerState(GameRenderable gameRenderable)
    {
        /*if (Engine.Camera.GetZoomFactor() < 0.5f)
        {
            SampleState = SamplerStateCollection.AnisotropicClamp;
        }
        else
        {*/
            SampleState = SamplerStateCollection.PointClamp;
        /*}*/
        
        var rasterizerState = GameDebug.EnableWireframe && gameRenderable is not IDeferredRenderable
            ? GraphicsDevice.Value.WireframeRasterizerState
            : GraphicsDevice.Value.SolidRasterizerState;
        GraphicsDevice.Value.D3DContext.RSSetState(rasterizerState);
    }

    public override void Dispose()
    {
        base.Dispose();
        
        GameManager.DisposeCollector.RemoveAndDispose(ref SampleState);
        GameManager.DisposeCollector.RemoveAndDispose(ref BlendAlpha);
        GameManager.DisposeCollector.RemoveAndDispose(ref BlendTemp);
        GameManager.DisposeCollector.RemoveAndDispose(ref BlendStatePreMultiplied);
        GameManager.DisposeCollector.RemoveAndDispose(ref Blend);
        GameManager.DisposeCollector.RemoveAndDispose(ref PixelShader);
        GameManager.DisposeCollector.RemoveAndDispose(ref VertexShader);
        SamplerCollection = null;
        VertexShaderCache.Clear();
        PixelShaderCache.Clear();
    }
}