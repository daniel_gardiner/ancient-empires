﻿using System.Numerics;
using AncientEmpires.Engine.Map.Rendering;
using AncientEmpires.Engine.Render.Quads;

namespace AncientEmpires.Engine.Render.Text;

public class TextContext
{
    public GameEngine Engine { get; set; }
    public TextLayer TextLayer { get; set; }
    public IPositionStrategy PositionStrategy { get; set; }
    public Func<Sprite, Glyph, Vector4, float, TextQuad> QuadFactory { get; set; }
    public Font Font { get; set; }
    public List<int> TextureIndexes { get; set; }
}