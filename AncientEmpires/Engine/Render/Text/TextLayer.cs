using System.Numerics;
using AncientEmpires.Engine.Map.Rendering;
using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Groups;
using AncientEmpires.Engine.Render.Quads;

namespace AncientEmpires.Engine.Render.Text;

public abstract class TextLayer : GameBatchLayer
{
    public TextQuadCache TextQuadCache;
    public TextContext TextContext;
    public Font Font { get; set; }
    public bool AlignTop { get; set; }

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();

        TextQuadCache = AddComponent<TextQuadCache>();
    }

    public override void OnInitialize()
    {
        base.OnInitialize();
        
        TextContext = new TextContext
        {
            Font = Font,
            Engine = Engine,
            TextLayer = this,
            PositionStrategy = Engine.GetComponent<InterfacePositionStrategy>(),
            QuadFactory = QuadFactory
        };  
        ChangeFont(KnownFont.JetBrains);
    }

    private void ChangeFont(KnownFont font)
    {
        Font = Engine.GetComponent<FontManager>().GetFont(font);
        TextContext.Font = Font;
        TextContext.TextureIndexes = Engine.GetComponent<GameTextures>().FindTextureIndexes(Font.Texture.FileName);
    }

    public TextMeshFragment AddFragment(TextMeshFragment fragment)
    {
        var existingMesh = Batches.GetMesh(fragment.Key);

        return (TextMeshFragment)Batches.AddFragment(existingMesh);
    }

    public virtual TextMeshFragment AddText(string text, GamePosition position, Vector4 color, float scale = 0.5f) => AddText(
        text,
        position,
        Vector4.Zero,
        color,
        scale);

    public virtual TextMeshFragment AddText(string text, GamePosition position, Vector4 offset, Vector4 color, float scale = 0.5f)
    {
        position = new Vector4(position.MapSpace.X, position.MapSpace.Y, ZIndex, 0);

        return (TextMeshFragment)Batches.AddFragment(TextMeshFragment.Create(
            TextContext, 
            AlignTop,
            color,
            position,
            offset,
            scale,
            text));
    }

    protected override void PrepareBatches()
    {
        base.PrepareBatches();
    }

    public override void OnDraw()
    {
        base.OnDraw();
    }

    protected abstract TextQuad QuadFactory(Sprite sprite, Glyph glyph, Vector4 position, float scale);
}