using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Map.Rendering;
using AncientEmpires.Engine.Render.Shaders;

namespace AncientEmpires.Engine.Render.Text;

public class DebugTextLayer : MapTextLayer
{
    protected GameDebug GameDebug;

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();

        Shader = AddComponent(new TextShader(Font));
    }

    public override void OnInitialize()
    {
        base.OnInitialize();

        var fontManager = Engine.GetComponent<FontManager>();
        ZIndex = Engine.Configuration.GetRenderLayer(KnownRenderLayers.MapOverlay) - 0.9f;
        Font = fontManager.GetFont(KnownFont.JetBrains);
        Textures = new[] { Font.Texture };
        GameDebug = Engine.GetComponent<GameDebug>();
        TextContext.PositionStrategy = Engine.GetComponent<IsometricPositionStrategy>();
    }

    protected override void PrepareBatches()
    {
        base.PrepareBatches();
    }

    public override void OnDraw()
    {
        if (!GameDebug.Enabled)
            return;

        base.OnDraw();
    }
}