using System.Numerics;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Groups;
using AncientEmpires.Engine.Render.Quads;

namespace AncientEmpires.Engine.Render.Text;

public class TextMeshFragment : MeshFragment
{
    protected TextContext Context;
    protected bool AlignTop;
    protected string Text;
    protected Vector4 Offset;
    protected float Scale;
    protected Vector4 Color;

    public TextMeshFragment()
    {
        Key = OnRegisterFragment();
    }

    private TextMeshFragment(MeshKey meshKey) : base(meshKey)
    {
        Key = OnRegisterFragment();
    }

    public bool UpdateText(string text) => UpdateText(text, Position);


    public bool UpdateText(string text, GamePosition position)
    {
        if (text == Text)
            return false;

        Position = position;
        Text = text;
        MarkDirty();
        Refresh(0);
        return Changed;
    }

    public override void OnUpdateFragment(float timeDelta)
    {
        if (!Changed)
            return;

        Position += Offset;
        PositionOffset = Position;

        var quads = Context.TextLayer.TextQuadCache.GetQuads(Text, TextToQuads);
        quads.ForEach(o => o.SetZIndex(ZIndex));
        Quads.Clear();
        Quads.AddRange(quads);
    }

    private List<Quad> TextToQuads(string text)
    {
        var quads = new List<Quad>();
        var currentPosition = Vector4.Zero;
        foreach (var character in text)
        {
            if (!Context.Font.Glyphs.ContainsKey(character))
                continue;

            var glyph = Context.Font.Glyphs[character];
            var sprite = new Sprite(new Vector4(glyph.Position, 0, 0), new Vector4(glyph.Size, 0, 0), Color, Context.TextureIndexes);

            var alignment = AlignTop
                ? new Vector4(glyph.Offset.X, 0, 0, 0) * Scale
                : new Vector4(glyph.Offset.X, glyph.Offset.Y, 0, 0) * Scale;
            var position = currentPosition + alignment;

            quads.Add(Context.QuadFactory(sprite, glyph, position, Scale));
            currentPosition += new Vector4(glyph.XAdvance / 1.2f, 0, 0, 0) * Scale;
        }

        return quads;
    }

    public static TextMeshFragment Create(
        TextContext context,
        bool alignTop,
        Vector4 color,
        GamePosition position,
        Vector4 offset,
        float scale,
        string text)
    {
        var textMesh = new TextMeshFragment();
        textMesh.Context = context;
        textMesh.ZIndex = position.MapSpace.Z;
        textMesh.AlignTop = alignTop;
        textMesh.Offset = offset;
        textMesh.Scale = scale;
        textMesh.Color = color;
        textMesh.Position = position;
        textMesh.UpdateText(text, position);
        return textMesh;
    }
}