using AncientEmpires.Engine.Render.Components;
using Vortice.Direct3D;
using Vortice.Direct3D11;
using Vortice.DXGI;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Render;

public abstract class RenderTarget : EngineComponent
{
    public ID3D11Texture2D RenderTargetTexture;
    public ID3D11RenderTargetView RenderTargetView;
    public ID3D11ShaderResourceView ShaderResourceView;
    public ID3D11DepthStencilView DepthStencilView;
    public ID3D11Texture2D DepthStencilBuffer;
    public Size BufferSize;
    public Size Size;
    public Viewport Viewport;
    protected ComponentLookup<GraphicsDevice> GraphicsDevice;
    protected ComponentLookup<GraphicsPresentation> GraphicsPresentation;
    public string DebugName;

    public override void OnEngineReady()
    {
        base.OnEngineReady();

        GraphicsDevice = Engine.ComponentLookup<GraphicsDevice>();
        GraphicsPresentation = Engine.ComponentLookup<GraphicsPresentation>();
        Size = new Size((int)Engine.ScreenSize.X, (int)Engine.ScreenSize.Y);
        Viewport = Engine.GetComponent<GraphicsDevice>().GetViewport();
    }

    public override void OnReleaseResources()
    {
        base.OnReleaseResources();
        Dispose();
    }

    public virtual void SetRenderTarget(ID3D11DeviceContext1 context)
    {
        if (RenderTargetView == null)
            return;

        context.OMSetRenderTargets(RenderTargetView, DepthStencilView);
        context.RSSetViewport(Viewport);
    }


    internal ID3D11RenderTargetView CreateRenderTargetView(Format format)
    {
        return GraphicsDevice.Value.D3DDevice.CreateRenderTargetView(RenderTargetTexture, new RenderTargetViewDescription
        {
            Format = format,
            ViewDimension = RenderTargetViewDimension.Texture2D,
            Texture2D =
            {
                MipSlice = 0
            },
        });
    }

    protected virtual ID3D11DepthStencilView CreateDepthStencilView()
    {
        var swapChainDescription = GraphicsPresentation.Value.SwapChain.Description;
        return GraphicsDevice.Value.D3DDevice.CreateDepthStencilView(
            DepthStencilBuffer,
            new DepthStencilViewDescription
            {
                ViewDimension = swapChainDescription.SampleDescription.Count > 1 || swapChainDescription.SampleDescription.Quality > 0
                    ? DepthStencilViewDimension.Texture2DMultisampled
                    : DepthStencilViewDimension.Texture2D
            });
    }


    protected ID3D11Texture2D CreateDepthStencilBuffer()
    {
        ConsoleWriter.WriteLine($"DepthStencilView Size = {Size.Width} x {Size.Height}");
        return GraphicsDevice.Value.D3DDevice.CreateTexture2D(new Texture2DDescription()
        {
            Width = Size.Width,
            Height = Size.Height,
            MipLevels = 1,
            ArraySize = 1,
            Format = Format.D24_UNorm_S8_UInt,
            SampleDescription = new SampleDescription(1, 0),
            Usage = ResourceUsage.Default,
            BindFlags = BindFlags.DepthStencil,
            CpuAccessFlags = CpuAccessFlags.None,
            OptionFlags = ResourceOptionFlags.None
        });
    }

    protected ID3D11Texture2D CreateDepthBuffer()
    {
        return GraphicsDevice.Value.D3DDevice.CreateTexture2D(new Texture2DDescription
        {
            Format = Format.D32_Float_S8X24_UInt,
            ArraySize = 1,
            MipLevels = 1,
            Width = Size.Width,
            Height = Size.Height,
            SampleDescription = GraphicsPresentation.Value.SwapChain.Description.SampleDescription,
            BindFlags = BindFlags.DepthStencil
        });
    }

    protected ID3D11ShaderResourceView CreateShaderResourceView(Format format)
    {
        return GraphicsDevice.Value.D3DDevice.CreateShaderResourceView(RenderTargetTexture, new ShaderResourceViewDescription
        {
            Format = format,
            ViewDimension = ShaderResourceViewDimension.Texture2D,
            Texture2D =
            {
                MipLevels = 1,
                MostDetailedMip = 0
            },
        });
    }

    public void ClearRenderTarget(ID3D11DeviceContext1 context, Color4 color)
    {
        if (RenderTargetView == null)
            return;

        context.ClearRenderTargetView(RenderTargetView, color);
        context.ClearDepthStencilView(DepthStencilView, DepthStencilClearFlags.Depth, 1.0f, 0);
    }

    public override void Dispose()
    {
        base.Dispose();

        GameManager.DisposeCollector.RemoveAndDispose(ref RenderTargetView);
        GameManager.DisposeCollector.RemoveAndDispose(ref ShaderResourceView);
        GameManager.DisposeCollector.RemoveAndDispose(ref DepthStencilView);
        GameManager.DisposeCollector.RemoveAndDispose(ref DepthStencilBuffer);
        GameManager.DisposeCollector.RemoveAndDispose(ref RenderTargetTexture);
    }
}