using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Render.Components;

namespace AncientEmpires.Engine.Render.Groups;

public class GameMeshBatches : EngineComponent
{
    protected ComponentLookup<BatchManager> BatchManager;
    protected readonly IMeshBatchStrategy _batchStrategy;

    public readonly List<GameMeshBatch> Batches = new();
    public IMeshBatchStrategy BatchStrategy;

    public GameMeshBatches(IMeshBatchStrategy batchStrategy)
    {
        BatchStrategy = batchStrategy;
    }

    public override void OnInitialize()
    {
        base.OnInitialize();

        BatchManager = new ComponentLookup<BatchManager>();
        
        var batches = BatchStrategy.CreateBatches(_ => new GameMeshBatch());
        foreach (var batch in batches)
        {
            Batches.Add(RegisterNewBatch(batch));
        }
    }
    
    public virtual MeshFragment AddFragment(MeshFragment fragment)
    {
        var meshGroup = BatchStrategy.GetBatch(Batches, fragment, fragment.Position);

        if (meshGroup != null)
        {
            meshGroup.AddFragment(fragment);
        }
        else
        {
            meshGroup = RegisterNewBatch(new GameMeshBatch());
            meshGroup.AddFragment(fragment);
            Batches.Add(meshGroup);
        }

        return fragment;
    }

    public MeshFragment GetMesh(MeshKey key)
    {
        foreach (var meshGroup in Batches)
            if (meshGroup.TryGetMesh(key, out var mesh))
                return mesh;

        return null;
    }

    public TMeshFragment GetMesh<TMeshFragment>(MeshKey key)
        where TMeshFragment : MeshFragment
    {
        foreach (var meshGroup in Batches)
            if (meshGroup.TryGetMesh(key, out var mesh))
            {
                if (mesh is not TMeshFragment fragment)
                    throw new Exception($"Mesh {mesh.GetType()} is not {typeof(TMeshFragment)}");

                return fragment;
            }

        return null;
    }

    public GameMeshBatch RegisterNewBatch(GameMeshBatch newGroup)
    {
        var batch = RegisterComponent(newGroup);
        BatchManager.Value.Register(batch);
        return batch;
    }

    public void RemoveMesh(string key)
    {
        throw new NotImplementedException();
    }

    public void Clear()
    {
        for (var index = Batches.Count - 1; index >= 0; index--)
        {
            var group = Batches[index];
            group.Clear();
        }
    }
}