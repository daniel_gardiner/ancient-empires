﻿using AncientEmpires.Engine.Interface;

namespace AncientEmpires.Engine.Render.Groups;

public class FragmentManager
{
    public static FragmentManager Instance { get; }
    private const ulong FirstFragmentIndex = 100_000;
    public ulong FragmentIndex = FirstFragmentIndex;

    static FragmentManager()
    {
        Instance = new FragmentManager();
    }

    //private Dictionary<MeshKey, MeshFragment> FragmentIndexes { get; } = new();

    public MeshKey Register(MeshFragment fragment, ushort keyType)
    {
        var key = new MeshKey(keyType, FragmentIndex);
        //FragmentIndexes.Add(fragment.Key, fragment);
        FragmentIndex++;

        UpdateMetrics();
        return key;
    }

    public MeshKey Register(MeshFragment fragment)
    {
        var key = new MeshKey(KeyType.None, FragmentIndex);
        //FragmentIndexes.Add(fragment.Key, fragment);
        FragmentIndex++;

        UpdateMetrics();
        return key;
    }

    public MeshKey Register(MeshFragment fragment, MeshKey key)
    {
        //FragmentIndexes.Add(key, fragment);
        FragmentIndex++;

        UpdateMetrics();
        return key;
    }

    private void UpdateMetrics()
    {
        GameContext.Instance.Engine.Timing.GameMetrics.Fragments = (long)(FragmentIndex - FirstFragmentIndex);
    }
}