namespace AncientEmpires.Engine.Render.Groups;

public enum BatchRenderMode
{
    Static,
    Dynamic
}