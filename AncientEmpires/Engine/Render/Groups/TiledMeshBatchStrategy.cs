using System.Numerics;

namespace AncientEmpires.Engine.Render.Groups;

public class TiledMeshBatchStrategy : IMeshBatchStrategy
{
    public bool IsDepthSorted { get; }
    
    private readonly GameEngine _engine;
    private readonly Dictionary<Vector2, GameMeshBatch> _batches = new();
    private static readonly Comparer<int> EntityDepthComparer = Comparer<int>.Create((a, b) => b - a);
    private Size _batchTileSize;

    public TiledMeshBatchStrategy(GameEngine engine, bool isDepthSorted = false)
    {
        IsDepthSorted = isDepthSorted;
        _engine = engine;
    }

    public List<GameMeshBatch> CreateBatches(Func<int, GameMeshBatch> meshFactory)
    {
        var batchSize = CalculateGroups(_engine);
        var order = 0;
        
        for (var y = batchSize.Height - 1; y >= 0; y--)
        for (var x = batchSize.Width - 1; x >= 0; x--)
        {
            var quadCapacity = _batchTileSize.Width * _batchTileSize.Height + 1;
            _batches[new Vector2(x, y)] = meshFactory(quadCapacity);
            _batches[new Vector2(x, y)].Order = order++;
            _batches[new Vector2(x, y)].Position = new Vector2(x, y);
            _batches[new Vector2(x, y)].Size = _batchTileSize;
            _batches[new Vector2(x, y)].TriggerNextFrame(0);
        }

        return _batches.Values.ToList();
    }

    public GameMeshBatch GetBatch(List<GameMeshBatch> batches, MeshFragment fragment, GamePosition position)
    {
        var mesh = CalculateBatch(position);
        return _batches[mesh];
    }

    private Size CalculateGroups(GameEngine engine)
    {
        _batchTileSize = new Size(
            engine.Configuration.TileBatchDimensions.Width,
            engine.Configuration.TileBatchDimensions.Height);

        return new Size(
            engine.Configuration.MapSize.Width / _batchTileSize.Width,
            engine.Configuration.MapSize.Height / _batchTileSize.Height);
    }

    private Vector2 CalculateBatch(GamePosition position)
    {
        var groupCoordinates = new Point(
            (int)Math.Floor(position.MapSpace.X / _batchTileSize.Width),
            (int)Math.Floor(position.MapSpace.Y / _batchTileSize.Height));
        return new Vector2(groupCoordinates.X, groupCoordinates.Y);
    }
}