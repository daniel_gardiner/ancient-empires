namespace AncientEmpires.Engine.Render.Groups;

public struct FragmentAllocation : IEquatable<FragmentAllocation>
{
    public int Offset;
    public int Length;
    public int DepthIndex = -1;
    public MeshFragment Fragment;
    public bool IsDepthSorted = false;

    public int CalculateOffset() => IsDepthSorted
        ? DepthIndex * Length
        : Offset;

    public bool Equals(FragmentAllocation other) => Fragment.Key.Equals(other.Fragment.Key);
    public override bool Equals(object obj) => obj is FragmentAllocation other && Equals(other);
    public override int GetHashCode() => Fragment.Key.GetHashCode();
}