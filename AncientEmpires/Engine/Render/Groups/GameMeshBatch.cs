using System.Diagnostics;
using System.Numerics;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Render.Components;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Render.Groups;

public class GameMeshBatch : RenderableComponent
{
    public readonly Dictionary<MeshKey, FragmentAllocation> AllocationLookup = new();
    protected List<MeshFragment> AllocatedFragments = new();
    protected List<FragmentAllocation> Allocations = new();
    protected bool SortingChanged;
    protected int MaxAllocationLength = 500_000;
    public int VertexCount;
    public bool IsEmpty => Allocations.Count == 0;
    public int Order;
    public Size Size;
    public Vector2 Position;
    protected GameMeshBatches Batches;
    protected BoundingBox Bounds;
    protected virtual int MinimumVertexAllocation => VerticesPerQuad;
    protected virtual decimal AllocationFactor => 1;

    public GameMeshBatch()
    {
        Changed = true;
    }

    public IEnumerable<MeshFragment> Fragments
    {
        get
        {
            if (Batches.BatchStrategy.IsDepthSorted && SortingChanged)
            {
                AllocatedFragments.ForEach(o => o.OnUpdateDepth());
                AllocatedFragments.Sort(MeshFragment.DepthComparer);
                SortingChanged = false;
            }

            foreach (var fragment in AllocatedFragments)
            {
                if (!fragment.IsVisible)
                    continue;

                yield return fragment;
            }
        }
    }

    public override void OnInitialize()
    {
        base.OnInitialize();

        Batches = GetParent<GameMeshBatches>();
    }

    public override void OnNextFrame(float timeDelta)
    {
        base.OnNextFrame(timeDelta);

        UpdateBounds();
    }

    public bool Contains(MeshFragment mesh) => !AllocationLookup.ContainsKey(mesh.Key);

    public void MarkClean()
    {
        Changed = false;
    }

    public void MarkDirty()
    {
        Changed = true;
        SortingChanged = true;
    }

    public BoundingBox GetBoundingBox() => Bounds;

    public bool TryGetMesh(MeshKey key, out MeshFragment mesh)
    {
        if (AllocationLookup.ContainsKey(key))
        {
            mesh = AllocationLookup[key].Fragment;
            return true;
        }

        mesh = null;
        return false;
    }

    public void UpdateBounds()
    {
        var corner = Position * new Vector2(Size.Width, Size.Height);
        var corner1 = corner.ToVector3();
        var corner2 = (corner + new Vector2(Size.Width - 1, 0)).ToVector3();
        var corner3 = (corner + new Vector2(0, Size.Height - 1)).ToVector3();
        var corner4 = (corner + new Vector2(Size.Width - 1, Size.Height - 1)).ToVector3();

        Bounds = BoundingBox.CreateFromPoints(new[] { corner1, corner2, corner3, corner4 });
    }


    public virtual TMeshFragment AddFragment<TMeshFragment>(TMeshFragment fragment) where TMeshFragment : MeshFragment
    {
        if (!fragment.IsVisible)
        {
            RemoveFragment(fragment);
            return fragment;
        }

        return !AllocationLookup.ContainsKey(fragment.Key)
            ? InsertFragment<TMeshFragment>(fragment)
            : UpdateFragment<TMeshFragment>(fragment);
    }

    public void Refresh(MeshFragment fragment)
    {
        AddFragment(fragment);
    }

    private TMeshFragment InsertFragment<TMeshFragment>(TMeshFragment fragment) where TMeshFragment : MeshFragment
    {
        Debug.Assert(AllocationFactor >= 1, $"Allocation factor must be greater than 1 (AllocationFactor: {AllocationFactor})");

        var allocation = new FragmentAllocation
        {
            Length = fragment.Length,
            Fragment = fragment,
            Offset = VertexCount,
            IsDepthSorted = Batches.BatchStrategy.IsDepthSorted
        };

        if (!CanFit(allocation.Length))
            Debug.Assert(false, $"Could not increase vertex buffer: {Name}");

        fragment.Batch = this;
        fragment.OnNextFrame(Engine.Timing.SecondFraction);
        UpdateAllocationIndexes(fragment, ref allocation);
        Engine.Timing.GameMetrics.MetricFragmentsCreated++;

        VertexCount += fragment.Quads.Count * 4;
        return fragment;
    }

    public virtual TMeshFragment UpdateFragment<TMeshFragment>(TMeshFragment fragment) where TMeshFragment : MeshFragment
    {
        Debug.Assert(AllocationLookup.ContainsKey(fragment.Key), $"Allocations does not contain the expected key (Key: {fragment.Key})");
        Debug.Assert(AllocationFactor >= 1, $"Allocation factor must be greater than 1 (AllocationFactor: {AllocationFactor})");

        var allocation = AllocationLookup[fragment.Key];
        VertexCount -= allocation.Length;
        if (allocation.Length < fragment.Length)
        {
            RemoveFragment(fragment);
            InsertFragment(fragment);
            return fragment;
        }

        AllocatedFragments.Remove(fragment);
        Allocations.Remove(allocation);

        fragment.Batch = this;
        fragment.OnNextFrame(Engine.Timing.SecondFraction);
        UpdateAllocationIndexes(fragment, ref allocation);
        Engine.Timing.GameMetrics.MetricFragmentsUpdated++;

        if (fragment.Changed)
            MarkDirty();

        VertexCount += fragment.Quads.Count * 4;
        return fragment;
    }

    public void RemoveFragment(MeshFragment fragment)
    {
        if (!AllocationLookup.ContainsKey(fragment.Key))
            return;

        var existing = AllocationLookup[fragment.Key];
        existing.DepthIndex = Allocations.IndexOf(existing);

        AllocatedFragments.Remove(fragment);
        Allocations.Remove(existing);
        AllocationLookup.Remove(fragment.Key);
        MarkDirty();
    }

    private void UpdateAllocationIndexes(MeshFragment fragment, ref FragmentAllocation allocation)
    {
        if (!Batches.BatchStrategy.IsDepthSorted)
            UpdateUnsortedAllocationIndexes(fragment, allocation);
        else
            UpdateSortedAllocationIndexes(fragment, ref allocation);
    }

    private void UpdateUnsortedAllocationIndexes(MeshFragment fragment, FragmentAllocation allocation)
    {
        AllocatedFragments.Add(fragment);
        Allocations.Add(allocation);
        AllocationLookup[fragment.Key] = allocation;
    }

    private void UpdateSortedAllocationIndexes(MeshFragment fragment, ref FragmentAllocation allocation)
    {
        AllocatedFragments.Add(fragment);
        Allocations.Add(allocation);
        AllocationLookup[fragment.Key] = allocation;
        SortingChanged = true;
    }

    public void Clear()
    {
        AllocatedFragments.Clear();
        Allocations.Clear();
        AllocationLookup.Clear();
        VertexCount = 0;
        MarkDirty();
    }

    public bool CanFit(MeshFragment fragment) => CanFit(GetAllocationSize(fragment) * 4);

    private bool CanFit(int allocation) => VertexCount + allocation * 4 < MaxAllocationLength - 1000;

    private int GetAllocationSize(MeshFragment fragment) => Math.Max((int)Math.Ceiling(fragment.Length * AllocationFactor), MinimumVertexAllocation);
}