using Vortice.Mathematics;

namespace AncientEmpires.Engine.Render.Groups;

public abstract class GameLayer : GameRenderable
{
    public override void OnInitialize()
    {
        base.OnInitialize();

        Engine.Camera.SubscribeToScroll(MarkDirty);
        Engine.Camera.SubscribeToZoom(MarkDirty);
    }

    public override void OnEngineReady()
    {
        base.OnEngineReady();

        GameRenderer.Value.RegisterLayer(this);
    }

    public virtual bool CanDraw()
    {
        if (!IsVisible)
            return false;

        if (GeometryBuffer == null)
            return false;

        /*
        if (!Changed && Engine.Timing.Slice - CleanSlice > 2)
            return false;
            */

        return true;
    }

    public override void Clear(string reason)
    {
        GeometryBuffer.Clear(reason);
        RenderTarget.ClearRenderTarget(GraphicsDevice.Value.D3DContext, Color4.Transparent);
    }
}