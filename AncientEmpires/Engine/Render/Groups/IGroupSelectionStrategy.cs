namespace AncientEmpires.Engine.Render.Groups;

public interface IMeshBatchStrategy
{
    bool IsDepthSorted { get; }
    List<GameMeshBatch> CreateBatches(Func<int, GameMeshBatch> meshFactory);
    GameMeshBatch GetBatch(List<GameMeshBatch> batches, MeshFragment fragment, GamePosition position);
}