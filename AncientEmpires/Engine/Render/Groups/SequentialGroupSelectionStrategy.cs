namespace AncientEmpires.Engine.Render.Groups;

public class SequentialMeshBatchStrategy : IMeshBatchStrategy
{
    public bool IsDepthSorted { get; }

    public SequentialMeshBatchStrategy(bool isDepthSorted = false)
    {
        IsDepthSorted = isDepthSorted;
    }

    public List<GameMeshBatch> CreateBatches(Func<int, GameMeshBatch> meshFactory) => new()
    {
        meshFactory(-1)
    };

    public GameMeshBatch GetBatch(List<GameMeshBatch> batches, MeshFragment fragment, GamePosition position)
    {
        foreach (var batch in batches)
            if (batch.Contains(fragment))
                return batch;

        for (var index = batches.Count - 1; index >= 0; index--)
        {
            var batch = batches[index];

            if (batch.CanFit(fragment))
                return batch;
        }

        return null;
    }
}