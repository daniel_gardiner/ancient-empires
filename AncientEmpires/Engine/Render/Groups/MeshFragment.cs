using System.Diagnostics;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Quads;

namespace AncientEmpires.Engine.Render.Groups;

public abstract class MeshFragment : EngineComponent, IEquatable<MeshFragment>, IComparable<MeshFragment>
{
    public static readonly IComparer<MeshFragment> DepthComparer = Comparer<MeshFragment>.Create((a, b) => a.SortingDepth.CompareTo(b.SortingDepth));

    public MeshKey Key { get; protected init; }
    public ShaderMaterial ShaderMaterial;
    public float ZIndex;
    public float Depth;
    public float SortingDepth;
    public GamePosition PositionOffset;
    public GamePosition Position;
    public GameMeshBatch Batch;
    public bool Changed = true;
    public List<Quad> Quads = new();
    public override bool EnableMetrics => false;
    public int Length => Quads.Count * 4;

    public MeshFragment(MeshKey key)
    {
        Key = key;
    }

    protected MeshFragment()
    {
    }

    protected virtual MeshKey OnRegisterFragment()
    {
        return Key.Equals(MeshKey.Empty)
            ? FragmentManager.Instance.Register(this)
            : FragmentManager.Instance.Register(this, Key);
    }

    public virtual void OnUpdateFragment(float timeDelta)
    {
    }

    public virtual void OnUpdateDepth()
    {
        Depth = ZIndex;
        SortingDepth = 10 - ZIndex;
    }

    public override void OnNextFrame(float timeDelta)
    {
        OnUpdateDepth();
        OnUpdateFragment(timeDelta);

        if (ZIndex <= 0.1f)
            ZIndex = 9f;

        Debug.Assert(ZIndex > 0, "ZIndex must be greater than 0");
    }

    public void Refresh(float timeDelta)
    {
        OnNextFrame(timeDelta);
    }

    public void MarkDirty()
    {
        Changed = true;
        Batch?.MarkDirty();
        OnDirty();
    }

    public virtual void OnDirty()
    {
    }

    public virtual void MarkClean()
    {
        Changed = false;
    }

    public void UpdateQuads(params Quad[] quads)
    {
        var changed = !Quads.SequenceEqual(quads);

        Quads.Clear();
        Quads.AddRange(quads);

        if (changed)
            MarkDirty();
    }

    public int CompareTo(MeshFragment other)
    {
        if (ReferenceEquals(this, other)) return 0;
        if (ReferenceEquals(null, other)) return 1;
        return SortingDepth.CompareTo(other.SortingDepth);
    }

    public bool Equals(MeshFragment other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return Key.Equals(other.Key);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != GetType()) return false;
        return Equals((MeshFragment)obj);
    }

    public override int GetHashCode() => Key.GetHashCode();

    public override string ToString() => $"MeshFragment#{Key}";

    public MeshFragment SetZIndex(float zIndex)
    {
        ZIndex = zIndex;
        return this;
    }
}