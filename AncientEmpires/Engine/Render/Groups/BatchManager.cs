﻿using AncientEmpires.Engine.Render.Components;

namespace AncientEmpires.Engine.Render.Groups;

public class BatchManager : EngineComponent
{
    public List<GameMeshBatch> Batches { get; } = new();

    public void Register(GameMeshBatch batch)
    {
        Batches.Add(batch);
    }
    
    public void Unregister(GameMeshBatch batch)
    {
        Batches.Remove(batch);
    }
    
    public void Refresh()
    {
        Batches.ForEach(o => o.MarkDirty());
    }
}