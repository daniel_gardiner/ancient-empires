﻿using AncientEmpires.Engine.Render.Components;
using Vortice.Mathematics;
using MoreEnumerable = MoreLinq.MoreEnumerable;

namespace AncientEmpires.Engine.Render.Groups;

public abstract class GameBatchLayer : GameLayer
{
    protected IMeshBatchStrategy BatchStrategy;
    protected BatchRenderMode RenderMode = BatchRenderMode.Dynamic;
    public bool EnableCulling = false;
    public GameMeshBatches Batches;
    
    public override void OnCreateComponents()
    {
        base.OnCreateComponents();

        CreateMeshBatches();
    }
    
    protected virtual void CreateMeshBatches()
    {
        Batches = AddComponent(new GameMeshBatches(BatchStrategy));
    }

    public override void OnBeforeDraw()
    {
        base.OnBeforeDraw();
        PrepareBatches();
    }

    public override void OnDraw()
    {
        base.OnDraw();
    }

    protected virtual void PrepareBatches()
    {
        var drawableBatches = CollectBatches();
        OnUpdateBuffer(drawableBatches);
    }

    public virtual bool OnUpdateBuffer(List<GameMeshBatch> drawableBatches)
    {
        var result = RenderMode == BatchRenderMode.Dynamic
            ? CopyBatchesDynamic(drawableBatches)
            : CopyBatchesStatic(drawableBatches);

        Changed = Changed || result;
        return result;
    }

    protected virtual bool CopyBatchesStatic(List<GameMeshBatch> drawableBatches)
    {
        if (!drawableBatches.Any(o => o.Changed))
            return false;

        var required = drawableBatches.Sum(o => o.VertexCount);
        if (!GeometryBuffer.CanFit(required))
        {
            GeometryBuffer.Clear("GeometryBuffer is full");
            if (!GeometryBuffer.CanFit(required))
            {
                throw new Exception("Vertex buffer is not big enough");
            }
        }

        GeometryBuffer.Map();
        GeometryBuffer.WriteBatches(drawableBatches);
        GeometryBuffer.Unmap();
        drawableBatches.ForEach(o => o.MarkClean());
        ConsoleWriter.WriteLine($"CopyBatchesStatic {TypeName.Value} | {GeometryBuffer.VertexCount} vertices written | {GeometryBuffer.AllocationOffset} offset");
        return true;
    }

    protected virtual bool CopyBatchesDynamic(List<GameMeshBatch> drawableBatches)
    {
        if (!drawableBatches.Any(o => o.Changed))
            return false;

        var requiredVertices = drawableBatches.Sum(o => o.VertexCount);

        if (!GeometryBuffer.NewAllocation(requiredVertices))
        {
            GeometryBuffer.Clear("No space for new allocation");
            drawableBatches.ForEach(o => o.MarkDirty());
        }

        GeometryBuffer.Map();
        GeometryBuffer.WriteBatches(drawableBatches);
        GeometryBuffer.Unmap();
        drawableBatches.ForEach(o => o.MarkClean());
        return true;
    }

    protected List<GameMeshBatch> CollectBatches()
    {
        var batches = Batches.Batches
            .Where(o => !o.IsEmpty)
            .OrderByDescending(o => o.Order)
            .ToList();
        batches = CullBatches(batches);
        return batches;
    }

    private List<GameMeshBatch> CullBatches(List<GameMeshBatch> filledBatches)
    {
        if (!EnableCulling)
            return filledBatches;

        if (!filledBatches.Any())
            return filledBatches;

        return filledBatches
            .Where(batch =>
            {
                var viewportBox = Engine.Camera.GetBoundingBox();
                var batchBox = batch.GetBoundingBox();
                var boxContainsPoint = viewportBox.Contains(batchBox);
                return boxContainsPoint != ContainmentType.Disjoint;
            })
            .ToList();
    }

    public override void Clear(string reason)
    {
        GeometryBuffer.Clear(reason);
        RenderTarget.ClearRenderTarget(GraphicsDevice.Value.D3DContext, Color4.Transparent);
        Batches?.Batches.ForEach(o =>
        {
            o.MarkDirty(); 
            o.Clear();
        });
    }
    
    public override void Refresh(string reason)
    {
        GeometryBuffer.Clear(reason);
        RenderTarget.ClearRenderTarget(GraphicsDevice.Value.D3DContext, Color4.Transparent);
        Batches?.Batches.ForEach(o => o.MarkDirty());
        Batches?.Batches.ForEach(o => MoreEnumerable.ForEach(o.Fragments, f => f.MarkDirty()));
    }
}