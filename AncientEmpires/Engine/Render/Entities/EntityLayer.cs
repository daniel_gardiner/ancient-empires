using AncientEmpires.Engine.Entities;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Groups;
using AncientEmpires.Engine.Render.Shaders;

namespace AncientEmpires.Engine.Render.Entities;

public class EntityLayer : GameBatchLayer
{
    public MapTextLayer TextLayer;

    public EntityLayer()
    {
        BatchStrategy = new TiledMeshBatchStrategy(Engine, true);
    }

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();

        TextLayer = AddComponent<MapTextLayer>();
    }

    public override void OnInitialize()
    {
        base.OnInitialize();

        ZIndex = Engine.Configuration.GetRenderLayer(KnownRenderLayers.Entities);
        EnableCulling = true;
    }

    public override void OnCreateShaders()
    {
        Shader = AddComponent<EntityShader>();
        WireframeShader = AddComponent(new EntityWireframeShader("entity"));
    }

    public EntityMeshFragment CreateFragment(Entity entity)
    {
        if (entity.Fragment != null)
        {
            Batches.AddFragment(entity.Fragment);
        }
        else
        {
            entity.Fragment = EntityMeshFragment.Create(Engine, this, entity, ZIndex);
            Batches.AddFragment(entity.Fragment);
        }

        return entity.Fragment;
    }

    public override void OnBeforeDraw()
    {
        Changed = true;
        base.OnBeforeDraw();
    }

    protected override void PrepareBatches()
    {
        base.PrepareBatches();
    }

    public override void OnDraw()
    {
        base.OnDraw();
    }
}