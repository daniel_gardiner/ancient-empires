﻿using System.Diagnostics;
using System.Numerics;
using AncientEmpires.Engine.Map;

namespace AncientEmpires.Engine.Render.Entities;

public class DepthCalculator
{
    protected readonly GameMap Map;
    protected static int? MinMapPositionX;
    protected static int? MinMapPositionY;
    protected static float? MinDepthValue;
    protected static float? MaxDepthValue;
    protected static float? DepthValueRange;

    public DepthCalculator(GameMap map)
    {
        Map = map;
    }

    public float CalculateDepth(GamePosition position)
    {
        MinMapPositionX ??= (int)new Vector4(0, Map.Size.Y, 0, 0).X / Map.TileSizeInt.X;
        MinMapPositionY ??= -(int)new Vector4(0, 0, 0, 0).Y / Map.TileSizeInt.Y;
        MinDepthValue ??= Math.Abs(CalculateXYDepth(GamePosition.FromMapSpace(Map.Size - Vector4.One)));
        MaxDepthValue ??= Math.Abs(CalculateXYDepth(GamePosition.FromMapSpace(Vector4.Zero)));
        DepthValueRange ??= MaxDepthValue.Value - MinDepthValue.Value;

        var xyDepth = CalculateXYDepth(position);
        var normalizedDepth = (xyDepth - MinDepthValue.Value) / DepthValueRange.Value;
        return normalizedDepth;
    }

    private float CalculateXYDepth(GamePosition mapPosition)
    {
        Debug.Assert(MinMapPositionX != null, nameof(MinMapPositionX) + " != null");
        Debug.Assert(MinMapPositionY != null, nameof(MinMapPositionY) + " != null");

        var depthPosition = mapPosition;
        var xDepth = depthPosition.MapSpace.X - MinMapPositionX.Value;
        var yDepth = (depthPosition.MapSpace.Y - MinMapPositionY.Value) * (Map.Size.Y * 2);
        var xyDepth = yDepth + xDepth;
        return xyDepth;
    }
}