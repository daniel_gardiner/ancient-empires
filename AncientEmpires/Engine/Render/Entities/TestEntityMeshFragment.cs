using AncientEmpires.Engine.Entities;
using AncientEmpires.Engine.Interface;

namespace AncientEmpires.Engine.Render.Entities;

public class TestEntityMeshFragment : EntityMeshFragment
{
    public TestEntityMeshFragment(GameEngine engine, EntityLayer parent, Entity entity, MeshKey key) : base(entity)
    {
        Key = key;
    }
}