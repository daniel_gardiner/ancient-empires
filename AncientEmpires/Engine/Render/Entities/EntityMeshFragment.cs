﻿using System.Numerics;
using AncientEmpires.Engine.Entities;
using AncientEmpires.Engine.Entities.Units;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Render.Groups;
using AncientEmpires.Engine.Render.Quads;
using AncientEmpires.Engine.Render.Text;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Render.Entities;

public class EntityMeshFragment : MeshFragment
{
    private static readonly Type Type = typeof(EntityMeshFragment);
    private TextMeshFragment _text1;
    private TextMeshFragment _text2;
    protected TextureType TextureType;
    public BoundingBox Bounds;
    private GameMap Map;
    public Entity Entity { get; }
    public Texture[] Textures { get; set; }
    public DepthCalculator DepthCalculator { get; }

    public EntityMeshFragment(Entity entity)
    {
        Entity = entity;
        Map = Engine.GetComponent<GameMap>();
        DepthCalculator = new DepthCalculator(Map);
        Key = OnRegisterFragment();
    }

    public override void OnUpdateFragment(float timeDelta)
    {
        base.OnUpdateFragment(timeDelta);

        if (!GameManager.Instance.IsTestMode)
        {
            _text1?.UpdateText($"{Depth}", Entity.Position.MapSpace);
            _text2?.UpdateText($"{Depth}", Entity.Position.MapSpace);
        }

        if (Entity is Unit unit)
        {
            Position = Entity.Position;
            new Vector4(Map.CenterInt.X, Map.CenterInt.Y, Depth, 0);
            UpdateQuads(CreateQuad());
            var bounds = new Vector4(
                Entity.Position.MapSpace.X,
                Entity.Position.MapSpace.Y,
                Entity.Position.MapSpace.X,
                Entity.Position.MapSpace.Y);

            for (var i = 0; i < Quads.Count; i++)
            {
                var quad = Quads[i];
                for (var index = 0; index < 4; index++)
                {
                    //quad[index].Collision = collision;
                    quad[index].Bounds = bounds;
                }
            }
        }
        else
        {
            Position = Entity.Position;
            UpdateQuads(CreateQuad());
        }
    }

    private Quad CreateQuad()
    {
        var entitySize = Entity.SpriteSize * Map.TileSize;
        var entitySprite = Entity.GetSprite();

        Quad quad = Entity switch
        {
            Unit unit => new UnitQuad(unit, Depth, entitySize, entitySprite, ShaderMaterial),
            _ => new EntityQuad(Entity, Position, entitySize, entitySprite, ShaderMaterial)
        };

        var vertices = quad.AllVertices.Select(o => o.Position.ToVector3());
        Bounds = BoundingBox.CreateFromPoints(vertices.ToArray());

        return quad;
    }

    public override void OnUpdateDepth()
    {
        var depth = DepthCalculator.CalculateDepth(Entity.Position);
        Depth = ZIndex - (1 - depth) + Entity.DepthOffset;
        SortingDepth = (1 - depth) + Entity.SortingDepthOffset;
    }

    public override void OnDirty()
    {
        base.OnDirty();
 
        Entity.MarkDirty();
    }

    public static EntityMeshFragment Create(GameEngine engine, EntityLayer parent, Entity entity, float zIndex)
    {
        var fragment = new EntityMeshFragment(entity);
        fragment.TextureType = entity.SpriteManager.TextureType;
        fragment.ZIndex = zIndex;
        fragment.Position = entity.Position;

        if (!GameManager.Instance.IsTestMode && fragment._text1 == null)
        {
            fragment._text1 = parent.TextLayer.AddText($"{fragment.Depth}", fragment.Entity.Position, new Vector4(0, 20, 0, 0), Vector4.One, 0.3f);
            fragment._text2 = parent.TextLayer.AddText($"{fragment.Depth}", fragment.Entity.Position, new Vector4(50, 20, 0, 0), Vector4.One, 0.3f);
        }

        return fragment;
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != Type) return false;
        return Key.Equals(((EntityMeshFragment)obj).Key);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            return (base.GetHashCode() * 397) ^ (Entity != null
                ? Entity.GetHashCode()
                : 0);
        }
    }

    public override string ToString() => $"{base.ToString()} for {Entity}";

    public override void MarkClean()
    {
        base.MarkClean();
        Entity.MarkClean();
    }
}