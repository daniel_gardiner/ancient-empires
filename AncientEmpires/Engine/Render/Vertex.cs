using System.Numerics;
using System.Runtime.InteropServices;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Render;

[StructLayout(LayoutKind.Explicit, Pack = 4)]
public struct Vertex
{
    [FieldOffset(0)]
    public Vector4 Position;

    [FieldOffset(16)]
    public Vector4 Tile;

    [FieldOffset(32)]
    public Vector4 Color;

    [FieldOffset(48)]
    public Vector4 OverlayColor;

    [FieldOffset(64)]
    public Int4 EntityId;

    [FieldOffset(80)]
    public Vector4 UV0;

    [FieldOffset(96)]
    public Vector4 UV1;

    [FieldOffset(112)]
    public Vector4 Collision;

    [FieldOffset(128)]
    public Vector4 Bounds;

    [FieldOffset(144)]
    public Int4 Material;
}