﻿using System.Runtime.InteropServices;
using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Helpers;
using AncientEmpires.Engine.Render.Interface;
using SharpGen.Runtime;
using Vortice.Direct2D1;
using Vortice.Direct3D;
using Vortice.Direct3D11;
using Vortice.Direct3D11.Debug;
using Vortice.Direct3D12;
using Vortice.Direct3D12.Debug;
using Vortice.DirectWrite;
using Vortice.DXGI;
using Vortice.Mathematics;
using static Vortice.Direct3D11.DeviceCreationFlags;
using FactoryType = Vortice.Direct2D1.FactoryType;
using FeatureLevel = Vortice.Direct3D.FeatureLevel;
using RasterizerDescription = Vortice.Direct3D11.RasterizerDescription;
using ReportLiveDeviceObjectFlags = Vortice.Direct3D11.Debug.ReportLiveDeviceObjectFlags;

namespace AncientEmpires.Engine.Render;

public class GraphicsDevice : EngineComponent
{
    public IGameWindow Window { get; }
    public virtual int Width => (int)Window.GetSurfaceSize().X;
    public virtual int Height => (int)Window.GetSurfaceSize().Y;
    public Rectangle Bounds => Window.GetBounds();
    public ID3D11Device1 D3DDevice;
    public ID3D11DeviceContext1 D3DContext;
    public ID3D11On12Device1 D3D111On12Device;
    public Dictionary<ConstantBufferSlot, ConstantBuffer> ConstantBuffers = new();

    private int CurrentFrameIndex = 0;
    //private bool HasPresented;
    private FeatureLevel FeatureLevel;
    protected bool IsResizing;
    public ID3D12Device1 D3D12Device;
    public ID3D12GraphicsCommandList2 D3D12CommandList;
    public ID3D12CommandQueue D3D12CommandQueue;
    public ID2D1Device2 D2DDevice;
    public ID2D1DeviceContext2 D2DContext;
    public IDWriteFactory DWriteFactory;
    public ID2D1Factory3 D2DFactory;
    public GraphicsPresentation Presentation;
    public RenderTarget RenderTarget;
    public ID3D11RasterizerState SolidRasterizerState;
    public ID3D11RasterizerState WireframeRasterizerState;
    protected Viewport Viewport;

    public GraphicsDevice(IGameWindow window)
    {
        Window = window;
    }

    public override void OnInitialize()
    {
        base.OnInitialize();

        //CreateD3D11On12();
        CreateD3D11();
    }

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();

        Presentation = AddComponent(new GraphicsPresentation(this));
        AddComponent<D3DFactory>();
        AddComponent<BufferPoolManager>();
    }

    private void EnsureSuccessful(Result result)
    {
        if (result.Success)
            return;

        ConsoleWriter.WriteLine("Failed to create: ");
        throw new Exception(result.Description);
    }

    public void SetRenderTarget(RenderTarget target)
    {
        target.SetRenderTarget(D3DContext);
        //target.ClearRenderTarget(D3DContext, 0.2f, 0.2f, 0, 0.2f);
        RenderTarget = target;
    }

    public void StartDraw()
    {
        var renderBuffers = GetComponent<GraphicsPresentation>().RenderBuffers;

        if (D3D111On12Device == null)
        {
            D3DContext.IASetPrimitiveTopology(PrimitiveTopology.TriangleList);
            Presentation.StartDraw();
        }
        else
        {
            var commandAllocator = Presentation.RenderBuffers.CommandAllocators[CurrentFrameIndex];
            /*
            commandAllocator.Reset();
            D3D12CommandList = D3D12Device.CreateCommandList<ID3D12CommandList>(CommandListType.Direct, commandAllocator );
            D3D12CommandList
            var currentBackBufferIndex = GetComponent<GraphicsPresentation>().SwapChain.QueryInterface<IDXGISwapChain3>().CurrentBackBufferIndex;
            D3DContext.OMSetRenderTargets(renderBuffers.RenderTargetViews12[0], renderBuffers.DepthStencilView);
            */
            D3DContext.IASetPrimitiveTopology(PrimitiveTopology.TriangleList);
            //D3DContext.ClearDepthStencilView(renderBuffers.DepthStencilView, DepthStencilClearFlags.Depth | DepthStencilClearFlags.Stencil, 1, 0);
            //D3DContext.ClearRenderTargetView(renderBuffers.RenderTargetViews11[0], new Color4(0, 0, 0f, 0.1f));
        }
    }

    public void Present()
    {
        GetComponent<GraphicsPresentation>().Present();
    }

    public override void OnReleaseResources()
    {
        if (D3DDevice == null)
            return;

        ClearShaderResources();
        ClearConstantBuffers();

        ConstantBuffers?.Clear();

        var debugInterface = D3DDevice.QueryInterface<ID3D11Debug>();
        debugInterface.ReportLiveDeviceObjects(ReportLiveDeviceObjectFlags.Detail);
        debugInterface.Dispose();

        Dispose();
    }

    public virtual Viewport GetViewport() => Presentation.Viewport;

    public virtual void SetVertexBuffer(GameGeometryBuffer geometryBuffer)
    {
        D3DContext.IASetIndexBuffer(geometryBuffer.IndexBuffer, Format.R32_UInt, 0);
        D3DContext.IASetVertexBuffer(0, geometryBuffer.VertexBuffer, Marshal.SizeOf<Vertex>());
    }

    public void SetConstantBuffer(ConstantBufferSlot slot, ConstantBuffer constantBuffer)
    {
        ConstantBuffers[slot] = constantBuffer;
    }

    public void Draw(GameRenderable renderable)
    {
        if (renderable.IsEmpty)
            return;

        if (renderable is DeferredLayer || renderable is IInterfaceRenderable)
        {
            var constantBuffers = Engine.GetComponent<InterfaceConstantBuffers>();
            constantBuffers.OnBeforeDraw();
        }
        else
        {
            var constantBuffers = Engine.GetComponent<WorldConstantBuffers>();
            constantBuffers.OnBeforeDraw();
        }

        var gameDebug = Engine.GetComponent<GameDebug>();
        var shader = gameDebug.EnableWireframe && renderable is not IDeferredRenderable
            ? renderable.WireframeShader
            : renderable.Shader;
        shader.OnBeforeDraw(renderable);

        ClearShaderResources();

        var renderTarget = renderable.RenderTarget ?? RenderTarget;
        if (!ReferenceEquals(renderTarget, Presentation.RenderBuffers.BackBufferTarget))
        {
            renderTarget.ClearRenderTarget(D3DContext, Color4.Transparent);
        }
        else
        {
        }

        SetRenderTarget(renderTarget);
        SetVertexBuffer(renderable.GeometryBuffer);

        D3DContext.PSSetShaderResources(0, renderable.ShaderResources.Length, renderable.ShaderResources);
        D3DContext.VSSetShader(shader.VertexShader);
        D3DContext.PSSetShader(shader.PixelShader);
        D3DContext.PSSetSampler(0, shader.SampleState);
        D3DContext.OMSetBlendState(shader.Blend);

        foreach (var constantBuffer in ConstantBuffers)
        {
            constantBuffer.Value.MapAndWrite();
            D3DContext.PSSetConstantBuffer((int)constantBuffer.Key, constantBuffer.Value.Buffer);
            D3DContext.VSSetConstantBuffer((int)constantBuffer.Key, constantBuffer.Value.Buffer);
        }

        Engine.Timing.GameMetrics.MetricDrawCalls++;
        Engine.Timing.GameMetrics.MetricVerticesDrawn += renderable.GeometryBuffer.VertexCount;

        if (renderable.GeometryBuffer.IndexBuffer == null || renderable.GeometryBuffer.IndexBuffer.ToString().Contains("NULL"))
            return;

        //var indexCount = Math.Min(DateTime.Now.Millisecond * 3, renderable.GeometryBuffer.IndexCount);
        //D3DContext.DrawIndexed(indexCount, renderable.GeometryBuffer.IndexOffset, 0);
        D3DContext.DrawIndexed(renderable.GeometryBuffer.IndexCount, renderable.GeometryBuffer.IndexOffset, 0);

        SetRenderTarget(Presentation.RenderBuffers.BackBufferTarget);
    }

    public void ClearShaderResources()
    {
        D3DContext.PSSetSampler(0, null);
        D3DContext.PSSetShaderResources(0,
            ID3D11DeviceContext.CommonShaderInputResourceSlotCount,
            Enumerable.Repeat<ID3D11ShaderResourceView>(null,
                ID3D11DeviceContext.CommonShaderInputResourceSlotCount).ToArray());
        D3DContext.VSSetShaderResources(0,
            ID3D11DeviceContext.CommonShaderInputResourceSlotCount,
            Enumerable.Repeat<ID3D11ShaderResourceView>(null,
                ID3D11DeviceContext.CommonShaderInputResourceSlotCount).ToArray());
    }

    public void ClearConstantBuffers()
    {
        D3DContext.PSSetConstantBuffers(0,
            ID3D11DeviceContext.CommonShaderConstantBufferSlotCount,
            Enumerable.Repeat<ID3D11Buffer>(null,
                ID3D11DeviceContext.CommonShaderConstantBufferSlotCount).ToArray());
        D3DContext.PSSetConstantBuffers(0,
            ID3D11DeviceContext.CommonShaderConstantBufferSlotCount,
            Enumerable.Repeat<ID3D11Buffer>(null,
                ID3D11DeviceContext.CommonShaderConstantBufferSlotCount).ToArray());
    }

    public virtual SamplerStateCollection CreateSamplerCollection()
    {
        return new SamplerStateCollection(D3DDevice);
    }

    public void CreateD3D11()
    {
        var featureLevels = new[]
        {
            FeatureLevel.Level_11_1,
            FeatureLevel.Level_11_0
        };
        var result = D3D11.D3D11CreateDevice(IntPtr.Zero, DriverType.Hardware, BgraSupport | Debug, featureLevels, out var device);

        if (!result.Success || device == null)
        {
            ConsoleWriter.WriteLine("Failed to create D3D11 device. Trying to create D3D11 device with FeatureLevel.Level_11_1...");
            throw new Exception(result.Description);
        }

        D3DDevice = device.QueryInterface<ID3D11Device1>();
        D3DContext = D3DDevice.ImmediateContext.QueryInterface<ID3D11DeviceContext1>();
        D3DDevice.DebugName = "D3DDevice";
        D3DContext.DebugName = "D3DContext";
        FeatureLevel = device.FeatureLevel;

        SolidRasterizerState = device.CreateRasterizerState(RasterizerDescription.Cullback).Collect();
        WireframeRasterizerState = device.CreateRasterizerState(RasterizerDescription.Wireframe).Collect();

        CreateD2D(D3DDevice);
    }

    private void CreateD3D11On12()
    {
        EnsureSuccessful(D3D12.D3D12GetDebugInterface<ID3D12Debug>(out var d3d12Debug));
        d3d12Debug?.EnableDebugLayer();
        d3d12Debug?.Release();
        EnsureSuccessful(D3D12.D3D12CreateDevice(IntPtr.Zero, FeatureLevel.Level_11_0, out D3D12Device));

        try
        {
            D3D12CommandQueue = D3D12Device.CreateCommandQueue<ID3D12CommandQueue>(CommandListType.Direct);
            var result = D3D11.D3D11On12CreateDevice(
                D3D12Device,
                BgraSupport | Debug,
                new[] { FeatureLevel.Level_11_1 },
                new[] { D3D12CommandQueue },
                0,
                out var device,
                out var context,
                out FeatureLevel);

            if (!result.Success || device == null)
                throw new Exception(result.Description);

            D3DDevice = device.QueryInterface<ID3D11Device1>();
            D3DContext = context.QueryInterface<ID3D11DeviceContext1>();
            D3D111On12Device = D3DDevice.QueryInterface<ID3D11On12Device1>();

            CreateD2D(D3D111On12Device);
        }
        catch
        {
            ConsoleWriter.WriteLine("Failed to create D3D11On12 device");
            ConsoleWriter.WriteLine(D3D12Device.DeviceRemovedReason);
            Dispose();
        }
    }

    public void CreateD2D(ComObject device)
    {
        D2DFactory = D2D1.D2D1CreateFactory<ID2D1Factory3>(FactoryType.SingleThreaded, new FactoryOptions
        {
            DebugLevel = DebugLevel.Information,
        });

        using var dxgiDevice = device.QueryInterface<IDXGIDevice>();
        D2DDevice = D2DFactory.CreateDevice(dxgiDevice);
        D2DContext = D2DDevice.CreateDeviceContext(DeviceContextOptions.None);
        DWriteFactory = DWrite.DWriteCreateFactory<IDWriteFactory>();
    }

    public override void Dispose()
    {
        base.Dispose();

        D3DContext?.Flush();

        if (D3DDevice != null)
        {
            var dxgiDebug = D3DDevice.QueryInterface<ID3D11Debug>();
            dxgiDebug.ReportLiveDeviceObjects(ReportLiveDeviceObjectFlags.Detail);
            dxgiDebug.Dispose();
        }

        D2DContext.Target = null;
        GameManager.DisposeCollector.RemoveAndDispose(ref DWriteFactory);
        GameManager.DisposeCollector.RemoveAndDispose(ref D2DContext);
        GameManager.DisposeCollector.RemoveAndDispose(ref D3DContext);
        GameManager.DisposeCollector.RemoveAndDispose(ref D3DDevice);

        GameManager.DisposeCollector.RemoveAndDispose(ref D3D12CommandList);
        GameManager.DisposeCollector.RemoveAndDispose(ref D3D12CommandQueue);
        GameManager.DisposeCollector.RemoveAndDispose(ref D3D12Device);
    }
}