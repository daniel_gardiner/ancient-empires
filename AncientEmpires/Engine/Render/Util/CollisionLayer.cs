using System.Numerics;
using AncientEmpires.Engine.Actions;
using AncientEmpires.Engine.Data;
using AncientEmpires.Engine.Entities;
using AncientEmpires.Engine.Entities.Units.Physics;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Map.Rendering;
using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Groups;
using AncientEmpires.Engine.Render.Interface;
using AncientEmpires.Engine.Render.Shaders;
using MoreLinq.Extensions;
using Vortice.Mathematics;
using Vortice.Direct2D1;
using Vortice.DirectWrite;
using FontStyle = Vortice.DirectWrite.FontStyle;
using Color4 = Vortice.Mathematics.Color4;
using BoundingSphere = Vortice.Mathematics.BoundingSphere;
using BoundingBox = Vortice.Mathematics.BoundingBox;
using Color = System.Drawing.Color;
using ContainmentType = Vortice.Mathematics.ContainmentType;
using Ellipse = Vortice.Direct2D1.Ellipse;

namespace AncientEmpires.Engine.Render.Util;

public class CollisionLayer : GameLayer, IWorldRenderable
{
    protected ID2D1Bitmap1 D2DRenderTarget;
    protected IDWriteTextFormat TextFormat;
    protected ID2D1SolidColorBrush MapQuadRectBrush;
    protected ID2D1SolidColorBrush WorldQuadRectBrush;
    protected ID2D1SolidColorBrush RedBrush;
    protected ID2D1SolidColorBrush BlueBrush;
    protected ID2D1SolidColorBrush YellowBrush;
    protected ID2D1StrokeStyle StrokeStyle;
    protected int LastCleanSlice;
    protected IDictionary<RectangleF, ID2D1GeometryRealization> CachedRectangles = new Dictionary<RectangleF, ID2D1GeometryRealization>();
    protected IDictionary<Ellipse, ID2D1GeometryRealization> CachedEllipses = new Dictionary<Ellipse, ID2D1GeometryRealization>();
    protected ComponentLookup<GameDebug> GameDebug;
    protected ComponentLookup<EntityManager> EntityManager;
    protected ComponentLookup<IsometricPositionStrategy> PositionStrategy;

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();

        Shader = AddComponent<StandardShader>();
    }

    public override void OnReleaseResources()
    {
        base.OnReleaseResources();
        Dispose();
    }

    public override void OnInitialize()
    {
        base.OnInitialize();

        ZIndex = Engine.Configuration.GetRenderLayer(KnownRenderLayers.Map) - 1f;
        GameDebug = Engine.ComponentLookup<GameDebug>();
        EntityManager = Engine.ComponentLookup<EntityManager>();
        PositionStrategy = Engine.ComponentLookup<IsometricPositionStrategy>();
    }

    public override void OnEngineReady()
    {
        base.OnEngineReady();

        D2DRenderTarget ??= RenderBuffers.Value.CreateD2DRenderTarget(RenderTarget.RenderTargetTexture).Collect();
        RenderTarget ??= RenderBuffers.Value.CreateTextureRenderTarget(nameof(CollisionLayer));
        TextFormat = GraphicsDevice.Value.DWriteFactory.CreateTextFormat("Consolas", FontWeight.Normal, FontStyle.Normal, FontStretch.Normal, 20f).Collect();
        MapQuadRectBrush = GraphicsDevice.Value.D2DContext.CreateSolidColorBrush(new Color4(Color.FromArgb(200, 100, 255, 100)), new BrushProperties(1)).Collect();
        WorldQuadRectBrush = GraphicsDevice.Value.D2DContext.CreateSolidColorBrush(new Color4(Color.FromArgb(200, 50, 155, 50)), new BrushProperties(1)).Collect();
        BlueBrush = GraphicsDevice.Value.D2DContext.CreateSolidColorBrush(new Color4(Color.FromArgb(200, 13, 89, 171)), new BrushProperties(1)).Collect();
        YellowBrush = GraphicsDevice.Value.D2DContext.CreateSolidColorBrush(new Color4(Color.FromArgb(200, 214, 202, 24)), new BrushProperties(1)).Collect();
        RedBrush = GraphicsDevice.Value.D2DContext.CreateSolidColorBrush(new Color4(Color.FromArgb(200, 150, 50, 50)), new BrushProperties(1)).Collect();
        StrokeStyle = GraphicsDevice.Value.D2DFactory.CreateStrokeStyle(new StrokeStyleProperties()).Collect();
    }

    public override void OnNextFrame(float timeDelta)
    {
        base.OnNextFrame(timeDelta);
    }

    public override void OnDraw()
    {
        if (!GameDebug.Value.Enabled)
        {
            GraphicsDevice.Value.D2DContext.BeginDraw();
            GraphicsDevice.Value.D2DContext.Clear(Color.Transparent);
            GraphicsDevice.Value.D2DContext.EndDraw();
            return;
        }

        if (!Changed && !Engine.Timing.IsNewFrame)
            return;

        GraphicsDevice.Value.D2DContext.BeginDraw();
        GraphicsDevice.Value.D2DContext.Clear(Color.Transparent);
        GraphicsDevice.Value.D2DContext.Target = D2DRenderTarget;

        var entityLookup = Engine.GetComponent<EntityLookup>();
        if (entityLookup == null)
            return;

        var viewportBox = Engine.Camera.GetBoundingBox();
        DrawMapQuadRects(entityLookup);
        DrawWorldQuadRects(entityLookup);
        DrawSelectionRect();
        DrawEntityCollisions();

        GraphicsDevice.Value.D2DContext.EndDraw();
        MarkClean();
    }

    private void DrawSelectionRect()
    {
        GraphicsDevice.Value.D2DContext.Transform =
            //Matrix3x2.CreateRotation(MathHelper.ToRadians(224)) *
            //Matrix3x2.CreateScale(1, 0.5f) *
            Matrix3x2.CreateTranslation(-Engine.Camera.CurrentScroll.X, -Engine.Camera.CurrentScroll.Y) *
            Matrix3x2.CreateScale(Engine.Camera.GetZoomFactor()) *
            Matrix3x2.CreateTranslation(Engine.ScreenSize / 2);

        var dragSelectEntities = Engine.GetComponent<DragSelectEntities>();
        if (dragSelectEntities.IsActive)
        {
            var origin = dragSelectEntities.Origin;
            var current = dragSelectEntities.Current;

            DrawEllipse(RedBrush, origin.WorldSpace * new Vector4(1, -1, 0, 0), 20);
            DrawEllipse(RedBrush, current.WorldSpace * new Vector4(1, -1, 0, 0), 20);

            var minX = Math.Min(origin.WorldSpace.X, current.WorldSpace.X);
            var minY = Math.Min(origin.WorldSpace.Y, current.WorldSpace.Y);
            var maxX = Math.Max(origin.WorldSpace.X, current.WorldSpace.X);
            var maxY = Math.Max(origin.WorldSpace.Y, current.WorldSpace.Y);
            DrawRectangleGeometry(YellowBrush, new RectangleF(
                minX,
                -minY,
                maxX - minX,
                -(maxY - minY)));
        }
    }

    private void DrawEllipse(ID2D1SolidColorBrush brush, Vector4 worldSpace, int radiusX)
    {
        var sphereCenter = new PointF(worldSpace.X, worldSpace.Y);
        var ellipse = new Ellipse(sphereCenter, radiusX, radiusX);
        DrawEllipseGeometry(brush, ellipse);
    }

    private void DrawMapQuadRects(EntityLookup entityLookup)
    {
        var tilePixelSize = Engine.Configuration.TilePixelSize;

        GraphicsDevice.Value.D2DContext.Transform = Matrix3x2.Identity;
        //Matrix3x2.CreateRotation(MathHelper.ToRadians(45f)) *
        //Matrix3x2.CreateScale((float)(1 / Math.Sqrt(2))) *
        //Matrix3x2.CreateTranslation(Engine.Camera.CurrentScroll.X, Engine.Camera.CurrentScroll.Y) *
        //Matrix3x2.CreateScale(Engine.Camera.GetZoomFactor()) //*
        //Matrix3x2.CreateTranslation(Engine.ScreenSize / 2)
        ;

        foreach (var quads in entityLookup.MapTree.GetGrid())
        {
            var topLeft = new Vector4(quads.X, quads.Y, 0, 0);
            var bottomRight = new Vector4(quads.X + quads.Width, quads.Y + quads.Height, 0, 0);
            var worldTopLeft = Engine.Camera.MapSpaceToScreenSpace(topLeft);
            var worldBottomRight = Engine.Camera.MapSpaceToScreenSpace(bottomRight);
            DrawEllipse(RedBrush, worldTopLeft * new Vector4(1, -1, 0, 0), 20);
            DrawEllipse(BlueBrush, worldBottomRight * new Vector4(1, -1, 0, 0), 20);
        }

        GraphicsDevice.Value.D2DContext.Transform =
            Matrix3x2.CreateTranslation(-PositionStrategy.Value.HalfProjectedMapSize.ToVector2()) *
            Matrix3x2.CreateScale(1, 2f) *
            Matrix3x2.CreateRotation(MathHelper.ToRadians(45f)) *
            Matrix3x2.CreateScale(1, 0.5f) *
            Matrix3x2.CreateScale((float)(1 / Math.Sqrt(2))) *
            Matrix3x2.CreateTranslation(-Engine.Camera.CurrentScroll.X, -Engine.Camera.CurrentScroll.Y) *
            Matrix3x2.CreateScale(Engine.Camera.GetZoomFactor()) *
            Matrix3x2.CreateTranslation(Engine.ScreenSize / 2)
            ;

        foreach (var quads in entityLookup.MapTree.GetGrid())
        {
            var vector1 = tilePixelSize * new Vector4(quads.X, quads.Y, 0, 0);
            var vector2 = tilePixelSize * new Vector4(quads.X, quads.Y + quads.Height, 0, 0);
            var vector3 = tilePixelSize * new Vector4(quads.X + quads.Width, quads.Y, 0, 0);
            var vector4 = tilePixelSize * new Vector4(quads.X + quads.Width, quads.Y + quads.Height, 0, 0);

            var minX = Math.Min(Math.Min(Math.Min(vector1.X, vector2.X), vector3.X), vector4.X);
            var minY = Math.Min(Math.Min(Math.Min(vector1.Y, vector2.Y), vector3.Y), vector4.Y);
            var maxX = Math.Max(Math.Max(Math.Max(vector1.X, vector2.X), vector3.X), vector4.X);
            var maxY = Math.Max(Math.Max(Math.Max(vector1.Y, vector2.Y), vector3.Y), vector4.Y);

            var rectangle = new RectangleF(
                minX,
                minY,
                maxX - minX,
                maxY - minY);
            DrawRectangleGeometry(MapQuadRectBrush, rectangle);
        }
    }

    private void DrawWorldQuadRects(EntityLookup entityLookup)
    {
        var tilePixelSize = Engine.Configuration.TilePixelSize;

        GraphicsDevice.Value.D2DContext.Transform = Matrix3x2.Identity;
        //Matrix3x2.CreateRotation(MathHelper.ToRadians(45f)) *
        //Matrix3x2.CreateScale((float)(1 / Math.Sqrt(2))) *
        //Matrix3x2.CreateTranslation(Engine.Camera.CurrentScroll.X, Engine.Camera.CurrentScroll.Y) *
        //Matrix3x2.CreateScale(Engine.Camera.GetZoomFactor()) //*
        //Matrix3x2.CreateTranslation(Engine.ScreenSize / 2)
        ;

        var quadTreeRects = entityLookup.WorldTree.GetGrid();
        foreach (var quads in quadTreeRects)
        {
            var worldTopLeft = Engine.Camera.WorldSpaceToScreenSpace(new Vector4(quads.X, quads.Y, 0, 0));
            var worldBottomRight = Engine.Camera.WorldSpaceToScreenSpace(new Vector4(quads.X + quads.Width, quads.Y + quads.Height, 0, 0));
            DrawEllipse(RedBrush, worldTopLeft * new Vector4(1, -1, 0, 0), 20);
            DrawEllipse(BlueBrush, worldBottomRight * new Vector4(1, -1, 0, 0), 20);
        }

        GraphicsDevice.Value.D2DContext.Transform =
            Matrix3x2.Identity;
            //-Matrix3x2.CreateTranslation(Engine.ScreenSize / 2);
        //Matrix3x2.CreateScale(Engine.Camera.GetZoomFactor());

        foreach (var quads in quadTreeRects)
        {
            var topLeft = Engine.Camera.WorldSpaceToScreenSpace(new Vector4(quads.X, quads.Y, 0, 0)) * new Vector4(1, -1, 0, 0);
            var bottomRight = Engine.Camera.WorldSpaceToScreenSpace(new Vector4(quads.X + quads.Width, quads.Y + quads.Height, 0, 0)) * new Vector4(1, -1, 0, 0);

            var rectangle = new RectangleF(
                topLeft.X,
                topLeft.Y,
                bottomRight.X,
                bottomRight.Y);
            DrawRectangleGeometry(WorldQuadRectBrush, rectangle);
        }
    }

    private void DrawEntityCollisions()
    {
        GraphicsDevice.Value.D2DContext.Transform =
            Matrix3x2.CreateTranslation(-Engine.Camera.CurrentScroll.X, -Engine.Camera.CurrentScroll.Y) *
            Matrix3x2.CreateScale(Engine.Camera.GetZoomFactor()) *
            Matrix3x2.CreateTranslation(Engine.ScreenSize / 2)
            ;

        var entities = EntityManager.Value.GetAllEntities();

        foreach (var entity in entities)
        {
            if (EntityManager.Value.LastSearchResults.Contains(entity) && Engine.Timing.Slice % 2 == 0)
                continue;

            var collisionShapes = entity.Collision.GetCollisionShapes();
            var selectionShapes = entity.Collision.GetSelectionShapes();

            DrawShapes(entity, collisionShapes, BlueBrush);
            DrawShapes(entity, selectionShapes, YellowBrush);
        }
    }

    private void DrawShapes(Entity entity, CollisionShapes collisionShapes, ID2D1SolidColorBrush brush)
    {
        foreach (var shape in collisionShapes)
        {
            switch (shape)
            {
                case SphereCollisionShape sphere:
                    DrawUnitSphere(sphere.GetWorldSpaceSphere(entity.Position), brush);
                    break;

                case BoxCollisionShape box:
                    DrawUnitRectangle(box.GetWorldSpaceBox(), brush);
                    break;
            }
        }
    }

    private void DrawUnitRectangle(BoundingBox box, ID2D1SolidColorBrush brush)
    {
        var selectionRect = new RectangleF(
            box.Minimum.X,
            -box.Minimum.Y,
            box.Maximum.X - box.Minimum.X,
            -(box.Maximum.Y - box.Minimum.Y));
        DrawRectangleGeometry(brush, selectionRect);
    }

    private void DrawUnitSphere(BoundingSphere sphere, ID2D1SolidColorBrush brush)
    {
        var sphereCenter = new PointF(sphere.Center.X, -sphere.Center.Y);
        var ellipse = new Ellipse(
            sphereCenter,
            sphere.Radius,
            sphere.Radius);

        DrawEllipseGeometry(brush, ellipse);
    }

    private void DrawEllipseGeometry(ID2D1SolidColorBrush brush, Ellipse ellipse)
    {
        var d2DContext = GraphicsDevice.Value.D2DContext;
        if (!CachedEllipses.TryGetValue(ellipse, out var realization))
        {
            using var ellipseGeometry = d2DContext.Factory.CreateEllipseGeometry(ellipse);
            realization = d2DContext.CreateFilledGeometryRealization(ellipseGeometry, 1f).Collect();
            CachedEllipses.Add(ellipse, realization);
        }

        d2DContext.DrawGeometryRealization(realization, brush);
    }

    private void DrawRectangleGeometry(ID2D1SolidColorBrush brush, RectangleF rectangle)
    {
        var d2DContext = GraphicsDevice.Value.D2DContext;
        if (!CachedRectangles.TryGetValue(rectangle, out var realization))
        {
            using var rectangleGeometry = d2DContext.Factory.CreateRectangleGeometry(rectangle);
            realization = d2DContext.CreateStrokedGeometryRealization(rectangleGeometry, 96f, 8f, StrokeStyle).Collect();
            CachedRectangles.Add(rectangle, realization);
        }

        d2DContext.DrawGeometryRealization(realization, brush);
    }

    private static bool IsOutsideViewport(Vector3 topLeft, Vector3 bottomRight, BoundingBox viewportBox)
    {
        var boundingBox = new BoundingBox(topLeft, bottomRight);
        var boxContainsPoint = viewportBox.Contains(boundingBox);
        var result = boxContainsPoint == ContainmentType.Disjoint;
        return result;
    }

    public override bool CanDraw() => false;

    public override void MarkDirty()
    {
        base.MarkDirty();
    }

    public override void MarkClean()
    {
        base.MarkClean();
        LastCleanSlice = Engine.Timing.Slice;
    }

    public override void Dispose()
    {
        base.Dispose();

        CachedRectangles?.ForEach(o => o.Value.Dispose());
        CachedEllipses?.ForEach(o => o.Value.Dispose());
        CachedRectangles?.Clear();
        CachedEllipses?.Clear();
        RenderTarget?.Dispose();
        RenderTarget = null;
        D2DRenderTarget?.Dispose();
        D2DRenderTarget = null;
        TextFormat?.Dispose();
        TextFormat = null;
        MapQuadRectBrush?.Dispose();
        MapQuadRectBrush = null;
        BlueBrush?.Dispose();
        BlueBrush = null;
        YellowBrush?.Dispose();
        YellowBrush = null;
        RedBrush?.Dispose();
        RedBrush = null;
        StrokeStyle?.Dispose();
        StrokeStyle = null;
    }
}