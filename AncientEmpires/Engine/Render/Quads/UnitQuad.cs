﻿using System.Numerics;
using AncientEmpires.Engine.Entities;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Render.Quads;

public class UnitQuad : Quad
{
    protected static Vector4? TexelSize;

    public UnitQuad(Entity entity, float depth, Vector4 quadSize, Sprite sprite, ShaderMaterial shaderMaterial) : base(entity.DebugColor, shaderMaterial)
    {
        TexelSize ??= new Vector4(1f / 4096, 1f / 4096, 0, 0);

        var padding = new Vector4(0, 0, 0, 0);
        var spriteTexelPosition = sprite.Offset * TexelSize.Value;
        var spriteTexelSize = (sprite.Size + padding) * TexelSize.Value;

        var topLeft = TopLeft * quadSize + padding;
        var topRight = TopRight * quadSize + padding;
        var bottomRight = BottomRight * quadSize + padding;
        var bottomLeft = BottomLeft * quadSize + padding;

        Vertex1.Position = topLeft + new Vector4(0, 0, depth, 0);
        Vertex2.Position = topRight + new Vector4(0, 0, depth, 0);
        Vertex3.Position = bottomRight + new Vector4(0, 0, depth, 0);
        Vertex4.Position = bottomLeft + new Vector4(0, 0, depth, 0);
            
        Vertex1.Tile = new Vector4(entity.Position.MapSpace.X, entity.Position.MapSpace.Y, quadSize.X, quadSize.Y);
        Vertex2.Tile = Vertex1.Tile;
        Vertex3.Tile = Vertex1.Tile;
        Vertex4.Tile = Vertex1.Tile;

        var entityTypeId = entity.GetEntityTypeId();
        Vertex1.EntityId = new Int4(entity.Id, entityTypeId, entity.Id, entity.Id);
        Vertex2.EntityId = new Int4(entity.Id, entityTypeId, entity.Id, entity.Id);
        Vertex3.EntityId = new Int4(entity.Id, entityTypeId, entity.Id, entity.Id);
        Vertex4.EntityId = new Int4(entity.Id, entityTypeId, entity.Id, entity.Id);

        Vertex1.Color = entity.DebugColor;
        Vertex2.Color = entity.DebugColor;
        Vertex3.Color = entity.DebugColor;
        Vertex4.Color = entity.DebugColor;

        if (sprite.TextureIndexes.Count > 0)
        {
            var textureIndex0 = sprite.TextureIndexes[0];
            Vertex1.UV0 = ToVector4(UvTopLeft * spriteTexelSize, textureIndex0);
            Vertex2.UV0 = ToVector4(UvTopRight * spriteTexelSize, textureIndex0);
            Vertex3.UV0 = ToVector4(UvBottomRight * spriteTexelSize, textureIndex0);
            Vertex4.UV0 = ToVector4(UvBottomLeft * spriteTexelSize, textureIndex0);

            var textureIndex1 = sprite.TextureIndexes.Count > 1
                ? sprite.TextureIndexes[1]
                : textureIndex0;
            Vertex1.UV1 = ToVector4(UvTopLeft, textureIndex1);
            Vertex2.UV1 = ToVector4(UvTopRight, textureIndex1);
            Vertex3.UV1 = ToVector4(UvBottomRight, textureIndex1);
            Vertex4.UV1 = ToVector4(UvBottomLeft, textureIndex1);
        }

        Vector4 ToVector4(Vector4 vector, int textureIndex0)
        {
            return new Vector4(vector.X, vector.Y, textureIndex0, 0);
        }
    }
}