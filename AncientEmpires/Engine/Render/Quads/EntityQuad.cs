﻿using System.Numerics;
using AncientEmpires.Engine.Entities;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Render.Quads;

public class EntityQuad : TexturedQuad
{
    public EntityQuad(Entity entity, GamePosition quadPosition, Vector4 quadSize, Sprite sprite, ShaderMaterial shaderMaterial) : base(entity.Position, in quadPosition, in quadSize, in sprite, shaderMaterial)
    {
        Vertex1.Tile = new Vector4(entity.Position.MapSpace.X, entity.Position.MapSpace.Y, quadSize.X, quadSize.Y);
        Vertex2.Tile = Vertex1.Tile;
        Vertex3.Tile = Vertex1.Tile;
        Vertex4.Tile = Vertex1.Tile;
            
        var entityTypeId = entity.GetEntityTypeId();
        Vertex1.EntityId = new Int4(entity.Id, entityTypeId, entity.Id, entity.Id);
        Vertex2.EntityId = new Int4(entity.Id, entityTypeId, entity.Id, entity.Id);
        Vertex3.EntityId = new Int4(entity.Id, entityTypeId, entity.Id, entity.Id);
        Vertex4.EntityId = new Int4(entity.Id, entityTypeId, entity.Id, entity.Id);

        Vertex1.Color = entity.DebugColor;
        Vertex2.Color = entity.DebugColor;
        Vertex3.Color = entity.DebugColor;
        Vertex4.Color = entity.DebugColor;
    }
}