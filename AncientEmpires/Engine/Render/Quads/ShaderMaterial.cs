namespace AncientEmpires.Engine.Render.Quads;

public enum ShaderMaterial
{
    Default = 0,
    Transparent = 10,
    Interface = 70,
    InterfaceButton = 71,
    InterfacePanel = 72,
    InterfaceText = 73,
    DragSelectionAction = 80,
    Entity = 100,
    Texture = 300
}