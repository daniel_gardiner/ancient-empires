﻿using System.Numerics;

namespace AncientEmpires.Engine.Render.Quads;

public class TexturedQuad : Quad
{
    protected static Vector4? TexelSize;

    public TexturedQuad(in GamePosition position, in GamePosition quadPosition, in Vector4 quadSize, in Sprite sprite, ShaderMaterial shaderMaterial) : this(position, quadPosition, quadSize, sprite, Vector4.Zero, shaderMaterial)
    {
    }

    public TexturedQuad(in GamePosition position, in GamePosition quadPosition, in Vector4 quadSize, in Sprite sprite, in Vector4 padding, ShaderMaterial shaderMaterial)
        : base(sprite?.Color ?? new Vector4(0.8f, 0.8f, 1, 0.1f), shaderMaterial)
    {
        TexelSize ??= GameContext.Instance.Engine.Configuration.TexelSize;

        var spriteTexelPosition = sprite.Offset * TexelSize.Value;
        var spriteTexelSize = (sprite.Size + padding) * TexelSize.Value;

        var topLeft = TopLeft * quadSize + padding;
        var topRight = TopRight * quadSize + padding;
        var bottomRight = BottomRight * quadSize + padding;
        var bottomLeft = BottomLeft * quadSize + padding;

        Vertex1.Position = quadPosition.MapSpace + topLeft;
        Vertex2.Position = quadPosition.MapSpace + topRight;
        Vertex3.Position = quadPosition.MapSpace + bottomRight;
        Vertex4.Position = quadPosition.MapSpace + bottomLeft;

        Vertex1.Tile = new Vector4(position.MapSpace.X, position.MapSpace.Y, quadSize.X, quadSize.Y);
        Vertex2.Tile = Vertex1.Tile;
        Vertex3.Tile = Vertex1.Tile;
        Vertex4.Tile = Vertex1.Tile;

        if (sprite.TextureIndexes.Count > 0)
        {
            var textureIndex0 = sprite.TextureIndexes[0];
            var uvPadding = 0;
            Vertex1.UV0 = ToVector4(spriteTexelPosition + UvTopLeft * spriteTexelSize - (UvTopLeft * uvPadding * TexelSize.Value * new Vector4(1, 0.5f, 0, 0)), textureIndex0);
            Vertex2.UV0 = ToVector4(spriteTexelPosition + UvTopRight * spriteTexelSize - (UvTopRight * uvPadding * TexelSize.Value * new Vector4(1, 0.5f, 0, 0)), textureIndex0);
            Vertex3.UV0 = ToVector4(spriteTexelPosition + UvBottomRight * spriteTexelSize - (UvBottomRight * uvPadding * TexelSize.Value * new Vector4(1, 0.5f, 0, 0)), textureIndex0);
            Vertex4.UV0 = ToVector4(spriteTexelPosition + UvBottomLeft * spriteTexelSize - (UvBottomLeft * uvPadding * TexelSize.Value * new Vector4(1, 0.5f, 0, 0)), textureIndex0);

            var textureIndex1 = sprite.TextureIndexes.Count > 1
                ? sprite.TextureIndexes[1]
                : textureIndex0;
            Vertex1.UV1 = ToVector4(UvTopLeft, textureIndex1);
            Vertex2.UV1 = ToVector4(UvTopRight, textureIndex1);
            Vertex3.UV1 = ToVector4(UvBottomRight, textureIndex1);
            Vertex4.UV1 = ToVector4(UvBottomLeft, textureIndex1);
        }

        Vector4 ToVector4(Vector4 vector, int textureIndex0)
        {
            return new Vector4(vector.X, vector.Y, textureIndex0, 0);
        }
    }
}