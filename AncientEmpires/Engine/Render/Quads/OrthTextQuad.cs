using System.Numerics;

namespace AncientEmpires.Engine.Render.Quads;

public class OrthoTextQuad : TextQuad
{
    public OrthoTextQuad(Vector4 quadPosition, Vector4 quadSize, Sprite sprite, Texture[] textures) : base(quadPosition, quadSize, sprite, textures)
    {
        var topLeft = new Vector4(0, 0, 0, 0);
        var topRight = new Vector4(quadSize.X, 0, 0, 0);
        var bottomRight = new Vector4(quadSize.X, quadSize.Y, 0, 0);
        var bottomLeft = new Vector4(0, quadSize.Y, 0, 0);

        Vertex1.Position = quadPosition + topLeft;
        Vertex2 .Position = quadPosition + topRight;
        Vertex3.Position = quadPosition + bottomRight;
        Vertex4.Position = quadPosition + bottomLeft;
    }
}