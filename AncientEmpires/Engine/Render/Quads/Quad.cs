﻿using System.Numerics;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Render.Quads;

public class Quad
{
    private static readonly float _min = -0.5f;
    private static readonly float _max = 0.5f;

    public static readonly Vector4 UvTopLeft = new(0, 0, 0, 0);
    public static readonly Vector4 UvTopRight = new(1, 0, 0, 0);
    public static readonly Vector4 UvBottomRight = new(1, 1, 0, 0);
    public static readonly Vector4 UvBottomLeft = new(0, 1, 0, 0);

    public static readonly Vector4 TopLeft = new(_min, _max, 0, 0);
    public static readonly Vector4 TopRight = new(_max, _max, 0, 0);
    public static readonly Vector4 BottomRight = new(_max, _min, 0, 0);
    public static readonly Vector4 BottomLeft = new(_min, _min, 0, 0);

    private Vertex[] _allVertices;

    public Vertex[] AllVertices => _allVertices ??= new[]
    {
        Vertex1,
        Vertex2,
        Vertex3,
        Vertex4
    };

    public Vertex Vertex1;
    public Vertex Vertex2;
    public Vertex Vertex3;
    public Vertex Vertex4;

    public Quad(Vector4 color, ShaderMaterial shaderMaterial)
    {
        GameContext.Instance.Engine.Timing.GameMetrics.MetricQuadsCreated++;

        Vertex1.Color = color;
        Vertex2.Color = color;
        Vertex3.Color = color;
        Vertex4.Color = color;

        Vertex1.OverlayColor = color;
        Vertex2.OverlayColor = color;
        Vertex3.OverlayColor = color;
        Vertex4.OverlayColor = color;

        Vertex1.Material = new Int4((int)shaderMaterial, 0, 0, 0);
        Vertex2.Material = Vertex1.Material;
        Vertex3.Material = Vertex1.Material;
        Vertex4.Material = Vertex1.Material;
    }

    public ref Vertex this[int index]
    {
        get
        {
            switch (index)
            {
                case 0:
                    return ref Vertex1;
                case 1:
                    return ref Vertex2;
                case 2:
                    return ref Vertex3;
                case 3:
                    return ref Vertex4;
                default:
                    throw new ArgumentOutOfRangeException(nameof(index), "Index must be between 0 and 3");
            }
        }
    }

    public int Length => 4;

    public void SetColor(Vector4 color)
    {
        Vertex1.Color = color;
        Vertex2.Color = color;
        Vertex3.Color = color;
        Vertex4.Color = color;
        Vertex1.OverlayColor = color;
        Vertex2.OverlayColor = color;
        Vertex3.OverlayColor = color;
        Vertex4.OverlayColor = color;
    }

    public void SetZIndex(float zIndex)
    {
        Vertex1.Position.Z = zIndex;
        Vertex2.Position.Z = zIndex;
        Vertex3.Position.Z = zIndex;
        Vertex4.Position.Z = zIndex;
    }
}