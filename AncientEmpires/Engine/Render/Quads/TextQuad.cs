using System.Numerics;

namespace AncientEmpires.Engine.Render.Quads;

public class TextQuad : Quad
{
    protected static Vector4? TexelSize;
    
    public TextQuad(Vector4 quadPosition, Vector4 quadSize, Sprite sprite, Texture[] textures) : base(sprite.Color, ShaderMaterial.Default)
    {
        UpdateVertices(quadPosition, quadSize, sprite, textures);
    }

    private TextQuad UpdateVertices(Vector4 quadPosition, Vector4 quadSize, Sprite sprite, Texture[] textures)
    {
        TexelSize = new Vector4(1f / 512, 1f / 512, 0, 0);

        if (sprite == null)
            return this;
            
        var topRight = TopRight * quadSize;
        var bottomLeft = BottomLeft * quadSize;
        var bottomRight = BottomRight * quadSize;
        var topLeft = TopLeft * quadSize;

        Vertex1.Position = quadPosition + topLeft;
        Vertex2.Position = quadPosition + topRight;
        Vertex3.Position = quadPosition + bottomRight;
        Vertex4.Position = quadPosition + bottomLeft;

        var spriteSize = sprite.Size;
        var texelSize = TexelSize.Value;
        var texelPosition = sprite.Offset * texelSize;

        Vertex1.Color = sprite.Color;
        Vertex2.Color = sprite.Color;
        Vertex3.Color = sprite.Color;
        Vertex4.Color = sprite.Color;

        var textureIndex0 = sprite.TextureIndexes.First();
        Vertex1.UV0 = ToVector4(texelPosition + UvTopLeft * texelSize * spriteSize, textureIndex0);
        Vertex2.UV0 = ToVector4(texelPosition + UvTopRight * texelSize * spriteSize, textureIndex0);
        Vertex3.UV0 = ToVector4(texelPosition + UvBottomRight * texelSize * spriteSize, textureIndex0);
        Vertex4.UV0 = ToVector4(texelPosition + UvBottomLeft * texelSize * spriteSize, textureIndex0);

        var textureIndex1 = sprite.TextureIndexes.Count > 1 ? sprite.TextureIndexes.Skip(1).First() : textureIndex0;
        Vertex1.UV1 = ToVector4(texelPosition + UvTopLeft * texelSize * spriteSize, textureIndex1);
        Vertex2.UV1 = ToVector4(texelPosition + UvTopRight * texelSize * spriteSize, textureIndex1);
        Vertex3.UV1 = ToVector4(texelPosition + UvBottomRight * texelSize * spriteSize, textureIndex1);
        Vertex4.UV1 = ToVector4(texelPosition + UvBottomLeft * texelSize * spriteSize, textureIndex1);
        return this;

        Vector4 ToVector4(Vector4 vector, int textureIndex0)
        {
            return new Vector4(vector.X, vector.Y, textureIndex0, 0);
        }
    }
}