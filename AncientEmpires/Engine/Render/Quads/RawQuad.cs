using System.Numerics;

namespace AncientEmpires.Engine.Render.Quads;

public class RawQuad : Quad
{
    public RawQuad(Quad quad) : base(Vector4.Zero, ShaderMaterial.Default)
    {
        Vertex1 = quad.Vertex1;
        Vertex2 = quad.Vertex2;
        Vertex3 = quad.Vertex3;
        Vertex4 = quad.Vertex4;
    }
}