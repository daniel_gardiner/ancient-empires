using System.Numerics;

namespace AncientEmpires.Engine.Render.Quads;

public class DeferredTextureQuad : TexturedQuad
{
    public static readonly Vector4 OrthoTopLeft = new(0, 1, 0, 0);
    public static readonly Vector4 OrthoTopRight = new(1, 1, 0, 0);
    public static readonly Vector4 OrthoBottomRight = new(1, 0, 0, 0);
    public static readonly Vector4 OrthoBottomLeft = new(0, 0, 0, 0);
    
    public DeferredTextureQuad(Vector4 quadPosition, Vector4 quadSize, Sprite sprite, ShaderMaterial shaderMaterial) : base(Vector4.Zero, quadPosition, quadSize, sprite, shaderMaterial)
    {
        var topLeft = OrthoTopLeft * quadSize;
        var topRight = OrthoTopRight * quadSize;
        var bottomRight = OrthoBottomRight * quadSize;
        var bottomLeft = OrthoBottomLeft * quadSize;

        Vertex4.Position = quadPosition + topLeft;
        Vertex3.Position = quadPosition + topRight;
        Vertex2.Position = quadPosition + bottomRight;
        Vertex1.Position = quadPosition + bottomLeft;

        Vertex1.UV0 = Vertex1.UV1;
        Vertex2.UV0 = Vertex2.UV1;
        Vertex3.UV0 = Vertex3.UV1;
        Vertex4.UV0 = Vertex4.UV1;
    }
}