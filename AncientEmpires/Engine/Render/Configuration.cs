﻿namespace AncientEmpires.Engine.Render;

public class Configuration
{
    public string Title { get; set; }
    public int Width { get; set; }
    public int Height { get; set; }

    public static bool FullScreen { get; private set; }
    public static bool VerticalSyncEnabled { get; private set; }
    public static float ScreenDepth { get; private set; }
    public static float ScreenNear { get; private set; }
    public static string ShaderPath { get; private set; }

    public Configuration(bool fullScreen, bool vSync) : this("Ancient Empires", fullScreen, vSync) { }
    public Configuration(string title, bool fullScreen, bool vSync) : this(title, 800, 600, fullScreen, vSync) { }
    public Configuration(string title, float width, float height, bool fullScreen, bool vSync)
    {
        FullScreen = fullScreen;
        Title = title;
        VerticalSyncEnabled = vSync;

        if (!FullScreen)
        {
            Width = (int) width;
            Height = (int) height;
        }
        else
        {
            Width = Screen.PrimaryScreen.Bounds.Width;
            Height = Screen.PrimaryScreen.Bounds.Height;
        }
    }

    static Configuration()
    {
        VerticalSyncEnabled = true;
        ScreenDepth = 1000.0f;
        ScreenNear = 0.1f;
        ShaderPath = @"Shaders";
    }
}