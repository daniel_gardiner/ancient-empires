using System.Numerics;
using AncientEmpires.Engine.Map.Rendering;
using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Groups;
using AncientEmpires.Engine.Render.Quads;
using AncientEmpires.Engine.Render.Shaders;
using AncientEmpires.Engine.Render.Text;

namespace AncientEmpires.Engine.Render.Interface;

public class InterfaceTextLayer : TextLayer, IInterfaceRenderable
{
    public ComponentLookup<GameTextures> GameTextures;

    public InterfaceTextLayer()
    {
        BatchStrategy = new SequentialMeshBatchStrategy();
    }

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();

        Shader = AddComponent(new TextShader(Font));
    }

    public override void OnInitialize()
    {
        base.OnInitialize();

        GameTextures = new ComponentLookup<GameTextures>();
        ZIndex = Engine.Configuration.GetRenderLayer(KnownRenderLayers.Interface) - 0.5f;
        Font = Engine.GetComponent<FontManager>().GetFont(KnownFont.JetBrains);
        Textures = new[] { Font.Texture };
        TextContext.PositionStrategy = Engine.GetComponent<InterfacePositionStrategy>();
    }

    protected override void PrepareBatches()
    {
        base.PrepareBatches();
    }

    public override void OnDraw()
    {
        base.OnDraw();
    }

    public override TextMeshFragment AddText(string text, GamePosition position, Vector4 offset, Vector4 color, float scale = 0.5f) => base.AddText(text, position, offset, color, scale);
    public override TextMeshFragment AddText(string text, GamePosition position, Vector4 color, float scale = 0.5f) => base.AddText(text, position, color, scale);

    protected override TextQuad QuadFactory(Sprite sprite, Glyph glyph, Vector4 position, float scale)
    {
        sprite.TextureIndexes = GameTextures.Value.FindTextureIndexes(Textures);
        return new OrthoTextQuad(position, new Vector4(glyph.Size, 0, 0) * scale, sprite, Textures);
    }
}