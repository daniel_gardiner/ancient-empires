using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Groups;
using AncientEmpires.Engine.Render.Shaders;

namespace AncientEmpires.Engine.Render.Interface;

public abstract class DeferredLayer : GameLayer, IDeferredRenderable
{
    protected List<RenderableMeshFragment> Fragments;
    protected Dictionary<GameLayer, RenderableMeshFragment> RenderableFragments;

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();

        Shader = AddComponent<StandardShader>();
    }

    public override void OnInitialize()
    {
        base.OnInitialize();
        
        Fragments = new List<RenderableMeshFragment>();
        RenderableFragments = new Dictionary<GameLayer, RenderableMeshFragment>(new Dictionary<GameLayer, RenderableMeshFragment>());
    }

    public override void OnEngineReady()
    {
        base.OnEngineReady();

        RenderTarget = RenderBuffers.Value.BackBufferTarget;
    }

    public override void OnReleaseResources()
    {
        base.OnReleaseResources();

        Fragments.Clear();
        RenderableFragments.Clear();
        ShaderResources = null;

        for (var index = Components.OfType<MeshFragment>().ToList().Count - 1; index >= 0; index--)
        {
            var component = Components.OfType<MeshFragment>().ToList()[index];
            RemoveComponent(component);
        }
    }

    public override void OnNextFrame(float timeDelta)
    {
        base.OnNextFrame(timeDelta);

        CollectRenderLayers();
        SortRenderLayers(timeDelta);
    }

    protected abstract void CollectRenderLayers();

    protected virtual void SortRenderLayers(float timeDelta)
    {
        RenderTarget[] renderTargets = RenderBuffers.Value.ListTextureRenderTargets().ToArray();
        ShaderResources = renderTargets.Select(o => o.ShaderResourceView).ToArray();    
        Fragments.Sort(Comparer<RenderableMeshFragment>.Create((a, b) => b.ZIndex.CompareTo(a.ZIndex)));
        foreach (var fragment in Fragments)
        {
            //fragment.ZIndex = (10 - Array.IndexOf(renderTargets, fragment.Renderable));
            fragment.TextureIndex = Array.IndexOf(renderTargets, fragment.Renderable.RenderTarget);
            fragment.OnUpdateFragment(timeDelta);
        }
    }
    
    public override void OnDraw()
    {
        base.OnDraw();
        
        GeometryBuffer.MapAndWrite(Fragments);
    }
}