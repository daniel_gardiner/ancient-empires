using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Renderers;

namespace AncientEmpires.Engine.Render.Interface;

public class DeferredWorldLayer : DeferredLayer
{
    protected ComponentLookup<GameWorldRenderer> WorldRenderer;
    protected ComponentLookup<GameInterfaceRenderer> InterfaceRenderer;

    public override void OnInitialize()
    {
        base.OnInitialize();
        
        ZIndex = 0.6f;
        WorldRenderer = Engine.ComponentLookup<GameWorldRenderer>();
        InterfaceRenderer = Engine.ComponentLookup<GameInterfaceRenderer>();
    }

    protected override void CollectRenderLayers()
    {
        foreach (var renderable in WorldRenderer.Value.Layers)
        {
            if (!RenderableFragments.ContainsKey(renderable))
            {
                var fragment = RegisterComponent(new RenderableMeshFragment(renderable));
                Fragments.Add(fragment);
                RenderableFragments[renderable] = fragment;
            }
        }
        
        foreach (var renderable in InterfaceRenderer.Value.Layers)
        {
            if (!RenderableFragments.ContainsKey(renderable))
            {
                var fragment = RegisterComponent(new RenderableMeshFragment(renderable));
                Fragments.Add(fragment);
                RenderableFragments[renderable] = fragment;
            }
        }
    }
}