using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Renderers;
using AncientEmpires.Engine.Render.Shaders;

namespace AncientEmpires.Engine.Render.Interface;

public class DeferredInterfaceLayer : DeferredLayer
{
    protected ComponentLookup<GameInterfaceRenderer> InterfaceRenderer;

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();

        Shader = AddComponent<StandardShader>();
    }

    public override void OnInitialize()
    {
        base.OnInitialize();

        ZIndex = 0.5f;
        InterfaceRenderer = Engine.ComponentLookup<GameInterfaceRenderer>();
    }

    protected override void CollectRenderLayers()
    {
        foreach (var renderable in InterfaceRenderer.Value.Layers)
        {
            if (!RenderableFragments.ContainsKey(renderable))
            {
                var fragment = RegisterComponent(new RenderableMeshFragment(renderable));
                Fragments.Add(fragment);
                RenderableFragments[renderable] = fragment;
            }
        }
    }
}