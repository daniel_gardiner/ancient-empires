using System.Numerics;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Render.Groups;

namespace AncientEmpires.Engine.Render.Interface;

public class InterfaceMeshFragment : MeshFragment
{
    public InterfaceMeshFragment()
    {
        Key = OnRegisterFragment();
    }

    protected override MeshKey OnRegisterFragment()
    {
        return FragmentManager.Instance.Register(this, KeyType.Interface);
    }

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();

        foreach (var quad in Quads)
        {
            quad.Vertex1.Color = new Vector4(1, 0, 0, 1);
            quad.Vertex2.Color = new Vector4(1, 0, 0, 1);
            quad.Vertex3.Color = new Vector4(1, 0, 0, 1);
            quad.Vertex4.Color = new Vector4(1, 0, 0, 1);
            quad.Vertex1.OverlayColor = new Vector4(1, 0, 0, 1);
            quad.Vertex2.OverlayColor = new Vector4(1, 0, 0, 1);
            quad.Vertex3.OverlayColor = new Vector4(1, 0, 0, 1);
            quad.Vertex4.OverlayColor = new Vector4(1, 0, 0, 1);
        }
    }
}