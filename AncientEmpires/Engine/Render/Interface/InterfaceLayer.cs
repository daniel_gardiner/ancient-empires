﻿using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Groups;
using AncientEmpires.Engine.Render.Shaders;

namespace AncientEmpires.Engine.Render.Interface;

public class InterfaceLayer : GameBatchLayer, IInterfaceRenderable
{
    public InterfaceLayer() : base()
    {
        BatchStrategy = new SequentialMeshBatchStrategy(true);
    }

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();
        
        Shader = AddComponent<InterfaceShader>();
    }

    public override void OnInitialize()
    {
        base.OnInitialize();
        
        ZIndex = Engine.Configuration.GetRenderLayer(KnownRenderLayers.Interface);
    }
    
    protected override void PrepareBatches()
    {
        base.PrepareBatches();
    }

    public override void OnDraw()
    {
        base.OnDraw();
    }
}