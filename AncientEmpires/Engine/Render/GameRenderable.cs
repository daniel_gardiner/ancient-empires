using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Renderers;
using AncientEmpires.Engine.Render.Shaders;
using MoreLinq;
using Vortice.Direct3D11;

namespace AncientEmpires.Engine.Render;

public abstract class GameRenderable : RenderableComponent
{
    protected static ID3D11ShaderResourceView1[] AllTextureResources;
    protected Texture[] Textures = Array.Empty<Texture>();
    public static int[] TextureIndexes;
    public static List<int> MapTileMaskIndex;

    public bool IsEmpty => GeometryBuffer.VertexCount == 0;

    public GameShader Shader;
    public GameShader WireframeShader;
    public RenderTarget RenderTarget;
    public GameGeometryBuffer GeometryBuffer;
    public ID3D11ShaderResourceView[] ShaderResources;
    protected ComponentLookup<RenderBuffers> RenderBuffers;
    protected ComponentLookup<GraphicsDevice> GraphicsDevice;
    protected ComponentLookup<BufferPoolManager> BufferPool;
    protected ComponentLookup<GameRenderManager> GameRenderer;

    public override void OnInitialize()
    {
        base.OnInitialize();

        BufferPool = new ComponentLookup<BufferPoolManager>();
        GameRenderer = new ComponentLookup<GameRenderManager>();
        GraphicsDevice = new ComponentLookup<GraphicsDevice>();
        RenderBuffers = new ComponentLookup<RenderBuffers>();
        OnCreateVertexBuffers();
    }

    public override void OnEngineReady()
    {
        base.OnEngineReady();

        ConsoleWriter.WriteLine($"OnEngineReady {GetType().Name} {Path}");
        RenderTarget ??= RenderBuffers.Value.CreateTextureRenderTarget(Name);
        var textureManager = Engine.GetComponent<GameTextures>();
        GetAllTextures(textureManager);
        ShaderResources = AllTextureResources.Select(o => o.Collect()).ToArray();
        MapTileMaskIndex = textureManager.FindTextureIndexes("TerrainTileMask.dds");

        // TODO: Hack figure this out for the config
        Engine.Configuration.TileMaskTextureIndex = MapTileMaskIndex[0];
    }

    public override void OnReleaseResources()
    {
        base.OnReleaseResources();

        Dispose();
    }

    private static void GetAllTextures(GameTextures textureManager)
    {
        if (AllTextureResources != null && AllTextureResources.Any())
            return;

        AllTextureResources = textureManager.GetAllTextureResources();
    }

    protected virtual void OnCreateVertexBuffers()
    {
        GeometryBuffer = BufferPool.Value.Allocate(GeometryBuffer, this);
    }

    public override void OnCreateShaders()
    {
        WireframeShader = AddComponent<WireframeShader>();
        Shader = AddComponent<StandardShader>();
    }

    public override void OnBeforeDraw()
    {
        base.OnBeforeDraw();

        OnCreateVertexBuffers();
    }

    public virtual void MarkClean()
    {
        Changed = false;
    }

    public virtual void MarkDirty()
    {
        Changed = true;
        CleanSlice = Engine.Timing.Slice;
    }

    public override void Dispose()
    {
        base.Dispose();
        
        ShaderResources?.ForEach(o => GameManager.DisposeCollector.RemoveAndDispose(ref o));
        AllTextureResources?.ForEach(o => GameManager.DisposeCollector.RemoveAndDispose(ref o));
        ShaderResources = null;
        AllTextureResources = null;
    }
}