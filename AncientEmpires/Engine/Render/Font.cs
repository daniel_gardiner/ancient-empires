using System.IO;
using System.Numerics;
using System.Text.RegularExpressions;
using AncientEmpires.Engine.Render.Components;

namespace AncientEmpires.Engine.Render;

public class Font : EngineComponent
{
    private string _fontTexture;
    public Dictionary<int, Glyph> Glyphs { get; set; } = new();
    public Dictionary<string, string> Info { get; set; } = new();
    public Dictionary<string, string> Common { get; set; } = new();
    public Dictionary<string, string> Page { get; set; } = new();
    public string CharCount { get; set; } = "";
    public KnownFont KnownFont { get; set; }
    public Texture Texture { get; protected set; }
    public float LineHeight { get; set; }

    public override void OnCreateComponents()
    {
        Texture = AddComponent(new Texture(Engine, this, $"{Application.StartupPath}\\Resources\\Textures\\Fonts\\{_fontTexture}"));
    }

    public int GetCommonInt(string key)
    {
        return Convert.ToInt32(Common[key]);
    }

    public Font Configure(KnownFont knownFont, string fontConfiguration, string fontTexture)
    {
        _fontTexture = fontTexture;
        KnownFont = knownFont;
        ReadConfiguration($"{Application.StartupPath}\\Resources\\Textures\\Fonts\\{fontConfiguration}");
        return this;
    }

    protected void ReadConfiguration(string path)
    {
        var lines = File.ReadLines(path);
        foreach (var line in lines)
        {
            var parts = line.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
            var firstPart = parts[0];
            switch (firstPart)
            {
                case "info":
                {
                    Info = ParseValues("info ", line);
                    break;
                }
                case "common":
                {
                    Common = ParseValues("common ", line);
                    break;
                }
                case "page":
                {
                    Page = ParseValues("page ", line);
                    break;
                }
                case "chars":
                {
                    CharCount = ParseValues("chars ", line)["count"];
                    break;
                }
                case "char":
                {
                    var values = ParseValues("char ", line);
                    var glyph = new Glyph
                    {
                        Id = Convert.ToInt32(values["id"]),
                        Position = new Vector2(Convert.ToSingle(values["x"]), Convert.ToSingle(values["y"])),
                        Size = new Vector2(Convert.ToSingle(values["width"]), Convert.ToSingle(values["height"])),
                        Offset = new Vector2(Convert.ToSingle(values["xoffset"]), Convert.ToSingle(values["yoffset"])),
                        XAdvance = Convert.ToInt32(values["xadvance"])
                    };
                    Glyphs.Add(glyph.Id, glyph);
                    break;
                }
            }
        }
    }

    private static Dictionary<string, string> ParseValues(string prefix, string line)
    {
        return ParseValuesInternal().ToDictionary(o => o.key, o => o.value);

        IEnumerable<(string key, string value)> ParseValuesInternal()
        {
            var matches = Regex.Matches(line.Replace(prefix, ""), @"\s*(?<key>.*?)\=\""?(?<value>.*?)\""?(\s|$)\s*", RegexOptions.Compiled);
            foreach (Match match in matches)
            {
                yield return (key: match.Groups["key"].Value, value: match.Groups["value"].Value);
            }
        }
    }
}