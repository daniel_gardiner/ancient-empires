namespace AncientEmpires.Engine.Render;

public static class KeyType
{
    public const ushort None = 1000;
    public const ushort Map = 1001;
    public const ushort Coordinates = 1002;
    public const ushort Interface = 1003;
    public const ushort Overlay = 1004;
    public const ushort Entity = 2000;
    public const ushort EntityMovement = 2001;
    public const ushort UnitsSelected = 2100;
    public const ushort Test1 = 10000;
    public const ushort Test2 = 10001;
    public const ushort Test3 = 10003;
}