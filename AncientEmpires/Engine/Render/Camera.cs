﻿using System.Numerics;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Map.Picker;
using AncientEmpires.Engine.Map.Rendering;
using AncientEmpires.Engine.Render.Components;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Render;

public class Camera : EngineComponent
{
    public static readonly Vector3 Up = new(0.0f, 1.0f, 0.0f);
    public static readonly Vector3 Down = new(0.0f, -1.0f, 0.0f);
    public static readonly Vector3 Left = new(-1.0f, 0.0f, 0.0f);
    public static readonly Vector3 Right = new(1.0f, 0.0f, 0.0f);
    public static readonly Vector3 ForwardRH = new(0.0f, 0.0f, -1.0f);
    public static readonly Vector3 ForwardLH = new(0.0f, 0.0f, 1.0f);
    public static readonly Vector3 BackwardRH = new(0.0f, 0.0f, 1.0f);
    public static readonly Vector3 BackwardLH = new(0.0f, 0.0f, -1.0f);

    public List<Action> ScrollListeners = new();
    public List<Action> ZoomListeners = new();
    public MatrixTransition PanTransition;
    public MatrixTransition ScaleTransition;
    public MatrixTransition ViewMatrixTransition;
    public BoundingBox Bounds;
    public int CurrentZoom = 5;
    public Vector2 CurrentScroll;
    public Vector3 Position;

    public Matrix4x4 Projection = Matrix4x4.Identity;
    public Matrix4x4 View => ViewMatrixTransition.Current;
    public Matrix4x4 Zoom => ScaleTransition.Current;
    public Matrix4x4 Scroll;
    public Matrix4x4 PanZoom => Scroll * Zoom;
    public Viewport Viewport => GraphicsDevice.Value.GetViewport();
    protected Matrix4x4 ViewMatrix = Matrix4x4.Identity;
    private Vector4 LastScrollOffset;
    protected ComponentLookup<GraphicsDevice> GraphicsDevice;
    protected ComponentLookup<MapViewport> MapViewport;
    protected ComponentLookup<MapPicker> MapPicker;
    protected ComponentLookup<GameMap> Map;
    protected ComponentLookup<IsometricPositionStrategy> PositionStrategy;
    public Point ScrollOffset => new((int)(CurrentScrollOffset - LastScrollOffset).X, (int)(CurrentScrollOffset - LastScrollOffset).Y);

    private Vector4 CurrentScrollOffset => Vector4.Transform(Vector2.Zero, PanTransition.Current);

    public override void OnInitialize()
    {
        base.OnInitialize();

        Map = new ComponentLookup<GameMap>();
        MapPicker = new ComponentLookup<MapPicker>();
        MapViewport = new ComponentLookup<MapViewport>();
        GraphicsDevice = new ComponentLookup<GraphicsDevice>();
        PositionStrategy = new ComponentLookup<IsometricPositionStrategy>();
        ViewMatrixTransition = new MatrixTransition(Engine);
        ScaleTransition = new MatrixTransition(Engine);
        PanTransition = new MatrixTransition(Engine);
        Projection = Matrix4x4.CreateOrthographic(Engine.ScreenSize.X, Engine.ScreenSize.Y, 0.1f, 10f);
        Projection.M33 *= -1.0f;
    }

    public override void OnEngineReady()
    {
        base.OnEngineReady();

        UpdateZoomScroll();
        UpdateBounds();
    }

    public override void OnResize()
    {
        Position = BackwardLH;
        Projection = Matrix4x4.CreateOrthographic(Engine.ScreenSize.X, Engine.ScreenSize.Y, 0.1f, 10f);
        Projection.M33 *= -1.0f;
    }

    public override void OnNextFrame(float timeDelta)
    {
        base.OnNextFrame(timeDelta);

        var cameraTarget = ForwardLH;
        var cameraUp = Up;
        var viewMatrix = Matrix4x4.CreateLookAt(Position, cameraTarget, cameraUp);

        if (ViewMatrixTransition == null)
            UpdateZoomScroll();

        ViewMatrix = viewMatrix;
        ViewMatrix *= ViewMatrixTransition.Current;
    }

    public BoundingBox GetBoundingBox() => Bounds;

    private void OnZoom()
    {
        UpdateZoomScroll();
        UpdateBounds();
        ZoomListeners.ForEach(o => o.Invoke());
    }

    private void OnScroll()
    {
        UpdateBounds();
        ScrollListeners.ForEach(o => o.Invoke());
    }

    public void ScrollBy(float x, float y)
    {
        if (x == 0 && y == 0)
            return;

        var previousScroll = CurrentScroll;
        var scrollX = Math.Max(Engine.Timing.SecondFraction / Zoom.M11, 1);
        var scrollY = Math.Max(Engine.Timing.SecondFraction / Zoom.M22, 1);
        scrollX = x * Math.Min(1, scrollX);
        scrollY = y * Math.Min(1, scrollY);
        CurrentScroll += new Vector2(scrollX, scrollY);
        UpdateZoomScroll();

        var center = Engine.Camera.Viewport.Bounds.Center().ToVector4();
        if (!MapPicker.Value.PickFromCursor(center).found)
        {
            CurrentScroll = previousScroll;
            UpdateZoomScroll();
        }
        else
        {
            OnScroll();
        }
    }

    public void ZoomBy(int delta)
    {
        if (ScaleTransition.RemainingMs > 100)
            return;

        var zoomChanged = false;

        CurrentZoom += delta > 0
            ? 1
            : -1;

        switch (CurrentZoom)
        {
            case < 1:
                CurrentZoom = 1;
                break;
            case > 10:
                CurrentZoom = 10;
                break;
            default:
                zoomChanged = true;
                break;
        }

        if (zoomChanged)
        {
            OnZoom();
        }
    }

    public void ZoomTo(int zoom)
    {
        CurrentZoom = zoom;
        UpdateZoomScroll();
    }

    public void ResetZoom()
    {
        ZoomTo(5);
        UpdateZoomScroll();
    }

    private Matrix4x4 CalculateScalingMatrix()
    {
        var scale = GetZoomFactor();

        return Matrix4x4.CreateScale(scale, scale, 1);
    }

    public float GetZoomFactor()
    {
        var scale = CurrentZoom switch
        {
            1 => 0.1f,
            2 => 0.25f,
            3 => 0.50f,
            4 => 0.75f,
            5 => 1f,
            6 => 1.25f,
            7 => 1.5f,
            8 => 2f,
            9 => 4f,
            10 => 8f,
            _ => 1f
        };
        return scale;
    }

    public float GetInverseZoomFactor()
    {
        var scale = CurrentZoom switch
        {
            10 => 0.1f,
            9 => 0.25f,
            8 => 0.50f,
            7 => 0.75f,
            6 => 1f,
            5 => 1.25f,
            4 => 1.5f,
            3 => 2f,
            2 => 4f,
            1 => 8f,
            _ => 1f
        };
        return scale;
    }

    public void ResetScroll()
    {
        CurrentScroll = Vector2.Zero;
    }

    protected void UpdateZoomScroll()
    {
        ViewMatrixTransition.Queue(Matrix4x4.Identity, 0f);
        ScaleTransition.Queue(CalculateScalingMatrix(), 50f);
        Scroll = Matrix4x4.CreateTranslation(Round(-CurrentScroll.X), Round(CurrentScroll.Y), 0);
        //PanTransition.Queue(Matrix4x4.CreateTranslation(Round(-CurrentScroll.X), Round(CurrentScroll.Y), 0), 0);
    }

    private float Round(float value)
    {
        return (float)(Math.Round(value * 100) / 100);
    }

    public float Project(float scalar)
    {
        return Matrix4x4.Multiply(Zoom, scalar).M11;
    }


    public Vector4 ScreenSpaceToWorldSpace(Vector4 cursorPosition)
    {
        return Unproject(cursorPosition) * new Vector4(1, 1, 0, 0);
    }

    public Vector4 MapSpaceToWorldSpace(Vector4 mapSpace)
    {
        return PositionStrategy.Value.ProjectWithCenterAtZero(in mapSpace) * new Vector4(1, 1, 0, 0);
    }

    public Vector4 WorldSpaceToMapSpace(Vector4 worldSpace)
    {
        return PositionStrategy.Value.Unproject(in worldSpace) * new Vector4(1, 1, 0, 0);
    }

    public BoundingBox MapSpaceToWorldSpace(BoundingBox mapSpaceBox)
    {
        var center = MapSpaceToWorldSpace(mapSpaceBox.Center.ToVector4()).ToVector3();
        var extent = mapSpaceBox.Extent / 2;
        var a = new BoundingBox(
            center - extent,
            center + extent);
        return a;
    }

    public Vector4 MapSpaceToScreenSpace(Vector4 mapSpace)
    {
        var worldSpace = PositionStrategy.Value.ProjectWithCenterAtZero(in mapSpace);
        var screenSpace = Engine.Camera.Project(worldSpace);
        return new Vector4(screenSpace.X, screenSpace.Y, 0, 0) * new Vector4(1, 1, 0, 0);
    }

    public Vector4 ScreenSpaceToMapSpace(Vector4 screenSpace)
    {
        var worldSpaceCursor = Engine.Camera.Unproject(screenSpace);
        var exactPosition = PositionStrategy.Value.Unproject(in worldSpaceCursor);
        return ConstrainMapSpace(new Vector4(exactPosition.X, exactPosition.Y, 0, 0)) * new Vector4(1, 1, 0, 0);
    }

    public Vector4 WorldSpaceToScreenSpace(float x, float y)
    {
        var worldSpace = new Vector4(x, y, 0, 0);
        return WorldSpaceToScreenSpace(in worldSpace);
    }

    public Vector4 WorldSpaceToScreenSpace(in Vector4 worldSpace)
    {
        return Project(worldSpace) * new Vector4(1, 1, 0, 0);
    }
    
    public Vector4 WorldSpaceToScreenSpaceFlipped(in Vector4 worldSpace)
    {
        return WorldSpaceToScreenSpace(in worldSpace) * new Vector4(1, -1, 0, 0);
    }

    public Vector4 Project(Vector4 vector)
    {
        var projectionView = Projection * View;
        var worldViewProjection = Matrix4x4.Identity * PanZoom * projectionView;
        return new Vector4(ProjectVector(
            vector.ToVector3(),
            Viewport.X,
            Viewport.Y,
            Viewport.Width,
            Viewport.Height,
            Viewport.MinDepth,
            Viewport.MaxDepth,
            in worldViewProjection), 0) * new Vector4(1, 1, 0, 0);
    }

    public Vector4 Unproject(Vector4 vector)
    {
        var projectionView = Projection * View;
        var worldViewProjection = Matrix4x4.Identity * PanZoom * projectionView;
        return new Vector4(UnprojectVector(
            vector.ToVector3(),
            Viewport.X,
            Viewport.Y,
            Viewport.Width,
            Viewport.Height,
            Viewport.MinDepth,
            Viewport.MaxDepth,
            ref worldViewProjection), 0) * new Vector4(1, 1, 0, 0);
    }

    public static Vector3 ProjectVector(
        Vector3 vector,
        float x,
        float y,
        float width,
        float height,
        float minZ,
        float maxZ,
        in Matrix4x4 worldViewProjection)
    {
        var v = TransformCoordinate(in vector, in worldViewProjection);

        return new Vector3(
            (1.0f + v.X) * 0.5f * width + x,
            (1.0f - v.Y) * 0.5f * height + y,
            v.Z * (maxZ - minZ) + minZ);
    }

    public static Vector3 UnprojectVector(
        Vector3 vector,
        float x,
        float y,
        float width,
        float height,
        float minZ,
        float maxZ,
        ref Matrix4x4 worldViewProjection)

    {
        Matrix4x4.Invert(worldViewProjection, out var matrix);

        var v = new Vector3
        {
            X = (vector.X - x) / width * 2.0f - 1.0f,
            Y = -((vector.Y - y) / height * 2.0f - 1.0f),
            Z = (vector.Z - minZ) / (maxZ - minZ)
        };

        return TransformCoordinate(in v, in matrix);
    }

    public static Vector3 TransformCoordinate(in Vector3 coordinate, in Matrix4x4 transform)
    {
        Vector4 vector = new Vector4();
        vector.X = coordinate.X * transform.M11 + coordinate.Y * transform.M21 + coordinate.Z * transform.M31 + transform.M41;
        vector.Y = coordinate.X * transform.M12 + coordinate.Y * transform.M22 + coordinate.Z * transform.M32 + transform.M42;
        vector.Z = coordinate.X * transform.M13 + coordinate.Y * transform.M23 + coordinate.Z * transform.M33 + transform.M43;
        vector.W = 1f / (coordinate.X * transform.M14 + coordinate.Y * transform.M24 + coordinate.Z * transform.M34 + transform.M44);

        return new Vector3(vector.X * vector.W, vector.Y * vector.W, vector.Z * vector.W);
    }

    public void UpdateScrollOffset()
    {
        LastScrollOffset = CurrentScrollOffset;
    }

    private BoundingBox UpdateBounds()
    {
        var vectors = new[]
        {
            new Vector4(0, 0, 0, 0),
            new Vector4(0, Engine.ScreenSize.Y, 0, 0),
            new Vector4(Engine.ScreenSize.X, Viewport.Y, 0, 0),
            new Vector4(Engine.ScreenSize.X, Viewport.Y + Viewport.Height, 0, 0)
        };

        for (var index = 0; index < vectors.Length; index++)
        {
            var worldVector = Unproject(vectors[index]);
            var unIsometric = Map.Value.PositionStrategy.Unproject(in worldVector);
            vectors[index] = new Vector4(unIsometric.X, unIsometric.Y, 0, 0);
        }

        var vector3s = vectors.Select(o => o.ToVector3()).ToArray();
        return Bounds = BoundingBox.CreateFromPoints(vector3s);
    }

    public void SubscribeToScroll(Action handler)
    {
        ScrollListeners.Add(handler);
    }

    public void UnsubscribeToScroll(Action handler)
    {
        ScrollListeners.Remove(handler);
    }

    public void SubscribeToZoom(Action handler)
    {
        ZoomListeners.Add(handler);
    }

    public void UnsubscribeToZoom(Action handler)
    {
        ZoomListeners.Remove(handler);
    }

    private Vector4 ConstrainMapSpace(Vector4 result) =>
        new(
            Math.Min(Engine.Configuration.MapSize.Width - 1, Math.Max(0, result.X)),
            Math.Min(Engine.Configuration.MapSize.Height - 1, Math.Max(0, result.Y)),
            result.Z,
            result.W);

    public Vector4 WorldSpaceToGameSpace(in Vector4 worldSpace)
    {
        throw new NotImplementedException();
    }

    public Vector4 MapSpaceToGameSpace(in Vector4 worldSpace)
    {
        throw new NotImplementedException();
    }
}