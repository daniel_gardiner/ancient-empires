﻿using AncientEmpires.Engine.Render.Components;
using SharpGen.Runtime;
using Vortice.DCommon;
using Vortice.Direct2D1;
using Vortice.Direct3D11;
using Vortice.Direct3D12;
using Vortice.DXGI;
using Vortice.Mathematics;
using AlphaMode = Vortice.DCommon.AlphaMode;
using ComparisonFunction = Vortice.Direct3D11.ComparisonFunction;
using DepthStencilDescription = Vortice.Direct3D11.DepthStencilDescription;
using DepthStencilOperationDescription = Vortice.Direct3D11.DepthStencilOperationDescription;
using DepthWriteMask = Vortice.Direct3D11.DepthWriteMask;
using ResourceFlags = Vortice.Direct3D11.ResourceFlags;
using StencilOperation = Vortice.Direct3D11.StencilOperation;

namespace AncientEmpires.Engine.Render;

public class RenderBuffers : EngineComponent
{
    private readonly GraphicsDevice _graphicsDevice;
    private readonly GraphicsPresentation _graphicsPresentation;
    public ID2D1Bitmap1 D2DRenderTarget;
    public List<ID3D11Resource> WrappedBackBuffers12;
    public List<ID3D12Resource1> RenderTargets12;
    public List<ID3D12CommandAllocator> CommandAllocators;
    public ID3D11DepthStencilState DepthStencilState;
    public ID3D11DepthStencilState DisabledDepthStencilState;
    public Viewport Viewport;
    public BackBufferRenderTarget BackBufferTarget;

    public RenderBuffers(GameEngine engine, GraphicsDevice graphicsDevice, GraphicsPresentation graphicsPresentation)
    {
        _graphicsDevice = graphicsDevice;
        _graphicsPresentation = graphicsPresentation;
    }

    public override void OnEngineReady()
    {
        base.OnEngineReady();

        CreateDepthStencil();
        CreateViewport();

        if (_graphicsDevice.D3D12Device == null)
        {
            CreateD3D11Buffers();
        }
        else
        {
            CreateD3D12Buffers();
        }
    }

    public override void OnReleaseResources()
    {
        base.OnReleaseResources();
        Dispose();
    }

    private void CreateD3D12Buffers()
    {
        RenderTargets12 = new List<ID3D12Resource1>();
        WrappedBackBuffers12 = new List<ID3D11Resource>();
        CommandAllocators = new List<ID3D12CommandAllocator>();

        var descriptorHeap = _graphicsDevice.D3D12Device.CreateDescriptorHeap<ID3D12DescriptorHeap>(new DescriptorHeapDescription
        {
            Type = DescriptorHeapType.RenderTargetView,
            Flags = DescriptorHeapFlags.None
        });

        var rtvDescriptorHandleIncrementSize = _graphicsDevice.D3D12Device.GetDescriptorHandleIncrementSize(DescriptorHeapType.RenderTargetView);
        _graphicsDevice.D3DContext.Flush();

        for (int i = 0; i < _graphicsPresentation.BufferCount; i++)
        {
            RenderTargets12.Add(_graphicsPresentation.SwapChain.GetBuffer<ID3D12Resource1>(i));
            var rtvDescriptorHandle = descriptorHeap.GetCPUDescriptorHandleForHeapStart();
            //_graphicsDevice.D3D12Device.CreateRenderTargetView(RenderTargets12[i], null, rtvDescriptorHandle);
            var resourceFlags = new ResourceFlags
            {
                BindFlags = (int)BindFlags.RenderTarget
            };
            var wrapped = _graphicsDevice.D3D111On12Device.CreateWrappedResource(
                RenderTargets12[i],
                resourceFlags,
                (int)ResourceStates.RenderTarget,
                (int)ResourceStates.Present);

            WrappedBackBuffers12.Add(wrapped);

            _graphicsDevice.D2DFactory.GetDesktopDpi(out var dpiX, out var dpiY);
            var bitmapProperties1 = new BitmapProperties1(
                new PixelFormat(Format.Unknown, AlphaMode.Premultiplied),
                dpiX,
                dpiY,
                BitmapOptions.Target | BitmapOptions.CannotDraw);
            var surface = wrapped.QueryInterface<IDXGISurface>();
            //D2DRenderTarget.Add(_graphicsDevice.D2DContext.CreateBitmapFromDxgiSurface(surface, bitmapProperties1));
            surface.Release();

            rtvDescriptorHandle.Ptr += rtvDescriptorHandleIncrementSize;

            EnsureSuccessful(_graphicsDevice.D3D12Device.CreateCommandAllocator<ID3D12CommandAllocator>(CommandListType.Direct, out var commandAllocator));
            CommandAllocators.Add(commandAllocator);
        }
    }

    public virtual RenderTarget CreateTextureRenderTarget(string name)
    {
        return RegisterComponent(new TextureRenderTarget().SetDebugName(name));
    }

    private void CreateD3D11Buffers()
    {
        if (BackBufferTarget == null)
        {
            BackBufferTarget = RegisterComponent<BackBufferRenderTarget>();
        }

        if (_graphicsDevice.D2DFactory != null)
        {
            var buffer = _graphicsPresentation.SwapChain.GetBuffer<IDXGISurface>(0);
            using var surface = buffer.QueryInterface<IDXGISurface>();
            D2DRenderTarget = CreateD2DRenderTarget(surface);
        }
    }

    public virtual ID2D1Bitmap1 CreateD2DRenderTarget(ID3D11Texture2D surface)
    {
        return CreateD2DRenderTarget(surface.QueryInterface<IDXGISurface>());
    }
    
    public virtual ID2D1Bitmap1 CreateD2DRenderTarget(IDXGISurface surface)
    {
        _graphicsDevice.D2DFactory.GetDesktopDpi(out var dpiX, out var dpiY);
        var bitmapProperties1 = new BitmapProperties1(
            new PixelFormat(Format.Unknown, AlphaMode.Premultiplied),
            dpiX,
            dpiY,
            BitmapOptions.Target | BitmapOptions.CannotDraw);
        
        return _graphicsDevice.D2DContext.CreateBitmapFromDxgiSurface(surface, bitmapProperties1).Collect();
    }

    private void CreateDepthStencil()
    {
        DepthStencilState = _graphicsDevice.D3DDevice.CreateDepthStencilState(new DepthStencilDescription
        {
            DepthEnable = true,
            DepthWriteMask = DepthWriteMask.All,
            DepthFunc = ComparisonFunction.Less,
            StencilEnable = true,
            StencilReadMask = 0xFF,
            StencilWriteMask = 0xFF,
            // Stencil operation if pixel front-facing.
            FrontFace = new DepthStencilOperationDescription()
            {
                StencilFailOp = StencilOperation.Zero,
                StencilDepthFailOp = StencilOperation.Increment,
                StencilPassOp = StencilOperation.Keep,
                StencilFunc = ComparisonFunction.Always
            },
            // Stencil operation if pixel is back-facing.
            BackFace = new DepthStencilOperationDescription()
            {
                StencilFailOp = StencilOperation.Keep,
                StencilDepthFailOp = StencilOperation.Decrement,
                StencilPassOp = StencilOperation.Keep,
                StencilFunc = ComparisonFunction.Always
            }
        }).Collect();
        DisabledDepthStencilState = _graphicsDevice.D3DDevice.CreateDepthStencilState(new DepthStencilDescription
        {
            DepthEnable = false,
            DepthWriteMask = DepthWriteMask.All,
            DepthFunc = ComparisonFunction.Less,
            StencilEnable = true,
            StencilReadMask = 0xFF,
            StencilWriteMask = 0xFF,
            // Stencil operation if pixel front-facing.
            FrontFace = new DepthStencilOperationDescription()
            {
                StencilFailOp = StencilOperation.Keep,
                StencilDepthFailOp = StencilOperation.Increment,
                StencilPassOp = StencilOperation.Keep,
                StencilFunc = ComparisonFunction.Always
            },
            // Stencil operation if pixel is back-facing.
            BackFace = new DepthStencilOperationDescription()
            {
                StencilFailOp = StencilOperation.Keep,
                StencilDepthFailOp = StencilOperation.Decrement,
                StencilPassOp = StencilOperation.Keep,
                StencilFunc = ComparisonFunction.Always
            }
        }).Collect();
        _graphicsDevice.D3DDevice.ImmediateContext.OMSetDepthStencilState(DepthStencilState);
    }

    private void CreateViewport()
    {
        Viewport = new Viewport(
            0,
            0,
            _graphicsDevice.Width,
            _graphicsDevice.Height);
        _graphicsDevice.D3DContext.RSSetViewport(Viewport);
    }

    private void EnsureSuccessful(Result result)
    {
        if (result.Success)
            return;

        ConsoleWriter.WriteLine("Failed to create: ");
        throw new Exception(result.Description);
    }

    public IEnumerable<TextureRenderTarget> ListTextureRenderTargets()
    {
        return Components.OfType<TextureRenderTarget>();
    }

    public void Refresh()
    {
        foreach (var renderTarget in ListTextureRenderTargets())
        {
            renderTarget.ClearRenderTarget(_graphicsDevice.D3DContext, Color4.Transparent);
        }
    }

    public override void Dispose()
    {
        base.Dispose();
        
        WrappedBackBuffers12?.ForEach(o => GameManager.DisposeCollector.RemoveAndDispose(ref o));
        RenderTargets12?.ForEach(o => GameManager.DisposeCollector.RemoveAndDispose(ref o));
        WrappedBackBuffers12?.ForEach(o => GameManager.DisposeCollector.RemoveAndDispose(ref o));
        RenderTargets12?.Clear();
        WrappedBackBuffers12?.Clear();
        GameManager.DisposeCollector.RemoveAndDispose(ref D2DRenderTarget);
        GameManager.DisposeCollector.RemoveAndDispose(ref DepthStencilState);
        GameManager.DisposeCollector.RemoveAndDispose(ref DisabledDepthStencilState);
    }
}