using AncientEmpires.Engine.Render.Components;

namespace AncientEmpires.Engine.Render;

public class BufferPoolManager : EngineComponent
{
    public int BufferCount = 20;
    public List<GameGeometryBuffer> LargeBuffers { get; } = new();
    public Queue<GameGeometryBuffer> AvailableLargeBuffers { get; } = new();
    public List<GameGeometryBuffer> AssignedLargeBuffers { get; } = new();

    public List<GameGeometryBuffer> Buffers { get; } = new();
    public List<GameGeometryBuffer> AssignedBuffers { get; } = new();
    public Queue<GameGeometryBuffer> AvailableBuffers { get; } = new();

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();

        InitializeBuffers();
    }

    private void InitializeBuffers()
    {
        for (var i = 0; i < BufferCount; i++)   
        {
            var buffer = AddComponent<GameGeometryBuffer>();
            Buffers.Add(buffer);
            AvailableBuffers.Enqueue(buffer);
        }

        for (var i = 0; i < BufferCount / 5; i++)
        {
            var buffer = AddComponent<LargeGameGeometryBuffer>();
            LargeBuffers.Add(buffer);
            AvailableLargeBuffers.Enqueue(buffer);
        }
    }
    
    public GameGeometryBuffer GetBuffer(EngineComponent requester)
    {
        if (!AvailableBuffers.Any())
            throw new Exception("No available buffer");

        ConsoleWriter.WriteLine($"{requester.GetType().Name} requested a buffer");

        var buffer = AvailableBuffers.Dequeue();
        buffer.OnAssigned(requester);
        AssignedBuffers.Add(buffer);
        return buffer;
    }

    public GameGeometryBuffer GetLargeBuffer(EngineComponent requester, GameGeometryBuffer oldBuffer)
    {
        if (oldBuffer != null)
        {
            AssignedBuffers.Remove(oldBuffer);
            oldBuffer.OnRecycle(requester);
            AvailableBuffers.Enqueue(oldBuffer);
        }

        if (!AvailableLargeBuffers.Any())
            throw new Exception("No available buffer");

        var buffer = AvailableLargeBuffers.Dequeue();
        buffer.OnAssigned(requester);
        AssignedLargeBuffers.Add(buffer);
        return buffer;
    }

    public GameGeometryBuffer CycleBuffer(EngineComponent requester, GameGeometryBuffer currentBuffer)
    {
        var buffer = currentBuffer is LargeGameGeometryBuffer
            ? GetLargeBuffer(requester, currentBuffer)
            : GetBuffer(requester);

        if (currentBuffer != null)
        {
            AssignedBuffers.Remove(currentBuffer);
            currentBuffer.OnRecycle(requester);
            AvailableBuffers.Enqueue(currentBuffer);
        }

        return buffer;
    }

    private void DisposeBuffers()
    {
        for (var index = Components.Count - 1; index >= 0; index--)
        {
            var buffer = Components[index];
            RemoveComponent(buffer);
        }

        Buffers.Clear();
        LargeBuffers.Clear();
        AvailableBuffers.Clear();
        AvailableLargeBuffers.Clear();
        AssignedBuffers.Clear();
        AssignedLargeBuffers.Clear();
    }

    public GameGeometryBuffer Allocate(GameGeometryBuffer geometryBuffer, EngineComponent requestor)
    {
        return geometryBuffer?.IsReady == true
            ? geometryBuffer
            : CycleBuffer(requestor, geometryBuffer);
    }

    public override void Dispose()
    {
        base.Dispose();
        DisposeBuffers();
    }
}