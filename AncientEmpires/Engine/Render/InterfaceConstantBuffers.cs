using System.Numerics;
using AncientEmpires.Engine.Render.Components;

namespace AncientEmpires.Engine.Render;

public class InterfaceConstantBuffers : GameConstantBuffers
{
    public override Matrix4x4 Projection => CreateOrthographicOffCenterLH();
    public override Matrix4x4 View => Matrix4x4.Identity;
    public override Matrix4x4 World => Matrix4x4.Identity;

    private Matrix4x4 CreateOrthographicOffCenterLH()
    {
        var matrix4X4 = Matrix4x4.CreateOrthographicOffCenter(0, Engine.ScreenSize.X, Engine.ScreenSize.Y, 0, 0.1f, 10f);
        matrix4X4.M33 *= -1.0f;
        return matrix4X4;
    }

    public override void OnBeforeDraw()
    {
        base.OnBeforeDraw();
    }
}