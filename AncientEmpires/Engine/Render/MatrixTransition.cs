using System.Numerics;

namespace AncientEmpires.Engine.Render;

public class MatrixTransition
{
    private readonly GameEngine _engine;
    public bool IsRunning;
    public double Duration;
    public double StartMs;
    public Matrix4x4 Target;
    public Matrix4x4 Origin;
    public double RemainingMs => Math.Max(0, Duration - ElapsedMs);
    private double ElapsedMs => _engine.Timing.TotalElapsedMs - StartMs;
    public (Matrix4x4 target, float duration) Next { get; set; } = (Matrix4x4.Identity, 1f);

    public MatrixTransition(GameEngine engine)
    {
        _engine = engine;
        StartMs = _engine.Timing.TotalElapsedMs;
        Origin = Matrix4x4.Identity;
        Target = Matrix4x4.Identity;
    }

    public Matrix4x4 Current
    {
        get
        {
            if (IsRunning && RemainingMs <= 0)
                Stop();
            else if (!IsRunning)
                Start(Next.target, Next.duration);

            var percent = (float)Math.Min(1f, ElapsedMs / Duration);
                
            if (float.IsNaN(percent))
                percent = 1;
                
            return Matrix4x4.Lerp(Origin, Target, percent);
        }
    }

    private void Stop()
    {
        Origin = Target;
        IsRunning = false;
        Duration = 1;
    }

    public void Queue(Matrix4x4 target, float duration)
    {
        if (!IsRunning)
            Start(target, duration);

        Next = (target, duration);
    } 

    private void Start(Matrix4x4 target, float duration)
    {
        if (target == Target)
            return;

        Origin = Target;
        Target = target;
        Duration = duration;
        StartMs = _engine.Timing.TotalElapsedMs;
        IsRunning = true;
    }
}