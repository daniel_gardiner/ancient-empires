using System.Numerics;

namespace AncientEmpires.Engine.Render;

public class Glyph
{
    public int Id { get; set; }
    public Vector2 Position { get; set; }
    public Vector2 Size { get; set; }
    public Vector2 Offset { get; set; }
    public int XAdvance { get; set; }
}