using System.Diagnostics;
using System.Runtime.InteropServices;
using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Groups;
using Vortice.Direct3D11;

namespace AncientEmpires.Engine.Render;

public class GameGeometryBuffer : EngineComponent
{
    protected ComponentLookup<GraphicsDevice> GraphicsDevice;
    protected ComponentLookup<D3DFactory> D3DFactory;
    public virtual int GpuQuadCapacity => 100_000;
    public int CapacityBuffer = 10_000;
    public EngineComponent Tag;
    public ID3D11Buffer IndexBuffer;
    public ID3D11Buffer VertexBuffer;
    public int RemainingCapacity => VertexCapacity - (AllocationOffset + VertexCount);
    public int VertexCapacity;
    public int VertexCount;
    public int AllocationOffset;
    public int IndexCount => (int)(VertexCount * 1.5f);
    public int IndexOffset => (int)(AllocationOffset * 1.5f);
    public bool IsReady => VertexBuffer != null;

    public int TimesUsed;
    public int VerticesWritten;
    private MappedSubresource? _mappedSubResource;
    private MapMode _mapMode;
    public bool IsLocked;
    public bool IsMapped;

    public override void OnInitialize()
    {
        base.OnInitialize();

        VertexCapacity = GpuQuadCapacity * 4;
        D3DFactory = new ComponentLookup<D3DFactory>();
        GraphicsDevice = new ComponentLookup<GraphicsDevice>();
    }

    public override void OnEngineReady()
    {
        base.OnEngineReady();

        IndexBuffer = D3DFactory.Value.GetIndexBuffer($"{Tag?.Name}IndexBuffer");
        VertexBuffer = D3DFactory.Value.CreateVertexBuffer($"{Tag?.Name}VertexBuffer", GpuQuadCapacity * 4);
    }

    public override void OnReleaseResources()
    {
        base.OnReleaseResources();

        Dispose();
    }

    public void OnAssigned(EngineComponent requester)
    {
        TimesUsed++;

        Tag = requester;
    }

    public void OnRecycle(EngineComponent requester)
    {
        VertexCount = 0;
        AllocationOffset = 0;
    }

    public bool NewAllocation(int requiredVertices)
    {
        AllocationOffset += VertexCount;
        VertexCount = 0;

        return CanFit(requiredVertices);
    }

    public MappedSubresource Map()
    {
        if (IsLocked) throw new Exception("Vertex buffer is already locked");
        IsMapped = !IsMapped
            ? true
            : throw new Exception("Vertex buffer is already mapped");

        AllocationOffset = 0;
        VertexCount = 0;
        _mapMode = AllocationOffset + VertexCount == 0
            ? MapMode.WriteDiscard
            : MapMode.WriteNoOverwrite;

        _mappedSubResource = GraphicsDevice.Value.D3DDevice.ImmediateContext.Map(VertexBuffer, _mapMode);
        return _mappedSubResource.Value;
    }

    public void Unmap()
    {
        IsMapped = IsMapped
            ? false
            : throw new Exception("Vertex buffer is already unmapped");

        GraphicsDevice.Value.D3DDevice.ImmediateContext.Unmap(VertexBuffer, 0);
    }

    public bool CanFit(int length) => RemainingCapacity >= length && VertexBuffer != null;

    public void MapAndWrite(IEnumerable<MeshFragment> fragments)
    {
        Map();
        WriteFragments(fragments);
        Unmap();
    }

    public void WriteBatches(List<GameMeshBatch> batches)
    {
        if (IsLocked)
            throw new Exception("Vertex buffer is already locked");

        foreach (var batch in batches)
        {
            EnsureCanFit(batch);
            WriteFragments(batch.Fragments);
        }
    }

    public unsafe void WriteFragments(IEnumerable<MeshFragment> fragments)
    {
        //if (batch.IsEmpty)
        //return;

        Debug.Assert(_mappedSubResource != null, "Vertex buffer is not mapped");
        var stride = Marshal.SizeOf<Vertex>();

        var pointer = _mappedSubResource.Value.DataPointer;
        pointer += (AllocationOffset + VertexCount) * stride;
        var vertexPtr = (Vertex*)pointer;

        foreach (var fragment in fragments)
        {
            foreach (var quad in fragment.Quads)
            {
                if (RemainingCapacity <= quad.Length)
                {
                    Console.WriteLine("Vertex buffer is unexpectedly full");
                    Debug.Assert(RemainingCapacity >= quad.Length, "Vertex buffer is unexpectedly full");
                    Clear("Vertex buffer is unexpectedly full");
                }
                /*
                        var source = quad.AllVertices.AsSpan();
                        var dest = new Span<Vertex>(vertexPtr, source.Length);
                        source.CopyTo(dest);
    
                        VertexCount += source.Length;
                        Engine.Timing.MetricVerticesWritten++;
                        vertexPtr += source.Length;
                        */

                foreach (var vertex in quad.AllVertices)
                {
                    try
                    {
                        vertexPtr->Position = vertex.Position + fragment.PositionOffset.WorldSpace;
                        vertexPtr->Tile = vertex.Tile;
                        vertexPtr->Bounds = vertex.Bounds;
                        vertexPtr->Collision = vertex.Collision;
                        vertexPtr->Color = vertex.Color;
                        vertexPtr->Material = vertex.Material;
                        vertexPtr->EntityId = vertex.EntityId;
                        vertexPtr->OverlayColor = vertex.OverlayColor;
                        vertexPtr->UV0 = vertex.UV0;
                        vertexPtr->UV1 = vertex.UV1;
                        vertexPtr++;
                        pointer += stride;

                        VertexCount++;
                        Engine.Timing.GameMetrics.MetricVerticesWritten++;
                    }
                    catch (Exception e)
                    {
                        ConsoleWriter.WriteLine($"Error writing vertex: {e.Message}");
                    }
                }
            }

            fragment.MarkClean();
        }
    }

    private void EnsureCanFit(GameMeshBatch batch)
    {
        if (RemainingCapacity < batch.VertexCount + CapacityBuffer)
        {
            ConsoleWriter.WriteLine($"Vertex buffer is full @ {Tag?.Name}");
            Clear("Vertex buffer is full");
        }
    }

    public override void Clear(string reason)
    {
        ConsoleWriter.WriteLine($"Clear VertexBuffer for {Tag?.Name} being cleared because: {reason}");
        VertexCount = 0;
        AllocationOffset = 0;
    }

    public void Lock() => IsLocked = true;

    private void Unlock() => IsLocked = false;

    public override void Dispose()
    {
        base.Dispose();
        
        GameManager.DisposeCollector.RemoveAndDispose(ref VertexBuffer);
        GameManager.DisposeCollector.RemoveAndDispose(ref IndexBuffer);
    }
}