namespace AncientEmpires.Engine.Render.Components;

public static class DisposeCollectorExtensions
{
    public static T Collect<T>(this T obj)
    {
        return GameManager.DisposeCollector.Collect(obj);
    }
}