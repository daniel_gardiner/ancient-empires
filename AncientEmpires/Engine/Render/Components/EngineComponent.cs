using System.Diagnostics;
using System.Text;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Layout;
using AncientEmpires.Engine.Metrics;
using App.Metrics;
using App.Metrics.Meter;

namespace AncientEmpires.Engine.Render.Components;

public abstract class EngineComponent : EngineComponentBase<GameEngine, EngineComponent>, IEquatable<EngineComponent>
{
    public static int ObjectID = 1000;

    public int ID = ObjectID++;
    public ComponentMetrics Metrics;
    public virtual bool EnableMetrics => true;
    public bool IsInitialized;
    private bool HasCreatedComponents;
    protected int VerticesPerQuad = 4;
    protected int IndexesPerQuad = 6;
    private string _path;
    public Lazy<string> TypeName => new(GetTypeName);
    public override string Name => TypeName.Value;
    
    protected EngineComponent()
    {
        Engine = this is GameEngine engine
            ? engine
            : GameContext.Instance?.Engine;
        Metrics = new ComponentMetrics(Engine, this, EnableMetrics);
    }

    public EngineComponent SetParent(EngineComponent parent)
    {
        Parent = parent;
        return this;
    }

    [DebuggerStepThrough]
    public virtual TComponent GetComponent<TComponent>() where TComponent : EngineComponent
    {
        var component = Components.OfType<TComponent>().FirstOrDefault();
        if (component != null)
            return component;

        for (var index = 0; index < Components.Count; index++)
        {
            var childComponent = Components[index];
            component = childComponent.GetComponent<TComponent>();
            if (component != null)
                return component;
        }

        return null;
    }

    [DebuggerStepThrough]
    public virtual TComponent TryGetComponent<TComponent>() where TComponent : EngineComponent
    {
        return GetComponent<TComponent>();
    }

    [DebuggerStepThrough]
    public virtual TComponent AddComponent<TComponent>(TComponent component) where TComponent : EngineComponent
    {
        component.Engine = Engine;
        component = Engine.PrepareComponent(component);
        Components.Add(component.SetParent(this));
        return component;
    }

    [DebuggerStepThrough]
    public virtual TComponent AddComponent<TComponent>() where TComponent : EngineComponent, new()
    {
        var component = Engine.PrepareComponent(new TComponent());
        Components.Add(component.SetParent(this));
        return component;
    }

    [DebuggerStepThrough]
    public virtual TComponent AddComponent<TComponent>(GameLayout layout) where TComponent : EngineComponent, IUiElement, new()
    {
        var component = Engine.PrepareComponent(new TComponent());
        var uiElement = this as IUiElement;
        Components.Add(component.ChangeLayout(layout, uiElement) as EngineComponent);
        return component;
    }

    [DebuggerStepThrough]
    public virtual TComponent RegisterComponent<TComponent>(TComponent component) where TComponent : EngineComponent
    {
        component = Engine.PrepareComponent(component);
        Components.Add(component.SetParent(this));
        if (Engine.IsEngineReady)
        {
            component.Register();
            component.TriggerEngineReady();
        }

        return component;
    }

    [DebuggerStepThrough]
    public virtual TComponent RegisterComponent<TComponent>() where TComponent : EngineComponent, new()
    {
        return RegisterComponent(new TComponent());
    }

    [DebuggerStepThrough]
    public virtual void RemoveComponent<TComponent>(TComponent component) where TComponent : EngineComponent
    {
        Components.Remove(component);
        GameManager.DisposeCollector.RemoveAndDispose(ref component);
    }

    [DebuggerStepThrough]
    public virtual void RemoveComponent<TComponent>() where TComponent : EngineComponent
    {
        var component = Components.OfType<TComponent>().FirstOrDefault();
        if (component == null) return;

        Engine.RemoveComponent(component);
    }

    [DebuggerStepThrough]
    public virtual TComponent ReplaceComponent<TComponent>(TComponent component) where TComponent : EngineComponent
    {
        if (Components.OfType<TComponent>().Any())
        {
            RemoveComponent<TComponent>();
            Engine.RemoveComponent<TComponent>();
            AddComponent(component);
            return component;
        }

        foreach (var childComponent in Components)
            if (childComponent.ReplaceComponent(component) != null)
                return component;

        throw new Exception($"Component {typeof(TComponent).Name} not found");
    }

    public virtual void OnEngineReady()
    {
    }

    public virtual void OnReleaseResources()
    {
    }

    public virtual void OnCreateComponents()
    {
    }

    public virtual void OnInitialize()
    {
    }

    public virtual void OnResize()
    {
    }

    [DebuggerStepThrough]
    public virtual void Register()
    {
        using (Metrics.TrackTime(KnownMetrics.Timer, Metrics.RegisterTags))
        {
            TriggerCreateComponents();
            TriggerInitialize();
        }
    }

    [DebuggerStepThrough]
    public virtual void TriggerEngineReady()
    {
        using (Metrics.TrackTime(KnownMetrics.Timer, Metrics.TriggerEngineReadyTags))
        {
            Engine.IsEngineReady = true;
            OnEngineReady();

            foreach (var component in Components)
            {
                component.TriggerEngineReady();
            }
        }
    }

    [DebuggerStepThrough]
    public virtual void TriggerCreateComponents()
    {
        using (Metrics.TrackTime(KnownMetrics.Timer, Metrics.TriggerCreateComponentsTags))
        {
            if (!HasCreatedComponents)
            {
                HasCreatedComponents = true;
                OnCreateComponents();
            }

            foreach (var component in Components)
            {
                component.TriggerCreateComponents();
            }
        }
    }

    [DebuggerStepThrough]
    public virtual void TriggerInitialize()
    {
        using (Metrics.TrackTime(KnownMetrics.Timer, Metrics.TriggerInitializeTags))
        {
            if (!IsInitialized)
            {
                IsInitialized = true;
                OnInitialize();
            }

            foreach (var component in Components)
            {
                component.TriggerInitialize();
            }
        }
    }

    [DebuggerStepThrough]
    public virtual void TriggerResize()
    {
        if (!IsVisible)
            return;

        OnResize();

        foreach (var component in Components)
        {
            component.TriggerResize();
        }
    }

    [DebuggerStepThrough]
    public virtual void TriggerReleaseResources()
    {
        for (var index = Components.Count - 1; index >= 0; index--)
        {
            var component = Components[index];
            component.TriggerReleaseResources();
        }

        OnReleaseResources();
    }

    [DebuggerStepThrough]
    public override void TriggerNextSlice(float timeDelta)
    {
        if (Engine.State == GameEngineState.Quiting)
            return;

        using (Metrics.TrackTime(KnownMetrics.Timer, Metrics.TriggerUpdateTags))
        {
            OnNextSlice(timeDelta);

            if (Components.Count == 0)
                return;

            foreach (var component in Components)
            {
                component.TriggerNextSlice(timeDelta);
            }
        }
    }

    [DebuggerStepThrough]
    public override void TriggerNextFrame(float timeDelta)
    {
        if (Engine.State == GameEngineState.Quiting)
            return;

        using (Metrics.TrackTime(KnownMetrics.Timer, Metrics.TriggerUpdateTags))
        {
            OnNextFrame(timeDelta);

            if (Components.Count == 0)
                return;

            foreach (var component in Components)
            {
                component.TriggerNextFrame(timeDelta);
            }
        }
    }

    protected TParent GetParent<TParent>() where TParent : EngineComponent => (TParent)Parent;


    protected void TrackMark(MeterOptions meterOptions, MetricSetItem metricSetItem)
    {
        if (!EnableMetrics)
            return;

        Engine.MetricsRoot.Measure.Meter.Mark(meterOptions, metricSetItem);
    }

    public Lazy<string> Path => new(_path ??= GetComponentPath());

    private string GetComponentPath()
    {
        var parent = Parent;
        var path = new StringBuilder();

        while (parent != null)
        {
            var typeName = parent.TypeName.Value;
            path.Insert(0, " -> ");
            path.Insert(0, typeName);
            parent = parent.Parent;
        }

        path.Append(TypeName.Value);

        return path.ToString();
    }

    private string GetTypeName()
    {
        var type = GetType();
        if (type.IsGenericType || type.IsGenericTypeDefinition)
        {
            var list = type.GenericTypeArguments.Select(o => o.Name).ToList();
            return $"{type.Name.Remove(type.Name.IndexOf('`'))}<{string.Join(", ", list)}>";
        }

        return type.Name;
    }

    public bool Equals(EngineComponent other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return ID == other.ID;
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != GetType()) return false;
        return Equals((EngineComponent)obj);
    }

    public override int GetHashCode() => ID;

    public override void Dispose()
    {
        base.Dispose();
    }

    public virtual void TriggerDispose()
    {
        if (!IsDisposed)
        {
            IsDisposing = true;
            Dispose();
            IsDisposed = true;
        }

        for (var index = Components.Count - 1; index >= 0; index--)
        {
            var component = Components[index];
            component.TriggerDispose();
            RemoveComponent(component);
        }
    }

    public virtual void Refresh(string reason)
    {
    }

    public virtual void Clear(string reason)
    {
    }
}