namespace AncientEmpires.Engine.Render.Components;

public abstract class EngineComponentBase<TEngine, TParent> : Component
    where TEngine : GameEngine
    where TParent : EngineComponentBase<TEngine, TParent>
{
    public EngineComponent Parent { get; protected set; }
    public TEngine Engine { get; set; }
    public virtual bool IsVisible { get; set; } = true;
    
    protected virtual void InvokeResize()
    {
    }

    public abstract void TriggerNextFrame(float timeDelta);
    public abstract void TriggerNextSlice(float timeDelta);

    public virtual void OnNextSlice(float timeDelta)
    {

    }
    
    public virtual void OnNextFrame(float timeDelta)
    {

    } 
}