﻿using SharpDX;

namespace AncientEmpires.Engine.Render.Components;

/// <summary>
/// A struct to dispose <see cref="IDisposable"/>, <see cref="IReferencable"/> instances and allocated unmanaged memory.
/// </summary>
public class DisposeCollector : DisposeBase
{
    private List<object> disposables;

    /// <summary>Gets the number of elements to dispose.</summary>
    /// <value>The number of elements to dispose.</value>
    public int Count => disposables.Count;

    /// <summary>
    /// Disposes all object collected by this class and clear the list. The collector can still be used for collecting.
    /// </summary>
    /// <remarks>
    /// To completely dispose this instance and avoid further dispose, use <see cref="M:SharpDX.DisposeCollector.Dispose(System.Boolean)" /> method instead.
    /// </remarks>
    public void DisposeAndClear()
    {
        if (disposables == null)
            return;
        for (int index = disposables.Count - 1; index >= 0; --index)
        {
            if (index > disposables.Count - 1)
                continue;

            object disposable = disposables[index];
            if (disposable is IDisposable)
                ((IDisposable)disposable).Dispose();
            else
                Utilities.FreeMemory((IntPtr)disposable);
            disposables.Remove(disposable);
        }

        disposables.Clear();
    }

    /// <summary>Disposes of object resources.</summary>
    /// <param name="disposeManagedResources">If true, managed resources should be
    /// disposed of in addition to unmanaged resources.</param>
    protected override void Dispose(bool disposeManagedResources)
    {
        DisposeAndClear();
        disposables = (List<object>)null;
    }

    /// <summary>
    /// Adds a <see cref="T:System.IDisposable" /> object or a <see cref="T:System.IntPtr" /> allocated using <see cref="M:SharpDX.Utilities.AllocateMemory(System.Int32,System.Int32)" /> to the list of the objects to dispose.
    /// </summary>
    /// <param name="toDispose">To dispose.</param>
    /// <exception cref="T:System.ArgumentException">If toDispose argument is not IDisposable or a valid memory pointer allocated by <see cref="M:SharpDX.Utilities.AllocateMemory(System.Int32,System.Int32)" /></exception>
    public T Collect<T>(T toDispose)
    {
        if (GameManager.Instance.IsTestMode)
            return toDispose;
            
        if (!((object)toDispose is IDisposable) && !((object)toDispose is IntPtr))
            throw new ArgumentException("Argument must be IDisposable or IntPtr");
        if ((object)toDispose is IntPtr && !Utilities.IsMemoryAligned((IntPtr)(object)toDispose))
            throw new ArgumentException("Memory pointer is invalid. Memory must have been allocated with Utilties.AllocateMemory");
        if (!Equals((object)toDispose, (object)default(T)))
        {
            if (disposables == null)
                disposables = new List<object>();
            if (!disposables.Contains((object)toDispose))
                disposables.Add((object)toDispose);
        }

        return toDispose;
    }

    /// <summary>
    /// Dispose a disposable object and set the reference to null. Removes this object from this instance..
    /// </summary>
    /// <param name="objectToDispose">Object to dispose.</param>
    public void RemoveAndDispose<T>(ref T objectToDispose)
    {
        if (disposables == null || objectToDispose == null)
            return;
        Remove<T>(objectToDispose);
        if (objectToDispose is IDisposable disposable)
            disposable.Dispose();
        else
            Utilities.FreeMemory((IntPtr)(object)objectToDispose);
        objectToDispose = default(T);
    }

    /// <summary>
    /// Removes a disposable object to the list of the objects to dispose.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="toDisposeArg">To dispose.</param>
    public void Remove<T>(T toDisposeArg)
    {
        if (disposables == null || !disposables.Contains((object)toDisposeArg))
            return;
        disposables.Remove((object)toDisposeArg);
    }
}