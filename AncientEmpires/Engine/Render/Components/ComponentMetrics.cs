﻿using System.Diagnostics;
using App.Metrics;
using App.Metrics.Counter;
using App.Metrics.Gauge;
using App.Metrics.Timer;

namespace AncientEmpires.Engine.Render.Components;

public class ComponentMetrics
{
    public readonly IDisposable EmptyDisposable = new EmptyDisposable();
    public GameEngine Engine;
    public MetricTags TriggerCreateComponentsTags;
    public MetricTags TriggerUpdateTags;
    public MetricTags TriggerEngineReadyTags;
    public MetricTags RegisterTags;
    public MetricTags TriggerInitializeTags;
    public MetricTags TriggerResizeTags;
    public MetricTags TriggerBeforeDrawTags;
    public MetricTags TriggerDrawTags;
    public MetricTags TriggerAfterDrawTags;
    public static readonly Dictionary<Type, MetricTags> CachedTriggerUpdateTags = new();
    public static readonly Dictionary<Type, MetricTags> CachedTriggerEngineReadyTags = new();
    public static readonly Dictionary<Type, MetricTags> CachedOnCreateComponentsTags = new();
    public static readonly Dictionary<Type, MetricTags> CachedRegisterTags = new();
    public static readonly Dictionary<Type, MetricTags> CachedTriggerInitializeTags = new();
    public static readonly Dictionary<Type, MetricTags> CachedTriggerResizeTags = new();
    public static readonly Dictionary<Type, MetricTags> CachedTriggerBeforeDrawTags = new();
    public static readonly Dictionary<Type, MetricTags> CachedTriggerDrawTags = new();
    public static readonly Dictionary<Type, MetricTags> CachedTriggerAfterDrawTags = new();
    public static readonly Dictionary<(Type, string), MetricTags> CachedTags = new();
    public bool EnableMetrics = true;

    [DebuggerStepThrough]
    public MetricTags CreateTags(string method) => new(
        new[] { "Component", "Method", "Path" },
        new[] { _component.Name, method, _component.Path.Value }
    );

    private readonly EngineComponent _component;

    public ComponentMetrics(GameEngine engine, EngineComponent component, bool enableMetrics)
    {
        Engine = engine;
        EnableMetrics = enableMetrics;
        _component = component;

        if (!EnableMetrics)
            return;

        lock (GetType())
        {
            var type = _component.GetType();
            RegisterTags = CachedRegisterTags.ContainsKey(type)
                ? CachedRegisterTags[type]
                : CachedRegisterTags[type] = CreateTags(nameof(_component.Register));
            TriggerCreateComponentsTags = CachedOnCreateComponentsTags.ContainsKey(type)
                ? CachedOnCreateComponentsTags[type]
                : CachedOnCreateComponentsTags[type] = CreateTags(nameof(_component.OnCreateComponents));
            TriggerInitializeTags = CachedTriggerInitializeTags.ContainsKey(type)
                ? CachedTriggerInitializeTags[type]
                : CachedTriggerInitializeTags[type] = CreateTags(nameof(_component.OnInitialize));
            TriggerEngineReadyTags = CachedTriggerEngineReadyTags.ContainsKey(type)
                ? CachedTriggerEngineReadyTags[type]
                : CachedTriggerEngineReadyTags[type] = CreateTags(nameof(_component.TriggerEngineReady));
            TriggerUpdateTags = CachedTriggerUpdateTags.ContainsKey(type)
                ? CachedTriggerUpdateTags[type]
                : CachedTriggerUpdateTags[type] = CreateTags(nameof(_component.OnNextFrame));
            TriggerResizeTags = CachedTriggerResizeTags.ContainsKey(type)
                ? CachedTriggerResizeTags[type]
                : CachedTriggerResizeTags[type] = CreateTags(nameof(_component.OnResize));
            TriggerBeforeDrawTags = CachedTriggerBeforeDrawTags.ContainsKey(type)
                ? CachedTriggerBeforeDrawTags[type]
                : CachedTriggerBeforeDrawTags[type] = CreateTags("OnBeforeDraw");
            TriggerDrawTags = CachedTriggerDrawTags.ContainsKey(type)
                ? CachedTriggerDrawTags[type]
                : CachedTriggerDrawTags[type] = CreateTags("OnDraw");
            TriggerAfterDrawTags = CachedTriggerAfterDrawTags.ContainsKey(type)
                ? CachedTriggerAfterDrawTags[type]
                : CachedTriggerAfterDrawTags[type] = CreateTags("OnAfterDraw");
        }
    }

    [DebuggerStepThrough]
    public IDisposable TrackTime(TimerOptions timerOptions, MetricTags metricTags, string userValue = null)
    {
        if (!EnableMetrics || Engine?.Timing == null)
            return EmptyDisposable;

        return Engine.Timing.Tick % 200 == 0
            ? Engine.MetricsRoot.Measure.Timer.Time(timerOptions, metricTags, userValue)
            : EmptyDisposable;
    }

    [DebuggerStepThrough]
    public void TrackValue(GaugeOptions gaugeOptions, string tag, double value)
    {
        if (!EnableMetrics || Engine?.Timing == null)
            return;

        Engine.MetricsRoot.Measure.Gauge.SetValue(gaugeOptions, GetTags(tag), value);
    }

    [DebuggerStepThrough]
    public void IncrementCounter(CounterOptions counterOptions, string tag)
    {
        if (!EnableMetrics || Engine?.Timing == null)
            return;

        Engine.MetricsRoot.Measure.Counter.Increment(counterOptions, GetTags(tag));
    }

    [DebuggerStepThrough]
    public IDisposable TrackTimeAlways(TimerOptions timerOptions, MetricTags metricTags, string userValue = null)
    {
        if (!EnableMetrics || Engine?.Timing == null)
            return EmptyDisposable;

        return EnableMetrics
            ? Engine.MetricsRoot.Measure.Timer.Time(timerOptions, metricTags, userValue)
            : EmptyDisposable;
    }

    [DebuggerStepThrough]
    private MetricTags GetTags(string tag)
    {
        if (!CachedTags.TryGetValue((_component.GetType(), tag), out var tags))
        {
            CachedTags[(_component.GetType(), tag)] = tags = CreateTags(tag);
        }

        return tags;
    }
}