using AncientEmpires.Engine.Metrics;

namespace AncientEmpires.Engine.Render.Components;

public class RenderableComponent : EngineComponent
{
    public bool Changed = true;
    public int CleanSlice = 0;
    public virtual float ZIndex { get; set; }

    public override void OnCreateComponents()
    {
        OnCreateShaders();

        base.OnCreateComponents();
    }

    public virtual void TriggerBeforeDraw()
    {
        using (Metrics.TrackTime(KnownMetrics.Timer, Metrics.TriggerBeforeDrawTags))
        {
            OnBeforeDraw();

            foreach (var component in Components.OfType<RenderableComponent>())
            {
                component.TriggerBeforeDraw();
            }
        }
    }

    public virtual void TriggerDraw()
    {
        using (Metrics.TrackTime(KnownMetrics.Timer, Metrics.TriggerDrawTags))
        {
            OnDraw();

            foreach (var component in Components.OfType<RenderableComponent>())
            {
                component.TriggerDraw();
            }
        }
    }

    public virtual void TriggerAfterDraw()
    {
        using (Metrics.TrackTime(KnownMetrics.Timer, Metrics.TriggerAfterDrawTags))
        {
            OnAfterDraw();

            foreach (var component in Components.OfType<RenderableComponent>())
            {
                component.TriggerAfterDraw();
            }
        }
    }

    public virtual void OnCreateShaders()
    {
    }

    public virtual void OnDraw()
    {
    }

    public virtual void OnBeforeDraw()
    {
    }

    protected virtual void OnAfterDraw()
    {
    }
}