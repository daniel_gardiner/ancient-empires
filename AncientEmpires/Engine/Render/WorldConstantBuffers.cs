using System.Diagnostics;
using System.Numerics;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Map.Picker;
using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Helpers;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Render;

public class WorldConstantBuffers : GameConstantBuffers
{
    public static ConstantBuffer<SelectionConstant> SelectionConstantBuffer;
    protected static readonly Int4[] PackedSelectedEntities = new Int4[128];
    protected ComponentLookup<EntityManager> EntityManager;
    protected ComponentLookup<MapPicker> MapPicker;
    
    public override Matrix4x4 Projection
    {
        [DebuggerStepThrough] get => Engine.Camera.Projection;
    }

    public override Matrix4x4 View
    {
        [DebuggerStepThrough] get => Engine.Camera.View;
    }

    public override Matrix4x4 World
    {
        [DebuggerStepThrough] get => Engine.Camera.PanZoom;
    }

    public override void OnInitialize()
    {
        base.OnInitialize();

        Map = new ComponentLookup<GameMap>();
        MapPicker = new ComponentLookup<MapPicker>();
        MapViewport = new ComponentLookup<MapViewport>();
        GameDebug = new ComponentLookup<GameDebug>();
        EntityManager = new ComponentLookup<EntityManager>();
    }

    public override void OnEngineReady()
    {
        base.OnEngineReady();

        WorldConstantBuffer = RegisterComponent(new ConstantBuffer<WorldConstant>(ConstantBufferSlot.World));
        TextureConstantBuffer = RegisterComponent(new ConstantBuffer<TextureConstant>(ConstantBufferSlot.Texture));
        SelectionConstantBuffer = RegisterComponent(new ConstantBuffer<SelectionConstant>(ConstantBufferSlot.Selection));
    }

    public override void OnBeforeDraw()
    {
        base.OnBeforeDraw();

        UpdateSelectionBuffer();
    }

    public void UpdateSelectionBuffer()
    {
        var cursorPosition = Engine.Input.Current.CursorWorldSpace;

        var selectedEntities = EntityManager.Value.SelectedUnits.Select(o => new Int4(o.Id)).ToArray();
        Debug.Assert(selectedEntities.Length < 128, "Cannot select more than 128 entities");
        Array.Fill(PackedSelectedEntities, new Int4(), selectedEntities.Length, 128 - selectedEntities.Length);
        Array.Copy(selectedEntities, 0, PackedSelectedEntities, 0, selectedEntities.Length);

        var constant = new SelectionConstant
        {
            Cursor = new Vector4(cursorPosition.X, cursorPosition.Y, cursorPosition.X, cursorPosition.Y),
            ViewportSize = new Vector4(Engine.Camera.Viewport.Width, Engine.Camera.Viewport.Height, 0, 0),
            HoverTiles = ToRawVectorArray(MapPicker.Value.HoverTiles),
            SelectedTiles = ToRawVectorArray(MapPicker.Value.SelectedTiles),
            SelectedEntities = PackedSelectedEntities
        };
        SelectionConstantBuffer.UpdateValue(in constant);

        Vector4[] ToRawVectorArray(List<Vector4> coordinates)
        {
            var tiles = new[]
            {
                new Vector4(-1, -1, -1, -1),
                new Vector4(-1, -1, -1, -1),
                new Vector4(-1, -1, -1, -1),
                new Vector4(-1, -1, -1, -1)
            };

            for (var index = 0; index < coordinates.Count && index < tiles.Length; index++)
            {
                var selectedTile = coordinates[index];
                tiles[index] = new Vector4(selectedTile.X, selectedTile.Y, selectedTile.Z, 1);
            }

            return tiles;
        }
    }
}