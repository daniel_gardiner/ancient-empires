﻿using System.Diagnostics;
using AncientEmpires.Engine.Render.Components;
using Vortice.DXGI;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Render;

public class GraphicsPresentation : EngineComponent
{
    protected readonly GraphicsDevice GraphicsDevice;
    public ModeDescription[] DisplayModeList;
    public IDXGISwapChain1 SwapChain;
    public RenderBuffers RenderBuffers;
    public IDXGIAdapter Adapter;
    public int BufferCount = 1;
    public Viewport Viewport => RenderBuffers.Viewport;

    public GraphicsPresentation(GraphicsDevice graphicsDevice)
    {
        GraphicsDevice = graphicsDevice;
    }

    public override void OnInitialize()
    {
        base.OnInitialize();

        SwapChain = CreateSwapChain();
        RenderBuffers = AddComponent(new RenderBuffers(Engine, GraphicsDevice, this));
    }
    
    public void Resize()
    {
        var resizeBuffers = SwapChain.ResizeBuffers(
            SwapChain.Description.BufferCount,
            GraphicsDevice.Width,
            GraphicsDevice.Height,
            SwapChain.Description.BufferDescription.Format,
            SwapChain.Description.Flags);
        
        
        if (resizeBuffers.Failure)
        {
            throw new Exception("Failed to resize buffers - " + resizeBuffers.Description);
        }

        ConsoleWriter.WriteLine($"SwapChain Resized | {GraphicsDevice.Width} x {GraphicsDevice.Height}");
    }

    protected IDXGISwapChain1 CreateSwapChain()
    {
        if (GraphicsDevice.D3D12Device != null)
        {
            return CreateD3D12SwapChain();
        }
        else
        {
            return CreateD3D11SwapChain();
        }
    }

    private IDXGISwapChain1 CreateD3D11SwapChain()
    {
        var desc = new SwapChainDescription
        {
            BufferDescription = new ModeDescription
            {
                Width = GraphicsDevice.Width,
                Height = GraphicsDevice.Height,
                Format = Format.B8G8R8A8_UNorm,
                Scaling = ModeScaling.Unspecified
            },
            SampleDescription = new SampleDescription(1, 0),
            BufferUsage = Usage.Backbuffer | Usage.RenderTargetOutput,
            BufferCount = BufferCount,
            SwapEffect = SwapEffect.Sequential,
            Flags = SwapChainFlags.AllowModeSwitch,
            OutputWindow = GraphicsDevice.Window.Handle,
            Windowed = true
        };

        using var dxgiDevice2 = GraphicsDevice.D3DDevice.QueryInterface<IDXGIDevice2>();
        using var dxgiAdapter = dxgiDevice2.GetAdapter();
        using var dxgiFactory2 = dxgiAdapter.GetParent<IDXGIFactory2>();
        using var output = dxgiAdapter.GetOutput(0);
        DisplayModeList = output?.GetDisplayModeList(Format.B8G8R8A8_UNorm, DisplayModeEnumerationFlags.Interlaced);

        return dxgiFactory2!.CreateSwapChain(GraphicsDevice.D3DDevice, desc).QueryInterface<IDXGISwapChain1>();
    }

    private IDXGISwapChain1 CreateD3D12SwapChain()
    {
        var desc = new SwapChainDescription1()
        {
            Width = GraphicsDevice.Width,
            Height = GraphicsDevice.Height,
            Format = Format.B8G8R8A8_UNorm,
            Scaling = Scaling.Stretch,
            SampleDescription = new SampleDescription(1, 0),
            BufferUsage = /*Usage.Backbuffer | */Usage.RenderTargetOutput,
            BufferCount = BufferCount,
            SwapEffect = SwapEffect.FlipDiscard,
            Flags = SwapChainFlags.AllowModeSwitch,
        };

        using var dxgiDevice1 = GraphicsDevice.D3DDevice.QueryInterface<IDXGIDevice1>();
        using var dxgiAdapter = dxgiDevice1.GetAdapter();
        using var dxgiFactory4 = dxgiAdapter.GetParent<IDXGIFactory4>();
        using var output = dxgiAdapter.GetOutput(0);
        DisplayModeList = output?.GetDisplayModeList(Format.B8G8R8A8_UNorm, DisplayModeEnumerationFlags.Interlaced);

        Debug.Assert(dxgiFactory4 != null, nameof(dxgiFactory4) + " != null");

        var swapChain = dxgiFactory4.CreateSwapChainForHwnd(GraphicsDevice.D3D12CommandQueue, GraphicsDevice.Window.Handle, desc);
        var swapChain3 = swapChain.QueryInterface<IDXGISwapChain3>();
        swapChain.Release();
        return swapChain3;
    }

    protected virtual SwapChainFullscreenDescription CreateFullScreenDescription() =>
        new()
        {
            RefreshRate = new Rational(144, 1),
            Scaling = ModeScaling.Centered,
            Windowed = true
        };

    public void StartDraw()
    {
        GraphicsDevice.ClearShaderResources();
        GraphicsDevice.D3DContext.OMSetDepthStencilState(RenderBuffers.DepthStencilState, 1);
        GraphicsDevice.D3DContext.RSSetViewport(RenderBuffers.BackBufferTarget.Viewport);
        RenderBuffers.BackBufferTarget.ClearRenderTarget(GraphicsDevice.D3DContext, Color4.Transparent);
    }

    public void Present()
    {
        /*GraphicsDevice.D3DContext.CopyResource(
            RenderBuffers.BackBufferTarget.RenderTargetTexture, 
            RenderBuffers.TextureTarget.RenderTargetTexture);*/
        SwapChain.Present(0, PresentFlags.None);
    }

    public override void Dispose()
    {
        RenderBuffers?.Dispose();
        RenderBuffers = null;
        SwapChain?.Dispose();
        SwapChain = null;
    }
}