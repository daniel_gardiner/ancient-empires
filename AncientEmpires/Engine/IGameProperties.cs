﻿namespace AncientEmpires.Engine.Events;

public interface IGameProperties
{
    Dictionary<string, object> Properties { get; }
}