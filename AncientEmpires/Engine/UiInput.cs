using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Render.Components;

namespace AncientEmpires.Engine;

public class UiInput : EngineComponent
{
    public void ProcessInput(InputState state)
    {
        ProcessInputForElements(state, Engine.Components.OfType<IUiElement>().ToList());
    }

    private void ProcessInputForElements(InputState state, List<IUiElement> uiElements)
    {
        foreach (var uiElement in uiElements)
        {
            if (!uiElement.HitTest(state.CursorPosition))
            {
                if (uiElement.IsMouseOver)
                {
                    uiElement.MouseExit(Engine, state);
                }
            }
            else
            {
                ProcessInputForElements(state, uiElement.UiChildren.ToList());

                uiElement.MouseOver(Engine, state);

                if (state.LeftClick) 
                    uiElement.LeftClick(Engine, state);

                if (state.LeftDown)
                    uiElement.LeftDown(Engine, state);

                if (state.RightClick)
                    uiElement.RightClick(Engine, state);

                if (state.RightDown)
                    uiElement.RightDown(Engine, state);
            }
        }
    }
}