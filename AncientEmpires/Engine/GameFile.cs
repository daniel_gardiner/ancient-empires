﻿using System.IO.Abstractions;

namespace AncientEmpires.Engine;

public class GameFile
{
    private readonly IFileSystem _fileSystem;
    public string Name { set; get; }
    public string FullPath { get; }
    public long Length { get; }
    public DateTime DateCreated { get; }
    public DateTime DateUpdated { get; }

    public GameFile(string name,
        string fullPath,
        long length,
        DateTime dateCreated,
        DateTime dateUpdated, 
        IFileSystem fileSystem)
    {
        _fileSystem = fileSystem;
        Name = name;
        FullPath = fullPath;
        Length = length;
        DateCreated = dateCreated;
        DateUpdated = dateUpdated;
    }

    public string ReadAllText()
    {
        return _fileSystem.File.ReadAllText(FullPath);
    }
}