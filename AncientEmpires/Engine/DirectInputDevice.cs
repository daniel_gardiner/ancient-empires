using Vortice.DirectInput;

namespace AncientEmpires.Engine;

public sealed class DirectInputDevice
{
    private readonly IDirectInput8 _directInput;

    private Dictionary<Guid, DeviceInstance> _keyboards;
    private Dictionary<Guid, DeviceInstance> _mouses;

    private readonly Dictionary<Guid, IDirectInputDevice8> _keyboardDevices;
    private readonly Dictionary<Guid, IDirectInputDevice8> _mouseDevices;

    public DirectInputDevice()
    {
        _directInput = DInput.DirectInput8Create();

        _keyboardDevices = new Dictionary<Guid, IDirectInputDevice8>();
        _mouseDevices = new Dictionary<Guid, IDirectInputDevice8>();
    }

    public void Initialize(IntPtr handle)
    {
        try
        {
            _keyboards = EnumerateAllAttachedKeyboardDevices(_directInput);

            foreach (var item in _keyboards.Keys)
            {
                InitializeKeyboardDevice(_directInput, item, handle);
            }

            _mouses = EnumerateAllAttachedPointerDevices(_directInput);

            foreach (var item in _mouses.Keys)
            {
                InitializeMouseDevice(_directInput, item, handle);
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }

    private void InitializeKeyboardDevice(IDirectInput8 directInput, Guid guid, IntPtr windowhandle)
    {
        var inputDevice = directInput.CreateDevice(guid);
        inputDevice.SetCooperativeLevel(windowhandle, CooperativeLevel.NonExclusive | CooperativeLevel.Foreground);

        inputDevice.Properties.BufferSize = 16;

        if (directInput.IsDeviceAttached(guid))
        {
            var result = inputDevice.SetDataFormat<RawKeyboardState>();

            if (result.Success)
            {
                _keyboardDevices.Add(guid, inputDevice);
            }
        }
    }

    private void InitializeMouseDevice(IDirectInput8 directInput, Guid guid, IntPtr windowhandle)
    {
        var inputDevice = directInput.CreateDevice(guid);
        inputDevice.SetCooperativeLevel(windowhandle, CooperativeLevel.NonExclusive | CooperativeLevel.Foreground);

        inputDevice.Properties.BufferSize = 16;

        if (directInput.IsDeviceAttached(guid))
        {
            var result = inputDevice.SetDataFormat<RawMouseState>();

            if (result.Success)
            {
                _mouseDevices.Add(guid, inputDevice);
            }
        }
    }

    public IEnumerable<KeyboardUpdate[]> GetKeyboardUpdates()
    {
        foreach (var keyboard in _keyboardDevices.Values)
        {
            var result = keyboard.Poll();

            if (result.Failure)
            {
                result = keyboard.Acquire();

                if (result.Failure)
                {
                    ConsoleWriter.WriteLine("Failed to acquire keyboard device");
                    yield break;
                }
            }

            yield return keyboard.GetBufferedKeyboardData();
        }
    }
    
    public IEnumerable<MouseUpdate[]> GetMouseUpdates()
    {
        foreach (var mouse in _mouseDevices)
        {
            var result = mouse.Value.Poll();

            if (result.Failure)
            {
                result = mouse.Value.Acquire();

                if (result.Failure)
                    yield break;
            }

            yield return mouse.Value.GetBufferedMouseData();
        }
    }

    public static Dictionary<Guid, DeviceInstance> EnumerateAllAttachedKeyboardDevices(IDirectInput8 directInput)
    {
        Dictionary<Guid, DeviceInstance> result = new();

        foreach (var deviceInstance in directInput.GetDevices(DeviceClass.Keyboard, DeviceEnumerationFlags.AttachedOnly))
        {
            if (deviceInstance.Type == DeviceType.Keyboard)
            {
                result.Add(deviceInstance.InstanceGuid, deviceInstance);
            }
            else
            {
                Console.WriteLine(deviceInstance.ProductName + " does not match input type, ignored.");
            }
        }

        return result;
    }

    static public Dictionary<Guid, DeviceInstance> EnumerateAllAttachedPointerDevices(IDirectInput8 directInput)
    {
        var connectedDeviceList = new Dictionary<Guid, DeviceInstance>();

        foreach (var deviceInstance in directInput.GetDevices(DeviceClass.Pointer, DeviceEnumerationFlags.AttachedOnly))
        {
            if (deviceInstance.Type == DeviceType.Mouse)
            {
                connectedDeviceList.Add(deviceInstance.InstanceGuid, deviceInstance);
            }
            else
            {
                Console.WriteLine(deviceInstance.ProductName + " does not match input type, ignored.");
            }
        }

        return connectedDeviceList;
    }
}