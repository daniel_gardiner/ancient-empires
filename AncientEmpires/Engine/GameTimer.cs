namespace AncientEmpires.Engine;

public class GameTimer
{
    public string Name;
    public double StartMs;

    public GameTimer(string name, float elapsedMs)
    {
        Name = name;
        StartMs = elapsedMs;
    }

    public bool IsComplete(float elapsedMs, double duration)
    {
        if (elapsedMs >= StartMs + duration)
            return true;

        return false;
    }

    public void Update(float elapsedMs, double duration)
    {
        StartMs += duration;

        if (IsComplete(elapsedMs, duration))
            StartMs = elapsedMs + duration;
    }
}