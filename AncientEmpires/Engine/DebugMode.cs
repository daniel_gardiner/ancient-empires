namespace AncientEmpires.Engine;

public enum DebugMode
{
    None,
    Path,
    Full,
    //PartialRender,
    //OffsetRender,
}