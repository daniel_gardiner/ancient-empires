namespace AncientEmpires.Engine;

public enum TextureType
{
    None = 0,
    Textures = 1,
    Buildings = 2,
    Units = 3,
    MiniMap = 5,
    Interface = 6,
    Paths = 7,
    Terrain = 8,
    Romans = 20,
    Romans2x = 21,
    RomansOriginal = 22
}

public class SpriteLoop
{
}