﻿using AncientEmpires.Engine.Render.Components;

namespace AncientEmpires.Engine;

public class StringCache : EngineComponent
{
    private readonly Dictionary<double, string> _stringCache = new();

    public string GetString(float number)
    {
        number *= 10;
        var intNumber = Math.Truncate(number);
            
        if (_stringCache.TryGetValue(intNumber, out var text))
            return text;

        text = $"{number:N1}";
        _stringCache.Add(intNumber, text);
        return text;
    }
}