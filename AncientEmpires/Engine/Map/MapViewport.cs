using System.Numerics;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Layout;
using AncientEmpires.Engine.Render.Interface;
using AncientEmpires.Engine.Render.Quads;

namespace AncientEmpires.Engine.Map;

public sealed class MapViewport : UiElement
{
    public MapViewport(GameEngine engine, GameInterface parent, GameLayout layout) : base(engine, parent, layout)
    {
        
    }

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();
        
        AddComponent(new DragSelection(Engine, this, new GameLayout(Engine, Layout).Fill()));
    }

    public override void OnInitialize()
    {
        base.OnInitialize();
        
        ZIndex = Engine.Configuration.GetRenderLayer(KnownRenderLayers.MapOverlay);
        BackgroundColor = new Vector4(0.8f, 0.2f, 0.2f, 1f);
        ShaderMaterial = ShaderMaterial.Transparent;
    }

    protected override void OnCreateFragment()
    {
        Fragment = RegisterComponent<InterfaceMeshFragment>().SetZIndex(ZIndex);
    }

    public override void OnNextFrame(float timeDelta)
    {
        base.OnNextFrame(timeDelta);
    }
}