using AncientEmpires.Engine.Render;

namespace AncientEmpires.Engine.Map.Rendering;

public class Terrain
{
    public static readonly List<Sprite> GrassSprites = new()
    {
        new Sprite(Rect(0, 0), new List<int>(1)),
        new Sprite(Rect (128, 0), new List<int>(1)),
        new Sprite(Rect(0, 64), new List<int>(1)),
        new Sprite(Rect (128, 64), new List<int>(1)),
    };
        
    private static RectangleF Rect(int left, int top) => new(left, top, left + 128, top + 60);

    public int Style;

    public TerrainType Type;
    public float MovementCost;

    public Sprite Sprite => Type switch
    {
        TerrainType.None => Sprite.Empty,
        TerrainType.Grass => GrassSprites[Style],
        TerrainType.Water => Sprite.Empty,
        _ => Sprite.Empty
    };
}