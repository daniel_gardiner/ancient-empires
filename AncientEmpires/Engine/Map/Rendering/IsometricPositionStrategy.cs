﻿using System.Numerics;
using AncientEmpires.Engine.Render.Components;

namespace AncientEmpires.Engine.Map.Rendering;

public class IsometricPositionStrategy : EngineComponent, IPositionStrategy
{
    public Vector4 ProjectedMapSize { get; }
    public Vector4 HalfProjectedMapSize { get; }
    public Vector4 CenterOfMapProjected { get; }
    public Vector2 CenterOfMapXY { get; }
    public readonly Vector4 TilePixelSize;
    protected readonly Vector4 HalfTileSize;
    protected readonly Vector4 TileSize;
    protected readonly Vector4 HalfTilePixelSize;
    protected readonly Size HalfMapSize;
    protected readonly Size MapSize;


    public IsometricPositionStrategy()
    {
        TilePixelSize = Engine.Configuration.TilePixelSize;
        TileSize = Engine.Configuration.TileSize;
        MapSize = Engine.Configuration.MapSize;
        HalfTileSize = TileSize / 2;
        HalfMapSize = MapSize / 2;
        HalfTilePixelSize = TilePixelSize / 2;
        ProjectedMapSize = new Vector4(MapSize.Width, MapSize.Height, 0, 0) * TilePixelSize;
        HalfProjectedMapSize = ProjectedMapSize / 2;
        CenterOfMapXY = new Vector2(HalfMapSize.Width, HalfMapSize.Height);
    }

    public Vector4 ProjectWithCenterAtZero(in Vector4 position) => ProjectWithCenterAtZero(in position, position.Z);

    public Vector4 ProjectWithCenterAtZero(in Vector4 position, Vector4 padding)
    {
        return ProjectWithCenterAtZero(in position, position.Z) - padding / 2;
    }

    public Vector4 ProjectWithCenterAtZero(in Vector4 position, float zIndex)
    {
        var projected = Project(in position, zIndex);
        var offset = projected - HalfProjectedMapSize;
        return new Vector4(offset.X, -offset.Y, zIndex, 0);
    }

    public Vector4 Project(Vector4 position, float tileHeight = 64f)
    {
        return Project(in position, tileHeight);
    }
    
    public Vector4 Project(in Vector4 position, float tileHeight = 64f)
    {
        var result = new Vector4(
            (position.X - position.Y) * HalfTilePixelSize.X + HalfProjectedMapSize.X,
            (position.X + position.Y) * HalfTilePixelSize.Y,
            position.Z,
            0);
        return result;
    }

    public Vector4 Unproject(in Vector4 position)
    {
        return new Vector4(
            (position.X / TilePixelSize.X - position.Y / TilePixelSize.Y + HalfMapSize.Width + 0.5f),
            -(position.Y / TilePixelSize.Y + position.X / TilePixelSize.X - HalfMapSize.Height - 0.5f),
            position.Z,
            position.W);
    }

    public Vector4 EnsureConstrained(Vector4 result)
    {
        if (result.X < 0 || result.X > MapSize.Width)
            return ConstrainPosition(result);
        if (result.Y < 0 || result.Y > MapSize.Height)
            return ConstrainPosition(result);
        return result;
    }

    public Vector4 ConstrainPosition(Vector4 result) =>
        new(
            Math.Min(MapSize.Width - 1, Math.Max(0, result.X)),
            Math.Min(MapSize.Height - 1, Math.Max(0, result.Y)),
            result.Z,
            result.W);
}