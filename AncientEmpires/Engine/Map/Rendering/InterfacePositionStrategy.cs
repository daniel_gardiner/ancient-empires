using System.Numerics;
using AncientEmpires.Engine.Render.Components;

namespace AncientEmpires.Engine.Map.Rendering;

public class InterfacePositionStrategy : EngineComponent, IPositionStrategy
{
    public Vector4 CenterOfMapProjected { get; }
    public Vector2 CenterOfMapXY { get; }
    public Vector4 ProjectedMapSize { get; }
    public Vector4 HalfProjectedMapSize { get; }
    public Vector4 Project(in GamePosition position, float tileHeight = 64f) => new(position.MapSpace.X, position.MapSpace.Y, position.MapSpace.Z, 0);
    public Vector4 Project(in Vector4 position, float tileHeight = 64) => throw new NotImplementedException();
    public Vector4 ProjectWithCenterAtZero(in Vector4 position) => throw new NotImplementedException();
    public Vector4 ProjectWithCenterAtZero(float x, float y, float zIndex) => throw new NotImplementedException();
    public Vector4 ProjectWithCenterAtZero(in Vector4 position, Vector4 padding) => throw new System.NotImplementedException();
    public Vector4 ProjectWithCenterAtZero(in GamePosition position, float depth) => throw new NotImplementedException();

    public Vector4 CalculateCenter() => throw new System.NotImplementedException();
    public Vector4 ProjectWithCenterAtZero(in Vector4 position, float depth) => throw new System.NotImplementedException();
}