namespace AncientEmpires.Engine.Map.Rendering;

public enum TerrainType
{
    None = 0,
    Grass = 1,
    Water = 100
}