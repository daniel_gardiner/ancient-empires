﻿using System.Numerics;

namespace AncientEmpires.Engine.Map.Rendering;

public interface IPositionStrategy
{
    Vector4 ProjectedMapSize { get; }
    Vector4 HalfProjectedMapSize { get; }
    Vector4 Project(in Vector4 position, float tileHeight = 64f);
    Vector4 ProjectWithCenterAtZero(in Vector4 position);
    Vector4 ProjectWithCenterAtZero(in Vector4 position, Vector4 padding);
    Vector4 ProjectWithCenterAtZero(in Vector4 position, float depth);
}