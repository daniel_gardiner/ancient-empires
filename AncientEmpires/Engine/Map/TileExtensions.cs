using AncientEmpires.Engine.Entities;
using AncientEmpires.Engine.Map.Picker;

namespace AncientEmpires.Engine.Map;

public static class MapTileExtensions
{
    public static bool Has<TEntity>(this MapTile tile) where TEntity : Entity =>
        tile?.Entities.OfType<TEntity>().Any() ?? false;

    public static List<TEntity> Get<TEntity>(this MapTile tile) where TEntity : Entity =>
        tile?.Entities.OfType<TEntity>().ToList();
    public static IEnumerable<MapTile> Flatten(this MapTile[,] tiles)
    {
        for (var y = 0; y < tiles.GetLength(1); y++)
        for (var x = 0; x < tiles.GetLength(0); x++)
        {
            yield return tiles[x, y];
        }
    }
}