using System.Numerics;
using AncientEmpires.Engine.Actions;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Layout;
using AncientEmpires.Engine.Render.Interface;
using AncientEmpires.Engine.Render.Quads;

namespace AncientEmpires.Engine.Map;

public class DragSelection : UiElement
{
    public DragSelection(GameEngine engine, IUiElement parent, GameLayout layout) : base(engine, parent, layout)
    {
            
    }

    public override void OnInitialize()
    {
        base.OnInitialize();

        ZIndex = Engine.Configuration.GetRenderLayer(KnownRenderLayers.MapOverlay) - 0.3f;
        BackgroundColor = new Vector4(0.5f, 0.5f, 0, 0.5f);
        ShaderMaterial = ShaderMaterial.DragSelectionAction;
        IsVisible = false;
    }

    protected override void OnCreateFragment()
    {
        Fragment = RegisterComponent<InterfaceMeshFragment>().SetZIndex(ZIndex);
    }

    private bool IsActive()
    {
        if (!Engine.Actions.TryGetAction<DragSelectEntities>(out var action))
            return false;

        return action.IsActive;
    }

    public override void OnNextFrame(float timeDelta)
    {
        if (IsActive())
        {
            Changed = true;
            IsVisible = true;
        }
        else
        {
            IsVisible = false;
        }

        base.OnNextFrame(timeDelta);
    }

    public override void OnDraw()
    {
        base.OnDraw();
    }
}