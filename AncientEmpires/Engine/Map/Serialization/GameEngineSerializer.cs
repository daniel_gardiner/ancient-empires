using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Serialization;

namespace AncientEmpires.Engine.Map.Serialization;

public class GameEngineSerializer : ISerializer<GameEngine, DehydratedGame>
{
    public GameEngine Hydrate(GameEngine hydrated, DehydratedGame dehydrated)
    {
        HydrateComponent(hydrated.GetComponent<GameMap>(), dehydrated.Map);
        HydrateComponent(hydrated.GetComponent<EntityManager>(), dehydrated.EntityManager);

        return hydrated;
    }

    public DehydratedGame Dehydrate(GameEngine input)
    {
        return new()
        {
            Map = DehydrateComponent<GameMap, DehydratedMap>(input),
            EntityManager = DehydrateComponent<EntityManager, DehydratedEntityManager>(input),
        };
    }

    private void HydrateComponent<THydrated, TDehydrated>(THydrated hydrated, TDehydrated dehydrated)
        where THydrated : EngineComponent, ISerializable<THydrated, TDehydrated>
    {
        hydrated.Serializer.Hydrate(hydrated, dehydrated);
    }

    private TDehydrated DehydrateComponent<THydrated, TDehydrated>(EngineComponent input)
        where THydrated : EngineComponent, ISerializable<THydrated, TDehydrated>
    {
        var component = input.GetComponent<THydrated>();
        return component.Serializer.Dehydrate(component);
    }
}