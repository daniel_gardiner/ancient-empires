using AncientEmpires.Engine.Entities;

namespace AncientEmpires.Engine.Map.Serialization;

public class DehydratedGame
{
    public DehydratedMap Map { get; set; }
    public DehydratedEntityManager EntityManager { get; set; }
}

public class DehydratedEntityManager
{
    public int NextEntityId { get; set; }
    public IEnumerable<DehydratedEntity> Entities { get; set; }
}

public class DehydratedMap
{
    public string MapSize { get; set; }
    public string TileSize { get; set; }
    public List<DehydratedTile> Tiles { get; set; }
}