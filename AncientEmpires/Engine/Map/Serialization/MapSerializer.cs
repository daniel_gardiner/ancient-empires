using System.Numerics;
using AncientEmpires.Engine.Map.Picker;
using AncientEmpires.Engine.Serialization;

namespace AncientEmpires.Engine.Map.Serialization;

public class MapSerializer : ISerializer<GameMap, DehydratedMap>
{
    public GameMap Hydrate(GameMap hydrated, DehydratedMap dehydrated)
    {
        var mapSize = ParseSize(dehydrated.MapSize);
        var tileSize = ParseSize(dehydrated.TileSize);
        hydrated.Size = mapSize;
        hydrated.TileSize = tileSize;

        foreach (var dehydratedMapTile in dehydrated.Tiles)
        {
            var tile = MapTile.Hydrate(hydrated, dehydratedMapTile);
            hydrated.Tiles[(int) tile.Coordinates.X, (int) tile.Coordinates.Y] = tile;
        }
        return hydrated;
    }

    public DehydratedMap Dehydrate(GameMap input)
    {
        return new()
        {
            MapSize = $"({input.Size.X},{input.Size.Y})",
            TileSize = $"({input.TileSize.X},{input.TileSize.Y})",
            Tiles = input.Tiles.Flatten().Select(o => o.Dehydrate()).ToList()
        };
    }

    private static Vector4 ParseSize(string dehydratedSize)
    {
        var size = dehydratedSize.Trim('(', ')').Split(',');
        return new Vector4(float.Parse(size[0]), float.Parse(size[1]), 0, 0);
    }
}