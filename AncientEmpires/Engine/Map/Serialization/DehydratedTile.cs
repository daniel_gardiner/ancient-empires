using Newtonsoft.Json;

namespace AncientEmpires.Engine.Map.Serialization;

public class DehydratedTile
{
    [JsonProperty("L")]
    public string Location { get; set; }

    [JsonProperty("T")]
    public string TerrainSprite { get; set; }

    [JsonProperty("E")]
    public string Entities { get; set; }
}