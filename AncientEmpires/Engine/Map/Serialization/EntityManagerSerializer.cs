using AncientEmpires.Engine.Entities;
using AncientEmpires.Engine.Serialization;

namespace AncientEmpires.Engine.Map.Serialization;

public class EntityManagerSerializer : ISerializer<EntityManager, DehydratedEntityManager>
{
    public EntityManager Hydrate(EntityManager hydrated, DehydratedEntityManager dehydrated)
    {
        hydrated.NextEntityId = dehydrated.NextEntityId;
            
        foreach (var dehydratedEntity in dehydrated.Entities)
            Entity.Hydrate(hydrated, dehydratedEntity);
            
        return hydrated;
    }

    public DehydratedEntityManager Dehydrate(EntityManager input)
    {
        return new()
        {
            NextEntityId = input.NextEntityId,
            Entities = DehydrateEntities(input.GetAllEntities())
        };
    }

    private IEnumerable<DehydratedEntity> DehydrateEntities(IEnumerable<Entity> entities)
    {
        foreach (var entity in entities)
        {
            yield return entity.Dehydrate();
        }
    }
}