using System.Numerics;
using AncientEmpires.Engine.Render.Components;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Map.Picker;

public class MapPicker : EngineComponent
{
    protected ComponentLookup<GameMap> Map;
    protected ComponentLookup<GameDebug> GameDebug;
    public (bool found, Vector4 coordinates) CursorTile { get; private set; }
    public (bool found, Vector4 coordinates) CursorTilePosition { get; private set; }
    public List<Vector4> HoverTiles { get; set; } = new();
    public List<Vector4> SelectedTiles { get; set; } = new();
    public Viewport ViewPortBounds { get; set; }

    public override void OnInitialize()
    {
        base.OnInitialize();

        GameDebug = new ComponentLookup<GameDebug>();
        Map = new ComponentLookup<GameMap>();
    }

    public override void OnNextFrame(float timeDelta)
    {
        base.OnNextFrame(timeDelta);

        CursorTile = PickFromCursor(Engine.Input.Current.CursorPosition);
        CursorTilePosition = PickPositionFromCursor(Engine.Input.Current.CursorPosition);

        if (GameDebug.Value.Enabled)
        {
            HoverTiles = new List<Vector4> { CursorTile.coordinates };
            SelectedTiles = new List<Vector4> { CursorTile.coordinates };
        }
        else
        {
            HoverTiles = new List<Vector4>();
            SelectedTiles = new List<Vector4>();
        }
    }

    public override void OnResize()
    {
        base.OnResize();

        ViewPortBounds = new Viewport(0, 0, Engine.ScreenSize.X, Engine.ScreenSize.Y);
    }

    public (bool found, Vector4 coordinates) PickPositionFromCursor(Vector4 cursor)
    {
        return (true, Engine.Camera.ScreenSpaceToMapSpace(cursor));
    }

    public (bool found, Vector4 coordinates) PickFromCursor(Vector4 cursor)
    {
        var exactPosition = Engine.Camera.ScreenSpaceToMapSpace(cursor);
        return (true, new Vector4(
            (int)Math.Floor(exactPosition.X),
            (int)Math.Floor(exactPosition.Y),
            0,
            0));
    }
}