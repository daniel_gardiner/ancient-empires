using System.Numerics;
using System.Text.RegularExpressions;
using AncientEmpires.Engine.Entities;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Map.Rendering;
using AncientEmpires.Engine.Map.Serialization;
using AncientEmpires.Engine.Render;
using AncientEmpires.Engine.Render.Quads;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Map.Picker;

public class MapTile : IEquatable<MapTile>, IGameSerializable
{
    public Dictionary<string, object> Properties { get; } = new();

    public Sprite Sprite
    {
        get => GetProperty<Sprite>(nameof(Sprite));
        set => SetProperty(nameof(Sprite), value);
    }

    public Vector4 Padding
    {
        get => (Vector4)Properties[nameof(Padding)];
        set => Properties[nameof(Padding)] = value;
    }

    public Terrain Terrain
    {
        get => (Terrain)Properties[nameof(Terrain)];
        set => Properties[nameof(Terrain)] = value;
    }

    public Quad Quad;
    public Vector4 Coordinates;
    public List<Entity> Entities;
    public BoundingBox BoundingBox;

    public MapTile(Vector4 coordinates, Sprite sprite, Vector4 padding)
    {
        Sprite = sprite;
        Padding = padding;
        Coordinates = coordinates;
        Configure();
    }

    public MapTile(Dictionary<string, object> properties)
    {
        Properties = new Dictionary<string, object>(Properties);
        Configure();
    }

    public T GetProperty<T>(string name) => Properties[name] is T
        ? (T)Properties[name]
        : throw new Exception($"Property {name} is not of type {typeof(T)}");

    public void SetProperty<T>(string name, T value)
    {
        Properties[name] = value;
    }

    private void Configure()
    {
        BoundingBox = new BoundingBox(
            Coordinates.ToVector3(),
            Coordinates.ToVector3() + Vector3.One
        );
        Terrain = new Terrain();
        Entities = new List<Entity>();
    }

    public MapTile(Vector4 coordinates)
    {
        Coordinates = coordinates;

        Terrain = new Terrain();
        Entities = new List<Entity>();
    }

    public DehydratedTile Dehydrate()
    {
        return new DehydratedTile()
        {
            Location = $"({Coordinates.X},{Coordinates.Y})",
            TerrainSprite = $"{Sprite.Offset}",
            Entities = Entities.Any()
                ? $"{string.Join("|", Entities.Select(o => $"{o.Id}; {o.Type}"))}"
                : null,
        };
    }

    public static MapTile Hydrate(GameMap map, DehydratedTile dehydratedTile)
    {
        var location = HydrateLocation();
        var terrainSprite = HydrateTerrain();

        return new MapTile(location)
        {
            Sprite = terrainSprite
        };

        Vector4 HydrateLocation()
        {
            var coords = dehydratedTile.Location.Trim('(', ')').Split(',');
            return new Vector4(Convert.ToInt32(coords[0]), Convert.ToInt32(coords[1]), 0, 0);
        }

        Sprite HydrateTerrain()
        {
            var terrain = dehydratedTile.TerrainSprite.Split('|');
            var groups = Regex.Match(terrain[0], @"X:(?<x>\d+)\sY:(?<y>\d+)\sZ:(?<z>\d+)");
            var offset = new Vector4(Convert.ToInt32(groups.Groups["x"].Value), Convert.ToInt32(groups.Groups["y"].Value), Convert.ToInt32(groups.Groups["z"].Value), 0);
            return new Sprite(offset, map.TileSize);
        }
    }

    public bool Equals(MapTile other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return Coordinates.Equals(other.Coordinates);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != GetType()) return false;
        return Equals((MapTile)obj);
    }

    public override int GetHashCode() => Coordinates.GetHashCode();
}