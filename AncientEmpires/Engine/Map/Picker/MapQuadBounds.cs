using AncientEmpires.Engine.Data;

namespace AncientEmpires.Engine.Map.Picker;

public class MapQuadBounds : IQuadTreeObjectBounds<TileBoundingBox>
{
    public float GetLeft(TileBoundingBox obj) => obj.BoundingBox.Minimum.X;
    public float GetRight(TileBoundingBox obj) => obj.BoundingBox.Maximum.X;
    public float GetTop(TileBoundingBox obj) => obj.BoundingBox.Minimum.Y;
    public float GetBottom(TileBoundingBox obj) => obj.BoundingBox.Maximum.Y;
}