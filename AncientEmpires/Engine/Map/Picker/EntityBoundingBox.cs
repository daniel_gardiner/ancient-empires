using System.Diagnostics;
using AncientEmpires.Engine.Entities;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Map.Picker;

public readonly struct EntityBoundingBox : IEquatable<EntityBoundingBox>, IFormattable
{
    public EntityBoundingBox(Entity entity, BoundingBox boundingBox)
    {
        Entity = entity;
        BoundingBox = boundingBox;
    }

    public readonly Entity Entity;
    public readonly BoundingBox BoundingBox;
    public bool Equals(EntityBoundingBox other) => Equals(Entity, other.Entity);
    public override bool Equals(object obj) => obj is EntityBoundingBox other && Equals(other);
    public override int GetHashCode() => Entity?.GetHashCode() ?? 0;
    public string ToString(string format, IFormatProvider formatProvider) => BoundingBox.ToString(format, formatProvider);
}