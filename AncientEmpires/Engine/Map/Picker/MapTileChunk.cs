namespace AncientEmpires.Engine.Map.Picker;

public class MapTileChunk
{
    public MapTile[,] Tiles { get; }

    public MapTileChunk(MapTile[,] tiles)
    {
        Tiles = tiles;
    }
}