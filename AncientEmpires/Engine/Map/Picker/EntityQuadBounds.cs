using AncientEmpires.Engine.Data;

namespace AncientEmpires.Engine.Map.Picker;

public class EntityQuadBounds : IQuadTreeObjectBounds<EntityBoundingBox>
{
    public float GetLeft(EntityBoundingBox obj) => obj.BoundingBox.Minimum.X;
    public float GetRight(EntityBoundingBox obj) => obj.BoundingBox.Maximum.X;
    public float GetTop(EntityBoundingBox obj) => obj.BoundingBox.Minimum.Y;
    public float GetBottom(EntityBoundingBox obj) => obj.BoundingBox.Maximum.Y;
}