using System.Numerics;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Map.Picker;

public struct TileBoundingBox : IEquatable<TileBoundingBox>, IFormattable
{
    public TileBoundingBox(Vector4 coordinates, BoundingBox boundingBox)
    {
        Coordinates = coordinates;
        BoundingBox = boundingBox;
    }

    public Vector4 Coordinates;
    public BoundingBox BoundingBox;

    public string ToString(string format, IFormatProvider formatProvider) => BoundingBox.ToString(format, formatProvider);
    public bool Equals(TileBoundingBox other) => Coordinates.Equals(other.Coordinates);
    public override bool Equals(object obj) => obj is TileBoundingBox other && Equals(other);
    public override int GetHashCode()
    {
        unchecked
        {
            return (Coordinates.GetHashCode() * 397) ^ BoundingBox.GetHashCode();
        }
    }
}