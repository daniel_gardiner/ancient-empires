using System.Numerics;
using AncientEmpires.Engine.Data;
using AncientEmpires.Engine.Entities;
using AncientEmpires.Engine.Render.Components;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Map.Picker;

public class EntityPicker : EngineComponent
{
    public (bool found, Vector4 coordinates) CursorTile;
    public List<Vector4> HoverTiles = new();
    public List<Vector4> SelectedTiles = new();
    public Viewport ViewPortBounds;
    protected ComponentLookup<EntityManager> EntityManager;
    protected ComponentLookup<EntityLookup> EntityLookup;
    protected ComponentLookup<GameDebug> GameDebug;

    public override void OnInitialize()
    {
        base.OnInitialize();

        EntityManager = new ComponentLookup<EntityManager>();
        EntityLookup = new ComponentLookup<EntityLookup>();
        GameDebug = new ComponentLookup<GameDebug>();
    }

    public override void OnNextFrame(float timeDelta)
    {
        base.OnNextFrame(timeDelta);

        if (Engine.Timing.Tick % 10 != 0)
            return;

        if (GameDebug.Value.Enabled)
        {
            HoverTiles = new List<Vector4> { CursorTile.coordinates };
            SelectedTiles = new List<Vector4> { CursorTile.coordinates };
        }
        else
        {
            HoverTiles = new List<Vector4>();
            SelectedTiles = new List<Vector4>();
        }
    }

    public override void OnResize()
    {
        base.OnResize();

        ViewPortBounds = new Viewport(0, 0, Engine.ScreenSize.X, Engine.ScreenSize.Y);
    }

    public IEnumerable<Entity> PickEntitiesAt(GamePosition position)
    {
        var searchSize = new Vector4(4, 4, 0, 0) * Engine.Configuration.TilePixelSize;
        var boundingBox = new BoundingBox(
            new Vector3(position.WorldSpace.X - searchSize.X / 2, position.WorldSpace.Y + searchSize.Y / 2, 0),
            new Vector3(position.WorldSpace.X + searchSize.X, position.WorldSpace.Y - searchSize.Y, 0));
        var entities = EntityLookup.Value.WorldTree.FindObjects(boundingBox).ToList();
        EntityManager.Value.LastSearchResults = entities;

        foreach (var entity in entities)
        {
            if (entity.Collision.SelectedBy(position))
                yield return entity;
        }
    }
}