﻿namespace AncientEmpires.Engine.Map.Picker;

public interface IGameSerializable
{
    Dictionary<string, object> Properties { get; }
    T GetProperty<T>(string name);
    void SetProperty<T>(string name, T value);
}