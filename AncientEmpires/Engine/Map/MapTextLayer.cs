using System.Numerics;
using AncientEmpires.Engine.Map.Rendering;
using AncientEmpires.Engine.Render;
using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Groups;
using AncientEmpires.Engine.Render.Quads;
using AncientEmpires.Engine.Render.Shaders;
using AncientEmpires.Engine.Render.Text;

namespace AncientEmpires.Engine.Map;

public class MapTextLayer : TextLayer
{
    protected ComponentLookup<GameMap> Map;

    public MapTextLayer()
    {
        BatchStrategy = new TiledMeshBatchStrategy(Engine);
    }

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();

        Shader = AddComponent(new TextShader(Font));
    }

    public override void OnInitialize()
    {
        base.OnInitialize();

        Map = new ComponentLookup<GameMap>();
        ZIndex = Engine.Configuration.GetRenderLayer(KnownRenderLayers.MapOverlay) - 0.2f;
        Font = Engine.GetComponent<FontManager>().GetFont(KnownFont.JetBrains);
        Textures = new[] { Font.Texture };
        TextContext.PositionStrategy = Engine.GetComponent<IsometricPositionStrategy>();
    }

    public override void OnDraw()
    {
        Shader.Blend = Shader.BlendStatePreMultiplied;

        base.OnDraw();
    }

    protected override TextQuad QuadFactory(Sprite sprite, Glyph glyph, Vector4 position, float scale)
    {
        return new(position, new Vector4(glyph.Size, 0, 0) * scale, sprite, Textures);
    }
}