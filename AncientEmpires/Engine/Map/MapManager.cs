using System.Numerics;
using AncientEmpires.Engine.Entities;
using AncientEmpires.Engine.Map.Picker;
using AncientEmpires.Engine.Render.Components;

namespace AncientEmpires.Engine.Map;

public class MapManager : EngineComponent
{
    public static GamePosition OutOfMap = GamePosition.FromMapSpace(new Vector4(-1, -1, -1, -1));
    public Vector4 TileSize => Map.TileSize;

    private readonly Dictionary<EntityType, SortedList<int, Entity>> _entitiesByType = new();
    public GameMap Map { get; private set; }

    public MapTile[,] Tiles => Map.Tiles;

    public EntitySpawner EntitySpawner { get; set; }


    public MapManager(GameEngine engine, EngineComponent parent, GameMap map)
    {
        Map = map;
    }

    public override void OnCreateComponents()
    {
        EntitySpawner = AddComponent<EntitySpawner>();
    }

    public void GetEntity(EntityType entityType, Guid id)
    {
        throw new NotImplementedException();
    }
    
    public MapTile GetTile(GamePosition coordinates) =>
        Map.IsInsideMap(coordinates, Vector4.One)
            ? Map.Tiles[(int)Math.Floor(coordinates.MapSpace.X), (int)Math.Floor(coordinates.MapSpace.Y)]
            : null;
    
    public void Refresh(GamePosition tile)
    {
        ConsoleWriter.WriteLine("Update to use CollisionManager MapManager.Refresh");
    }

    public void RefreshSurrounding(GamePosition position)
    {
        foreach (var surrounding in Neighbours(position))
            Refresh(surrounding);
    }
    
    public void AddToMap(Entity entity)
    {
        if (!_entitiesByType.ContainsKey(entity.Type))
            _entitiesByType.Add(entity.Type, new SortedList<int, Entity>());

        _entitiesByType[entity.Type].Add(entity.Id, entity);
    }

    public void RemoveFromMap(Entity entity)
    {
        _entitiesByType[entity.Type].Remove(entity.Id);
        Engine.GetComponent<EntityManager>().Remove(entity);
    }
    
    /*
    public static IEnumerable<Entity> GetEntities(MapTile[,] tiles)
    {
        for (var y = 0; y < tiles.GetLength(1); y++)
        for (var x = 0; x < tiles.GetLength(0); x++)
        {
            if (tiles[x, y] == null)
                continue;
            
            foreach (var entity in tiles[x, y].Entities.Values)
                yield return entity;
        }
    }        
    */

    private bool IsTileEmpty((float x, float y) position) => Tiles[(int)position.x, (int)position.y].Entities.Any();

    private GamePosition GetDirection(in GamePosition position, in Vector4 offset)
    {
        var coordinates = GamePosition.FromMapSpace(position.MapSpace + offset);
        return Map.IsInsideMap(coordinates, Vector4.One)
            ? coordinates
            : OutOfMap;
    }

    public GamePosition North(in GamePosition position) => GetDirection(in position, new Vector4(0, -1, 0, 0));
    public GamePosition NorthEast(in GamePosition position) => GetDirection(in position, new Vector4(1, -1, 0, 0));
    public GamePosition NorthWest(in GamePosition position) => GetDirection(in position, new Vector4(-1, -1, 0, 0));
    public GamePosition South(in GamePosition position) => GetDirection(in position, new Vector4(0, 1, 0, 0));
    public GamePosition SouthEast(in GamePosition position) => GetDirection(in position, new Vector4(1, 1, 0, 0));
    public GamePosition SouthWest(in GamePosition position) => GetDirection(in position, new Vector4(-1, 1, 0, 0));
    public GamePosition East(in GamePosition position) => GetDirection(in position, new Vector4(1, 0, 0, 0));
    public GamePosition West(in GamePosition position) => GetDirection(in position, new Vector4(-1, 0, 0, 0));


    public IEnumerable<GamePosition> Neighbours(GamePosition position, bool includeCurrent = false)
    {
        if (North(in position) != OutOfMap) yield return North(in position);
        if (South(in position) != OutOfMap) yield return South(in position);
        if (East(in position) != OutOfMap) yield return East(in position);
        if (West(in position) != OutOfMap) yield return West(in position);
        if (NorthEast(in position) != OutOfMap) yield return NorthEast(in position);
        if (NorthWest(in position) != OutOfMap) yield return NorthWest(in position);
        if (SouthEast(in position) != OutOfMap) yield return SouthEast(in position);
        if (SouthWest(in position) != OutOfMap) yield return SouthWest(in position);
        if (includeCurrent) yield return position;
    }

    public IEnumerable<GamePosition> CardinalSurrounding(GamePosition tile, bool includeCurrent = false)
    {
        if (North(tile) != OutOfMap) yield return North(tile);
        if (South(tile) != OutOfMap) yield return South(tile);
        if (East(tile) != OutOfMap) yield return East(tile);
        if (West(tile) != OutOfMap) yield return West(tile);
        if (includeCurrent) yield return tile;
    }

    /*
    public List<Entity> GetEntitiesAt(Vector2a cursorPosition)
    {
        var entityResult = Engine.GetComponent<EntityPicker>().Pick(cursorPosition);
        var mapResult = Engine.GetComponent<MapPicker>().Pick(cursorPosition);

        if (!mapResult.found)
            return new List<Entity>();

        return GetTile(mapResult.coordinates)?.Entities ?? new List<Entity>();
    }
    */
}