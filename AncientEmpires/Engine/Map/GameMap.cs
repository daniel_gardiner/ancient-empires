using System.Numerics;
using AncientEmpires.Engine.Data;
using AncientEmpires.Engine.Map.Picker;
using AncientEmpires.Engine.Map.Rendering;
using AncientEmpires.Engine.Map.Serialization;
using AncientEmpires.Engine.Metrics;
using AncientEmpires.Engine.Render;
using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Map;
using AncientEmpires.Engine.Serialization;
using Vortice.Mathematics;
using RectangleF = System.Drawing.RectangleF;
using Vector2 = System.Numerics.Vector2;
using Vector3 = System.Numerics.Vector3;

namespace AncientEmpires.Engine.Map;

public class GameMap : RenderableComponent, ISerializable<GameMap, DehydratedMap>
{
    public Vector4 Size;
    public Int4 SizeInt;
    public Vector4 Center;
    public Vector4 WorldCenter;
    public Int4 CenterInt;
    public Vector4 TileSize = new(128, 64, 0, 0);
    public Int4 TileSizeInt = new(128, 64, 0, 0);
    public Vector4 TilePadding;
    public IsometricPositionStrategy PositionStrategy;
    private GameMapGenerator Generator;
    public ISerializer<GameMap, DehydratedMap> Serializer { get; set; }
    public QuadTree<TileBoundingBox> MapTree;
    public MapTile[,] Tiles;
    public RectangleF MapWorldBounds;
    private bool IsMapGenerated;

    public GameMap()
    {
        Serializer = new MapSerializer();
    }

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();

        AddComponent(new MapManager(Engine, this, this));
        Generator = AddComponent<GameMapGenerator>();
    }

    public override void OnInitialize()
    {
        base.OnInitialize();
        
        ZIndex = Engine.Configuration.GetRenderLayer(KnownRenderLayers.Map);
        Size = new Vector4(
            Engine.Configuration.MapSize.Width,
            Engine.Configuration.MapSize.Height,
            0, 
            0);
        SizeInt = new Int4(
            (int)Math.Ceiling(Size.X),
            (int)Math.Ceiling(Size.Y),
            (int)Math.Ceiling(Size.Z),
            (int)Math.Ceiling(Size.W));
        TileSize = Engine.Configuration.TilePixelSize;
        TilePadding = new Vector4(0, 0, 0, 0);
        Center = Size / 2;
        CenterInt = new Int4(
            (int)Math.Ceiling(Center.X),
            (int)Math.Ceiling(Center.Y),
            (int)Math.Ceiling(Center.Z),
            (int)Math.Ceiling(Center.W));
        PositionStrategy = Engine.GetComponent<IsometricPositionStrategy>();
        WorldCenter = PositionStrategy.Project(Center);
    }

    public override void OnEngineReady()
    {
        base.OnEngineReady();
        
        if (!IsMapGenerated)
        {
            using var initializeTimer = Engine.MetricsRoot.Measure.Timer.Time(KnownMetrics.Timer, Metrics.CreateTags("InitializeMap"));
            Tiles = InitializeMap();
            ConsoleWriter.WriteLine($"InitializeMap finished | Duration: {initializeTimer.Elapsed.TotalMilliseconds:N1} ms");

            using var generateTimer = Engine.MetricsRoot.Measure.Timer.Time(KnownMetrics.Timer, Metrics.CreateTags("InitializeMap"));
            Tiles = Generator.GenerateMap(Tiles);
            MapWorldBounds = Generator.MapWorldBounds;
            ConsoleWriter.WriteLine($"GenerateMap finished | Duration: {generateTimer.Elapsed.TotalMilliseconds:N1} ms");

            using var generateQuadTree = Engine.MetricsRoot.Measure.Timer.Time(KnownMetrics.Timer, Metrics.CreateTags("QuadTree"));
            MapTree = GenerateMapQuadTree(Tiles);
            ConsoleWriter.WriteLine($"GenerateMapQuadTree finished | Duration: {generateQuadTree.Elapsed.TotalMilliseconds:N1} ms");
            IsMapGenerated = true;
        }
    }

    private MapTile[,] InitializeMap()
    {
        var mapSize = SizeInt;
        var tiles = new MapTile[mapSize.X, mapSize.Y];
        var tilePadding = TilePadding;

        for (var x = 0; x < mapSize.X; x++)
        {
            for (var y = 0; y < mapSize.Y; y++)
            {
                var coordinates = new Vector4(x, y, 0, 0);
                var mapTile = new MapTile(coordinates, Sprite.Empty, tilePadding);
                tiles[x, y] = mapTile;
            }
        }

        return tiles;
    }


    public QuadTree<TileBoundingBox> GenerateMapQuadTree(MapTile[,] tiles)
    {
        var mapTree = new QuadTree<TileBoundingBox>(
            new Vector2(MapWorldBounds.Left, MapWorldBounds.Top),
            new Vector2(MapWorldBounds.Width, MapWorldBounds.Height),
            new MapQuadBounds(),
            maxObjects: 12);

        var mapSize = SizeInt;

        for (var x = 0; x < mapSize.X; x++)
        {
            for (var y = 0; y < mapSize.Y; y++)
            {
                var mapTile = tiles[x, y];
                var minTileX = mapTile.Quad.Vertex1.Position.X;
                var maxTileX = mapTile.Quad.Vertex2.Position.X;
                var minTileY = mapTile.Quad.Vertex3.Position.Y;
                var maxTileY = mapTile.Quad.Vertex1.Position.Y;

                var boundingBox = new BoundingBox(new Vector3(minTileX, minTileY, 0), new Vector3(maxTileX, maxTileY, 0));
                mapTree.Insert(new TileBoundingBox(mapTile.Coordinates, boundingBox));
            }
        }

        return mapTree;
    }

    public bool IsInsideMap(GamePosition location, Vector4 size)
    {
        if (location.MapSpace.X < 0 || location.MapSpace.Y < 0)
            return false;

        if (location.MapSpace.X + size.X - 1 > Size.X - 1 || location.MapSpace.Y + size.Y - 1 > Size.Y - 1)
            return false;

        return true;
    }

    public Vector4 EnsureConstrained(Vector4 result)
    {
        if (result.X < 0 || result.X > Size.X)
            return ConstrainPosition(result);
        if (result.Y < 0 || result.Y > Size.Y)
            return ConstrainPosition(result);
        return result;
    }

    public Vector4 ConstrainPosition(Vector4 result) =>
        new(
            Math.Min(Size.X - 1, Math.Max(0, result.X)),
            Math.Min(Size.Y - 1, Math.Max(0, result.Y)),
            result.Z,
            result.W);

    public void Hydrate(GameMap hydrated, DehydratedMap dehydrated) => Serializer.Hydrate(hydrated, dehydrated);

    public DehydratedMap Dehydrate() => Serializer.Dehydrate(this);
}