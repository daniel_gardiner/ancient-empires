﻿using AncientEmpires.Engine.Entities;
using AncientEmpires.Engine.Render.Components;

namespace AncientEmpires.Engine.Data;

public class EntityLookup : EngineComponent
{
    public EntityQuadTree MapTree;
    public EntityQuadTree WorldTree;

    public override void OnEngineReady()
    {
        MapTree = RegisterComponent(new MapEntityQuadTree());
        WorldTree = RegisterComponent(new WorldEntityQuadTree());
        
        base.OnEngineReady();
    }

    public void Insert(Entity entity)
    {
        MapTree.Insert(entity);
        WorldTree.Insert(entity);
    }

    public void Remove(Entity entity)
    {
        MapTree.Remove(entity);
        WorldTree.Remove(entity);
    }
}