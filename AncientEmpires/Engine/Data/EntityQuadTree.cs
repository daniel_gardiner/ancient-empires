﻿using AncientEmpires.Engine.Entities;
using AncientEmpires.Engine.Map.Picker;
using AncientEmpires.Engine.Render.Components;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Data;

public abstract class EntityQuadTree : EngineComponent
{
    protected QuadTree<EntityBoundingBox> QuadTree;
    public int Count() => QuadTree.Count();
    public abstract void Insert(Entity entity);
    public abstract void Remove(Entity entity);

    public QuadTreeRect[] GetGrid()
    {
        return QuadTree.GetGrid();
    }

    public abstract IEnumerable<Entity> FindObjects(GamePosition position);

    public IEnumerable<Entity> FindObjects(BoundingBox boundingBox)
    {
        var box = new EntityBoundingBox(null, boundingBox);
        return QuadTree.FindObjects(box).Select(o => o.Entity);
    }
}