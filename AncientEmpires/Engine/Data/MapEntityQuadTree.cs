﻿using System.Numerics;
using AncientEmpires.Engine.Entities;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Map.Picker;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Data;

public class MapEntityQuadTree : EntityQuadTree
{
    public MapEntityQuadTree()
    {
        var mapSize = Engine.Configuration.MapSize;
        QuadTree = new QuadTree<EntityBoundingBox>(
            0,
            0,
            mapSize.Width - 1,
            mapSize.Height - 1,
            new EntityQuadBounds(),
            5,
            5);
    }

    public override IEnumerable<Entity> FindObjects(GamePosition position)
    {
        /*
        Debug.Assert(boundingBox.Minimum.X >= 0, "boundingBox.Minimum.X >= 0");
        Debug.Assert(boundingBox.Minimum.Y >= 0, "boundingBox.Minimum.Y >= 0");
        Debug.Assert(boundingBox.Maximum.X < Engine.Configuration.MapSize.Width, "boundingBox.Maximum.X < Engine.Configuration.MapSize.Width");
        Debug.Assert(boundingBox.Maximum.Y < Engine.Configuration.MapSize.Height, "boundingBox.Maximum.Y < Engine.Configuration.MapSize.Height");
        */

        return QuadTree.FindObjects(new QuadTreeRect(
                position.MapSpace.X - 1,
                position.MapSpace.Y - 1,
                2,
                2))
            .Select(o => o.Entity);
    }

    public override void Insert(Entity entity)
    {
        Remove(entity);
        QuadTree.Insert(GetMapSpaceBox(entity));
    }

    public override void Remove(Entity entity)
    {
        QuadTree.Remove(GetMapSpaceBox(entity));
    }

    private EntityBoundingBox GetMapSpaceBox(Entity entity)
    {
        var position = entity.Position.MapSpace.ToVector3();
        var size = Vector3.One / 2;
        var box = new BoundingBox(position - size, position + size);
        return new EntityBoundingBox(entity, box);
    }
}