﻿using System.Numerics;
using AncientEmpires.Engine.Entities;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Map.Picker;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Data;

public class WorldEntityQuadTree : EntityQuadTree
{
    public WorldEntityQuadTree()
    {
        var worldBounds = Engine.GetComponent<GameMap>().MapWorldBounds;
        QuadTree = new QuadTree<EntityBoundingBox>(
            worldBounds.Left,
            -worldBounds.Top,
            worldBounds.Width,
            worldBounds.Height,
            new EntityQuadBounds(),
            maxObjects: 5,
            maxLevel: 5);
    }

    public override IEnumerable<Entity> FindObjects(GamePosition position)
    {
        var tilePixelSize = Engine.Configuration.TilePixelSize;
        return QuadTree.FindObjects(new QuadTreeRect(
                position.WorldSpace.X - tilePixelSize.X,
                position.WorldSpace.Y + tilePixelSize.Y,
                position.WorldSpace.X + tilePixelSize.X * 2,
                position.WorldSpace.Y - -tilePixelSize.Y * 2))
            .Select(o => o.Entity);
    }

    public override void Insert(Entity entity)
    {
        Remove(entity);
        QuadTree.Insert(GetWorldSpaceBox(entity));
    }

    public override void Remove(Entity entity)
    {
        QuadTree.Remove(GetWorldSpaceBox(entity));
    }

    private EntityBoundingBox GetWorldSpaceBox(Entity entity)
    {
        var position = entity.Position.WorldSpace.ToVector3();
        var halfSize = Vector3.One / 2 * Engine.Configuration.TilePixelSize.ToVector3();
        var topLeft = position - halfSize;
        var bottomLeft = topLeft + halfSize;
        var box = new BoundingBox(topLeft, bottomLeft);
        return new EntityBoundingBox(entity, box);
    }
}