namespace AncientEmpires.Engine;

public enum KnownRenderLayers
{
    Map,
    MapOverlay,
    Entities,
    Interface,
}