using System.Numerics;
using AncientEmpires.Engine.Entities;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Interface.ProvincialBuild;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Render.Components;
using Panel = AncientEmpires.Engine.Interface.Panel;

namespace AncientEmpires.Engine;

public class GameBindings : EngineComponent
{
    private float rotation;

    public List<(Keys key, KeyModifier modifiers, Action<GameEngine, InputState> action)> KeyDownBindings { get; set; }
    public List<(Keys key, KeyModifier modifiers, Action<GameEngine, InputState> action)> KeyPressBindings { get; set; }

    public override void OnInitialize()
    {
        base.OnInitialize();

        KeyDownBindings = new List<(Keys key, KeyModifier modifiers, Action<GameEngine, InputState> action)>
        {
            (Keys.Back, KeyModifier.Control, (engine, state) => engine.Camera.ResetZoom()),
            (Keys.Left, KeyModifier.None, (engine, _) => engine.Camera.ScrollBy(-Engine.ScrollSpeed, 0)),
            (Keys.Right, KeyModifier.None, (engine, _) => engine.Camera.ScrollBy(Engine.ScrollSpeed, 0)),
            (Keys.Up, KeyModifier.None, (engine, _) => engine.Camera.ScrollBy(0, -Engine.ScrollSpeed)),
            (Keys.Down, KeyModifier.None, (engine, _) => engine.Camera.ScrollBy(0, Engine.ScrollSpeed)),
            (Keys.A, KeyModifier.None, (engine, _) => engine.Camera.ScrollBy(-Engine.ScrollSpeed, 0)),
            (Keys.D, KeyModifier.None, (engine, _) => engine.Camera.ScrollBy(Engine.ScrollSpeed, 0)),
            (Keys.W, KeyModifier.None, (engine, _) => engine.Camera.ScrollBy(0, -Engine.ScrollSpeed)),
            (Keys.S, KeyModifier.None, (engine, _) => engine.Camera.ScrollBy(0, Engine.ScrollSpeed)),
            (Keys.PageDown, KeyModifier.None, (engine, _) =>
            {
                //engine.Camera.ZoomBy(-1);
                rotation += Engine.Timing.SecondFraction * 0.1f;
                Engine.Camera.ViewMatrixTransition.Queue(Matrix4x4.CreateRotationY(rotation), 0);
            }),
            (Keys.PageUp, KeyModifier.None, (engine, _) => engine.Camera.ZoomBy(1)),
        };

        KeyPressBindings = new List<(Keys key, KeyModifier modifiers, Action<GameEngine, InputState> action)>
        {
            (Keys.L, KeyModifier.None, (engine, state) => engine.GetComponent<GameInterface>().ShowPanel(Panel.ProvincialBuild).ClickButton<BuildWallButton>(engine, state)),
            (Keys.R, KeyModifier.None, (engine, state) => engine.GetComponent<GameInterface>().ShowPanel(Panel.ProvincialBuild).ClickButton<BuildRoadButton>(engine, state)),
            (Keys.B, KeyModifier.None, (engine, state) => engine.GetComponent<GameInterface>().ShowPanel(Panel.ProvincialBuild).ClickButton<BulldozeButton>(engine, state)),
            (Keys.T, KeyModifier.None, (engine, state) => engine.GetComponent<GameInterface>().ShowPanel(Panel.ProvincialBuild).ClickButton<TestEntity1Button>(engine, state)),
            (Keys.Q, KeyModifier.None, (engine, state) => engine.GetComponent<MapManager>().EntitySpawner.Spawn(new Vector2(100, 100), Entity.Create(EntityType.RomanLegion))),
            (Keys.Back, KeyModifier.None, (engine, _) => engine.Actions.Clear()),
            (Keys.F3, KeyModifier.None, (engine, _) => engine.TogglePause()),
            (Keys.F4, KeyModifier.None, (engine, _) => engine.TestBinding()),
            (Keys.F5, KeyModifier.None, (engine, _) => engine.Refresh()),
            (Keys.F6, KeyModifier.None, (engine, _) =>
            {
                var mapManager = Engine.GetComponent<MapManager>();
                for (var j = 40; j >= 0; j--)
                for (var i = 40; i >= 0; i--)
                    mapManager.EntitySpawner.Spawn(new Vector2(80, 80) - new Vector2(-j, -i), Entity.Create(EntityType.Wall));
            }),
            (Keys.F8, KeyModifier.None, (engine, _) => GameManager.Instance.SaveGame()),
            (Keys.F9, KeyModifier.None, (engine, _) => GameManager.Instance.LoadGame()),
            (Keys.F10, KeyModifier.None, (engine, _) => engine.GetComponent<GameDebug>().EnableWireframe = !engine.GetComponent<GameDebug>().EnableWireframe),
            (Keys.F11, KeyModifier.None, (engine, _) =>  engine.GetComponent<GameDebug>().Toggle()),
            (Keys.F12, KeyModifier.None, (engine, _) => engine.GetComponent<GameDebug>().CycleMode()),
            (Keys.Pause, KeyModifier.None, (engine, _) => engine.TogglePause()),
            (Keys.Escape, KeyModifier.None, (engine, _) => GameManager.Instance.Exit()),
            (Keys.Oemplus, KeyModifier.None, (engine, _) => engine.Timing.IncreaseGameSpeed()),
            (Keys.OemMinus, KeyModifier.None, (engine, _) => engine.Timing.DecreaseGameSpeed()),
            (Keys.D0, KeyModifier.None, (engine, _) => engine.Timing.ResetGameSpeed()),
        };
    }

    public void ProcessInput(InputState state)
    {
        MouseZoom(state);

        var modifiers = KeyModifier.None;

        foreach (var key in state.KeysDown)
        {
            var matches = KeyDownBindings.Where(o => o.key == key && o.modifiers == modifiers).ToList();
            matches.ForEach(o => o.action(Engine, state));
        }

        foreach (var key in state.KeysReleased)
        {
            var matches = KeyPressBindings.Where(o => o.key == key && o.modifiers == modifiers).ToList();
            matches.ForEach(o => o.action(Engine, state));
        }
    }

    // TODO: Move replace with bindings
    private void MouseZoom(InputState state)
    {
        if (Engine.State == GameEngineState.Running)
        {
            if (state.CurrentZ != 0)
            {
                var zoom = new Vector2(state.CurrentZ, 0);
                zoom = Vector2.Normalize(zoom);
                Engine.Camera.ZoomBy((int)zoom.X);
            }

            if (state.MiddleClick)
            {
                Engine.Camera.ResetZoom();
            }
        }
    }
}