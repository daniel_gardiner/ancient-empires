using System.IO;
using App.Metrics;
using App.Metrics.Counter;
using App.Metrics.Filtering;
using App.Metrics.Filters;
using App.Metrics.Formatters;
using App.Metrics.Gauge;

namespace AncientEmpires.Engine.Metrics;

public class GameOutputFormatter : IMetricsOutputFormatter
{
    public MetricsMediaTypeValue MediaType => new("text", "vnd.custom.metrics", "v1", "plain");
    public MetricFields MetricFields { get; set; }

    public Task WriteAsync(Stream output,
        MetricsDataValueSource snapshot,
        CancellationToken cancellationToken = default)
    {
        ConsoleWriter.WriteLine($"");

        foreach (var snapshotContext in snapshot.Filter(new MetricsFilter().WhereContext("Application")).Contexts)
        {
            ReportTimers(snapshotContext);
        }

        return Task.CompletedTask;
    }

    private static void ReportTimers(MetricsContextValueSource snapshotContext)
    {
        var tickTime = FilterByTag(snapshotContext, "Type", "Tick").Timers.Sum(o => o.Value.Histogram.Mean);
        var componentTimers = snapshotContext.Filter(new MetricsFilter());
        var allTimers = componentTimers.Timers.Select(timerValueSource =>
            {
                var parts = timerValueSource.Tags.Values[2].Split(new[] { " -> " }, StringSplitOptions.RemoveEmptyEntries);
                parts = parts.Prepend(timerValueSource.Tags.Values[1]).ToArray();
                return new
                {
                    Parts = parts,
                    TimerValueSource = timerValueSource
                };
            })
            .OrderBy(o => o.TimerValueSource.Tags.Values[2])
            .ThenBy(o => o.TimerValueSource.Tags.Values[1]);

        ConsoleWriter.WriteLine($"{"Component",-50} {"Operation",-20} {"Mean",11} {"Percent",11}");
        ConsoleWriter.WriteLine($"{new string('=', 100)}");

        foreach (var timer in allTimers)
        {
            var totalMean = timer.TimerValueSource.Value.Histogram.Mean;
            var componentMean = timer.TimerValueSource.Value.Histogram.Mean;

            if (totalMean < 0.05)
                continue;

            var componentPercent = componentMean / tickTime;
            var parts = timer.Parts.Length;
            var padding = (parts - 2) * 5;
            var reversedParts = timer.Parts.Reverse().ToList();
            var name = $"{reversedParts.Skip(0).FirstOrDefault()}";
            var method = timer.TimerValueSource.Tags.Values[1];

            var itemPadding = padding > 0
                ? $"{new string('-', padding - 2)}> "
                : "";

            ConsoleWriter.WriteLine($"{itemPadding}{(name + "." + method).PadRight(70 - padding)} {componentMean,11:0.00;0.00;-} ms {componentPercent,11:0.00%;0.00%;-}");
        }

        WriteDivider('-');
        ConsoleWriter.WriteLine($"{"".PadRight(5)}{"TOTAL",-76} {tickTime,10:0.00;0.00;-} ms");
        WriteDivider('-');
        ReportCounters(componentTimers.Counters);
        ReportGauges(componentTimers.Gauges);
    }

    private static void ReportCounters(IEnumerable<CounterValueSource> counters)
    {
        var allGauge = counters.Select(timerValueSource =>
        {
            var label = timerValueSource.Tags.Values.Length > 0
                ? timerValueSource.Tags.Values[0]
                : timerValueSource.Name;
            return new
            {
                Name = label,
                TimerValueSource = timerValueSource
            };
        });
        
        foreach (var gauge in allGauge)
        {
            var value = gauge.TimerValueSource.Value;

            if (value.Count < 1)
                continue;
            
            var name = gauge.TimerValueSource.Tags.Values.Length > 1
                ? gauge.TimerValueSource.Tags.Values[1]
                : gauge.TimerValueSource.Tags.Values[0];
            
            ConsoleWriter.WriteLine($"{"".PadRight(5)}{name.PadRight(30)} {value.Count,11:0.00;0.00;-}");
        }

        WriteDivider('-');

    }

    private static void ReportGauges(IEnumerable<GaugeValueSource> gauges)
    {
        var allGauge = gauges.Select(timerValueSource =>
        {
            var label = timerValueSource.Tags.Values.Length > 0
                ? timerValueSource.Tags.Values[0]
                : timerValueSource.Name;
            return new
            {
                Name = label,
                TimerValueSource = timerValueSource
            };
        });
        
        foreach (var gauge in allGauge)
        {
            var value = gauge.TimerValueSource.Value;

            if (value < 0.05)
                continue;
            
            var name = gauge.TimerValueSource.Tags.Values.Length > 1
                ? gauge.TimerValueSource.Tags.Values[1]
                : gauge.TimerValueSource.Tags.Values[0];
            
            ConsoleWriter.WriteLine($"{"".PadRight(5)}{name.PadRight(30)} {value,11:0.00;0.00;-}");
        }

        WriteDivider('-');
    }

    private static void ReportMeters(MetricsContextValueSource snapshotContext)
    {
        ConsoleWriter.WriteLine($"");
        ConsoleWriter.WriteLine($"{"Meter",-70} {"Rate",-20} {"Percent",11}");
        WriteDivider('=');

        foreach (var counter in snapshotContext.Meters)
        {
            foreach (var counterItem in counter.Value.Items)
            {
                var tags = counterItem.Tags.Values[0].Split('|').Select(o => o.Split(':').ToArray()).ToArray();
                ConsoleWriter.WriteLine($"{$"{tags[1][1]} -> {tags[0][1]}",-70:0.00;0.00;-} {counterItem.Value.MeanRate,11:N1}/s {counterItem.Percent,11:N0}");
            }

            ConsoleWriter.WriteLine($"{" ",5}{"TOTAL",-65} {counter.Value.Count,11:0.00;0.00;-}");
        }
    }

    private static void WriteDivider(char c)
    {
        ConsoleWriter.WriteLine($"{"".PadRight(0)}{new string(c, 100)}");
    }

    private static MetricsContextValueSource FilterByTag(MetricsContextValueSource snapshotContext, string key, string value)
    {
        var filterMetrics = new MetricsFilter().WhereTaggedWithKeyValue(new TagKeyValueFilter { { key, value } });
        return snapshotContext.Filter(filterMetrics);
    }
}