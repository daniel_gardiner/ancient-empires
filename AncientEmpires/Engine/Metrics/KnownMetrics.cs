using App.Metrics;
using App.Metrics.Apdex;
using App.Metrics.Counter;
using App.Metrics.Filtering;
using App.Metrics.Filters;
using App.Metrics.Formatters;
using App.Metrics.Gauge;
using App.Metrics.Infrastructure;
using App.Metrics.Meter;
using App.Metrics.Reporting;
using App.Metrics.ReservoirSampling.SlidingWindow;
using App.Metrics.Timer;

namespace AncientEmpires.Engine.Metrics;

public class KnownMetrics : IMetricsRoot
{
    private readonly GameEngine _engine;

    public static readonly MeterOptions CollisionChecks = new()
    {
        Name = "EntityCollisionChecks",
        MeasurementUnit = Unit.Events,
        RateUnit = TimeUnit.Seconds,
        ResetOnReporting = true,
    };
    public static readonly GaugeOptions TimingMetrics = new()
    {
        Name = "TimingMetrics",
        MeasurementUnit = Unit.Results,
        ResetOnReporting = true
    };
    
    public static readonly GaugeOptions Fps = new()
    {
        Name = "FPS",
        MeasurementUnit = Unit.Results,
        ResetOnReporting = true
    };
    
    public static readonly GaugeOptions Gauge = new()
    {
        Name = "Gauge",
        MeasurementUnit = Unit.Results,
        ResetOnReporting = true
    };

    public static readonly CounterOptions PerTickCounter = new()
    {
        Name = "PerTickCounter",
        MeasurementUnit = Unit.Calls,
        ResetOnReporting = true
    };
    
    public static readonly CounterOptions CacheCounter = new()
    {
        Name = "CacheCounter",
        MeasurementUnit = Unit.Calls,
        ResetOnReporting = true
    };

    public static readonly MeterOptions PerTickMeter = new()
    {
        Name = "Meter",
        MeasurementUnit = Unit.Calls,
        RateUnit = TimeUnit.Milliseconds,
        ResetOnReporting = true
    };

    public static readonly TimerOptions Timer = new()
    {
        Name = "Timer",
        MeasurementUnit = Unit.Items,
        DurationUnit = TimeUnit.Milliseconds,
        RateUnit = TimeUnit.Seconds,
        ResetOnReporting = true
    };

    public static readonly ApdexOptions SampleApdex = new()
    {
        Name = "Apdex",
        ResetOnReporting = true
    };

    private readonly IMetricsRoot _metrics;

    public KnownMetrics(GameEngine engine)
    {
        var filter = new MetricsFilter();
        _engine = engine;
        _metrics = new MetricsBuilder()
            .OutputMetrics.AsPlainText()
            .SampleWith.Reservoir(() => new DefaultSlidingWindowReservoir(1))
            .Report.ToConsole(
                options =>
                {
                    options.Filter = filter;
                    options.MetricsOutputFormatter = new GameOutputFormatter();
                })
            .Build();
    }


    public IBuildMetrics Build => _metrics.Build;

    public IClock Clock => _metrics.Clock;

    public IFilterMetrics Filter => _metrics.Filter;

    public IManageMetrics Manage => _metrics.Manage;

    public IMeasureMetrics Measure => _metrics.Measure;

    public IProvideMetrics Provider => _metrics.Provider;

    public IProvideMetricValues Snapshot => _metrics.Snapshot;

    public IReadOnlyCollection<IMetricsOutputFormatter> OutputMetricsFormatters => _metrics.OutputMetricsFormatters;

    public IMetricsOutputFormatter DefaultOutputMetricsFormatter => _metrics.DefaultOutputMetricsFormatter;

    public IEnvOutputFormatter DefaultOutputEnvFormatter => _metrics.DefaultOutputEnvFormatter;

    public IReadOnlyCollection<IEnvOutputFormatter> OutputEnvFormatters => _metrics.OutputEnvFormatters;

    public IReadOnlyCollection<IReportMetrics> Reporters => _metrics.Reporters;

    public IRunMetricsReports ReportRunner => _metrics.ReportRunner;

    public MetricsOptions Options => _metrics.Options;
    
    public EnvironmentInfo EnvironmentInfo => _metrics.EnvironmentInfo;
}