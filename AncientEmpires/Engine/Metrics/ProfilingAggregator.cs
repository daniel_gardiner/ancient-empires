using App.Metrics;
using Humanizer;

namespace AncientEmpires.Engine.Metrics;

public class ProfilingAggregator : IDisposable
{
    private readonly TimeSpan _interval = 10.Seconds();
    private GameTimer _timer;
    private readonly Timing _timing;
    private readonly GameEngine _engine;
    private Thread _thread;
    private CancellationTokenSource _cancellationTokenSource;

    public bool IsRunning { get; set; } = true;

    public ProfilingAggregator(GameEngine engine, Timing timing)
    {
        _engine = engine;
        _timing = timing;
    }

    public void Start()
    {
        if (GameManager.Instance.IsTestMode)
            return;

        IsRunning = true;
        _cancellationTokenSource = new CancellationTokenSource();
        ThreadPool.QueueUserWorkItem(CollectTiming, _cancellationTokenSource.Token);
        _thread = new Thread(CollectTiming);
        _thread.Start();
    }

    private void CollectTiming(object obj)
    {
        _timer = _timing.GetTimer(nameof(ReportProfiling));

        while (IsRunning)
        {
            try
            {
                if (!_timing.HasBeen(_timer, _interval))
                {
                    Thread.Yield();
                    continue;
                }

                ReportProfiling();
            }
            catch (Exception ex)
            {
                ConsoleWriter.WriteLine($"Timing exception: {ex.Message}");
            }
        }
    }

    private void ReportProfiling()
    {
        double elapsedTicks = _engine.Timing.Tick - _engine.Timing.GameMetrics.LastTick;

        _engine.MetricsRoot.Measure.Gauge.SetValue(KnownMetrics.TimingMetrics, new MetricTags("Metric", "DrawCalls"), _engine.Timing.GameMetrics.MetricDrawCalls / elapsedTicks);
        _engine.MetricsRoot.Measure.Gauge.SetValue(KnownMetrics.TimingMetrics, new MetricTags("Metric", "VerticesDrawn"), _engine.Timing.GameMetrics.MetricVerticesDrawn / elapsedTicks);
        _engine.MetricsRoot.Measure.Gauge.SetValue(KnownMetrics.TimingMetrics, new MetricTags("Metric", "VerticesWritten"), _engine.Timing.GameMetrics.MetricVerticesWritten / elapsedTicks);
        _engine.MetricsRoot.Measure.Gauge.SetValue(KnownMetrics.TimingMetrics, new MetricTags("Metric", "MetricQuadsCreated"), _engine.Timing.GameMetrics.MetricQuadsCreated / elapsedTicks);
        _engine.MetricsRoot.Measure.Gauge.SetValue(KnownMetrics.TimingMetrics, new MetricTags("Metric", "FragmentsCreated"), _engine.Timing.GameMetrics.MetricFragmentsCreated / elapsedTicks);
        _engine.MetricsRoot.Measure.Gauge.SetValue(KnownMetrics.TimingMetrics, new MetricTags("Metric", "FragmentsUpdated"), _engine.Timing.GameMetrics.MetricFragmentsUpdated / elapsedTicks);
        _engine.MetricsRoot.Measure.Gauge.SetValue(KnownMetrics.TimingMetrics, new MetricTags("Metric", "Fragments"), _engine.Timing.GameMetrics.Fragments);

        _engine.Timing.GameMetrics.LastTick = _engine.Timing.Tick;
        _engine.Timing.GameMetrics.MetricDrawCalls = 0;
        _engine.Timing.GameMetrics.MetricVerticesDrawn = 0;
        _engine.Timing.GameMetrics.MetricVerticesWritten = 0;
        _engine.Timing.GameMetrics.MetricQuadsCreated = 0;
        _engine.Timing.GameMetrics.MetricFragmentsCreated = 0;
        _engine.Timing.GameMetrics.MetricFragmentsUpdated = 0;

        var snapshot = _engine.MetricsRoot.Snapshot.Get();
        var allAsync = _engine.MetricsRoot.ReportRunner.RunAllAsync();
        Task.WhenAll(allAsync).Wait();
        _engine.MetricsRoot.Manage.Reset();
    }

    public void Dispose()
    {
        IsRunning = false;
        _cancellationTokenSource?.Cancel();
        _cancellationTokenSource = null;
        
        Thread.Sleep(500);
    }
}