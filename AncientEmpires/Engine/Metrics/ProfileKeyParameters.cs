using AncientEmpires.Engine.Render.Components;

namespace AncientEmpires.Engine.Metrics;

public struct ProfileKeyParameters
{
    public ProfileKeyParameters(EngineComponent context, string key)
    {
        Context = context;
        Key = key;
    }

    public string Key;
    public EngineComponent Context;
}