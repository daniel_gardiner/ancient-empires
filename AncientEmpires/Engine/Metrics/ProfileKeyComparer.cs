namespace AncientEmpires.Engine.Metrics;

internal class ProfileKeyComparer : IEqualityComparer<ProfileKeyParameters>
{
    public bool Equals(ProfileKeyParameters x, ProfileKeyParameters y)
    {
        return x.Key == y.Key && Equals(x.Context, y.Context);
    }

    public int GetHashCode(ProfileKeyParameters obj)
    {
        unchecked
        {
            return ((obj.Key != null
                ? obj.Key.GetHashCode()
                : 0) * 397) ^ (obj.Context != null
                ? obj.Context.GetHashCode()
                : 0);
        }
    }
}