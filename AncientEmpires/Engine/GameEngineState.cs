namespace AncientEmpires.Engine;

public enum GameEngineState
{
    New,
    Initializing,
    Running,
    Quiting,
    Paused,
    Finished
}