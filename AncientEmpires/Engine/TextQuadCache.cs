﻿using AncientEmpires.Engine.Metrics;
using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Quads;

namespace AncientEmpires.Engine;

public class TextQuadCache : EngineComponent
{
    private readonly Dictionary<string, List<Quad>> _textQuadCache = new();

    public List<Quad> GetQuads(string text, Func<string, List<Quad>> quadFactory)
    {
        return _textQuadCache.TryGetValue(text, out var quads)
            ? CacheHit(quads)
            : CacheMiss(text, quadFactory);
    }

    private List<Quad> CacheHit(List<Quad> quads)
    {
        Metrics.IncrementCounter(KnownMetrics.CacheCounter, "CacheHit");
        return quads;
    }

    private List<Quad> CacheMiss(string text, Func<string, List<Quad>> quadFactory)
    {
        Metrics.IncrementCounter(KnownMetrics.CacheCounter, "CacheMiss");
        var quads = quadFactory(text);
        _textQuadCache.Add(text, quads);
        return quads;
    }
}