using System.Numerics;

namespace AncientEmpires.Engine;

public static class DistanceMethods
{
    public static int ManhattanDistance(int x1, int x2, int y1, int y2)
    {
        return Math.Abs(x1 - x2) + Math.Abs(y1 - y2);
    }

    public static float ManhattanDistance(float x1, float x2, float y1, float y2)
    {
        return Math.Abs(x1 - x2) + Math.Abs(y1 - y2);
    }

    public static float EuclideanDistance(GamePosition source, GamePosition dest)
    {
        return (float)Math.Sqrt(
            (source.MapSpace.X - dest.MapSpace.X) *
            (source.MapSpace.X - dest.MapSpace.X) +
            (source.MapSpace.Y - dest.MapSpace.Y) *
            (source.MapSpace.Y - dest.MapSpace.Y));
    }

    public static float EuclideanDistanceFast(Vector4 x, Vector4 y)
    {
        return EuclideanDistanceFast((x.X, x.Y), (y.X, y.Y));
    }

    public static float EuclideanDistanceFast((float x, float y) source, (float x, float y) dest)
    {
        return (source.x - dest.x) * (source.x - dest.x) + (source.y - dest.y) * (source.y - dest.y);
    }

    // implementation for floating-point EuclideanDistance

    public static float EuclideanDistance(float x1, float x2, float y1, float y2)
    {
        float square = (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
        return square;
    }

    // implementation for integer based Chebyshev Distance

    public static int ChebyshevDistance(int dx, int dy)
    {
        // not quite sure if the math is correct here
        return 1 * (dx + dy) + (1 - 2 * 1) * (dx - dy);
    }

    // implementation for floating-point Chebyshev Distance

    public static float ChebyshevDistance(float dx, float dy)
    {
        // not quite sure if the math is correct here
        return 1 * (dx + dy) + (1 - 2 * 1) * (dx - dy);
    }
}