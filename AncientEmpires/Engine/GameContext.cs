﻿using AncientEmpires.Engine;

public class GameContext
{
    public static GameContext Instance;

    public GameEngine Engine;
    public GameManager Manager;

    public static void Set(GameEngine currentGame, GameManager gameManager)
    {
        Instance = new GameContext
        {
            Engine = currentGame,
            Manager = gameManager
        };
    }
}