using AncientEmpires.Engine.Map.Serialization;
using AncientEmpires.Engine.Metrics;
using AncientEmpires.Engine.Render;
using AncientEmpires.Engine.Render.Components;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using File = System.IO.File;

namespace AncientEmpires.Engine;

public class GameManager : Component
{
    public static DisposeCollector DisposeCollector = new();
    public static GameEngine CurrentGame;
    public static GameManager Instance;
    public readonly IGameWindow Window;
    private readonly GameFileSystem FileSystem;
    private readonly GraphicsDevice GraphicsDevice;
    public bool IsTestMode;
    public bool IsRunning = true;
    public Action WhenGameEnds;
    private bool IsResizeRequested;

    public override string Name => Application.ProductName;

    private bool HasFocus => Window.HasFocus;

    public GameManager(IGameWindow window,
        GameFileSystem fileSystem,
        Func<IGameWindow, GraphicsDevice> graphicsDeviceFactory)
    {
        FileSystem = fileSystem;
        GraphicsDevice = graphicsDeviceFactory(window);
        Window = window;
    }

    private void OnResize()
    {
        if (Window.GetSurfaceSize() == CurrentGame.ScreenSize)
            return;

        CurrentGame.ScreenSize = Window.GetSurfaceSize();
        CurrentGame.TriggerReleaseResources();
        DisposeCollector.DisposeAndClear();
        
        GraphicsDevice.CreateD3D11();
        GraphicsDevice.Presentation.OnInitialize();
        
        CurrentGame.TriggerEngineReady();
        CurrentGame.TriggerResize();
        CurrentGame.Refresh();
        CurrentGame.ClearLookups();
        IsResizeRequested = false;
    }

    public void OnGameRegister(GameEngine engine)
    {
        FileSystem.Engine = engine;
        engine.AddComponent(GraphicsDevice);
        engine.AddComponent(FileSystem);
    }

    public void OnQuit()
    {
        GameContext.Instance.Engine.Timing.Aggregator.IsRunning = false;
        CurrentGame.Dispose();
        IsRunning = false;
        Application.Exit();
    }

    private void OnGameOver()
    {
        if (WhenGameEnds == null)
        {
            Exit();
            return;
        }

        WhenGameEnds();
        WhenGameEnds = null;
    }

    public virtual void StartGame()
    {
        CurrentGame = GameEngine.Create(this);
        CurrentGame.StartNew();
        CurrentGame.State = GameEngineState.Running;
        Run();
    }

    public virtual void NewGame()
    {
        CurrentGame.Pause();
        CurrentGame.State = GameEngineState.Quiting;
        CurrentGame.Dispose();

        CurrentGame = GameEngine.Create(this);
    }

    public void Resize()
    {
        IsResizeRequested = true;
    }

    public virtual void LoadGame(GameEngine engine = null)
    {
        CurrentGame.State = GameEngineState.Quiting;
        CurrentGame.Dispose();

        CurrentGame = GameEngine.Create(this);
        CurrentGame.StartNew();
        
        var dehydratedGame = ReadGameFromFile("AutoSave001.json");
        using var timerContext = CurrentGame.MetricsRoot.Measure.Timer.Time(KnownMetrics.Timer, CurrentGame.Metrics.CreateTags("LoadGame"));

        CurrentGame = new GameEngineSerializer().Hydrate(CurrentGame, dehydratedGame);
        CurrentGame.State = GameEngineState.Running;
        CurrentGame.TriggerResize();

        ConsoleWriter.WriteLine($"LoadGame finished | Duration: {timerContext.Elapsed.TotalMilliseconds:N1} ms");

        Run();
    }

    public virtual void SaveGame()
    {
        CurrentGame.Pause();

        using var timerContext = CurrentGame.MetricsRoot.Measure.Timer.Time(KnownMetrics.Timer, CurrentGame.Metrics.CreateTags("SaveGame"));
        WriteGameToFile($"{DateTime.Now:yyyyMMdd_HHmmss}.save", CurrentGame.Dehydrate());
        ConsoleWriter.WriteLine($"SaveGame finished | Duration: {timerContext.Elapsed.TotalMilliseconds:N1} ms");

        CurrentGame.Resume();
    }

    private void WriteGameToFile(string fileName, DehydratedGame dehydratedGame)
    {
        var saveGame = JsonConvert.SerializeObject(dehydratedGame, new JsonSerializerSettings
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
            Formatting = Formatting.Indented,
            DefaultValueHandling = DefaultValueHandling.Ignore,
            Converters = new List<JsonConverter>
            {
                new StringEnumConverter()
            }
        });

        CurrentGame.GetComponent<GameFileSystem>().WriteSavedGame(fileName, saveGame);
    }

    public DehydratedGame ReadGameFromFile(string fileName)
    {
        var text = File.ReadAllText($"{Application.StartupPath}\\SaveGame\\{fileName}");
        return JsonConvert.DeserializeObject<DehydratedGame>(text);
    }

    public void Run()
    {
        if (!IsRunning || IsTestMode)
            return;

        Task.Run(() =>
        {
            IsRunning = true;
            while (IsRunning)
            {
                if (IsResizeRequested)
                    OnResize();

                switch (CurrentGame.State)
                {
                    case GameEngineState.Paused:
                        CurrentGame.ProcessInput();
                        Thread.Sleep(10);
                        continue;
                    case GameEngineState.New:
                        continue;
                    case GameEngineState.Initializing:
                        CurrentGame.State = GameEngineState.Running;
                        break;
                    case GameEngineState.Quiting:
                        ConsoleWriter.WriteLine("Gaming quitting");
                        continue;
                }

                CurrentGame.StartTick();
                CurrentGame.Update(HasFocus);

                if (CurrentGame.State != GameEngineState.Running)
                    continue;

                CurrentGame.Draw();
                GraphicsDevice.Present();
            }

            CurrentGame.State = GameEngineState.Finished;
        });
    }

    public void Exit()
    {
        CurrentGame.Quit();
    }
}