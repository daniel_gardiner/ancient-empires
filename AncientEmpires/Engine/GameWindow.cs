using System.Numerics;

namespace AncientEmpires.Engine;

public interface IGameWindow
{
    bool HasFocus { get; }
    IntPtr Handle { get; }
    Control Control { get; }
    void SetText(string text);
    Vector2 GetSurfaceSize();
    Rectangle GetBounds();
    Vector4 MakeCursorRelative(int x, int y);
}

public class WinFormsGameWindow : IGameWindow
{
    public Form GameForm { get; }

    public WinFormsGameWindow(Form gameForm)
    {
        GameForm = gameForm;
        Handle = GameForm.Handle;
    }

    public bool HasFocus => System.Windows.Forms.Form.ActiveForm == GameForm;
    public IntPtr Handle { get; }
    public Control Control => GameForm;

    public Vector2 GetSurfaceSize()
    {
        return new(GameForm.ClientSize.Width, GameForm.ClientSize.Height);
    }

    public Vector4 MakeCursorRelative(int x, int y)
    {
        var position = new Point(x, y);
        var client = GameForm.PointToClient(position);
        return new Vector4(client.X, client.Y, 0, 0);
    }

    public Rectangle GetBounds() =>
        GameForm.IsDisposed
            ? new Rectangle(0, 0, 0, 0)
            : GameForm.Bounds;

    public void SetText(string text)
    {
        if (GameForm.IsDisposed)
            return;
        GameForm.Invoke(() => GameForm.Text = text);
    }
}