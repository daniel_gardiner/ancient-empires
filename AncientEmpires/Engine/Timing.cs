using System.Diagnostics;
using AncientEmpires.Engine.Metrics;
using AncientEmpires.Engine.Render.Components;
using App.Metrics;

namespace AncientEmpires.Engine;

public class Timing : EngineComponent, IDisposable
{
    public ProfilingAggregator Aggregator { get; set; }
    private static readonly Stopwatch Timer = Stopwatch.StartNew();

    private readonly Dictionary<EngineComponent, (int frame, float elapsedMs)> _frameCounts = new();
    private readonly Dictionary<EngineComponent, (int slice, float elapsedMs)> _sliceCounts = new();
    private readonly Dictionary<EngineComponent, (int tick, float elapsedMs)> _tickCounts = new();
    private readonly Dictionary<string, GameTimer> _timers = new();
    private readonly Dictionary<string, int> _sliceTimers = new();
    private readonly Dictionary<string, int> _tickTimers = new();
    public double Fps;
    public int Slice;
    public int Frame;
    public int Tick;
    public double LastSliceMs;
    public double LastFrameMs;
    public float SliceFraction; 
    public float SecondFraction;
    public int LastTick;
    public float TotalElapsedMs;
    public float LastTotalElapsedMs;
    private double _lastFpsElapsed;
    public float Multiplier;
    public readonly GameMetrics GameMetrics = new GameMetrics();
    public bool IsNewFrame;
    public bool IsNewSlice;
    public float FrameDurationMs => 1000f / 120f;
    public float SliceDurationMs => 100f / Multiplier;
    public float SliceDurationS => SliceDurationMs / 1000f;
    public float FrameDurationS => FrameDurationMs / 1000f;

    public override void OnInitialize()
    {
        Aggregator = new ProfilingAggregator(Engine, this);
        Aggregator.Start();
        ChangeMultiplier(1);
        base.OnInitialize();
    }
    
    public virtual double PeekTick()
    {
        var currentMs = GetRealtimeElapsed();
        var tickSize = (currentMs - LastTotalElapsedMs) / 1000f;

        return tickSize * Multiplier;
    }

    public virtual void Update()
    {
        Tick++;
        TotalElapsedMs = WaitForNewFrame();
        SecondFraction = (TotalElapsedMs - LastTotalElapsedMs) / 1000f;

        if (Tick % 100 == 0)
        {
            //ConsoleWriter.WriteLine($"Frametime: {(TotalElapsedMs - LastTotalElapsedMs):N2}ms | Second fraction: {SecondFraction}");
        }

        if (SecondFraction > SliceDurationS)
        {
            SecondFraction = SliceDurationS;
            ConsoleWriter.WriteLine("=============");
            ConsoleWriter.WriteLine("SLICE DROPPED TODO: HANDLE THIS");
            ConsoleWriter.WriteLine($"Frametime: {(TotalElapsedMs - LastTotalElapsedMs):N2}ms | Second fraction: {SecondFraction}");
            ConsoleWriter.WriteLine("=============");
        }

        IsNewFrame = TotalElapsedMs - LastFrameMs >= FrameDurationMs;
        IsNewSlice = TotalElapsedMs - LastSliceMs >= SliceDurationMs;

        if (IsNewFrame)
        {
            Frame++;
            LastFrameMs = TotalElapsedMs;
        }

        if (IsNewSlice)
        {
            Slice++;
            LastSliceMs = TotalElapsedMs;
        }

        LastTotalElapsedMs = TotalElapsedMs;

        if (HasBeen("fps", TimeSpan.FromMilliseconds(500)))
        {
            CalculateFps();
        }
    }

    private float WaitForNewFrame()
    {
        var elapsedMs = Timer.ElapsedTicks / 10000f;

        /*if (elapsedMs - LastFrameMs < FrameDurationMs)
        {
            while (elapsedMs - LastTotalElapsedMs < FrameDurationMs)
            {
                elapsedMs = Timer.ElapsedTicks / 10000f;
                Thread.Yield();
            }
        }*/
        return elapsedMs;
    }

    public virtual void ChangeMultiplier(float multiple)
    {
        Multiplier = multiple;
    }

    public virtual void CalculateFps()
    {
        Fps = (Tick - LastTick) / (GetRealtimeElapsed() - _lastFpsElapsed) * 1000f;
        GameContext.Instance.Engine.MetricsRoot.Measure.Gauge.SetValue(KnownMetrics.Fps, new MetricTags("Type", "Fps"), Fps);

        LastTick = Tick;
        _lastFpsElapsed = GetRealtimeElapsed();
    }

    //public virtual bool IsNewFrame(EngineComponent component) => IsNewFrame(component, out _);

    /*
    public virtual bool IsNewFrame(EngineComponent component, out float timeDelta)
    {
        if (!_frameCounts.TryGetValue(component, out var lastFrame))
        {
            _frameCounts[component] = (Frame, TotalElapsedMs);
            timeDelta = 0;
            return false;
        }

        if (lastFrame.frame >= Frame)
        {
            timeDelta = 0;
            return false;
        }

        _frameCounts[component] = (Frame, TotalElapsedMs);
        var elapsed = TotalElapsedMs - lastFrame.elapsedMs;
        elapsed = Math.Min(elapsed, FrameDurationMs / 2);
        timeDelta = elapsed / 1000 * Multiplier;
        return true;
    }
    */

    /*
    public virtual bool IsNewSlice(EngineComponent component)
    {
        if (!_sliceCounts.TryGetValue(component, out var lastSlice))
        {
            _sliceCounts[component] = (Slice, TotalElapsedMs);
            return false;
        }

        if (lastSlice.slice >= Slice)
            return false;

        _sliceCounts[component] = (Slice, TotalElapsedMs);
        return true;
    }
    */

    public virtual bool IsNewTick(EngineComponent component)
    {
        if (!_tickCounts.TryGetValue(component, out var lastTick))
        {
            _tickCounts[component] = (Tick, TotalElapsedMs);
            return false;
        }

        if (lastTick.tick >= Tick)
            return false;

        _tickCounts[component] = (Tick, TotalElapsedMs);
        return true;
    }

    public virtual bool HasBeen(string timerName, TimeSpan duration)
    {
        var timer = GetTimer(timerName);
        return HasBeen(timer, duration);
    }

    public virtual bool HasBeen(GameTimer timer, TimeSpan duration)
    {
        if (!timer.IsComplete(TotalElapsedMs, duration.TotalMilliseconds))
            return false;

        timer.Update(TotalElapsedMs, duration.TotalMilliseconds);

        return true;
    }

    public GameTimer GetTimer(string timerName)
    {
        return !_timers.TryGetValue(timerName, out var timer)
            ? _timers[timerName] = new GameTimer(timerName, TotalElapsedMs)
            : timer;
    }

    public virtual bool HasBeen(string timerName, int sliceDuration)
    {
        return HasBeen(timerName, TimeSpan.FromMilliseconds(sliceDuration));
    }

    public override void Dispose()
    {
        Aggregator.Dispose();
    }

    public double GetRealtimeElapsed() => Timer.Elapsed.TotalMilliseconds;

    public void DecreaseGameSpeed()
    {
        ChangeMultiplier(Multiplier * 0.5f);
    }

    public void IncreaseGameSpeed()
    {
        ChangeMultiplier(Multiplier * 1.5f);
    }

    public void ResetGameSpeed()
    {
        ChangeMultiplier(1f);
    }
}