using AncientEmpires.Engine.Actions;
using AncientEmpires.Engine.Interface;

namespace AncientEmpires.Engine;

public static class Condition
{
    public static readonly Func<GameEngine, InputState, bool> Always = (_, _) => true;
    public static readonly Func<GameEngine, InputState, bool> Spawning = (engine, _) => engine.CurrentActions.OfType<EntitySpawn>().Any();
    public static readonly Func<GameEngine, InputState, bool> SpawningDraggable = (engine, _) => engine.Actions.Actions.OfType<EntitySpawn>().Any(o => o.Draggable);
}