using System.Numerics;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Render.Components;
using Gma.System.MouseKeyHook;
using WinKeys = System.Windows.Forms.Keys;

namespace AncientEmpires.Engine.Input;

public class GameInput : EngineComponent
{
    public InputState Current = new();
    public InputState NextState = new();
    private static IKeyboardMouseEvents _events;

    public override void OnInitialize()
    {
        base.OnInitialize();

        _events = Hook.AppEvents();
        _events.KeyDown += OnKeyDown;
        _events.KeyUp += OnKeyUp;
        _events.MouseWheel += OnMouseWheel;
        _events.MouseDown += OnMouseDown;
        _events.MouseUp += OnMouseUp;
        _events.MouseMove += OnMouseMove;
    }

    private void OnMouseMove(object sender, MouseEventArgs e)
    {
        NextState.CursorPosition = GameManager.Instance.Window.MakeCursorRelative(e.X, e.Y);
    }

    private void OnKeyDown(object sender, KeyEventArgs e)
    {
        lock (typeof(GameInput))
        {
            if (!NextState.KeysDown.Contains(e.KeyCode))
            {
                NextState.KeysDown.Add(e.KeyCode);
            }
            else
            {
                NextState.KeysRepeated[e.KeyCode] = NextState.KeysRepeated.ContainsKey(e.KeyCode)
                    ? NextState.KeysRepeated[e.KeyCode]++
                    : NextState.KeysRepeated[e.KeyCode] = 1;
            }
        }
    }

    private void OnKeyUp(object sender, KeyEventArgs e)
    {
        lock (typeof(GameInput))
        {
            if (NextState.KeysDown.Contains(e.KeyCode) && !NextState.KeysPressed.Contains(e.KeyCode))
                NextState.KeysPressed.Add(e.KeyCode);

            if (!NextState.KeysReleased.Contains(e.KeyCode))
                NextState.KeysReleased.Add(e.KeyCode);

            if (NextState.KeysRepeated.ContainsKey(e.KeyCode))
                NextState.KeysRepeated.Remove(e.KeyCode);

            if (NextState.KeysDown.Contains(e.KeyCode))
                NextState.KeysDown.Remove(e.KeyCode);
        }
    }

    private void OnMouseWheel(object sender, MouseEventArgs e)
    {
        Interlocked.Exchange(ref NextState.CurrentZ, e.Delta switch
        {
            < 0 => -1,
            > 0 => 1,
            _ => 0
        });
    }

    private void OnMouseDown(object sender, MouseEventArgs e)
    {
        NextState.LeftDown = e.Button == MouseButtons.Left;
        NextState.RightDown = e.Button == MouseButtons.Right;
        NextState.MiddleDown = e.Button == MouseButtons.Middle;
    }

    private void OnMouseUp(object sender, MouseEventArgs e)
    {
        NextState.LeftClick = Current.LeftDown && e.Button == MouseButtons.Left;
        NextState.RightClick = Current.RightDown && e.Button == MouseButtons.Right;
        NextState.MiddleClick = Current.MiddleDown && e.Button == MouseButtons.Middle;
        
        switch (e.Button)
        {
            case MouseButtons.Left:
                NextState.LeftDown = false;
                break;
            case MouseButtons.Right:
                NextState.RightDown = false;
                break;
            case MouseButtons.Middle:
                NextState.MiddleDown = false;
                break;
        }
    }

    public void ReadInput()
    {
        lock (typeof(GameInput))
        {
            Current = new InputState(Engine, Engine.Timing, NextState, Current);

            NextState.LeftClick = false;
            NextState.LeftUp = false;
            NextState.RightClick = false;
            NextState.RightUp = false;
            NextState.MiddleClick = false;
            NextState.MiddleUp = false;
            NextState.CurrentZ = 0;
            NextState.KeysReleased.Clear();
            NextState.KeysPressed.Clear();
        }
    }

    private Vector2 GetNormalizedViewPortCursor(Vector2 cursorPosition)
    {
        var mapViewport = Engine.GetComponent<MapViewport>();
        var relativePosition = cursorPosition - new Vector2(mapViewport?.Layout.OuterBounds.Left ?? 0, mapViewport?.Layout.OuterBounds.Top ?? 0);
        return new Vector2(
            relativePosition.X / (mapViewport?.Layout.OuterBounds.Left ?? Engine.ScreenSize.X),
            relativePosition.Y / (mapViewport?.Layout.OuterBounds.Top ?? Engine.ScreenSize.Y));
    }
}