using AncientEmpires.Engine.Render.Components;
using Font = AncientEmpires.Engine.Render.Font;

namespace AncientEmpires.Engine;

public class FontManager : EngineComponent
{
    public readonly Dictionary<KnownFont, Font> Fonts = new();

    public override void OnCreateComponents()
    {
        RegisterFont(KnownFont.JetBrains);
        RegisterFont(KnownFont.Jokerman);
    }

    public Font RegisterFont(KnownFont knownFont)
    {
        var font = new Font();
        font.Configure(knownFont, $"{knownFont}.fnt", $"{knownFont}.dds");
        return Fonts[knownFont] = AddComponent(font);
    }

    public Font GetFont(KnownFont knownFont) => Fonts[knownFont];
}