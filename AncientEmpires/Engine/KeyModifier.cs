namespace AncientEmpires.Engine;

public enum KeyModifier
{
    None,
    Control,
    Alt,
    Shift
}