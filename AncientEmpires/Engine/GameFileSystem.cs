﻿using System.IO.Abstractions;
using AncientEmpires.Engine.Render.Components;
using FileSystem = System.IO.Abstractions.FileSystem;

namespace AncientEmpires.Engine;

public class GameFileSystem : EngineComponent
{
    protected IFileSystem FileSystem;
    private string SavedGamesFolder = "SavedGames";

    public override void OnInitialize()
    {
        base.OnInitialize();

        FileSystem = new FileSystem();
        FileSystem.Directory.SetCurrentDirectory(Application.StartupPath);
    }

    public IEnumerable<GameFile> ListSavedGames()
    {
        var savedGameFolder = GetSavedGameFolder();
        var files = FileSystem.Directory.GetFiles(savedGameFolder.FullName);

        foreach (var file in files)
        {
            var fileInfo = FileSystem.FileInfo.FromFileName(file);
            yield return new GameFile(
                fileInfo.Name,
                file,
                fileInfo.Length,
                fileInfo.CreationTime,
                fileInfo.LastWriteTime,
                FileSystem);
        }
    }

    public void WriteSavedGame(string fileName, string contents)
    {
        var sanitizedFile = SanitizeFileName(fileName);
        var savedGameFolder = GetSavedGameFolder();
        var file = System.IO.Path.Combine(savedGameFolder.FullName, sanitizedFile);
        FileSystem.File.WriteAllText(file, contents);
    }

    private string SanitizeFileName(string fileName)
    {
        var name = fileName
            .Replace(".", "_")
            .Replace("/", "")
            .Replace("\\", "");

        foreach (var invalid in System.IO.Path.GetInvalidFileNameChars())
            name = name.Replace(invalid, '_');

        foreach (var invalid in System.IO.Path.GetInvalidPathChars())
            name = name.Replace(invalid, '_');

        return name;
    }

    private IDirectoryInfo GetSavedGameFolder()
    {
        return !FileSystem.Directory.Exists(SavedGamesFolder)
            ? FileSystem.Directory.CreateDirectory(SavedGamesFolder)
            : FileSystem.DirectoryInfo.FromDirectoryName(SavedGamesFolder);
    }
}