using System.Diagnostics;
using System.Numerics;
using AncientEmpires.Engine.Data;
using AncientEmpires.Engine.Entities;
using AncientEmpires.Engine.Entities.Buildings;
using AncientEmpires.Engine.Entities.Units;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Map.Picker;
using AncientEmpires.Engine.Map.Rendering;
using AncientEmpires.Engine.Map.Serialization;
using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Serialization;
using Vortice.Mathematics;
using Path = AncientEmpires.Engine.Entities.Buildings.Path;

namespace AncientEmpires.Engine;

public class EntityManager : RenderableComponent, ISerializable<EntityManager, DehydratedEntityManager>
{
    public ISerializer<EntityManager, DehydratedEntityManager> Serializer { get; } = new EntityManagerSerializer();
    protected Queue<Entity> NewEntities = new();
    protected SortedList<int, Entity> Entities { get; } = new();
    public List<Unit> Units = new();
    public List<Building> Buildings = new();
    public List<Path> Paths = new();
    public List<Entity> LastSearchResults = new();
    public SortedList<Entity, GamePosition> PreviousEntityQuadTreeQueuePosition { get; set; }
    public int NextEntityId { get; set; } = 1000;
    public List<Unit> SelectedUnits { get; private set; }
    public SortedSet<Entity> EntityQuadTreeQueue { get; set; }
    public EntityLookup EntityLookup { get; private set; }
    public Dictionary<Entity, EntityBoundingBox> EntityBoundBoxLookup { get; set; }
    public IsometricPositionStrategy PositionStrategy { get; set; }


    public override void OnCreateComponents()
    {
        base.OnCreateComponents();

        EntityLookup = AddComponent<EntityLookup>();
    }

    public override void OnInitialize()
    {
        base.OnInitialize();

        PreviousEntityQuadTreeQueuePosition = new SortedList<Entity, GamePosition>();
        SelectedUnits = new List<Unit>();
        EntityBoundBoxLookup = new Dictionary<Entity, EntityBoundingBox>();
        EntityQuadTreeQueue = new SortedSet<Entity>();
        PositionStrategy = Engine.GetComponent<IsometricPositionStrategy>();

        foreach (var entity in Entities.Values)
        {
            switch (entity)
            {
                case Unit unit:
                    unit.UnitId = Units.Count + 1;
                    Units.Add(unit);
                    break;
                case Building building:
                    Buildings.Add(building);
                    break;
                case Path path:
                    Paths.Add(path);
                    break;
            }
        }
    }

    public override void TriggerNextSlice(float timeDelta)
    {
        OnNextSlice(timeDelta);
    }

    public override void OnNextSlice(float timeDelta)
    {
        base.OnNextSlice(timeDelta);

        ProcessNewEntities();

        lock (EntityLookup)
        {
            //EntityQuadTree.Clear();

            foreach (var entity in EntityQuadTreeQueue)
            {
                //if (!entity.Changed)
                //continue;

                if (Math.Abs(Vector4.Distance(entity.LastQuadTreePosition.MapSpace, entity.Position.MapSpace)) < 0.1f)
                    continue;

                EntityLookup.Insert(entity);
                PreviousEntityQuadTreeQueuePosition[entity] = entity.Position;
                entity.LastQuadTreePosition = entity.Position;
            }
        }

        foreach (var entity in Entities.Values)
        {
            entity.OnNextSlice(timeDelta);
            entity.OnDraw();

            lock (EntityLookup)
            {
                EntityQuadTreeQueue.Add(entity);
                //var boundingBox = GetEntityBoundingBox(entity);
                //EntityQuadTree.Insert(boundingBox);
            }
        }
    }

    private void ProcessNewEntities()
    {
        while (NewEntities.Count > 0)
        {
            var entity = NewEntities.Dequeue();
            Entities.Add(entity.Id, entity);
            lock (EntityLookup)
            {
                EntityLookup.Insert(entity);
            }

            switch (entity)
            {
                case Unit unit:
                    unit.UnitId = Units.Count + 1;
                    Units.Add(unit);
                    break;
                case Building building:
                    Buildings.Add(building);
                    break;
                case Path path:
                    Paths.Add(path);
                    break;
            }

            RegisterComponent(entity);
        }
    }

    public void Select(List<Unit> units, bool append)
    {
        if (append)
        {   
            SelectedUnits.AddRange(units.Except(SelectedUnits));
        }
        else
        {
            SelectedUnits = units;
        }
    }

    public int GetNextEntityId()
    {
        return NextEntityId++;
    }

    public IEnumerable<Entity> GetEntities(List<int> entityIds)
    {
        foreach (var entityId in entityIds)
        {
            Debug.Assert(Entities.ContainsKey(entityId), $"Entities.ContainsKey({entityId})");

            yield return Entities[entityId];
        }
    }

    public IEnumerable<Entity> GetAllEntities()
    {
        return Entities.Values.AsEnumerable();
    }

    public Entity GetEntity(int entityId)
    {
        return Entities[entityId];
    }

    public TEntityType GetEntity<TEntityType>(int entityId) where TEntityType : Entity
    {
        return Entities[entityId] as TEntityType;
    }

    public void Add(Entity entity)
    {
        NewEntities.Enqueue(entity);
    }

    public void Remove(Entity entity)
    {
        lock (EntityLookup)
        {
            Entities.Remove(entity.Id);
            EntityLookup.Remove(entity);
        }
    }

    public void Hydrate(EntityManager hydrated, DehydratedEntityManager dehydrated)
    {
        Serializer.Hydrate(hydrated, dehydrated);
    }

    public DehydratedEntityManager Dehydrate() => Serializer.Dehydrate(this);
    
    /*
    public IEnumerable<Entity> SearchWorldSpace(BoundingBox boundingBox)
    {
        if (EntityLookup == null)
            return Enumerable.Empty<Entity>();
        
        var boxes = EntityLookup.FindWorldSpaceObjects(GamePosition.FromWorldSpace(boundingBox.Center.ToVector4()));
        return boxes.Select(o => o.Entity).ToList();
    }
    */
    
    public void Refresh()
    {
        foreach (var entity in GetAllEntities())
        {
            entity.Refresh();
        }
    }
}