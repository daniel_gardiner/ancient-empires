using System.Numerics;
using AncientEmpires.Engine.Entities.Pathing;
using AncientEmpires.Engine.Entities.Units;
using AncientEmpires.Engine.Render;
using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Map;
using AncientEmpires.Engine.Render.Shaders;
using AncientEmpires.Engine.Render.Text;

namespace AncientEmpires.Engine;

public class GameDebug : RenderableComponent
{
    public bool Enabled;
    public bool EnableWireframe;
    public string Text;
    public bool Trace;
    public DebugMode DebugMode = DebugMode.Path;
    protected int IndexCount;
    protected ComponentLookup<GraphicsDevice> GraphicsDevice;
    protected OverlayMapLayer OverlayMapLayer;
    protected DebugTextLayer DebugTextLayer;

    public override void OnCreateComponents()
    {
        AddComponent<PathDebug>();
        OverlayMapLayer = AddComponent<OverlayMapLayer>();
        DebugTextLayer = AddComponent<DebugTextLayer>();
    }

    public override void OnInitialize()
    {
        base.OnInitialize();

        GraphicsDevice = new ComponentLookup<GraphicsDevice>();
        ZIndex = Engine.Configuration.GetRenderLayer(KnownRenderLayers.MapOverlay);
        OverlayMapLayer.ZIndex = ZIndex - 0.1f;
        DebugTextLayer.ZIndex = ZIndex - 0.2f;
    }

    public override void OnNextFrame(float timeDelta)
    {
        base.OnNextFrame(timeDelta);
        IsVisible = Enabled;

        if (Enabled)
        {
            var shiftDown = Engine.Input.Current.KeysDown.Contains(Keys.ShiftKey);
            if (Engine.Input.Current.KeysDown.Contains(Keys.Subtract))
            {
                IndexCount = Math.Max(0, IndexCount - (shiftDown
                    ? 60
                    : 6));
                ConsoleWriter.WriteLineIf(IndexCount % 60 == 0, $"VertexRenderLimit: {IndexCount}");
            }

            if (Engine.Input.Current.KeysDown.Contains(Keys.Add))
            {
                IndexCount += (shiftDown
                    ? 60
                    : 6);
                ConsoleWriter.WriteLineIf(IndexCount % 60 == 0, $"VertexRenderLimit: {IndexCount}");
            }
        }
    }

    public override void OnDraw()
    {
        if (!Enabled)
            return;

        //DrawUnitPositions();
        DrawDebugString();

        //_pathDebug?.OnDraw();

        void DrawDebugString()
        {
            if (string.IsNullOrWhiteSpace(Text))
                return;

            var position = new Vector2(100, 100);
            //Engine.Renderer.FontManager.DrawText(position + new Vector2a(2, 2), $"{Text}");
        }


        /*
        void DrawUnitPositions()
        {
            var entities = Engine.GetComponent<EntityManager>()
                .Units
                .OrderBy(o => o.ZIndex)
                .ThenBy(o => o.Position.MapSpace.Y)
                .ThenBy(o => o.Position.MapSpace.X);

            foreach (var entity in entities)
            {
                //Engine.Renderer.FontManager.DrawText(MapTextMesh.Create(Engine, this, $"position-{entity.Id}", position, $"({entity.Position.X:N1}, {entity.Position.Y:N1})".Replace(".0", "")));
            }
        }
    */
    }

    public void ShowPathingScores(Unit unit, Stack<GamePosition> path, List<ScoredTile> scores)
    {
        if (!Enabled)
            return;

        ConsoleWriter.WriteLine($"[ShowPathingScores] for {unit}");
        Engine.GetComponent<PathDebug>().ShowScores(unit, path, scores);
    }

    public void ClearPathingScores()
    {
        Clear("ClearPathingScores");
    }

    public void Toggle()
    {
        Enabled = !Enabled;
        DebugMode = DebugMode.Full;
        DebugMode = DebugMode.Path;

        if (!Enabled)
        {
            DebugMode = DebugMode.None;
            IndexCount = 0;
            Clear("Debug disabled");
        }
    }

    public void CycleMode()
    {
        var modes = Enum.GetValues(typeof(DebugMode));
        DebugMode = (DebugMode)(((int)DebugMode + 1) % modes.Length);
        Console.WriteLine($"DebugMode: {DebugMode}");
    }

    public void Draw(GameShader shader, GameRenderable renderable)
    {
        switch (DebugMode)
        {
            /*
            case DebugMode.PartialRender:
                Engine.D3DContext.DrawIndexed(Math.Min(IndexCount, indexCount), startIndexLocation, baseVertexLocation);
                break;
            case DebugMode.OffsetRender:
                Engine.D3DContext.DrawIndexed(indexCount, Math.Min(IndexCount, startIndexLocation), baseVertexLocation);
                break;
            */
            default:
                GraphicsDevice.Value.Draw(renderable);
                break;
        }
    }

    public override void Clear(string reason)
    {
        DebugTextLayer.Clear(reason);
        OverlayMapLayer.Clear(reason);
    }
}