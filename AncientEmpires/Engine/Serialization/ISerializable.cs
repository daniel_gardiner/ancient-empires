namespace AncientEmpires.Engine.Serialization;

public interface ISerializable<THydrated, TDehydrated>
{
    ISerializer<THydrated, TDehydrated> Serializer { get; }
    void Hydrate(THydrated hydrated, TDehydrated dehydrated);
    TDehydrated Dehydrate();
}