namespace AncientEmpires.Engine.Serialization;

public interface ISerializer<THydrated, TDehydrated>
{
    THydrated Hydrate(THydrated hydrated, TDehydrated dehydrated);
    TDehydrated Dehydrate(THydrated input);
}