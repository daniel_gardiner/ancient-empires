using AncientEmpires.Engine.Layout;

namespace AncientEmpires.Engine.Interface;

public class MiniMapPanel : GameInterfacePanel
{
    public MiniMapPanel(GameEngine engine, IUiElement parent, GameLayout layout) : base(engine, parent, layout)
    {
    }

    public override void OnInitialize()
    {
        base.OnInitialize();
    }

    public override void OnDraw()
    {
        /*
        var bitmap = Engine.Textures[Texture.MiniMap];
        var destRect = VisibleBounds;
        var srcRect = new RectangleF(0, 0, 81, 81);
        Engine.D2DContext.DrawBitmap(bitmap, destRect, 1f, InterpolationMode.NearestNeighbor, srcRect, null);
        Engine.D2DContext.DRectangleF(VisibleBounds, Engine.GetColor(Color.Blue), 1);

        var viewport = GameContext.Instance.Engine.Viewport;
        var buildings = viewport.VisibleTiles.GetEntities().OfType<Building>().ToList();
        foreach (var building in buildings)
        {
            var scaled = (
                x: building.Position.X / viewport.MapSize.X * VisibleBounds.Width(),
                y: building.Position.Y / viewport.MapSize.Y * VisibleBounds.Height());

            switch (building.Type)
            {
                case EntityType.Road:
                    Engine.D2DContext.FillRectangle(
                        new RectangleF(scaled.x, scaled.y, scaled.x + 1, scaled.y + 1).Offset(VisibleBounds),
                        Engine.GetColor(Color.LightGray));
                    break;

                case EntityType.Wall:
                    Engine.D2DContext.FillRectangle(
                        new RectangleF(scaled.x, scaled.y, scaled.x + 1, scaled.y + 1).Offset(VisibleBounds),
                        Engine.GetColor(Color.Brown));
                    break;

                default:
                    Engine.D2DContext.FillRectangle(
                        new RectangleF(scaled.x, scaled.y, scaled.x + 2, scaled.y + 2).Offset(VisibleBounds),
                        Engine.GetColor(Color.Black));
                    break;
            }
        }
    */
    }
}