namespace AncientEmpires.Engine.Interface;

public readonly struct MeshKey : IEquatable<MeshKey>
{
    public static MeshKey Empty = new(0, 0);
    public readonly ushort KeyType;
    public readonly ulong Key;

    public MeshKey(ushort keyType, int entityId) : this(keyType, (ulong)entityId)
    {
    }

    public MeshKey(ushort keyType, in GamePosition position)
    {
        KeyType = keyType;
        Key = keyType * ((ulong)position.MapSpace.X * 10 + (ulong)position.MapSpace.Y * 10000);
    }

    public MeshKey(ushort keyType, ulong key)
    {
        KeyType = keyType;
        Key = keyType * key * 10;
    }

    public override string ToString()
    {
        return Key.ToString();
    }

    public bool Equals(MeshKey other) => Key == other.Key && KeyType == other.KeyType;

    public override bool Equals(object obj) => obj is MeshKey other && Equals(other);

    public override int GetHashCode() => HashCode.Combine(KeyType, Key);
}