using System.Numerics;
using AncientEmpires.Engine.Interface.Overview;
using AncientEmpires.Engine.Interface.ProvincialBuild;
using AncientEmpires.Engine.Layout;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Groups;
using AncientEmpires.Engine.Render.Interface;
using AncientEmpires.Engine.Render.Text;
using Humanizer;
using static System.Globalization.CultureInfo;
using static AncientEmpires.Engine.Layout.LayoutDirection;
using static AncientEmpires.Engine.Layout.LayoutUnits;

namespace AncientEmpires.Engine.Interface;

public class GameInterface : RenderableComponent, IUiElement
{
    private readonly TimeSpan _textUpdateDuration = 100.Milliseconds();
    private ProvincialBuildPanel _provincialBuildPanel;
    private OverviewPanel _overviewPanel;
    private ContainerPanel _debugPanel;
    private ContainerPanel _containerPanel;
    private MiniMapPanel _miniMapPanel;
    private TextMeshFragment _moneyText;
    private TextMeshFragment _populationText;
    private TextMeshFragment _monthText;
    private TextMeshFragment _yearText;
    private TextMeshFragment _fpsText;
    private EntityInformationPanel _entityInformationPanel;
    public ContainerPanel ManagementPanel;
    public ContainerPanel OverviewPanel;
    public GameInterfacePanel LeftPanel;
    public GameInterfacePanel MiddlePanel;
    public GameInterfacePanel RightPanel;
    public ComponentLookup<InterfaceTextLayer> TextRenderer;
    public ComponentLookup<InterfaceLayer> Renderer;
    public IUiElement UiParent => this;
    public IEnumerable<IUiElement> UiChildren => Components.OfType<UiElement>().ToList();
    public bool IsMouseOver { get; set; }
    public GameLayout Layout { get; set; }

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();

        AddComponent(new GameInterfaceBackground(Engine, this, new GameLayout(Engine, null)));
        AddComponent(new MapViewport(Engine, this, new GameLayout(Engine, null).Fill()));

        ManagementPanel = AddPanel(new ContainerPanel(Engine, this, "ManagementPanel", new GameLayout(Engine, Layout).Dock(Bottom, Left, Right).Padding(100, 5).Height(174)));
        OverviewPanel = AddPanel(new ContainerPanel(Engine, this, "OverviewPanel", new GameLayout(Engine, Layout).Dock(Top, Left, Right).Padding(100, 5)));
        LeftPanel = ManagementPanel.AddPanel(new ContainerPanel(Engine, ManagementPanel, new GameLayout(Engine, Layout).Dock(Left, Top, Bottom).Width(30, Percent).Margin(10, 10)));
        MiddlePanel = ManagementPanel.AddPanel(new ContainerPanel(Engine, ManagementPanel, new GameLayout(Engine, Layout).DockTo(LeftPanel.Layout, Left, 10).Dock(Top, Bottom).Width(40, Percent).Margin(10, 10)));
        RightPanel = ManagementPanel.AddPanel(new ContainerPanel(Engine, ManagementPanel, new GameLayout(Engine, Layout).DockTo(MiddlePanel.Layout, Left, 10).Width(30, Percent).Margin(10, 10)));

        _debugPanel = AddPanel(new DebugPanel(Engine, this, new GameLayout(Engine, Layout).Dock(Left).Margin(5, 100).Width(200).Height(300)));
        _containerPanel = AddPanel(new ContainerPanel(Engine, this, new GameLayout(Engine, Layout)));
        _overviewPanel = AddPanel(new OverviewPanel(Engine, this, new GameLayout(Engine, Layout).Padding(10)));
        _miniMapPanel = AddPanel(new MiniMapPanel(Engine, this, new GameLayout(Engine, Layout).Padding(10)));
        _provincialBuildPanel = AddPanel(new ProvincialBuildPanel(Engine, this, new GameLayout(Engine, Layout).Padding(10)));
        _entityInformationPanel = AddPanel(new EntityInformationPanel(Engine, this, new GameLayout(Engine, Layout).Padding(10)));
    }

    public override void OnInitialize()
    {
        base.OnInitialize();

        Renderer = Engine.ComponentLookup<InterfaceLayer>();
        TextRenderer = Engine.ComponentLookup<InterfaceTextLayer>();
        ZIndex = Engine.Configuration.GetRenderLayer(KnownRenderLayers.Interface);
        LeftPanel.ReplaceChild(_overviewPanel);
        MiddlePanel.ReplaceChild(_containerPanel);
        RightPanel.ReplaceChild(_miniMapPanel);
    }

    public override void OnEngineReady()
    {
        base.OnEngineReady();

        var topOffset = 12;
        var color = new Vector4(1, 1, 1, 0.8f);
        var monthName = CurrentCulture.DateTimeFormat.GetMonthName(Engine.GeneralStats.Month);
        var textDepth = 0.5f;
        _moneyText = TextRenderer.Value.AddText($"{Engine.GeneralStats.Money:N0} Sesterces", new Vector4(100, 5, 0, 0), color);
        _populationText = TextRenderer.Value.AddText($"{Engine.GeneralStats.Population} People", new Vector4(450, topOffset, ZIndex - textDepth, 0), color);
        _populationText = TextRenderer.Value.AddText($"{Engine.GeneralStats.Population} People", new Vector4(450, topOffset, ZIndex - textDepth, 0), color);
        _monthText = TextRenderer.Value.AddText(monthName, new Vector4(Engine.ScreenSize.X - 320, topOffset, ZIndex - textDepth, 0), color);
        _yearText = TextRenderer.Value.AddText($"{Engine.GeneralStats.Year} AD", new Vector4(Engine.ScreenSize.X - 200, topOffset, ZIndex - textDepth, 0), color);
        _fpsText = TextRenderer.Value.AddText($"{Math.Round(Engine.Timing.Fps, 1)} FPS", new Vector4(Engine.ScreenSize.X - 500, topOffset, ZIndex - textDepth, 0), color);

        _debugPanel.Show();
    }

    public override void OnResize()
    {
        base.OnResize();

        foreach (var component in Components.OfType<IUiElement>())
        {
            component.ChangeLayout(component.Layout, this);
        }
    }

    public override void OnDraw()
    {
        if (Engine.Timing.HasBeen("interface-update", _textUpdateDuration))
        {
            _moneyText.UpdateText($"{Engine.GeneralStats.Money:N0} Sesterces");
            _populationText.UpdateText($"{Engine.GeneralStats.Population} People");
            _monthText.UpdateText(CurrentCulture.DateTimeFormat.GetMonthName(Engine.GeneralStats.Month));
            _yearText.UpdateText($"{Engine.GeneralStats.Year} AD");
            _fpsText.UpdateText($"{Math.Round(Engine.Timing.Fps, 1)} FPS");
        }

        base.OnDraw();
    }

    public void WhenMouseExit(Func<GameEngine, InputState, bool> condition, Action<GameEngine, InputState> action)
    {
    }

    public bool HitTest(Vector4 location) => true;


    public void Hide()
    {
        IsVisible = false;
    }

    public void Show()
    {
        IsVisible = true;
    }

    public void MouseExit(GameEngine engine, InputState inputState)
    {
    }

    public IUiElement ChangeLayout(GameLayout layout, IUiElement parent)
    {
        Parent = (EngineComponent)parent;
        Layout = new GameLayout(Engine, UiParent.Layout, layout);
        return this;
    }

    public void WhenLeftClicked(Func<GameEngine, InputState, bool> condition, Action<GameEngine, InputState> action)
    {
    }

    public void WhenLeftDown(Func<GameEngine, InputState, bool> condition, Action<GameEngine, InputState> action)
    {
    }

    public void WhenRightClicked(Func<GameEngine, InputState, bool> condition, Action<GameEngine, InputState> action)
    {
    }

    public void WhenRightDown(Func<GameEngine, InputState, bool> condition, Action<GameEngine, InputState> action)
    {
    }

    public void WhenMouseOver(Func<GameEngine, InputState, bool> condition, Action<GameEngine, InputState> action)
    {
    }

    public override void TriggerDraw()
    {
        base.TriggerDraw();
    }

    public MeshFragment AddFragment(MeshFragment fragment)
    {
        switch (fragment)
        {
            case InterfaceMeshFragment interfaceMeshFragment:
            {
                var meshFragment = Renderer.Value.Batches.AddFragment(interfaceMeshFragment);
                meshFragment.OnInitialize();
                return meshFragment;
            }
            case TextMeshFragment texMeshFragment:
            {
                var meshFragment = Engine.GetComponent<InterfaceTextLayer>().AddFragment(texMeshFragment);
                meshFragment.OnInitialize();
                return meshFragment;
            }
            default:
                throw new ArgumentOutOfRangeException(nameof(fragment), fragment, null);
        }
    }

    public void LeftClick(GameEngine engine, InputState inputState)
    {
    }

    public void LeftDown(GameEngine engine, InputState inputState)
    {
    }

    public void RightDown(GameEngine engine, InputState inputState)
    {
    }

    public void RightClick(GameEngine engine, InputState inputState)
    {
    }

    public void MouseOver(GameEngine engine, InputState inputState)
    {
    }

    private TPanel AddPanel<TPanel>(TPanel panel) where TPanel : GameInterfacePanel
    {
        return RegisterComponent(panel);
    }

    public GameInterfacePanel ShowPanel(Panel panel)
    {
        switch (panel)
        {
            case Panel.ProvincialBuild:
                LeftPanel.ReplaceChild(_overviewPanel);
                MiddlePanel.ReplaceChild(_provincialBuildPanel);
                return _provincialBuildPanel;
            case Panel.EntityInformation:
                RightPanel.ReplaceChild(_entityInformationPanel);
                return _entityInformationPanel;
            default:
                MiddlePanel.ReplaceChild(_containerPanel);
                return _containerPanel;
        }
    }

    public void Butt()
    {
        // Draw Other Buttons

        /*
        switch (CurrentOptions)
        {
            case mAttackRequest:
            {
                do
                {
                    Buttons_AttackRequest();
                    //DDSPrimary.Flip(null, FlipFlags.Wait);
                }
                while (true);

                break;
            }

            case mGarrisonRequest:
            {
                do
                {
                    Application.DoEvents();
                    Buttons_GarrisonRequest();
                    //DDSPrimary.Flip(null, FlipFlags.Wait);
                }
                while (true);

                break;
            }

            case mBuild:
            {
                for (var aa = 9; aa <= 14; aa++)
                {
                    var yPos = OtherButtons[1, aa].Top;
                    var xPos = OtherButtons[1, aa].Left;

                    if (!_engine.Input.State.LeftClick)
                        OtherButtons[1, aa].Press = 1;

                    var rectangle = OtherButtons[OtherButtons[1, aa].Press, aa].Location;
                    var src = new RectangleF(rectangle.Left, rectangle.Top, rectangle.Right, rectangle.Bottom);
                    D3D.D2Device.DrawBitmap(bitmap, new RectangleF(xPos, yPos, xPos + 30, yPos + 30), 1f, NearestNeighbor, src, null);
                }

                break;
            }

            case mGameOptions: // Menu
            {
                Buttons_Menu();
                break;
            }

            case mGameStats:
            {
                Buttons_GameStats();
                break;
            }

            case mBuildingData: // SHOW BUILDING DATA
            {
                switch (_engine.GeneralStats.BuildingType)
                {
                    case EntityType.House:
                        /*
                        SurfaceBack.DrawFast(165, 395, SurfaceMiniMap, Labels[9], DrawFastFlags.SourceColorKey | DrawFastFlags.Wait);
                        SurfaceBack.DrawFast(300, 390, SurfaceMiniMap, OutPutBox[3], DrawFastFlags.SourceColorKey | DrawFastFlags.Wait);
                        var buildingType = BuildingList[_engine.GeneralStats.BuildingTeam, CurrentProvince, (int) _engine.GeneralStats.BuildingType];
                        WriteText(Conversion.Str((int) Math.Round(buildingType.Instances[_engine.GeneralStats.BuildingNumber].Population)), 304, 395);
                        #1#
                        break;
                }

                break;
            }

            case mUnitData:
            {
                /*
                SurfaceBack.DrawFast(165, 395, SurfaceMiniMap, Labels[10], DrawFastFlags.SourceColorKey | DrawFastFlags.Wait);
                SurfaceBack.DrawFast(300, 390, SurfaceMiniMap, OutPutBox[3], DrawFastFlags.SourceColorKey | DrawFastFlags.Wait);
                #1#
                //WriteText($"{Leigion[GeneralData.TeamSelected[1], GeneralData.TroopSelected[1]].NumberInLeigion}", 304, 395);
                break;
            }
        }
    */
    }
}