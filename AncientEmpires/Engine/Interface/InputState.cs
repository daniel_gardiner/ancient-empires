using System.Numerics;
using AncientEmpires.Engine.Map.Rendering;

namespace AncientEmpires.Engine.Interface;

public class InputState
{
    public List<Keys> KeysDown = new();
    public List<Keys> KeysReleased = new();
    public List<Keys> KeysPressed = new();
    public Dictionary<Keys, int> KeysRepeated = new();
    public float LeftDownDuration;
    public float RightDownDuration;
    public float MiddleDownDuration;
    public bool LeftDown;
    public bool LeftHeld;
    public bool LeftUp;
    public Vector4 LeftDownPosition;
    public Vector4 RightDownPosition;
    public Vector4 MiddleDownPosition;
    public bool RightDown;
    public bool RightHeld;
    public bool RightUp;
    public bool MiddleDown;
    public bool MiddleHeld;
    public bool MiddleUp;
    public int CurrentZ;
    public bool LeftClick;
    public float LastLeftDown;
    public float LastRightDown;
    public float LastMiddleDown;
    public bool RightClick;
    public bool MiddleClick;
    public int WheelDelta;
    public int LastWheelDelta;
    public Vector4 CursorPosition;
    public Vector4 CursorWorldSpace;


    public InputState()
    {
    }

    public InputState(GameEngine engine, Timing timing, InputState state, InputState current)
    {
        var positionStrategy = engine.GetComponent<IsometricPositionStrategy>();
        CurrentZ = state.CurrentZ;
        KeysDown = state.KeysDown.ToList();
        KeysPressed = state.KeysPressed.ToList();
        KeysReleased = state.KeysReleased.ToList();
        KeysRepeated = new Dictionary<Keys, int>(state.KeysRepeated);
        LeftDown = state.LeftDown;
        LeftHeld = state.LeftDown;
        LeftClick = state.LeftClick; // && DistanceMethods.EuclideanDistance(current.LeftDownPosition, state.CursorPosition) < 10;
        LeftUp = !state.LeftDown && current.LeftDown;
        RightDown = state.RightDown;
        RightHeld = state.RightDown;
        RightUp = !state.RightDown && current.RightDown;
        RightClick = state.RightClick; // && DistanceMethods.EuclideanDistance(current.RightDownPosition, state.CursorPosition) < 10; 
        MiddleDown = state.MiddleDown;
        MiddleHeld = state.MiddleDown;
        MiddleClick = state.MiddleClick; //  && DistanceMethods.EuclideanDistance(current.MiddleDownPosition, state.CursorPosition) < 10;
        MiddleUp = !state.MiddleDown && current.MiddleDown;
        WheelDelta = state.WheelDelta;
        LastWheelDelta = state.LastWheelDelta;
        CursorPosition = state.CursorPosition;
        CursorWorldSpace = engine.Camera.ScreenSpaceToWorldSpace(CursorPosition);

        LeftDownPosition = (state.LeftDown, current.LeftDown) switch
        {
            (true, false) => state.CursorPosition,
            (true, true) => current.LeftDownPosition,
            _ => Vector4.Zero
        };
        RightDownPosition = (state.RightDown, current.RightDown) switch
        {
            (true, false) => state.CursorPosition,
            (true, true) => current.RightDownPosition,
            _ => Vector4.Zero
        };
        MiddleDownPosition = (state.MiddleDown, current.MiddleDown) switch
        {
            (true, false) => state.CursorPosition,
            (true, true) => current.MiddleDownPosition,
            _ => Vector4.Zero
        };
        LastLeftDown = (state.LeftDown, current.LeftDown) switch
        {
            (true, false) => timing.TotalElapsedMs,
            (true, true) => current.LastLeftDown,
            _ => timing.TotalElapsedMs
        };
        LeftDownDuration = state.LeftDown switch
        {
            true => timing.TotalElapsedMs - LastLeftDown,
            _ => 0
        };
        LastRightDown = (state.RightDown, current.RightDown) switch
        {
            (true, false) => timing.TotalElapsedMs,
            (true, true) => current.LastRightDown,
            _ => timing.TotalElapsedMs
        };
        RightDownDuration = state.RightDown switch
        {
            true => timing.TotalElapsedMs - LastRightDown,
            _ => 0
        };
        LastMiddleDown = (state.MiddleDown, current.MiddleDown) switch
        {
            (true, false) => timing.TotalElapsedMs,
            (true, true) => current.LastMiddleDown,
            _ => timing.TotalElapsedMs
        };
        MiddleDownDuration = state.MiddleDown switch
        {
            true => timing.TotalElapsedMs - LastMiddleDown,
            _ => 0
        };
    }
}