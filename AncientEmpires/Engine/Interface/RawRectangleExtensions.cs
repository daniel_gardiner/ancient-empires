using System.Numerics;

namespace AncientEmpires.Engine.Interface;

public static class RectangleFExtensions
{
    public static bool Contains(this RectangleF rect, Vector2 position)
    {
        var insideX = rect.Left <= position.X && rect.Right >= position.X;
        var insideY = rect.Top <= position.Y && rect.Bottom >= position.Y;
        return insideX && insideY;
    }

    public static RectangleF Constrain(this RectangleF rect, RectangleF constraint) => new(
        rect.Left,
        rect.Top,
        (int) (rect.Right - rect.Left > constraint.Width()
            ? rect.Left + constraint.Width()
            : rect.Right),
        (int) (rect.Bottom - rect.Top > constraint.Height()
            ? rect.Top + constraint.Height()
            : rect.Bottom));


    public static Vector2 Center(this RectangleF rect) => new(
        (rect.Right - rect.Left) / 2,
        (rect.Bottom - rect.Top) / 2);

    public static RectangleF Offset(this RectangleF rect, RectangleF by) => new(
        rect.Left + by.Left,
        rect.Top + by.Top,
        rect.Right + by.Left,
        rect.Bottom + by.Top);

    public static RectangleF Offset(this RectangleF rect, Vector2 offset) => new(
        (int) (rect.Left + offset.X),
        (int) (rect.Top + offset.Y),
        (int) (rect.Right + offset.X),
        (int) (rect.Bottom + offset.Y));

    public static RectangleF Offset(this RectangleF rect, Vector2 offset, SizeF constraint) => new(
        (int) (rect.Left + offset.X),
        (int) (rect.Top + offset.Y),
        (int) (rect.Right + offset.X > rect.Left + constraint.Width
            ? rect.Left + constraint.Width
            : rect.Right + offset.X),
        (int) (rect.Bottom + offset.Y > rect.Top + constraint.Height
            ? rect.Top + constraint.Height
            : rect.Bottom + offset.Y));

    public static Vector2 ToVector2(this RectangleF rect) => new(
        rect.Left,
        rect.Top);

    public static RectangleF Shrink(this RectangleF rect, float size) => new((int) (rect.Left + size), (int) (rect.Top + size), (int) (rect.Right - size), (int) (rect.Bottom - size));

    public static RectangleF Expand(this RectangleF rect, float size) => rect.Shrink(-size);

    public static float Width(this RectangleF rect) => rect.Right - rect.Left;

    public static float Height(this RectangleF rect) => rect.Bottom - rect.Top;
}