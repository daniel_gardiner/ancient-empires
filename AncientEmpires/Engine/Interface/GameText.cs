using System.Numerics;
using AncientEmpires.Engine.Layout;
using AncientEmpires.Engine.Render.Quads;
using AncientEmpires.Engine.Render.Text;

namespace AncientEmpires.Engine.Interface;

public class GameText : UiElement
{
    public Vector4 Color { get; }
    public override float ZIndex => UiParent.ZIndex - 0.1f;
    public override Vector4 BackgroundColor { get; set; } = new(25 / 256f, 50 / 256f, 60 / 256f, 1);
    public override bool IsVisible => Parent.IsVisible;
    public TextLayer TextLayer { get; }
    public string Text { get; set; }

    public GameText(GameEngine engine, UiElement parent, TextLayer textLayer, GameLayout layout, string text, Vector4 color, float zIndex) : base(engine, parent, layout)
    {
        TextLayer = textLayer;
        Text = text;
        Color = color;
        ZIndex = zIndex;
    }

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();

        ShaderMaterial = ShaderMaterial.InterfaceText;
    }

    public override void OnInitialize()
    {
        base.OnInitialize();

        Layout = new GameLayout(Engine, UiParent.Layout, Layout);
        ZIndex = Engine.Configuration.GetRenderLayer(KnownRenderLayers.Interface) - 0.3f;
        
        //WhenLeftDown(Condition.Always, (_, _) => IsPressed() = true);
        WhenLeftClicked(Condition.Always, (_, _) => SoundManager.Play(1));
        //WhenLeftClicked(Condition.Always, (_, _) => Pressed = false);
        //WhenMouseExit(Condition.Always, (_, _) => Pressed = false);
    }

    public override void OnNextFrame(float timeDelta)
    {
        if (Fragment == null)
            return;

        ((TextMeshFragment)Fragment).UpdateText(Text);

        base.OnNextFrame(timeDelta);
    }

    protected override void OnCreateFragment()
    {
        Fragment = TextLayer.AddText(
            Text,
            new Vector4(
                Layout.InnerBounds.X,
                Layout.InnerBounds.Y,
                ZIndex,
                0),
            Color);
    }

    public void UpdateText(string text)
    {
        Text = text;
    }
}