namespace AncientEmpires.Engine.Interface;

public enum Panel
{
    None,
    ProvincialBuild,
    EntityInformation
}