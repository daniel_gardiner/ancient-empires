using System.Numerics;

namespace AncientEmpires.Engine.Interface;

public static class GamePositionExtensions
{
    public static GamePosition MoveTowards(in this GamePosition current, GamePosition target, float step)
    {
        if (Vector2.Distance(target.MapSpace.ToVector2(), current.MapSpace.ToVector2()) < step)
            return target;

        var direction = target - current;
        var vector2 = direction.MapSpace.ToVector2();
        var normalizedDirection = Vector2.Normalize(vector2);
        var newPosition = current.MapSpace.ToVector2() + (normalizedDirection * step);
        return GamePosition.FromMapSpace(newPosition.ToVector4());
    }
}

public static class Vector2Extensions
{
    public static Vector3 ToVector3(in this Vector2 current, float z = 0)
    {
        return new(current.X, current.Y, z);
    }

    public static Vector4 ToVector4(in this Vector2 current, float z = 0)
    {
        return new(current.X, current.Y, z, 0);
    }

    public static Vector2 MoveTowards(in this Vector2 current, Vector2 target, float step)
    {
        if (Vector2.Distance(target, current) < step)
            return target;

        var direction = target - current;
        direction = Vector2.Normalize(direction);
        return current + (direction * step);
    }
}

public static class Vector4Extensions
{
    public static ref Vector4 AtDepth(this ref Vector4 current, float zIndex)
    {
        current.Z = zIndex;
        return ref current;
    }
    
    public static Vector2 ToVector2(this Vector4 current)
    {
        return new(current.X, current.Y);
    }

    public static Vector3 ToVector3(this Vector4 current)
    {
        return new(current.X, current.Y, current.Z);
    }

    public static Vector4 MoveTowards(this Vector4 current, Vector4 target, double step)
    {
        if (Vector4.Distance(target, current) < step)
            return target;

        var direction = Vector4.Normalize(target - current);
        return current + direction * (float)step;
    }
}

public static class Vector3Extensions
{
    public static Vector4 ToVector4(this Vector3 current)
    {
        return new(current.X, current.Y, current.Z, 0);
    }
}