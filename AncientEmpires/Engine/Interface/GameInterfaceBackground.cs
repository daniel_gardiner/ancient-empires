using System.Numerics;
using AncientEmpires.Engine.Layout;
using AncientEmpires.Engine.Render;
using AncientEmpires.Engine.Render.Interface;
using AncientEmpires.Engine.Render.Quads;

namespace AncientEmpires.Engine.Interface;

public sealed class GameInterfaceBackground : UiElement
{
    private static List<int> _textureIndexes;

    public GameInterfaceBackground(GameEngine engine, IUiElement parent, GameLayout layout) : base(engine, parent, layout)
    {
    }

    public override void OnInitialize()
    {
        base.OnInitialize();

        ZIndex = Engine.Configuration.GetRenderLayer(KnownRenderLayers.Interface) - 0.1f;
        ShaderMaterial = ShaderMaterial.Interface;
        _textureIndexes ??= Engine.GetComponent<GameTextures>().FindTextureIndexes("Interface.dds");
    }

    protected override void OnCreateFragment()
    {
        Fragment = RegisterComponent<InterfaceMeshFragment>().SetZIndex(ZIndex);
    }

    public override bool HitTest(Vector4 location) => false;

    public override void OnNextFrame(float timeDelta)
    {
        //if (!Changed)
        //  return;

        Fragment.IsVisible = IsVisible;

        var screenSize = new Vector4(Layout.OuterBounds.Right - Layout.OuterBounds.Left, Layout.OuterBounds.Bottom - Layout.OuterBounds.Top, 0, 0);
        var uiScale1 = 0.4f;
        var sideWidth = 236 * uiScale1;
        var topSideWidth = 292 * uiScale1;
        var topSideHeight = 230 * uiScale1;
        var topHeight = 105 * uiScale1;
        var bottomHeight = 435 * uiScale1;

        var uiScale2 = 1f;
        var sideTextureWidth = 236 * uiScale2;
        var topTextureSideWidth = 292 * uiScale2;
        var topTextureSideHeight = 230 * uiScale2;
        var topTextureHeight = 105 * uiScale2;
        var bottomTextureHeight = 435 * uiScale2;

        var textureWidth = 4096;
        var textureHeight = 1950;
        var textureIndexes = _textureIndexes;

        Fragment.UpdateQuads(
            new OrthoTextureQuad(
                new Vector4(0, 0, ZIndex, 0),
                new Vector4(topSideWidth, topSideHeight, 0, 0),
                new Sprite(0, 0, topTextureSideWidth, topTextureSideHeight, textureIndexes),
                ShaderMaterial),
            new OrthoTextureQuad(
                new Vector4(0, topHeight, ZIndex, 0),
                new Vector4(sideWidth, screenSize.Y - topHeight, 0, 0),
                new Sprite(0, topTextureSideHeight, topTextureSideHeight, textureHeight - topTextureSideHeight, textureIndexes),
                ShaderMaterial),
            new OrthoTextureQuad(
                new Vector4(screenSize.X - topSideWidth, 0, ZIndex, 0),
                new Vector4(topSideWidth, topSideHeight, 0, 0),
                new Sprite(textureWidth - topTextureSideWidth, 0, topTextureSideWidth, topTextureSideHeight, textureIndexes),
                ShaderMaterial),
            new OrthoTextureQuad(
                new Vector4(screenSize.X - sideWidth, topHeight, ZIndex, 0),
                new Vector4(sideWidth, screenSize.Y - topHeight, 0, 0),
                new Sprite(textureWidth - sideTextureWidth, topTextureSideHeight, sideTextureWidth, textureHeight - topTextureSideHeight, textureIndexes),
                ShaderMaterial),
            new OrthoTextureQuad(
                new Vector4(topSideWidth, 0, ZIndex, 0),
                new Vector4(screenSize.X - topSideWidth * 2, topHeight, 0, 0),
                new Sprite(topTextureSideWidth, 0, textureWidth - topTextureSideWidth * 2, topTextureHeight, textureIndexes),
                ShaderMaterial),
            new OrthoTextureQuad(
                new Vector4(sideWidth, screenSize.Y - bottomHeight, ZIndex, 0),
                new Vector4(screenSize.X - sideWidth * 2, bottomHeight, 0, 0),
                new Sprite(sideTextureWidth, textureHeight - bottomTextureHeight, textureWidth - sideTextureWidth * 2, bottomTextureHeight, textureIndexes),
                ShaderMaterial));
    }
}