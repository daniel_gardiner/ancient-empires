using System.Numerics;
using AncientEmpires.Engine.Layout;
using AncientEmpires.Engine.Render;
using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Groups;
using AncientEmpires.Engine.Render.Quads;

namespace AncientEmpires.Engine.Interface;

public abstract class UiElement : RenderableComponent, IUiElement
{
    private readonly List<(Func<GameEngine, InputState, bool> condition, Action<GameEngine, InputState> action)> _leftClickActions = new();
    private readonly List<(Func<GameEngine, InputState, bool> condition, Action<GameEngine, InputState> action)> _rightClickActions = new();
    private readonly List<(Func<GameEngine, InputState, bool> condition, Action<GameEngine, InputState> action)> _leftDownActions = new();
    private readonly List<(Func<GameEngine, InputState, bool> condition, Action<GameEngine, InputState> action)> _rightDownActions = new();
    private readonly List<(Func<GameEngine, InputState, bool> condition, Action<GameEngine, InputState> action)> _mouseOverActions = new();
    private readonly List<(Func<GameEngine, InputState, bool> condition, Action<GameEngine, InputState> action)> _mouseExitActions = new();
    protected MeshFragment Fragment;
    public virtual Vector4 BackgroundColor { get; set; } = new(25 / 256f, 26 / 256f, 27 / 256f, 1);
    public IEnumerable<IUiElement> UiChildren => Components.OfType<UiElement>().ToList();
    public bool IsMouseOver { get; set; }
    public IUiElement UiParent => (IUiElement)Parent;
    public GameLayout Layout { get; set; }
    protected ShaderMaterial ShaderMaterial;

    protected UiElement(GameEngine engine, IUiElement parent, GameLayout layout)
    {
        Parent = (EngineComponent)parent;
        Engine = engine;
        Layout = layout;
        Changed = true;
    }
    
    public override void OnNextFrame(float timeDelta)
    {
        base.OnNextFrame(timeDelta);

        if (Fragment == null || !Changed)
            return;

        Fragment.IsVisible = IsVisible;
        Fragment.ZIndex = ZIndex;
        var quad = new OrthoTextureQuad(
            new Vector4(Layout.OuterBounds.Left, Layout.OuterBounds.Top, ZIndex, 0),
            new Vector4(Layout.OuterBounds.Right - Layout.OuterBounds.Left, Layout.OuterBounds.Bottom - Layout.OuterBounds.Top, ZIndex, 0),
            Sprite.Empty,
            ShaderMaterial);
        quad.SetColor(BackgroundColor);
        Fragment.UpdateQuads(quad);

        Changed = false;
    }

    public override void OnBeforeDraw()
    {
        base.OnBeforeDraw();

        if (Fragment == null)
        {
            OnCreateFragment();
        }

        if (Fragment.Batch == null)
        {
            UiParent.AddFragment(Fragment);
            return;
        }

        Fragment.Refresh(Engine.Timing.SecondFraction);

        if (Fragment.Changed && Fragment.IsVisible)
            Fragment.MarkDirty();
    }

    protected abstract void OnCreateFragment();
    
    public override void TriggerResize()
    {
        MarkDirty();
        ChangeLayout(Layout, UiParent);
        Fragment?.Refresh(Engine.Timing.SecondFraction);

        base.TriggerResize();
    }

    public override void OnResize()
    {
        base.OnResize();
    }

    private void MarkDirty()
    {
        Changed = true;
    }

    public MeshFragment AddFragment(MeshFragment fragment) => UiParent.AddFragment(fragment);

    public virtual void LeftClick(GameEngine engine, InputState inputState)
    {
        if (!IsVisible)
            return;

        ConsoleWriter.WriteLine($"Im clicked! {TypeName}");
        foreach (var action in _leftClickActions)
            if (action.condition(engine, inputState))
                action.action(engine, inputState);
    }

    public virtual void LeftDown(GameEngine engine, InputState inputState)
    {
        foreach (var action in _leftDownActions)
            if (action.condition(engine, inputState))
                action.action(engine, inputState);
    }

    public void RightDown(GameEngine engine, InputState inputState)
    {
        foreach (var action in _rightDownActions)
            if (action.condition(engine, inputState))
                action.action(engine, inputState);
    }

    public virtual void RightClick(GameEngine engine, InputState inputState)
    {
        foreach (var action in _rightClickActions)
            if (action.condition(engine, inputState))
                action.action(engine, inputState);
    }

    public virtual void MouseOver(GameEngine engine, InputState inputState)
    {
        IsMouseOver = true;

        foreach (var action in _mouseOverActions)
            if (action.condition(engine, inputState))
                action.action(engine, inputState);
    }

    public virtual void MouseExit(GameEngine engine, InputState inputState)
    {
        IsMouseOver = false;

        foreach (var action in _mouseExitActions)
            if (action.condition(engine, inputState))
                action.action(engine, inputState);
    }

    public virtual IUiElement ChangeLayout(GameLayout layout, IUiElement parent)
    {
        Parent = (EngineComponent)parent;
        Layout = new GameLayout(Engine, UiParent.Layout, layout);
        foreach (var uiChild in UiChildren)
        {
            uiChild.ChangeLayout(uiChild.Layout, this);
        }

        return this;
    }

    public void WhenLeftClicked(Func<GameEngine, InputState, bool> condition, Action<GameEngine, InputState> action)
    {
        _leftClickActions.Add((condition, action));
    }

    public void WhenLeftDown(Func<GameEngine, InputState, bool> condition, Action<GameEngine, InputState> action)
    {
        _leftDownActions.Add((condition, action));
    }

    public void WhenRightClicked(Func<GameEngine, InputState, bool> condition, Action<GameEngine, InputState> action)
    {
        _rightClickActions.Add((condition, action));
    }

    public void WhenRightDown(Func<GameEngine, InputState, bool> condition, Action<GameEngine, InputState> action)
    {
        _rightDownActions.Add((condition, action));
    }

    public void WhenMouseOver(Func<GameEngine, InputState, bool> condition, Action<GameEngine, InputState> action)
    {
        _mouseOverActions.Add((condition, action));
    }

    public void WhenMouseExit(Func<GameEngine, InputState, bool> condition, Action<GameEngine, InputState> action)
    {
        _mouseExitActions.Add((condition, action));
    }

    public virtual bool HitTest(Vector4 location)
    {
        return Layout.Contains(location);
    }

    public void Hide()
    {
        IsVisible = false;
    }

    public void Show()
    {
        IsVisible = true;
    }
}