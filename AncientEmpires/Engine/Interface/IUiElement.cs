using System.Numerics;
using AncientEmpires.Engine.Layout;
using AncientEmpires.Engine.Render.Groups;

namespace AncientEmpires.Engine.Interface;

public interface IUiElement
{
    IEnumerable<IUiElement> UiChildren { get; }
    bool IsMouseOver { get; set; }
    bool IsVisible { get; set; }
    IUiElement UiParent { get; }
    float ZIndex { get; }
    GameLayout Layout { get; set; }
    MeshFragment AddFragment(MeshFragment fragment);
    void LeftClick(GameEngine engine, InputState inputState);
    void LeftDown(GameEngine engine, InputState inputState);
    void RightDown(GameEngine engine, InputState inputState);
    void RightClick(GameEngine engine, InputState inputState);
    void MouseOver(GameEngine engine, InputState inputState);
    void MouseExit(GameEngine engine, InputState inputState);
    IUiElement ChangeLayout(GameLayout layout, IUiElement parent);
    void WhenLeftClicked(Func<GameEngine, InputState, bool> condition, Action<GameEngine, InputState> action);
    void WhenLeftDown(Func<GameEngine, InputState, bool> condition, Action<GameEngine, InputState> action);
    void WhenRightClicked(Func<GameEngine, InputState, bool> condition, Action<GameEngine, InputState> action);
    void WhenRightDown(Func<GameEngine, InputState, bool> condition, Action<GameEngine, InputState> action);
    void WhenMouseOver(Func<GameEngine, InputState, bool> condition, Action<GameEngine, InputState> action);
    void WhenMouseExit(Func<GameEngine, InputState, bool> condition, Action<GameEngine, InputState> action);
    bool HitTest(Vector4 location);
    public void Hide();
    void Show();
}