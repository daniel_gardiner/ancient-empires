using System.Numerics;
using AncientEmpires.Engine.Layout;
using AncientEmpires.Engine.Render;
using AncientEmpires.Engine.Render.Interface;
using AncientEmpires.Engine.Render.Quads;
using AncientEmpires.Engine.Render.Text;
using MoreLinq.Extensions;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Interface;

public abstract class GameInterfacePanel : UiElement
{
    protected GameInterfacePanel(GameEngine engine, IUiElement parent, GameLayout layout) : base(engine, parent, layout)
    {
    }

    protected GameInterfacePanel(GameEngine engine, IUiElement parent, string name, GameLayout layout) : base(engine, parent, layout)
    {
    }

    public override void OnInitialize()
    {
        base.OnInitialize();

        ZIndex = Engine.Configuration.GetRenderLayer(KnownRenderLayers.Interface) - 0.2f;
        BackgroundColor = new Vector4(0.3f, 0.3f, 0.3f, 0.3f);
        ShaderMaterial = ShaderMaterial.InterfacePanel;
        IsVisible = false;
    }

    protected override void OnCreateFragment()
    {
        Fragment = RegisterComponent<InterfaceMeshFragment>().SetZIndex(ZIndex);
    }

    public override void OnNextFrame(float timeDelta)
    {
        Fragment.IsVisible = IsVisible;
        Fragment.UpdateQuads(new OrthoTextureQuad(
            new Vector4(Layout.OuterBounds.Left, Layout.OuterBounds.Top, ZIndex, 0),
            new Vector4(Layout.OuterBounds.Right - Layout.OuterBounds.Left, Layout.OuterBounds.Bottom - Layout.OuterBounds.Top, ZIndex, 0),
            new Sprite(Vector4.One, Vector4.One, new Color4(.3f, .3f, .3f, .7f)),
            ShaderMaterial));
    }

    public void ClickButton<TButton>(GameEngine engine, InputState state) where TButton : GameButton
    {
        Components.OfType<TButton>().ForEach(o => o.LeftClick(engine, state));
    }

    protected GameButton AddButton(GameButton button)
    {
        return RegisterComponent(button);
    }

    public GameText AddText(TextLayer layer, string text, Vector4 color, GameLayout layout)
    {
        var zIndex = ZIndex - 0.1f;
        return RegisterComponent(new GameText(Engine, this, layer, layout, text, color, zIndex));
    }

    public void ClearText()
    {
        for (var i = Components.Count - 1; i >= 0; i--)
        {
            if (Components[i] is TextMeshFragment)
            {
                RemoveComponent(Components[i]);
            }
        }
    }

    public void ReplaceChild(UiElement element)
    {
        Clear();
        Components.Add(element);
        element.Show();
        element.ChangeLayout(new GameLayout(Engine, Layout, element.Layout), this);
    }

    public void Clear()
    {
        var uiElements = Components.OfType<UiElement>().ToList();
        for (var i = 0; i < uiElements.Count; i++)
        {
            var uiElement = uiElements.ElementAt(i);
            uiElement.Hide();
            Components.Remove(uiElement);
        }
    }
}