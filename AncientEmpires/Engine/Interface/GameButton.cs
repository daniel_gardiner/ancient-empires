using System.Numerics;
using AncientEmpires.Engine.Interface.Overview;
using AncientEmpires.Engine.Layout;
using AncientEmpires.Engine.Render;
using AncientEmpires.Engine.Render.Interface;
using AncientEmpires.Engine.Render.Quads;

namespace AncientEmpires.Engine.Interface;

public abstract class GameButton : UiElement
{
    public Vector4 SmallButtonOffset = new(0, 2050, 0, 0);
    public Vector4 SmallButtonSize = new(45, 40, 0, 0);
    protected static List<int> TextureIndexes;
    public override Vector4 BackgroundColor { get; set; } = new(25 / 256f, 50 / 256f, 60 / 256f, 1);
    public override bool IsVisible => Parent.IsVisible;
    protected abstract bool IsPressed();
    protected Sprite PressedSprite { get; set; }
    public Sprite UnpressedSprite { get; set; }

    public Sprite Sprite => IsPressed()
        ? PressedSprite
        : UnpressedSprite;

    public GameButton(GameEngine engine, UiElement parent, GameLayout layout) : base(engine, parent, layout)
    {
        //WhenLeftDown(Condition.Always, (_, _) => IsPressed() = true);
        WhenLeftClicked(Condition.Always, (_, _) => SoundManager.Play(1));
        //WhenLeftClicked(Condition.Always, (_, _) => Pressed = false);
        //WhenMouseExit(Condition.Always, (_, _) => Pressed = false);
    }

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();

        ShaderMaterial = ShaderMaterial.InterfaceButton;
        TextureIndexes = Engine.GetComponent<GameTextures>().FindTextureIndexes("Interface.dds");
    }

    public override void OnInitialize()
    {
        base.OnInitialize();

        ZIndex = Engine.Configuration.GetRenderLayer(KnownRenderLayers.Interface) - 0.3f;
        ShaderMaterial = ShaderMaterial.InterfaceButton;
    }

    public override void OnNextFrame(float timeDelta)
    {
        Fragment.IsVisible = IsVisible;
        Fragment.UpdateQuads(new OrthoTextureQuad(
            new Vector4(Layout.OuterBounds.Left, Layout.OuterBounds.Top, ZIndex, 0),
            new Vector4(Layout.OuterBounds.Right - Layout.OuterBounds.Left, Layout.OuterBounds.Bottom - Layout.OuterBounds.Top, ZIndex, 0),
            Sprite,
            ShaderMaterial));
    }

    protected override void OnCreateFragment()
    {
        Fragment = RegisterComponent<InterfaceMeshFragment>().SetZIndex(ZIndex);
    }

    protected void SetButtonSprites(ButtonStyle style, int index)
    {
        switch (style)
        {
            case ButtonStyle.Small:
                var buttonOffset = new Vector4(0, SmallButtonSize.Y, 0, 0) * index;
                UnpressedSprite = new Sprite(SmallButtonOffset + buttonOffset, SmallButtonSize, TextureIndexes);
                PressedSprite = new Sprite(SmallButtonOffset + buttonOffset + new Vector4(SmallButtonSize.X, 0, 0, 0), SmallButtonSize, TextureIndexes);
                break;
            case ButtonStyle.Large:
                throw new NotImplementedException();
        }
    }
}