namespace AncientEmpires.Engine.Interface;

public class GameLayoutCache
{
    private readonly Func<RectangleF> _func;
    private RectangleF? _lastValue;
    private RectangleF? _value;
    private bool _isCached;

    public GameLayoutCache(Func<RectangleF> func)
    {
        _func = func;
    }

    public void Clear()
    {
        _value = null;
        _lastValue = null;
        _isCached = false;
    }

    public RectangleF Value => GetValue();

    private RectangleF GetValue()
    {
        if (_isCached && _lastValue != null)
            return _lastValue.Value;

        _lastValue = _value;
        _value = _func();
        
        if(_lastValue != null && _value != null)
            _isCached = _lastValue.Value.Equals(_value.Value);

        return _value.Value;
    }
}