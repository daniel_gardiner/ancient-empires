using AncientEmpires.Engine.Layout;

namespace AncientEmpires.Engine.Interface.ProvincialBuild;

public class ProvincialBuildPanel : GameInterfacePanel
{
    public ProvincialBuildPanel(GameEngine engine, GameInterface parent, GameLayout layout) : base(engine, parent, layout)
    {
    }

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();
            
        AddButton(new BuildFarmButton(Engine, this, new GameLayout(Engine, Layout).Margin(10).Size(60, 60)));
        AddButton(new BuildHouseButton(Engine, this, new GameLayout(Engine, Layout).Margin(10).Size(60, 60).DockTo<BuildFarmButton>(Components, LayoutDirection.Left)));
        AddButton(new BuildGarrisonButton(Engine, this, new GameLayout(Engine, Layout).Absolute(new LayoutOffset(10, 155), new LayoutSize(60, 60))));

        AddButton(new BuildRoadButton(Engine, this, new GameLayout(Engine, Layout).Absolute(new LayoutOffset(10, 245), new LayoutSize(60, 60))));
        AddButton(new BuildWallButton(Engine, this, new GameLayout(Engine, Layout).Absolute(new LayoutOffset(10, 315), new LayoutSize(60, 60))));
        AddButton(new BulldozeButton(Engine, this, new GameLayout(Engine, Layout).Absolute(new LayoutOffset(10, Layout.OuterBounds.Width() - 70), new LayoutSize(60, 60))));
        AddButton(new TestEntity1Button(Engine, this, new GameLayout(Engine, Layout).Absolute(new LayoutOffset(10, Layout.OuterBounds.Width() - 50), new LayoutSize(60, 60))));
    }
}