using AncientEmpires.Engine.Actions;
using AncientEmpires.Engine.Layout;

namespace AncientEmpires.Engine.Interface.ProvincialBuild;

public abstract class SpawnButton : GameButton
{
    protected EntityType EntityType = EntityType.None;

    public SpawnButton(GameEngine engine, UiElement parent, GameLayout layout) : base(engine, parent, layout)
    {
    }

    public override void OnInitialize()
    {
        base.OnInitialize();
            
        WhenLeftClicked(Condition.Always, (engine, state) => engine.Actions.Start<EntitySpawn>(state)?.Configure(EntityType));
    }
        
    protected override bool IsPressed()
    {
        return Engine.CurrentActions.OfType<EntitySpawn>().Any(o => o.EntityType == EntityType);
    }
}