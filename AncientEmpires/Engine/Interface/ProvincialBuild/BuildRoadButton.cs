using AncientEmpires.Engine.Layout;
using AncientEmpires.Engine.Render;

namespace AncientEmpires.Engine.Interface.ProvincialBuild;

public class BuildRoadButton : SpawnButton
{
    public BuildRoadButton(GameEngine engine, UiElement parent, GameLayout layout) : base(engine, parent, layout)
    {
    }

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();
            
        EntityType = EntityType.Road;
        UnpressedSprite = new Sprite(new RectangleF (128, 128, 150, 150), TextureIndexes);
        PressedSprite = new Sprite(new RectangleF(150, 128, 180, 150), TextureIndexes);
    }
}