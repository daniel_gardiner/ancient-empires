using AncientEmpires.Engine.Layout;
using AncientEmpires.Engine.Render;

namespace AncientEmpires.Engine.Interface.ProvincialBuild;

public class BuildWallButton : SpawnButton
{
    public BuildWallButton(GameEngine engine, UiElement parent, GameLayout layout) : base(engine, parent, layout)
    {
    }

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();
            
        EntityType = EntityType.Wall;
        UnpressedSprite = new Sprite(new RectangleF (128, 240, 150, 270), TextureIndexes);
        PressedSprite = new Sprite(new RectangleF(150, 240, 180, 270), TextureIndexes);
    }
}