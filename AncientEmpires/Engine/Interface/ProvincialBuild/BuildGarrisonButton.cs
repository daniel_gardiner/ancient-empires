using AncientEmpires.Engine.Layout;
using AncientEmpires.Engine.Render;

namespace AncientEmpires.Engine.Interface.ProvincialBuild;

public class BuildGarrisonButton : SpawnButton
{
    public BuildGarrisonButton(GameEngine engine, UiElement parent, GameLayout layout) : base(engine, parent, layout)
    {
    }

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();
            
        EntityType = EntityType.Garrison;
        UnpressedSprite = new Sprite(new RectangleF (128, 210, 30, 30), TextureIndexes);
        PressedSprite = new Sprite(new RectangleF(150, 210, 30, 30), TextureIndexes);
    }
}