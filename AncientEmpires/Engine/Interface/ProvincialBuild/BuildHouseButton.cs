using AncientEmpires.Engine.Layout;
using AncientEmpires.Engine.Render;

namespace AncientEmpires.Engine.Interface.ProvincialBuild;

public class BuildHouseButton : SpawnButton
{
    public BuildHouseButton(GameEngine engine, UiElement parent, GameLayout layout) : base(engine, parent, layout)
    {
    }

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();
            
        EntityType = EntityType.House;
        UnpressedSprite = new Sprite(new RectangleF (128, 180, 150, 210), TextureIndexes);
        PressedSprite = new Sprite(new RectangleF(150, 180, 180, 210), TextureIndexes);
    }
}