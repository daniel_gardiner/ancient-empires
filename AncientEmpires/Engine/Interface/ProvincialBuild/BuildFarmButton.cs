using AncientEmpires.Engine.Interface.Overview;
using AncientEmpires.Engine.Layout;
using static AncientEmpires.EntityType;

namespace AncientEmpires.Engine.Interface.ProvincialBuild;

public class BuildFarmButton : SpawnButton
{
    public BuildFarmButton(GameEngine engine, UiElement parent, GameLayout layout) : base(engine, parent, layout)
    {
    }

    public override void OnInitialize()
    {
        base.OnInitialize();
            
        EntityType = Farm;
        SetButtonSprites(ButtonStyle.Small, 3);            
    }
}