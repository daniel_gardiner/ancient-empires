using AncientEmpires.Engine.Layout;
using AncientEmpires.Engine.Render;

namespace AncientEmpires.Engine.Interface.ProvincialBuild;

public class BulldozeButton : SpawnButton
{
    public BulldozeButton(GameEngine engine, UiElement parent, GameLayout layout) : base(engine, parent, layout)
    {
    }

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();
            
        EntityType = EntityType.Bulldozer;
        UnpressedSprite = new Sprite(new RectangleF (128, 150, 150, 180), TextureIndexes);
        PressedSprite = new Sprite(new RectangleF(150, 150, 180, 180), TextureIndexes);
    }
}