using System.Numerics;
using System.Text;
using AncientEmpires.Engine.Layout;
using AncientEmpires.Engine.Render.Interface;

namespace AncientEmpires.Engine.Interface;

public class EntityInformationPanel : GameInterfacePanel
{
    protected ComponentLookup<GameInterface> GameInterface;
    protected ComponentLookup<EntityManager> EntityManager;
    protected ComponentLookup<InterfaceTextLayer> InterfaceTextRenderer;
    private GameText _item1;

    public EntityInformationPanel(GameEngine engine, GameInterface parent, GameLayout layout) : base(engine, parent, layout)
    {
    }

    public override void OnCreateComponents()
    {
    }

    public override void OnInitialize()
    {
        base.OnInitialize();

        GameInterface = new ComponentLookup<GameInterface>();
        EntityManager = new ComponentLookup<EntityManager>();
        InterfaceTextRenderer = new ComponentLookup<InterfaceTextLayer>();
    }

    public override void OnEngineReady()
    {
        base.OnEngineReady();
        
        _item1 = AddText(InterfaceTextRenderer.Value, "", new Vector4(1, 0, 0, 1), GameLayout.Create().Margin(5, 5));
    }

    public override void OnNextFrame(float timeDelta)
    {
        base.OnNextFrame(timeDelta);

        var selectedUnits = EntityManager.Value.SelectedUnits;
        var builder = new StringBuilder();
        
        foreach (var unit in selectedUnits) 
            builder.Append($"Selected {unit.Name}");

        _item1.UpdateText(builder.ToString());
    }

    /*
    var bitmap = Engine.Textures[Texture.MiniMap];
    var destRect = VisibleBounds;
    var srcRect = new RectangleF(0, 0, 81, 81);
    Engine.D2DContext.DrawBitmap(bitmap, destRect, 1f, InterpolationMode.NearestNeighbor, srcRect, null);
    Engine.D2DContext.DRectangleF(VisibleBounds, Engine.GetColor(Color.Blue), 1);

    var viewport = GameContext.Instance.Engine.Viewport;
    var buildings = viewport.VisibleTiles.GetEntities().OfType<Building>().ToList();
    foreach (var building in buildings)
    {
        var scaled = (
            x: building.Position.X / viewport.MapSize.X * VisibleBounds.Width(),
            y: building.Position.Y / viewport.MapSize.Y * VisibleBounds.Height());

        switch (building.Type)
        {
            case EntityType.Road:
                Engine.D2DContext.FillRectangle(
                    new RectangleF(scaled.x, scaled.y, scaled.x + 1, scaled.y + 1).Offset(VisibleBounds),
                    Engine.GetColor(Color.LightGray));
                break;

            case EntityType.Wall:
                Engine.D2DContext.FillRectangle(
                    new RectangleF(scaled.x, scaled.y, scaled.x + 1, scaled.y + 1).Offset(VisibleBounds),
                    Engine.GetColor(Color.Brown));
                break;

            default:
                Engine.D2DContext.FillRectangle(
                    new RectangleF(scaled.x, scaled.y, scaled.x + 2, scaled.y + 2).Offset(VisibleBounds),
                    Engine.GetColor(Color.Black));
                break;
        }
    }
*/
}