﻿using System.Numerics;
using AncientEmpires.Engine.Layout;
using AncientEmpires.Engine.Map.Picker;
using AncientEmpires.Engine.Render.Interface;
using AncientEmpires.Engine.Render.Quads;
using Humanizer;

namespace AncientEmpires.Engine.Interface;

public class DebugPanel : ContainerPanel
{
    private readonly TimeSpan _textUpdateDuration = 100.Milliseconds();
    private GameText _cursorTileText;
    private GameText _pickerPositionText;
    private GameText _cursorLocationText;
    private GameText _worldPositionLocation;
    private GameText _currentSlice;
    private GameText _currentFrame;

    public DebugPanel(GameEngine engine, IUiElement parent, GameLayout layout) : base(engine, parent, layout)
    {
    }

    public DebugPanel(GameEngine engine, IUiElement parent, string name, GameLayout layout) : base(engine, parent, name, layout)
    {
    }

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();
    }

    public override void OnInitialize()
    {
        base.OnInitialize();

        ZIndex = UiParent.ZIndex - 0.7f;
        ShaderMaterial = ShaderMaterial.InterfacePanel;
    }

    public override void OnEngineReady()
    {
        base.OnEngineReady();

        var textRenderer = Engine.GetComponent<InterfaceTextLayer>();
        var color = new Vector4(1, 1, 1, 0.8f);
        var textOffset = (x: 10f, y: 10f);
        var lineHeight = 30f;

        _cursorTileText = AddText(textRenderer, "", color, GameLayout.Create().Margin(textOffset.x, textOffset.y));
        _pickerPositionText = AddText(textRenderer, "", color, GameLayout.Create().Margin(textOffset.x, textOffset.y + lineHeight * 1));
        _cursorLocationText = AddText(textRenderer, "", color, GameLayout.Create().Margin(textOffset.x, textOffset.y + lineHeight * 2));
        _worldPositionLocation = AddText(textRenderer, "", color, GameLayout.Create().Margin(textOffset.x, textOffset.y + lineHeight * 3));
        _currentSlice = AddText(textRenderer, "", color, GameLayout.Create().Margin(textOffset.x, textOffset.y + lineHeight * 4));
        _currentFrame = AddText(textRenderer, "", color, GameLayout.Create().Margin(textOffset.x, textOffset.y + lineHeight * 5));
    }

    public override void OnNextFrame(float timeDelta)
    {
        base.OnNextFrame(timeDelta);

        if (Engine.Timing.HasBeen("debug-panel-update", _textUpdateDuration))
        {
            var mapPicker = Engine.GetComponent<MapPicker>();
            var (_, coordinates) = mapPicker.CursorTile;
            var inputCurrent = Engine.Input.Current;
            var gamePosition = new GamePosition(inputCurrent);
            _cursorTileText.UpdateText($"{coordinates.X} x {coordinates.Y}");
            _pickerPositionText.UpdateText($"{gamePosition.MapSpace.X:N1} x {gamePosition.MapSpace.Y:N1}");
            _cursorLocationText.UpdateText($"{gamePosition.CursorPosition.X:N1} x {gamePosition.CursorPosition.Y:N1}");
            _worldPositionLocation.UpdateText($"{inputCurrent.CursorWorldSpace.X:N0} x {inputCurrent.CursorWorldSpace.Y:N1} | {gamePosition.WorldSpace.X:N1} x {gamePosition.WorldSpace.Y:N1}");
            _currentSlice.UpdateText($"{Engine.Timing.Slice} slices");
            _currentFrame.UpdateText($"{Engine.Timing.Frame} frames");
        }
    }
}