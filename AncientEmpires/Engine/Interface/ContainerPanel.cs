using System.Numerics;
using AncientEmpires.Engine.Layout;
using AncientEmpires.Engine.Render.Quads;

namespace AncientEmpires.Engine.Interface;

public class ContainerPanel : GameInterfacePanel
{
    public ContainerPanel(GameEngine engine, IUiElement parent, GameLayout layout) : base(engine, parent, layout)
    {
    }

    public ContainerPanel(GameEngine engine, IUiElement parent, string name, GameLayout layout) : base(engine, parent, name, layout)
    {
    }

    public override void OnInitialize()
    {
        base.OnInitialize();
        ShaderMaterial = ShaderMaterial.Transparent;
    }

    public GameInterfacePanel AddPanel(ContainerPanel panel)
    {
        panel.ChangeLayout(new GameLayout(Engine, Layout, panel.Layout), this);
        return RegisterComponent(panel);
    }

    public override bool HitTest(Vector4 location) => false;
}