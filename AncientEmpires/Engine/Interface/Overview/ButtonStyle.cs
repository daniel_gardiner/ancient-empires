namespace AncientEmpires.Engine.Interface.Overview;

public enum ButtonStyle
{
    Small,
    Large,
}