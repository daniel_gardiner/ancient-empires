using AncientEmpires.Engine.Layout;
using AncientEmpires.Engine.Render;

namespace AncientEmpires.Engine.Interface.Overview;

public class SettingsButton : GameButton
{
    public SettingsButton(GameEngine engine, UiElement parent, GameLayout layout) : base(engine, parent, layout)
    {
    }

    public override void OnInitialize()
    {
        base.OnInitialize();
            
        UnpressedSprite = new Sprite(new RectangleF(0, 0, 30, 30), TextureIndexes);
        PressedSprite = new Sprite(new RectangleF(30, 0, 30, 30), TextureIndexes);
            
        WhenLeftClicked(Condition.Always, (engine, _) => engine.GetComponent<GameInterface>().ShowPanel(Panel.None));
    }
        
    protected override bool IsPressed() => false;
}