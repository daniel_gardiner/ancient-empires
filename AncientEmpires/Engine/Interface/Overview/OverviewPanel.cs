using AncientEmpires.Engine.Layout;
using static AncientEmpires.Engine.Layout.LayoutDirection;

namespace AncientEmpires.Engine.Interface.Overview;

public class OverviewPanel : GameInterfacePanel
{
    public OverviewPanel(GameEngine engine, GameInterface parent, GameLayout layout) : base(engine, parent, layout)
    {
    }

    public override void OnCreateComponents()
    {
        AddButton(new BuildButton(Engine, this, new GameLayout(Engine, Layout).Size(new LayoutSize(50, 50)).Margin(10)));
        AddButton(new SettingsButton(Engine, this, new GameLayout(Engine, Layout).Size(new LayoutSize(50, 50)).Dock(Left, Bottom)));
        AddButton(new OverviewButton(Engine, this, new GameLayout(Engine, Layout).Size(new LayoutSize(50, 50)).Dock(Bottom).DockTo<SettingsButton>(Components, Left, 20)));
    }
}