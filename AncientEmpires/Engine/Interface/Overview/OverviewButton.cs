using AncientEmpires.Engine.Layout;
using AncientEmpires.Engine.Render;

namespace AncientEmpires.Engine.Interface.Overview;

public class OverviewButton : GameButton
{
    public OverviewButton(GameEngine engine,  UiElement parent, GameLayout layout) : base(engine, parent, layout)
    {
    }

    public override void OnInitialize()
    {
        base.OnInitialize();
            
        UnpressedSprite = new Sprite(new RectangleF(0, 90, 30, 38), TextureIndexes);
        PressedSprite = new Sprite(new RectangleF(30, 90, 30, 38), TextureIndexes);
            
        WhenLeftClicked(Condition.Always, (engine, _) => engine.GetComponent<GameInterface>().ShowPanel(Panel.None));
    }

    protected override bool IsPressed()
    {
        return false;
    }
}