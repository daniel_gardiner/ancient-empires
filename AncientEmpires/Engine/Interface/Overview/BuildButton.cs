using AncientEmpires.Engine.Actions;
using AncientEmpires.Engine.Layout;

namespace AncientEmpires.Engine.Interface.Overview;

public class BuildButton : GameButton
{
    public BuildButton(GameEngine engine, UiElement parent, GameLayout layout) : base(engine, parent, layout)
    {
    }

    public override void OnInitialize()
    {
        base.OnInitialize();
            
        SetButtonSprites(ButtonStyle.Small, 1);
        WhenLeftClicked(Condition.Always, (engine, _) => engine.GetComponent<GameInterface>().ShowPanel(Panel.ProvincialBuild));
    }

    protected override bool IsPressed()
    {
        return Engine.CurrentActions.OfType<EntitySpawn>().Any();
    }
}