using System.Numerics;
using AncientEmpires.Engine.Entities.Pathing;
using AncientEmpires.Engine.Entities.Units;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Entities;
using AncientEmpires.Engine.Render.Map;
using AncientEmpires.Engine.Render.Text;

namespace AncientEmpires.Engine;

public class PathDebug : RenderableComponent
{
    private Unit _unit;
    private Stack<GamePosition> _path;
    private List<ScoredTile> _scores;
    private ComponentLookup<OverlayMapLayer> OverlayMapLayer;
    private ComponentLookup<DebugTextLayer> DebugTextLayer;
    private StringCache StringCache;
    public Guid Id => Guid.NewGuid();

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();

        StringCache = AddComponent<StringCache>();
    }

    public override void OnInitialize()
    {
        base.OnInitialize();

        OverlayMapLayer = Engine.ComponentLookup<OverlayMapLayer>();
        DebugTextLayer = Engine.ComponentLookup<DebugTextLayer>();
    }

    public void ShowScores(Unit unit, Stack<GamePosition> path, List<ScoredTile> scores)
    {
        _unit = unit;
        _scores = scores;
        _path = path;

        AddDebugTiles();
    }

    private void AddDebugTiles()
    {
        var mapManager = Engine.GetComponent<MapManager>();
        var scoredTiles = _scores.OrderBy(o => o.Score).Take(5000).ToList();
        var range = GetRange(scoredTiles, out var min, out var max);

        if (Engine.GetComponent<GameDebug>().DebugMode == DebugMode.Full)
        {
            foreach (var score in scoredTiles)
            {
                var colorScoreTile = ColorScoreTile(score, max, min, range);
                var textColor = Vector4.Lerp(Vector4.One, colorScoreTile, 0.5f);

                DebugTextLayer.Value.AddText(StringCache.GetString(score.Score), score.Position, textColor);
                OverlayMapLayer.Value.AddTile(score.Position, colorScoreTile, mapManager.GetTile(score.Position));
            }
        }

        var previousNode = _unit.Position;
        foreach (var node in _path)
        {
            OverlayMapLayer.Value.AddFragment(OverlayMapMeshFragment.Create(new EntityMeshFragment(_unit), node, previousNode));
            previousNode = node;
        }
    }

    private float GetRange(List<ScoredTile> scoredTiles, out int min, out float max)
    {
        var range = scoredTiles.Select(o => _path.Select(path => Math.Abs(DistanceMethods.EuclideanDistance(path.MapSpace, o.Position.MapSpace))).Min()).Max();
        range = Math.Max(range, 4);
        min = 0;
        max = range;
        return range;
    }

    Vector4 ColorScoreTile(ScoredTile score, float max, float min, float range)
    {
        var distance = _path
            .Select(path => Math.Abs(DistanceMethods.EuclideanDistance(score.Position.MapSpace, path.MapSpace)))
            .Min();

        if (distance == 0)
            return new Vector4(0, 0.2f, 0, 0.05f);

        var red = (distance - min) / range;
        var green = 1 - (distance - min) / range;
        return new Vector4(red, green, 0, 1);
    }
}