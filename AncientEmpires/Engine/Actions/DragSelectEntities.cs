using System.Numerics;
using AncientEmpires.Engine.Data;
using AncientEmpires.Engine.Entities;
using AncientEmpires.Engine.Entities.Units;
using AncientEmpires.Engine.Entities.Units.Physics;
using AncientEmpires.Engine.Events;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Map.Picker;
using AncientEmpires.Engine.Map.Rendering;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Actions;

public class DragSelectEntities : GameAction
{
    protected ComponentLookup<MapViewport> MapViewport;
    protected ComponentLookup<EntityLookup> EntityLookup;
    protected ComponentLookup<EntityManager> EntityManager;
    private ComponentLookup<IsometricPositionStrategy> PositionStrategy;

    private const double MinDistanceNormalized = 0.01;
    public override ActionType Type => ActionType.DragSelect;
    public GamePosition Origin = GamePosition.Empty;
    public GamePosition Current = GamePosition.Empty;

    public override void OnInitialize()
    {
        MapViewport = Engine.ComponentLookup<MapViewport>();
        EntityManager = Engine.ComponentLookup<EntityManager>();
        PositionStrategy = Engine.ComponentLookup<IsometricPositionStrategy>();
        EntityLookup = Engine.ComponentLookup<EntityLookup>();
    }

    public override void OnEngineReady()
    {
        base.OnEngineReady();

        MapViewport.Value.WhenLeftDown(ActionCondition.NotRunning<DragSelectEntities, EntitySpawn>(), (e, _) => e.Actions.Start<DragSelectEntities>());
        MapViewport.Value.WhenLeftDown(ActionCondition.All(ActionCondition.Active<DragSelectEntities>(), ActionCondition.LeftDownFor(150)), (e, _) =>
        {
            e.Actions.ChangeState<DragSelectEntities>("Visible");
            //Engine.Actions.Abort<ClickSelectEntity>();
        });
        MapViewport.Value.WhenLeftClicked(ActionCondition.Active<DragSelectEntities>(), (e, _) =>
        {
            if (IsVisible)
            {
                Engine.Actions.Abort<ClickSelectEntity>();
                Engine.Actions.End<DragSelectEntities>();
            }
            else
            {
                Engine.Actions.Abort<DragSelectEntities>();
            }
        });
        MapViewport.Value.WhenMouseOver(ActionCondition.Active<DragSelectEntities>(), (e, state) =>
        {
            if (state.LeftDown)
                return;

            //Engine.Actions.Abort<DragSelectEntities>();
        });
    }

    public override bool CanStart(GameActions actions, InputState state)
    {
        return actions.Active.All(o => o is not EntitySpawn);
    }


    protected override void OnStart(InputState state)
    {
        Origin = new GamePosition(state);
        Current = Origin;
        IsActive = false;
        IsVisible = false;
    }

    public override void OnProcess(InputState state)
    {
        IsActive = true;
        Current = new GamePosition(state);
    }

    public override void OnChangeState(string state)
    {
        base.OnChangeState(state);

        if (state == "Visible")
        {
            IsVisible = true;
        }
    }

    protected override void OnEnd(InputState state)
    {
        if (!IsActive)
        {
            Origin = GamePosition.Empty;
            Current = GamePosition.Empty;
            return;
        }

        SelectEntities();

        IsActive = false;
        Origin = GamePosition.Empty;
        Current = GamePosition.Empty;
    }

    public override void OnAbort()
    {
    }

    private void SelectEntities()
    {
        var worldSpaceBox = CreateWorldSpaceBox();
        var entities = EntityLookup.Value.WorldTree.FindObjects(worldSpaceBox).ToList();
        EntityManager.Value.LastSearchResults = entities;

        //var worldSpaceBox = Engine.Camera.MapSpaceToWorldSpace(mapSpaceBox);
        
        var selectedUnits = new List<Entity>();
        foreach (var unit in entities.OfType<Unit>())
        {
            // TODO: create space independent bounding box
            if (unit.Collision.SelectedBy(BoxCollisionShape.FromBoundingBox(worldSpaceBox)))
            {
                selectedUnits.Add(unit);
            }
        }

        var appendSelection = Engine.Input.Current.KeysDown.Contains(Keys.ShiftKey);
        if (selectedUnits.Any())
        {
            Engine.Events.Enqueue(new SelectEntitiesEvent(selectedUnits, appendSelection));
        }
        else
        {
            if (appendSelection)
                return;

            Engine.Events.Enqueue(new ClearEntitySelection());
        }
    }

    private BoundingBox CreateMapSpaceBox()
    {
        var minX = Math.Min(Origin.MapSpace.X, Current.MapSpace.X) - 1;
        var minY = Math.Min(Origin.MapSpace.Y, Current.MapSpace.Y) - 1;
        var maxX = Math.Max(Origin.MapSpace.X, Current.MapSpace.X) + 1;
        var maxY = Math.Max(Origin.MapSpace.Y, Current.MapSpace.Y) + 1;

        return new BoundingBox(
            new Vector3(minX, minY, 0),
            new Vector3(maxX, maxY, 0));
    }
    
    private BoundingBox CreateWorldSpaceBox()
    {
        var minX = Math.Min(Origin.WorldSpace.X, Current.WorldSpace.X);
        var minY = Math.Min(Origin.WorldSpace.Y, Current.WorldSpace.Y);
        var maxX = Math.Max(Origin.WorldSpace.X, Current.WorldSpace.X);
        var maxY = Math.Max(Origin.WorldSpace.Y, Current.WorldSpace.Y);

        return new BoundingBox(
            new Vector3(minX, minY, 0),
            new Vector3(maxX, maxY, 0));
    }
}