using System.Numerics;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Render.Components;

namespace AncientEmpires.Engine.Actions;

public class DragScroll : GameAction
{
    public override ActionType Type => ActionType.DragScroll;
    public Vector4 Origin { get; set; }
    public Vector4 Current { get; set; }
    public Vector2 Speed { get; set; }


    public override void OnInitialize()
    {
        var mapViewport = Engine.GetComponent<MapViewport>();
        mapViewport?.WhenRightDown(ActionCondition.NotActive<DragScroll>(), (e, state) => e.Actions.Start<DragScroll>(state));
        mapViewport?.WhenLeftClicked(ActionCondition.Active<DragScroll>(), (e, _) => e.Actions.End<DragScroll>());
        mapViewport?.WhenRightClicked(ActionCondition.Active<DragScroll>(), (e, state) =>
        {
            e.Actions.End<DragScroll>();
        });
    }

    protected override void OnStart(InputState state)
    {
        Origin = state.CursorPosition;
        Current = state.CursorPosition;
    }

    public override void OnProcess(InputState state)
    {
        if (Engine.State != GameEngineState.Running )
            return;
            
        var size = Engine.GetComponent<MapManager>().TileSize;
        Current = state.CursorPosition;
        Speed = new Vector2(
            (state.CursorPosition.X - Origin.X) / size.X,
            (state.CursorPosition.Y - Origin.Y) / size.Y);

        /*
        Speed = new Vector2a(
            Math.Abs(Speed.X) * Speed.X,
            Math.Abs(Speed.Y) * Speed.Y);
            */
        
        Engine.Camera.ScrollBy(Speed.X, Speed.Y);
    }

    protected override void OnEnd(InputState state)
    {
        Speed = new Vector2(0, 0);
        Origin = new Vector4(0, 0, 0, 0);
    }

    public override void OnAbort()
    {
            
    }
}