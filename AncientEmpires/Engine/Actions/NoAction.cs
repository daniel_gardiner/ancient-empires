using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Render.Components;

namespace AncientEmpires.Engine.Actions;

public class NoAction : GameAction
{
    public static readonly NoAction Instance = new();

    public override ActionType Type => ActionType.None;


    protected override void OnStart(InputState state)
    {
    }

    protected override void OnEnd(InputState state)
    {
    }

    public override void OnAbort()
    {
            
    }

    public override void OnProcess(InputState state)
    {
    }
}