using AncientEmpires.Engine.Entities.Units;
using AncientEmpires.Engine.Events;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Map.Picker;
using AncientEmpires.Engine.Render.Components;

namespace AncientEmpires.Engine.Actions;

public class ClickSelectEntity : GameAction
{
    protected ComponentLookup<GameMap> Map;
    protected ComponentLookup<MapViewport> MapViewport;
    protected ComponentLookup<EntityPicker> EntityPicker;
    public override ActionType Type => ActionType.ClickSelect;

    public List<Unit> SelectedEntities { get; set; }

    public override void OnInitialize()
    {
        Map = new ComponentLookup<GameMap>();
        MapViewport = new ComponentLookup<MapViewport>();
        EntityPicker = new ComponentLookup<EntityPicker>();

        SelectedEntities = new List<Unit>();
    }

    public override void OnEngineReady()
    {
        base.OnEngineReady();

        MapViewport.Value.WhenLeftDown(ActionCondition.NotActive<ClickSelectEntity>(), (engine, state) => engine.Actions.Start<ClickSelectEntity>());
        MapViewport.Value.WhenLeftClicked(ActionCondition.Active<ClickSelectEntity>(), (engine, state) => SelectEntityAt(state));
        MapViewport.Value.WhenRightClicked(ActionCondition.Active<ClickSelectEntity>(), (engine, _) => engine.Actions.End<ClickSelectEntity>());
    }

    protected override void OnStart(InputState state)
    {
        Engine.Actions.Abort<DragSelectEntities>();
    }

    public override void OnProcess(InputState state)
    {
        foreach (var unit in SelectedEntities.OfType<Unit>())
        {
            unit.RenderSelection(Engine);
        }
    }

    protected override void OnEnd(InputState state)
    {
    }

    private void SelectEntityAt(InputState state)
    {
        if (!IsActive)
            return;

        var gamePosition = new GamePosition(state);
        var entities = EntityPicker.Value.PickEntitiesAt(gamePosition).ToList();
        var appendSelection = Engine.Input.Current.KeysDown.Contains(Keys.ShiftKey);
        
        if (entities.Any())
        {
            Engine.Events.Enqueue(new SelectEntitiesEvent(entities, appendSelection));
        }
        else
        {
            Engine.Events.Enqueue(new ClearEntitySelection());
        }

        Engine.Actions.End<ClickSelectEntity>();
    }

    public override void OnAbort()
    {
    }
}