using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Render.Components;

namespace AncientEmpires.Engine.Actions;

public abstract class GameAction : EngineComponent
{
    public abstract ActionType Type { get; }
    public bool IsActive { get; set; }
    public bool IsAborted { get; set; }
    protected abstract void OnStart(InputState state);
    protected abstract void OnEnd(InputState state);
    public abstract void OnAbort();
    public abstract void OnProcess(InputState state);

    public virtual void OnChangeState(string state)
    {
        
    }

    public virtual void Start(InputState state)
    {
        if (!CanStart(Engine.Actions, state))
            return;

        IsActive = true;
        OnStart(state);
        ConsoleWriter.WriteLineIf(IsActive && Engine.GetComponent<GameDebug>().Enabled, $"[GameAction] -> [{Name}] Starting");
    }

    public virtual void End()
    {
        ConsoleWriter.WriteLineIf(IsActive && Engine.GetComponent<GameDebug>().Enabled, $"[GameAction] -> [{Name}] Ending");
        OnEnd(Engine.Input.Current);
        IsActive = false;
        Engine.Actions.Active.Remove(this);
    }

    public virtual void Abort()
    {
        ConsoleWriter.WriteLineIf(IsActive && Engine.GetComponent<GameDebug>().Enabled, $"[GameAction] -> [{Name}] Aborting");
        IsActive = false;
        IsAborted = true;
        OnAbort();
    }

    public virtual bool CanStart(GameActions actions, InputState state)
    {
        return true;
    }
}