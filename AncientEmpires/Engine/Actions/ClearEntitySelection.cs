using System.Collections.Concurrent;
using AncientEmpires.Engine.Events;
using AncientEmpires.Engine.Interface;

namespace AncientEmpires.Engine.Actions;

public class ClearEntitySelection : GameEvent
{
    public override void OnRun(GameEngine engine, ConcurrentQueue<GameEvent> gameEvents)
    {
        engine.GetComponent<EntityManager>().SelectedUnits.Clear();
        engine.GetComponent<GameInterface>().RightPanel.Clear();
    }
}