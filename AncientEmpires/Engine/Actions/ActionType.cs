namespace AncientEmpires.Engine.Actions;

public enum ActionType
{
    None,
    Spawning,
    EntityControl,
    DragScroll,
    DragSelect,
    ClickSelect
}