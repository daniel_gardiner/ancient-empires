using System.Numerics;
using AncientEmpires.Engine.Entities;
using AncientEmpires.Engine.Events.EntitySpawn;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Map.Picker;
using AncientEmpires.Engine.Render.Components;


namespace AncientEmpires.Engine.Actions;

public class EntitySpawn : GameAction
{
    protected ComponentLookup<MapViewport> MapViewport;
    protected ComponentLookup<MapPicker> MapPicker;
    protected ComponentLookup<EntitySpawner> EntitySpawner;

    protected bool HasFinished;
    public override ActionType Type => ActionType.Spawning;
    public EntityType EntityType { get; set; } = EntityType.None;
    public bool Spawning { get; set; }
    public bool Draggable { get; set; }
    public Vector4 Origin { get; set; }
    public Vector4 Current { get; set; }
    public List<(MapTile tile, bool allowed)> PreviewTiles { get; set; }
    public List<Vector4> SpawnedTiles = new();
    private Vector4 LastSpawnTile;


    public void Configure(EntityType entityType)
    {
        EntityType = entityType;
        Draggable = true;
        Spawning = true;
    }

    public override void OnInitialize()
    {
        MapViewport = new ComponentLookup<MapViewport>();
        MapPicker = new ComponentLookup<MapPicker>();
        EntitySpawner = new ComponentLookup<EntitySpawner
        >();
    }

    public override void OnEngineReady()
    {
        base.OnEngineReady();
        
        MapViewport.Value.WhenLeftClicked(Condition.Spawning, (e, state) => Spawn(state));
        MapViewport.Value.WhenLeftDown(Condition.SpawningDraggable, (e, state) => DragSpawn(state));
        MapViewport.Value.WhenRightClicked(Condition.Spawning, (e, state) => Engine.Actions.End<EntitySpawn>());
    }

    public override bool CanStart(GameActions actions, InputState state)
    {
        return true;
    }

    protected override void OnStart(InputState state)
    {
        var (found, coordinates) = MapPicker.Value.PickFromCursor(state.CursorPosition);
        HasFinished = false;
            
        if (!found)
            return;

        Origin = coordinates;
    }

    public override void OnProcess(InputState state)
    {
        if (HasFinished)
            return;

        if (EntityType == EntityType.None)
            return;

        var (found, coordinates) = Engine.GetComponent<MapPicker>().PickFromCursor(state.CursorPosition);

        if (!found)
            return;

        Current = coordinates;
        Engine.Events.Enqueue(new ShowEntitySpawnPreviewEvent(Origin, Current, EntityType, Draggable));
        HasFinished = true;
    }

    protected override void OnEnd(InputState state)
    {
        EntityType = EntityType.None;
        Draggable = false;
        PreviewTiles = new List<(MapTile tile, bool allowed)>();
        Spawning = false;
        SpawnedTiles.Clear();
    }

    public override void OnAbort()
    {
            
    }

    private void Spawn(InputState state)
    {
        TrySpawn(state);

        if (state.KeysDown.All(o => o != Keys.ShiftKey))
            Engine.Actions.End<EntitySpawn>();
    }

    private void DragSpawn(InputState state)
    {
        TrySpawn(state);
    }

    private bool TrySpawn(InputState state)
    {
        if (EntityType == EntityType.None)
            return false;

        if (!GetCoordinates(state, out var found, out var coordinates))
            return false;

        if (!found)
            return false;

        var entitySpawner = Engine.GetComponent<EntitySpawner>();
        var spawnEntityEvent = new SpawnEntityEvent(EntityType, coordinates);
            
        if (SpawnedTiles.Contains(coordinates))
            return false;
            
        if (!entitySpawner.CheckIfEntityCanSpawn(Entity.Create(EntityType), coordinates))
            return false;

        Engine.Events.Enqueue(spawnEntityEvent);
        SpawnedTiles.Add(coordinates);
        return true;
    }

    private bool GetCoordinates(InputState state, out bool found, out Vector4 coordinates)
    {
        (found, coordinates) = MapPicker.Value.PickFromCursor(state.CursorPosition);

        if (LastSpawnTile == coordinates)
            return false;

        LastSpawnTile = coordinates;
        return true;
    }
}