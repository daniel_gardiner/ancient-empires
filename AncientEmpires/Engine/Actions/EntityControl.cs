using AncientEmpires.Engine.Entities.Units;
using AncientEmpires.Engine.Events;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Map.Picker;
using AncientEmpires.Engine.Render.Components;

namespace AncientEmpires.Engine.Actions;

public class EntityControl : GameAction
{
    protected ComponentLookup<MapViewport> MapViewport;
    public override ActionType Type => ActionType.EntityControl;

    public List<Unit> SelectedEntities { get; set; }

    public void Configure(List<Unit> units)
    {
        SelectedEntities = units;
    }

    public override void OnInitialize()
    {
        SelectedEntities = new List<Unit>();
        MapViewport = new ComponentLookup<MapViewport>();
    }

    public override void OnEngineReady()
    {
        base.OnEngineReady();
        
        MapViewport.Value.WhenRightClicked(ActionCondition.Active<EntityControl>(), FindEntityPath);
    }

    protected override void OnStart(InputState state)
    {
        
    }

    public override void OnProcess(InputState state)
    {
        foreach (var unit in SelectedEntities.OfType<Unit>())
        {
            unit.RenderSelection(Engine);
        }
    }

    protected override void OnEnd(InputState state)
    {
    }

    public override void OnAbort()
    {
    }
        
    private void FindEntityPath(GameEngine engine, InputState state)
    {
        var mapPicker = engine.GetComponent<MapPicker>();
        foreach (var unit in engine.GetComponent<EntityManager>().SelectedUnits)
        {
            var (found, coordinates) = mapPicker.PickPositionFromCursor(state.CursorPosition);
            if (!found) return;
                
            engine.Events.Enqueue(new EntityStopEvent(unit.Id));
            engine.Events.Enqueue(new EntityFindPathEvent(unit.Id, coordinates));
        }
    }
}