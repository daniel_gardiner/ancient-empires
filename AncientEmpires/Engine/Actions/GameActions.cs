using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Render.Components;

namespace AncientEmpires.Engine.Actions;

public class GameActions : EngineComponent
{
    public List<GameAction> Actions = new();
    public List<GameAction> Active = new();

    public TAction GetAction<TAction>() where TAction : GameAction => Actions.OfType<TAction>().First();

    public bool TryGetAction<TAction>(out TAction action) where TAction : GameAction
    {
        action = Actions.OfType<TAction>().FirstOrDefault();
        return action != null;
    }

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();

        Actions.Add(AddComponent<EntityControl>());
        Actions.Add(AddComponent<EntitySpawn>());
        Actions.Add(AddComponent<DragScroll>());
        Actions.Add(AddComponent<DragSelectEntities>());
        Actions.Add(AddComponent<ClickSelectEntity>());
    }

    public override void OnInitialize()
    {
    }

    public TAction Start<TAction>() where TAction : GameAction
    {
        return Start<TAction>(Engine.Input.Current);
    }

    public TAction Start<TAction>(InputState state) where TAction : GameAction
    {
        ConsoleWriter.WriteLine($"Start {typeof(TAction).Name}");
        var action = GetAction<TAction>();

        if (!action.CanStart(Engine.Actions, state))
            return null;

        action.Start(state);
        AddToActive(action);
        return action;
    }

    public void ChangeState<TAction>(string state) where TAction : GameAction
    {
        var action = GetAction<TAction>();
        action.OnChangeState(state);
    }

    private void AddToActive<TAction>(TAction action) where TAction : GameAction
    {
        if (Active.OfType<TAction>().Any())
            return;

        Active.Add(action);
    }

    public void End<TType>() where TType : GameAction
    {
        foreach (var action in Active.OfType<TType>().ToList())
        {
            action.End();
            Active.Remove(action);
        }
    }

    public void Abort<TType>() where TType : GameAction
    {
        foreach (var action in Active.OfType<TType>().ToList())
        {
            action.Abort();
            Active.Remove(action);
        }
    }

    public void ProcessInput(InputState state)
    {
        foreach (var action in Active.ToList())
        {
            action.OnProcess(state);
        }
    }

    public void Clear()
    {
        foreach (var action in Active.ToList())
        {
            action.End();
        }

        Active.Clear();
    }
}