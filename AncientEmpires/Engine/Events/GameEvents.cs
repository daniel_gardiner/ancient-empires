using System.Collections.Concurrent;
using AncientEmpires.Engine.Render.Components;
using App.Metrics.Logging;

namespace AncientEmpires.Engine.Events;

public class GameEvents : EngineComponent
{
    public ConcurrentQueue<GameEvent> Events { get; private set; } = new();

    public override void OnNextFrame(float timeDelta)
    {
        base.OnNextFrame(timeDelta);

        if (!Events.Any())
            return;

        var currentQueue = Events;
        Events = new ConcurrentQueue<GameEvent>();

        while (currentQueue.TryDequeue(out var gameEvent) && Engine.State == GameEngineState.Running)
        {
            ConsoleWriter.WriteLineIf(gameEvent.IsLoggable && gameEvent.LogLevel == LogLevel.Info, $"[GameEvent] -> {gameEvent}");

            gameEvent.Run(Engine, Events);
        }
    }

    public void Enqueue(GameEvent gameEvent)
    {
        Events.Enqueue(gameEvent);
    }
}