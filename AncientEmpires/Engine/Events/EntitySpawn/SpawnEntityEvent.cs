using System.Collections.Concurrent;
using System.Numerics;
using AncientEmpires.Engine.Entities;
using AncientEmpires.Engine.Events.Targets;

namespace AncientEmpires.Engine.Events.EntitySpawn;

public class SpawnEntityEvent : GameEvent
{
    private readonly EntityType _entityType;
    private readonly Vector4 _coordinates;

    public SpawnEntityEvent(EntityType entityType, Vector4 coordinates)
    {
        _entityType = entityType;
        _coordinates = coordinates;
        Target = new NullTarget();
    }

    protected bool Equals(SpawnEntityEvent other) => _entityType == other._entityType && _coordinates.Equals(other._coordinates);

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != GetType()) return false;
        return Equals((SpawnEntityEvent) obj);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            return ((int) _entityType * 397) ^ _coordinates.GetHashCode();
        } 
    }

    public override void OnRun(GameEngine engine, ConcurrentQueue<GameEvent> gameEvents)
    {
        var spawner = engine.GetComponent<EntitySpawner>();
        spawner.Spawn(_coordinates, Entity.Create(_entityType));
    }

    public override string ToString() => $"{base.ToString()} {{{_entityType} @ {_coordinates}}}";
}