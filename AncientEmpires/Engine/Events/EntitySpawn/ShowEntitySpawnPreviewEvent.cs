using System.Collections.Concurrent;
using System.Numerics;
using App.Metrics.Logging;

namespace AncientEmpires.Engine.Events.EntitySpawn;

public class ShowEntitySpawnPreviewEvent : GameEvent
{
    public Vector4 Origin { get; }
    public Vector4 Current { get; }
    public EntityType EntityType { get; }
    public bool Draggable { get; }

    public ShowEntitySpawnPreviewEvent(Vector4 origin, Vector4 current, EntityType entityType, bool draggable)
    {
        IsLoggable = false;
        LogLevel = LogLevel.Trace;
        Origin = origin;
        Current = current;
        EntityType = entityType;
        Draggable = draggable;
    }

    public override void OnRun(GameEngine engine, ConcurrentQueue<GameEvent> gameEvents)
    {
            
    }
}