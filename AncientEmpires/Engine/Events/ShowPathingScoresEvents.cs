using System.Collections.Concurrent;
using AncientEmpires.Engine.Entities.Pathing;
using AncientEmpires.Engine.Entities.Units;
using AncientEmpires.Engine.Events.Targets;

namespace AncientEmpires.Engine.Events;

public class ShowPathingScoresEvents : GameEvent
{
    private readonly Stack<GamePosition> _path;
    private readonly List<ScoredTile> _scores;

    public ShowPathingScoresEvents(int entityId, Stack<GamePosition> path, List<ScoredTile> scores)
    {
        Target = new EntityTarget(entityId);
        _path = path;
        _scores = scores;
    }

    public override void OnRun(GameEngine engine, ConcurrentQueue<GameEvent> gameEvents)
    {
        var target = (EntityTarget)Target;
        var entity = engine.GetComponent<EntityManager>().GetEntity<Unit>(target.EntityId);
        engine.GetComponent<GameDebug>().ShowPathingScores(entity, _path, _scores);
    }
}