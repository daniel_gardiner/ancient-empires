using System.Collections.Concurrent;
using AncientEmpires.Engine.Events.Targets;
using App.Metrics.Logging;

namespace AncientEmpires.Engine.Events;

public abstract class GameEvent : IEquatable<GameEvent>, IGameProperties
{
    public Dictionary<string, object> Properties { get; } = new();
    public LogLevel LogLevel = LogLevel.Info;
    public virtual bool IsLoggable { get; set; } = true;
    public virtual GameTarget Target { get; set; }
    public virtual bool IsComplete { get; set; }
    public string Name => GetType().Name;

    public virtual void Run(GameEngine engine, ConcurrentQueue<GameEvent> gameEvents)
    {
        OnRun(engine, gameEvents);
        IsComplete = true;
    }

    public abstract void OnRun(GameEngine engine, ConcurrentQueue<GameEvent> gameEvents);


    public override string ToString() => !string.IsNullOrWhiteSpace(Target?.ToString()) && Target is not NullTarget
        ? $"[{Name}] targeting <{Target}>"
        : $"[{Name}]";

    private string GetTypeName()
    {
        var type = GetType();
        if (type.IsGenericType || type.IsGenericTypeDefinition)
        {
            var list = type.GenericTypeArguments.Select(o => o.Name).ToList();
            return $"{type.Name.Remove(type.Name.IndexOf('`'))}<{string.Join(", ", list)}>";
        }

        return type.Name;
    }

    public bool Equals(GameEvent other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        if (other.GetType() != GetType()) return false;
        return Equals(Target, other.Target);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != this.GetType()) return false;
        return Equals((GameEvent)obj);
    }

    public override int GetHashCode() =>
        (Target != null
            ? Target.GetHashCode()
            : 0);
}