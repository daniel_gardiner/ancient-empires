using System.Collections.Concurrent;
using AncientEmpires.Engine.Actions;
using AncientEmpires.Engine.Entities;
using AncientEmpires.Engine.Entities.Units;
using AncientEmpires.Engine.Events.Targets;
using AncientEmpires.Engine.Interface;
using Panel = AncientEmpires.Engine.Interface.Panel;

namespace AncientEmpires.Engine.Events;

public class SelectEntitiesEvent : GameEvent
{
    public bool AppendSelection
    {
        get => (bool)Properties[nameof(AppendSelection)];
        set => Properties[nameof(AppendSelection)] = value;
    }

    public SelectEntitiesEvent(List<Entity> entities, bool appendSelection)
    {
        Target = new EntitiesTarget(entities);
        AppendSelection = appendSelection;
    }

    public override void OnRun(GameEngine engine, ConcurrentQueue<GameEvent> gameEvents)
    {
        var entitiesTarget = (EntitiesTarget)Target;
        var entityManager = engine.GetComponent<EntityManager>();
        var entities = entityManager.GetEntities(entitiesTarget.EntityIds);
        var units = entities.OfType<Unit>().ToList();
        entityManager.Select(units, AppendSelection);
        engine.Actions.Start<EntityControl>().Configure(entityManager.SelectedUnits);
        engine.GetComponent<GameInterface>().ShowPanel(Panel.EntityInformation);
    }
}