namespace AncientEmpires.Engine.Events.Targets;

public class EntityTarget : GameTarget
{
    public int EntityId { get; set; }

    public EntityTarget(int entityId)
    {
        EntityId = entityId;
    }

    public override string ToString() => $"{GameContext.Instance.Engine.GetComponent<EntityManager>().GetEntity(EntityId)}";
}