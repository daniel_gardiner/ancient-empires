using AncientEmpires.Engine.Entities;

namespace AncientEmpires.Engine.Events.Targets;

public class EntitiesTarget : GameTarget
{
    public List<int> EntityIds { get; set; }

    public EntitiesTarget(List<Entity> entities)
    {
        EntityIds = entities.Select(o => o.Id).ToList();
    }

    public override string ToString() => $"Entities: {string.Join(", ", EntityIds)}";
}