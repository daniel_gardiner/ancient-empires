using System.Collections.Concurrent;
using System.Diagnostics;
using AncientEmpires.Engine.Entities.Units;
using AncientEmpires.Engine.Events.Targets;

namespace AncientEmpires.Engine.Events;

public class EntityMoveEvent : GameEvent
{
    private readonly Stack<GamePosition> _path;

    public EntityMoveEvent(int entityId, Stack<GamePosition> path)
    {
        Debug.Assert(path != null, "path != null");
            
        Target = new EntityTarget(entityId);
        _path = path;
    }

    public override void OnRun(GameEngine engine, ConcurrentQueue<GameEvent> gameEvents)
    {
        var target = (EntityTarget)Target;
        var entity = engine.GetComponent<EntityManager>().GetEntity<Unit>(target.EntityId);
        entity.Movement.Move(_path);
    }
}