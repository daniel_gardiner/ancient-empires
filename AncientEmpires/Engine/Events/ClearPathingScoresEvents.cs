using System.Collections.Concurrent;

namespace AncientEmpires.Engine.Events;

public class ClearPathingScoresEvents : GameEvent
{
    public override void OnRun(GameEngine engine, ConcurrentQueue<GameEvent> gameEvents)
    {
        engine.GetComponent<GameDebug>().ClearPathingScores();
    }
}