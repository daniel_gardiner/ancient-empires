using System.Collections.Concurrent;
using System.Numerics;
using AncientEmpires.Engine.Entities.Pathing;
using AncientEmpires.Engine.Entities.Units;
using AncientEmpires.Engine.Events.Targets;

namespace AncientEmpires.Engine.Events;

public class EntityFindPathEvent : AsyncGameEvent
{
    private GamePathResult _result;
    public Vector4 Coordinates;

    public EntityFindPathEvent(int entityId, Vector4 coordinates)
    {
        Coordinates = coordinates;
        Target = new EntityTarget(entityId);
    }

    public override void OnRun(GameEngine engine, ConcurrentQueue<GameEvent> gameEvents)
    {
        var target = (EntityTarget)Target;
        var entity = engine.GetComponent<EntityManager>().GetEntity<Unit>(target.EntityId);
        var pathManager = engine.GetComponent<GamePathManager>();
        var solver = pathManager.CreatePathSolver();
        gameEvents.Enqueue(new ClearPathingScoresEvents());
        _result = solver.Solve(entity, entity.Position, Coordinates);
    }

    public override void OnComplete(GameEngine engine, ConcurrentQueue<GameEvent> gameEvents)
    {
        var target = (EntityTarget)Target;
        var entity = engine.GetComponent<EntityManager>().GetEntity<Unit>(target.EntityId);
        gameEvents.Enqueue(new EntityMoveEvent(entity.Id, _result.Path));
        gameEvents.Enqueue(new ShowPathingScoresEvents(entity.Id, _result.Path, _result.Scores));
    }
}