using System.Collections.Concurrent;

namespace AncientEmpires.Engine.Events;

public abstract class AsyncGameEvent : GameEvent
{
    protected Task Task;
    public int RunCount;
    public float StartTime;
    public override bool IsComplete => IsStarted && Task?.IsCompleted != false;
    public bool IsStarted => Task != null;
    public override bool IsLoggable => RunCount < 10 || RunCount % 10 == 0;

    public override void Run(GameEngine engine, ConcurrentQueue<GameEvent> gameEvents)
    {
        if (IsComplete)
        {
            if (Task.IsFaulted)
                OnFailure(engine, gameEvents);
            else
                OnComplete(engine, gameEvents);
            return;
        }

        if (!IsStarted)
        {
            StartTime = engine.Timing.TotalElapsedMs;
            Task = Task.Run(() => OnRun(engine, gameEvents));
        }

        RunCount++;
        gameEvents.Enqueue(this);
    }

    public abstract void OnComplete(GameEngine engine, ConcurrentQueue<GameEvent> gameEvents);

    private void OnFailure(GameEngine engine, ConcurrentQueue<GameEvent> gameEvents)
    {
        ConsoleWriter.WriteLine($"{ToString()} | Failure - {Task.Exception?.InnerExceptions[0].Message}");
    }

    public override string ToString() => RunCount > 1
        ? $"{base.ToString()} ({RunCount} times for {GameContext.Instance.Engine.Timing.TotalElapsedMs - StartTime:N0}ms)"
        : base.ToString();
}