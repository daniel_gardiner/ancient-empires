using System.Collections.Concurrent;

namespace AncientEmpires.Engine.Events;

public class QuitGameEvent : GameEvent
{
    public override void OnRun(GameEngine engine, ConcurrentQueue<GameEvent> gameEvents)
    {
        engine.State = GameEngineState.Quiting;
        GameContext.Instance.Manager.OnQuit();
    }
}