using System.Collections.Concurrent;
using AncientEmpires.Engine.Entities.Units;
using AncientEmpires.Engine.Events.Targets;

namespace AncientEmpires.Engine.Events;

public class EntityStopEvent : GameEvent
{
    public EntityStopEvent(int entityId)
    {
        Target = new EntityTarget(entityId);
    }
        
    public override void OnRun(GameEngine engine, ConcurrentQueue<GameEvent> gameEvents)
    {
        var target = (EntityTarget)Target;
        var entity = engine.GetComponent<EntityManager>().GetEntity<Unit>(target.EntityId);
        entity.Movement.Stop();
    }
}