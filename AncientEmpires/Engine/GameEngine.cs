using System.Diagnostics;
using System.Numerics;
using AncientEmpires.Engine.Actions;
using AncientEmpires.Engine.Entities;
using AncientEmpires.Engine.Entities.Buildings;
using AncientEmpires.Engine.Entities.Pathing;
using AncientEmpires.Engine.Events;
using AncientEmpires.Engine.Input;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Layout;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Map.Picker;
using AncientEmpires.Engine.Map.Rendering;
using AncientEmpires.Engine.Map.Serialization;
using AncientEmpires.Engine.Metrics;
using AncientEmpires.Engine.Render;
using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Groups;
using AncientEmpires.Engine.Render.Renderers;
using AncientEmpires.Engine.Serialization;
using App.Metrics;
using Humanizer;
using static AncientEmpires.Engine.GameMode;

namespace AncientEmpires.Engine;

public class GameEngine : EngineComponent, ISerializable<GameEngine, DehydratedGame>
{
    static GameEngine()
    {
        _seed = Environment.TickCount;
    }

    private static readonly int _seed;
    public ISerializer<GameEngine, DehydratedGame> Serializer { get; }
    public readonly Random Random = new(Environment.TickCount);
    public StatisticType GeneralStats = new();
    public GameConfiguration Configuration;
    public IMetricsRoot MetricsRoot;
    public Camera Camera;
    public GameEvents Events;
    public GameActions Actions;
    public GameInput Input;
    public Timing Timing;
    public GameEngineState State;
    public List<GameAction> CurrentActions => Actions.Active;
    public float GameSpeed => Timing.Multiplier;
    public GameMode GameMode;

    public float ScrollSpeed => 1f;
    public Vector2 ScreenSize;

    protected Dictionary<Type, EngineComponent> ComponentCache = new();
    public bool IsEngineReady;
    protected List<IComponentLookup> ComponentLookups = new();

    public static GameEngine Create(GameManager manager)
    {
        var engine = new GameEngine();
        GameContext.Set(engine, manager);
        engine.InitializeEngine();
        return engine;
    }

    public GameEngine()
    {
        Serializer = new GameEngineSerializer();
        State = GameEngineState.New;
        Engine = this;
    }

    public void InitializeEngine()
    {
        IsEngineReady = false;
        GameMode = Created;
        Register();
        IsEngineReady = true;
    }

    public override void Register()
    {
        GameContext.Instance.Manager.OnGameRegister(this);
        ScreenSize = GameContext.Instance.Manager.Window.GetSurfaceSize();
        TriggerCreateComponents();
        TriggerInitialize();
        TriggerEngineReady();
    }

    public void StartNew()
    {
        var mapManager = GetComponent<MapManager>();
        if (mapManager == null)
            return;

        /*
        mapManager.EntitySpawner.Spawn(new Vector2(101, 102), Entity.Create(this, EntityType.Road));
        var x = 70;
        var y = 70;
        while (x > 0 && y > 0)
        {
            for (var i = x; i >= 0; i--)
                mapManager.EntitySpawner.Spawn(new Vector2(100, 100) - new Vector2(i, y), Entity.Create(this, EntityType.Road));
            for (var j = y; j >= 0; j--)
                mapManager.EntitySpawner.Spawn(new Vector2(100, 100) - new Vector2(x, j), Entity.Create(this, EntityType.Road));
            for (var i = x; i >= 0; i--)
                mapManager.EntitySpawner.Spawn(new Vector2(100, 100) + new Vector2(i, y), Entity.Create(this, EntityType.Road));
            for (var j = y; j >= 0; j--)
                mapManager.EntitySpawner.Spawn(new Vector2(100, 100) + new Vector2(x, j), Entity.Create(this, EntityType.Road));

            for (var i = x; i >= 0; i--)
                mapManager.EntitySpawner.Spawn(new Vector2(100, 100) - new Vector2(i, -y), Entity.Create(this, EntityType.Road));
            for (var j = y; j >= 0; j--)
                mapManager.EntitySpawner.Spawn(new Vector2(100, 100) - new Vector2(x, -j), Entity.Create(this, EntityType.Road));
            for (var i = x; i >= 0; i--)
                mapManager.EntitySpawner.Spawn(new Vector2(100, 100) + new Vector2(i, -y), Entity.Create(this, EntityType.Road));
            for (var j = y; j >= 0; j--)
                mapManager.EntitySpawner.Spawn(new Vector2(100, 100) + new Vector2(x, -j), Entity.Create(this, EntityType.Road));

            y -= 3;
            x -= 3;;
        }
        */

        /*
        for (var j = 0; j < 2; j++)
        for (var i = 0; i < 10; i++)
            mapManager.EntitySpawner.Spawn(new Vector2(100, 100) - new Vector2(-j, i), Entity.Create(this, EntityType.Road));
            */

        /*
        for (var j = 0; j < 2; j++)
        for (var i = 0; i < 10; i++)
            mapManager.EntitySpawner.Spawn(new Vector2(100, 100) - new Vector2(-j, i), Entity.Create(this, EntityType.Road));

        for (var j = 0; j < 2; j++)
        for (var i = 0; i < 10; i++)
            mapManager.EntitySpawner.Spawn(new Vector2(100, 110) - new Vector2(-i, j), Entity.Create(this, EntityType.Road));

        for (var j = 0; j < 2; j++)
        for (var i = 0; i < 10; i++)
            mapManager.EntitySpawner.Spawn(new Vector2(90, 90) - new Vector2(-i, j), Entity.Create(this, EntityType.Road));
            */

        for (var j = 0; j < 1; j++)
        for (var i = 0; i < 1; i++)
            mapManager.EntitySpawner.Spawn(new Vector2(0 + i * 4, 0 + j * 4), Entity.Create(EntityType.RomanLegion));

        for (var j = 0; j < 5; j++)
        for (var i = 0; i < 5; i++)
            mapManager.EntitySpawner.Spawn(new Vector2(90 + i * 4, 100 + j * 4), Entity.Create(EntityType.RomanLegion));

        mapManager.EntitySpawner.Spawn(new Vector4(98, 98, 0, 0), Entity.Create(EntityType.RomanLegion));
        mapManager.EntitySpawner.Spawn(new Vector4(97, 98, 0, 0), Entity.Create(EntityType.RomanLegion));
        mapManager.EntitySpawner.Spawn(new Vector4(96, 98, 0, 0), Entity.Create(EntityType.RomanLegion));
        mapManager.EntitySpawner.Spawn(new Vector4(95, 98, 0, 0), Entity.Create(EntityType.RomanLegion));
        mapManager.EntitySpawner.Spawn(new Vector4(98, 97, 0, 0), Entity.Create(EntityType.RomanLegion));
        mapManager.EntitySpawner.Spawn(new Vector4(97, 97, 0, 0), Entity.Create(EntityType.RomanLegion));
        mapManager.EntitySpawner.Spawn(new Vector4(96, 97, 0, 0), Entity.Create(EntityType.RomanLegion));
        mapManager.EntitySpawner.Spawn(new Vector4(95, 97, 0, 0), Entity.Create(EntityType.RomanLegion));
        mapManager.EntitySpawner.Spawn(new Vector4(88, 88, 0, 0), Entity.Create(EntityType.RomanLegion));
        mapManager.EntitySpawner.Spawn(new Vector4(78, 78, 0, 0), Entity.Create(EntityType.RomanLegion));
        mapManager.EntitySpawner.Spawn(new Vector4(68, 68, 0, 0), Entity.Create(EntityType.RomanLegion));
        mapManager.EntitySpawner.Spawn(new Vector4(58, 58, 0, 0), Entity.Create(EntityType.RomanLegion));


        SpawnRoad(new Vector2(5, 5), PathDirection.NorthSouth);
        SpawnRoad(new Vector2(7, 5), PathDirection.EastWest);
        SpawnRoad(new Vector2(9, 5), PathDirection.SouthEast);
        SpawnRoad(new Vector2(11, 5), PathDirection.SouthWest);
        SpawnRoad(new Vector2(13, 5), PathDirection.NorthWest);
        SpawnRoad(new Vector2(15, 5), PathDirection.NorthEast);
        SpawnRoad(new Vector2(17, 5), PathDirection.NorthEastWest);
        SpawnRoad(new Vector2(19, 5), PathDirection.NorthSouthEast);
        SpawnRoad(new Vector2(21, 5), PathDirection.SouthEastWest);
        SpawnRoad(new Vector2(23, 5), PathDirection.NorthSouthWest);
        SpawnRoad(new Vector2(25, 5), PathDirection.NorthSouthEastWest);

        void SpawnRoad(Vector2 coordinates, PathDirection direction)
        {
            var entity = Entity.Create(EntityType.Road) as Road;
            entity.Direction = direction;
            mapManager.EntitySpawner.Spawn(coordinates, entity);
        }
    }

    public void StartTick()
    {
    }

    public void Load(MapManager mapManager)
    {
        GameMode = Loaded;
        ReplaceComponent(mapManager);
    }

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();

        MetricsRoot = new KnownMetrics(this);
        ComponentCache = new Dictionary<Type, EngineComponent>();
        Timing = AddComponent<Timing>();
        Events = AddComponent<GameEvents>();
        Input = AddComponent<GameInput>();
        Camera = AddComponent<Camera>();
        Configuration = AddComponent<GameConfiguration>();

        AddComponent<InterfaceConstantBuffers>();
        AddComponent<WorldConstantBuffers>();
        AddComponent<UiInput>();
        AddComponent<GameBindings>();
        AddComponent<WorldConstantBuffers>();
        AddComponent<IsometricPositionStrategy>();
        AddComponent<InterfacePositionStrategy>();
        AddComponent<GameDebug>();
        AddComponent<BatchManager>();
        AddComponent<FontManager>();
        AddComponent<GamePathManager>();
        AddComponent<GameTextures>();

        AddComponent<GameMap>();
        AddComponent<MapPicker>();
        AddComponent<EntityPicker>();
        AddComponent<EntityManager>();
        AddComponent<GameInterface>(new GameLayout(Engine, null));
        AddComponent<GameRenderManager>();
        Actions = AddComponent<GameActions>();
    }

    public override void OnInitialize()
    {
        State = GameEngineState.Initializing;
        SoundManager.Init_Sounds();
        Application.DoEvents();
        State = GameEngineState.Running;
        Timing.ChangeMultiplier(1.5f);
    }

    public void ShowFps()
    {
        if (Timing.HasBeen(nameof(ShowFps), 1.Seconds()))
            GameManager.Instance.Window.SetText($@"{Math.Round(Timing.Fps * 10) / 10} fps");
    }

    public void ProcessInput()
    {
        Input.ReadInput();
        GetComponent<GameBindings>().ProcessInput(GameContext.Instance.Engine.Input.Current);
        GetComponent<UiInput>().ProcessInput(GameContext.Instance.Engine.Input.Current);
        Actions.ProcessInput(GameContext.Instance.Engine.Input.Current);
    }

    public void Update(bool hasFocus)
    {
        if (State == GameEngineState.Quiting)
            return;

        Timing.Update();
        if (hasFocus) ProcessInput();
        ShowFps();

        if (Timing.IsNewSlice)
        {
            for (var index = 0; index < Components.Count; index++)
            {
                var component = Components[index];
                component.TriggerNextSlice(Timing.SliceDurationS * Timing.Multiplier);
            }
        }

        if (Timing.IsNewFrame)
        {
            for (var index = 0; index < Components.Count; index++)
            {
                var component = Components[index];
                component.TriggerNextFrame(Timing.FrameDurationS * Timing.Multiplier);
            }
        }
    }

    public void Draw()
    {
        if (State == GameEngineState.Quiting)
            return;

        Engine.GetComponent<GraphicsDevice>().StartDraw();

        foreach (var component in Components.OfType<RenderableComponent>())
        {
            if (!IsVisible)
                continue;

            component.TriggerBeforeDraw();
            component.TriggerDraw();
            component.TriggerAfterDraw();
        }
    }

    public void TogglePause()
    {
        switch (State)
        {
            case GameEngineState.Running:
                State = GameEngineState.Paused;
                break;
            case GameEngineState.Paused:
                State = GameEngineState.Running;
                Timing.Update();
                Timing.Update();
                break;
        }
    }

    public void Pause()
    {
        State = GameEngineState.Paused;
    }

    public void Resume()
    {
        State = GameEngineState.Running;
    }

    public void Quit()
    {
        Events.Enqueue(new QuitGameEvent());
    }

    public DehydratedGame Dehydrate() => Serializer.Dehydrate(this);

    public void Hydrate(GameEngine hydrated, DehydratedGame dehydrated)
    {
        Serializer.Hydrate(hydrated, dehydrated);
    }

    public void Refresh()
    {
        foreach (var vertexBuffer in Engine.GetComponent<BufferPoolManager>().Buffers)
        {
            vertexBuffer.Clear("Refresh requested");
        }

        foreach (var gameLayer in Engine.GetComponent<GameRenderManager>().WorldRenderer.Layers)
        {
            gameLayer.Refresh("Refresh requested");
        }

        foreach (var gameLayer in Engine.GetComponent<GameRenderManager>().InterfaceRenderer.Layers)
        {
            gameLayer.Refresh("Refresh requested");
        }

        Engine.GetComponent<EntityManager>().Refresh();
        Engine.GetComponent<BatchManager>().Refresh();
        Engine.GetComponent<RenderBuffers>().Refresh();
    }

    public void TestBinding()
    {
        foreach (var entity in GetComponent<EntityManager>().Paths.OfType<Road>())
        {
            entity.AlignPath<Road>();
        }
    }

    [DebuggerStepThrough]
    public override TComponent GetComponent<TComponent>()
    {
        if (ComponentCache.TryGetValue(typeof(TComponent), out var cached))
        {
            return (TComponent)cached;
        }

        var component = base.GetComponent<TComponent>();
        ComponentCache[typeof(TComponent)] = component ?? throw new Exception($"Component {typeof(TComponent).Name} not found");
        return component;
    }


    [DebuggerStepThrough]
    public override TComponent TryGetComponent<TComponent>()
    {
        return base.GetComponent<TComponent>();
    }

    public override void RemoveComponent<TComponent>()
    {
        if (ComponentCache.ContainsKey(typeof(TComponent)))
            ComponentCache.Remove(typeof(TComponent));

        base.RemoveComponent<TComponent>();
    }

    public override void RemoveComponent<TComponent>(TComponent component)
    {
        base.RemoveComponent(component);

        // TODO: just clear types that match
        ClearLookups();
    }

    public virtual TComponent PrepareComponent<TComponent>(TComponent component) where TComponent : EngineComponent => component;

    public void ClearLookups()
    {
        foreach (var componentLookup in ComponentLookups)
            componentLookup.Clear();
    }

    public void RegisterComponentLookup(IComponentLookup componentLookup)
    {
        ComponentLookups.Add(componentLookup);
    }

    public ComponentLookup<T> ComponentLookup<T>() where T : EngineComponent
    {
        return new ComponentLookup<T>();
    }

    public override void Dispose()
    {
        Timing.Dispose();
    }
}