namespace AncientEmpires.Engine.Layout;

public struct LayoutRectangle
{
    public LayoutDimension Left;
    public LayoutDimension Top;
    public LayoutDimension Right;
    public LayoutDimension Bottom;
        
    public LayoutRectangle(LayoutDimension left, LayoutDimension top, LayoutDimension right, LayoutDimension bottom)
    {
        Left = left;
        Top = top;
        Right = right;
        Bottom = bottom;
    }
        
    public LayoutRectangle(float left, float top, float right, float bottom, LayoutUnits units)
    {
        Left = new LayoutDimension(left, units);
        Top = new LayoutDimension(top, units);
        Right = new LayoutDimension(right, units);
        Bottom = new LayoutDimension(bottom, units);
    }
}