namespace AncientEmpires.Engine.Layout;

public class LayoutDockTarget
{
    public GameLayout Target { get; }
    public LayoutDirection Direction { get; }
    public LayoutDimension Margin { get; }

    public LayoutDockTarget(GameLayout target, LayoutDirection direction) : this(target, direction, new LayoutDimension(0))
    {
    }

    public LayoutDockTarget(GameLayout target, LayoutDirection direction, LayoutDimension margin)
    {
        Target = target;
        Direction = direction;
        Margin = margin ?? new LayoutDimension(0);
    }
}