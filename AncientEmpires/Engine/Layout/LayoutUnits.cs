namespace AncientEmpires.Engine.Layout;

public enum LayoutUnits
{
    Pixels,
    Percent
}