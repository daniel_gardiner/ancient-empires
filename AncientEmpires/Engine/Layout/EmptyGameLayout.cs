namespace AncientEmpires.Engine.Layout;

public class EmptyGameLayout : GameLayout
{
    protected override RectangleF GetOuterBounds() => new();
    protected override RectangleF GetInnerBounds() => new();
}