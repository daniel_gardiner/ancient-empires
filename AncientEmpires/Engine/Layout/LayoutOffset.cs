namespace AncientEmpires.Engine.Layout;

public class LayoutOffset
{
    public LayoutDimension Left { get; }
    public LayoutDimension Top { get; }
    public static readonly LayoutOffset Empty = new(0, 0);

    public LayoutOffset(float top, float left, LayoutUnits units = LayoutUnits.Pixels)
    {
        Left = new LayoutDimension(left, units);
        Top = new LayoutDimension(top, units);
    }
        
    public LayoutOffset(LayoutDimension top, float left, LayoutUnits units = LayoutUnits.Pixels)
    {
        Left = new LayoutDimension(left, units);
        Top = top;
    }
        
    public LayoutOffset(float top, LayoutDimension left, LayoutUnits units = LayoutUnits.Pixels)
    {
        Left = left;
        Top = new LayoutDimension(top, units);
    }
        
    public LayoutOffset(LayoutDimension top, LayoutDimension left)
    {
        Left = left;
        Top = top;
    }      
        
    public LayoutOffset(LayoutOffset offset)
    {
        Left = offset.Left;
        Top = offset.Top;
    }

    public override string ToString() => $"{Left} x {Top}";
}