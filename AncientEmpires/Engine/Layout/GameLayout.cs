using System.Numerics;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Render.Components;
using Vortice.Mathematics;

namespace AncientEmpires.Engine.Layout;

public class GameLayout
{
    private readonly GameLayoutCache _innerBoundsCache;
    private readonly GameLayoutCache _outerBoundsCache;

    public static GameLayout Empty => new EmptyGameLayout();
    private LayoutSettings _settings;
    private readonly GameLayout _parent;
    private readonly GameEngine _engine;
    public RectangleF InnerBounds => _innerBoundsCache.Value;
    public RectangleF OuterBounds => _outerBoundsCache.Value;

    public GameLayout(GameEngine engine, GameLayout parent)
    {
        InitializeSettings();
        _innerBoundsCache = new GameLayoutCache(GetInnerBounds);
        _outerBoundsCache = new GameLayoutCache(GetOuterBounds);
        _engine = engine;
        _parent = parent;
    }

    public GameLayout(GameEngine engine, GameLayout parent, GameLayout layout)
    {
        _innerBoundsCache = new GameLayoutCache(GetInnerBounds);
        _outerBoundsCache = new GameLayoutCache(GetOuterBounds);
        _engine = engine;
        _parent = parent;
        _settings = layout._settings.Copy();
    }

    public void Refresh()
    {
        _innerBoundsCache.Clear();
        _outerBoundsCache.Clear();
    }

    protected GameLayout()
    {
        InitializeSettings();
    }

    private void InitializeSettings()
    {
        _settings = new LayoutSettings
        {
            Margin = new LayoutRectangle(0, 0, 0, 0, LayoutUnits.Pixels),
            Padding = new LayoutRectangle(0, 0, 0, 0, LayoutUnits.Pixels),
            Size = LayoutSize.Empty,
            Docking = new Int4()
        };
    }

    public virtual GameLayout Dock(params LayoutDirection[] direction)
    {
        _settings.Docking = new Int4(
            direction.Contains(LayoutDirection.Left)
                ? 1
                : 0,
            direction.Contains(LayoutDirection.Top)
                ? 1
                : 0,
            direction.Contains(LayoutDirection.Right)
                ? 1
                : 0,
            direction.Contains(LayoutDirection.Bottom)
                ? 1
                : 0);
        return this;
    }

    public GameLayout DockTo(GameLayout target, LayoutDirection direction, LayoutDimension margin = null)
    {
        _settings.DockTo = new LayoutDockTarget(target, direction, margin);
        UnDockFrom(direction);
        return this;
    }

    public GameLayout DockTo<TElement>(IEnumerable<EngineComponent> components, LayoutDirection direction, LayoutDimension margin = null) where TElement : IUiElement
    {
        var element = components.OfType<TElement>().FirstOrDefault();
        DockTo(element?.Layout, direction, margin);
        return this;
    }

    private void UnDockFrom(LayoutDirection direction)
    {
        _settings.Docking = new Int4(
            _settings.Docking.X == 1 && direction != LayoutDirection.Left
                ? 1
                : 0,
            _settings.Docking.Y == 1 && direction != LayoutDirection.Top
                ? 1
                : 0,
            _settings.Docking.Z == 1 && direction != LayoutDirection.Right
                ? 1
                : 0,
            _settings.Docking.W == 1 && direction != LayoutDirection.Bottom
                ? 1
                : 0);
    }

    public GameLayout Fill()
    {
        return Dock(
            LayoutDirection.Top,
            LayoutDirection.Left,
            LayoutDirection.Right,
            LayoutDirection.Bottom);
    }

    public virtual GameLayout Padding(LayoutDimension padding)
    {
        _settings.Padding = new LayoutRectangle(padding, padding, padding, padding);
        return this;
    }

    public virtual GameLayout Padding(LayoutDimension horizontal, LayoutDimension vertical)
    {
        _settings.Padding = new LayoutRectangle(horizontal, vertical, horizontal, vertical);
        return this;
    }

    public virtual GameLayout Padding(float padding, LayoutUnits units)
    {
        var dimension = new LayoutDimension(padding, units);
        _settings.Padding = new LayoutRectangle(dimension, dimension, dimension, dimension);
        return this;
    }

    public GameLayout Padding(float horizontalPadding, float verticalPadding, LayoutUnits units = LayoutUnits.Pixels)
    {
        var horizontalDimension = new LayoutDimension(horizontalPadding, units);
        var verticalDimension = new LayoutDimension(verticalPadding, units);
        _settings.Padding = new LayoutRectangle(horizontalDimension, verticalDimension, horizontalDimension, verticalDimension);
        return this;
    }

    public virtual GameLayout Margin(float margin, LayoutUnits units = LayoutUnits.Pixels)
    {
        var dimension = new LayoutDimension(margin, units);
        _settings.Margin = new LayoutRectangle(dimension, dimension, dimension, dimension);
        return this;
    }

    public GameLayout Margin(float horizontalMargin, float verticalMargin, LayoutUnits units = LayoutUnits.Pixels)
    {
        var horizontalDimension = new LayoutDimension(horizontalMargin, units);
        var verticalDimension = new LayoutDimension(verticalMargin, units);
        _settings.Margin = new LayoutRectangle(horizontalDimension, verticalDimension, horizontalDimension, verticalDimension);
        return this;
    }

    public virtual GameLayout Absolute(LayoutOffset offset, LayoutSize size)
    {
        _settings.Absolute = true;
        _settings.Offset = offset;
        _settings.Size = size;
        return this;
    }

    public GameLayout Size(float width, float height, LayoutUnits unit = LayoutUnits.Pixels)
    {
        _settings.Size = new LayoutSize(width, height, unit);
        return this;
    }

    public GameLayout Size(LayoutSize size)
    {
        _settings.Size = size;
        return this;
    }

    public GameLayout Width(int width, LayoutUnits units = LayoutUnits.Pixels)
    {
        _settings.Size = new LayoutSize(new LayoutDimension(width, units), _settings.Size.Height);
        return this;
    }

    public GameLayout Height(float height, LayoutUnits units = LayoutUnits.Pixels)
    {
        _settings.Size = new LayoutSize(_settings.Size.Width, new LayoutDimension(height, units));
        return this;
    }

    protected virtual RectangleF GetInnerBounds()
    {
        var outerBounds = GetOuterBounds();
        var offset = new LayoutOffset(outerBounds.Top, outerBounds.Left);
        var size = new LayoutSize(outerBounds.Width(), outerBounds.Height());
        var parentInnerBounds = GetInnerBounds(_parent);
        var parentSize = new LayoutSize(parentInnerBounds.Width(), parentInnerBounds.Height());

        ApplyPadding(parentSize, ref offset, ref size);

        return new RectangleF(
            offset.Left.Value,
            offset.Top.Value,
            size.Width.Value,
            size.Height.Value);
    }

    protected virtual RectangleF GetOuterBounds()
    {
        var parentInnerBounds = GetInnerBounds(_parent);
        var parentSize = new LayoutSize(parentInnerBounds.Width(), parentInnerBounds.Height());

        if (_settings.Size.IsEmpty)
            Fill();

        var offsetLeft = _settings.Offset.Left.ToValue(parentSize.Width);
        var offsetTop = _settings.Offset.Top.ToValue(parentSize.Height);

        var offset = new LayoutOffset(parentInnerBounds.Top + offsetTop, parentInnerBounds.Left + offsetLeft);
        var width = _settings.Size.Width?.ToPixels(parentSize.Width) ?? parentSize.Width;
        var height = _settings.Size.Height?.ToPixels(parentSize.Height) ?? parentSize.Height;
        var size = new LayoutSize(width, height);

        ApplyDocking(parentInnerBounds, ref offset, ref size);
        ApplyDockTo(parentInnerBounds, ref offset, ref size);
        ApplyMargin(parentInnerBounds, parentSize, ref offset, ref size);

        return new RectangleF(
            offset.Left.Value,
            offset.Top.Value,
            size.Width.Value,
            size.Height.Value);
    }

    private RectangleF GetOuterBounds(GameLayout parent)
    {
        return parent is EmptyGameLayout
            ? GetScreenSize()
            : parent?.GetOuterBounds() ?? GetScreenSize();
    }

    private RectangleF GetInnerBounds(GameLayout parent)
    {
        return parent is EmptyGameLayout
            ? GetScreenSize()
            : parent?.GetInnerBounds() ?? GetScreenSize();
    }

    private RectangleF GetScreenSize()
    {
        return new RectangleF(0, 0, _engine.ScreenSize.X, _engine.ScreenSize.Y);
    }

    private void ApplyMargin(RectangleF parentInnerBounds, LayoutSize parentSize, ref LayoutOffset offset, ref LayoutSize size)
    {
        var offsetLeft = parentInnerBounds.Left + _settings.Margin.Left.ToValue(parentSize.Width);
        var offsetTop = parentInnerBounds.Top + _settings.Margin.Top.ToValue(parentSize.Height);
        var offsetRight = parentInnerBounds.Right - _settings.Margin.Right.ToValue(parentSize.Width);
        var offsetBottom = parentInnerBounds.Bottom - _settings.Margin.Bottom.ToValue(parentSize.Height);
        offsetLeft = Math.Max(offset.Left.ToValue(parentSize.Width), offsetLeft);
        offsetTop = Math.Max(offset.Top.ToValue(parentSize.Height), offsetTop);
        offsetRight = Math.Min(offsetLeft + size.Width.ToValue(parentSize.Width), offsetRight);
        offsetBottom = Math.Min(offsetTop + size.Height.ToValue(parentSize.Height), offsetBottom);
        offset = new LayoutOffset(offsetTop, offsetLeft);
        size = new LayoutSize(offsetRight - offsetLeft, offsetBottom - offsetTop);
    }

    private void ApplyPadding(LayoutSize parentSize, ref LayoutOffset offset, ref LayoutSize size)
    {
        var paddingLeft = _settings.Padding.Left.ToPixels(parentSize.Width);
        var paddingRight = _settings.Padding.Right.ToPixels(parentSize.Width);
        var paddingTop = _settings.Padding.Top.ToPixels(parentSize.Height);
        var paddingBottom = _settings.Padding.Bottom.ToPixels(parentSize.Height);
        offset = new LayoutOffset(offset.Top + paddingTop, offset.Left + paddingLeft);
        size = new LayoutSize(size.Width - paddingLeft - paddingRight, size.Height - paddingTop - paddingBottom);
    }

    private void ApplyDocking(RectangleF parentInnerBounds, ref LayoutOffset offset, ref LayoutSize size)
    {
        if (_settings.Docking.X == 1 && _settings.Docking.Z == 1)
        {
            offset = new LayoutOffset(offset.Top, parentInnerBounds.Left);
            size = new LayoutSize(parentInnerBounds.Right - parentInnerBounds.Left, size.Height);
        }
        else if (_settings.Docking.X == 1)
        {
            offset = new LayoutOffset(offset.Top, parentInnerBounds.Left);
        }
        else if (_settings.Docking.Z == 1)
        {
            offset = new LayoutOffset(offset.Top, parentInnerBounds.Right - size.Width.Value);
        }

        if (_settings.Docking.Y == 1 && _settings.Docking.Z == 1)
        {
            offset = new LayoutOffset(parentInnerBounds.Top, offset.Left);
            size = new LayoutSize(size.Width, parentInnerBounds.Bottom - parentInnerBounds.Top);
        }
        else if (_settings.Docking.Y == 1)
        {
            offset = new LayoutOffset(parentInnerBounds.Top, offset.Left);
        }
        else if (_settings.Docking.W == 1)
        {
            offset = new LayoutOffset(parentInnerBounds.Bottom - size.Height.Value, offset.Left);
        }
    }

    private void ApplyDockTo(RectangleF parentInnerBounds, ref LayoutOffset offset, ref LayoutSize size)
    {
        var dockTo = _settings.DockTo;

        if (dockTo == null)
            return;

        var horizontalMargin = dockTo.Margin.ToValue(parentInnerBounds.Width());
        var verticalMargin = dockTo.Margin.ToValue(parentInnerBounds.Height());
        var targetBounds = dockTo.Target.GetOuterBounds();

        switch (dockTo.Direction)
        {
            case LayoutDirection.Left:
                offset = new LayoutOffset(offset.Top, targetBounds.Right + horizontalMargin);
                break;
            case LayoutDirection.Right:
                offset = new LayoutOffset(offset.Top, targetBounds.Left - size.Width.ToValue(parentInnerBounds.Width()) - horizontalMargin);
                break;
            case LayoutDirection.Top:
                offset = new LayoutOffset(targetBounds.Bottom + verticalMargin, offset.Left);
                break;
            case LayoutDirection.Bottom:
                offset = new LayoutOffset(targetBounds.Top - size.Height.ToValue(parentInnerBounds.Height()) + verticalMargin, offset.Left);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private RectangleF GetUnspecifiedBounds()
    {
        return _parent is EmptyGameLayout
            ? GetScreenSize()
            : _parent?.GetOuterBounds() ?? GetScreenSize();
    }

    public static GameLayout Create()
    {
        return new GameLayout(null, null);
    }

    public bool Contains(Vector4 location)
    {
        if (_engine == null)
            return false;

        return OuterBounds.Contains(location.ToVector2());
    }
}