using System.Diagnostics;

namespace AncientEmpires.Engine.Layout;

public class LayoutDimension
{
    public float Value { get; }
    public LayoutUnits Units { get; }
    public bool HasValue => Value > 0;

    public LayoutDimension(float value, LayoutUnits units = LayoutUnits.Pixels)
    {
        Value = value;
        Units = units;
    }

    public float ToValue(float bound)
    {
        return Units == LayoutUnits.Pixels
            ? Value
            : bound * (Value / 100);
    }

    public float ToValue(LayoutDimension bound)
    {
        Debug.Assert(bound.Units != LayoutUnits.Percent, "bound.Units != LayoutUnits.Percent");

        return ToValue(bound.Value);
    }

    public LayoutDimension ToPixels(LayoutDimension bound)
    {
        if (Units == LayoutUnits.Pixels)
            return this;

        return new LayoutDimension(ToValue(bound));
    }

    public override string ToString()
    {
        return Units == LayoutUnits.Pixels
            ? $"{Value}px"
            : $"{Value}%";
    }

    public static LayoutDimension operator +(LayoutDimension a, LayoutDimension b)
    {
        Debug.Assert(a.Units == b.Units, "a.Units == b.Units");
        return new LayoutDimension(a.Value + b.Value, a.Units);
    }
        
    public static LayoutDimension operator -(LayoutDimension a, LayoutDimension b)
    {
        Debug.Assert(a.Units == b.Units, "a.Units == b.Units");
        return new LayoutDimension(a.Value - b.Value, a.Units);
    }
        
    public static implicit operator LayoutDimension(float value)
    {
        return new LayoutDimension(value);
    }
}