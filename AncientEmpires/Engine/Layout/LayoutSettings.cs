using Vortice.Mathematics;

namespace AncientEmpires.Engine.Layout;

public class LayoutSettings
{
    public bool Absolute;
    public LayoutRectangle Padding;
    public LayoutRectangle Margin;
    public Int4 Docking = new(0);
    public LayoutSize Size = LayoutSize.Empty;
    public LayoutOffset Offset = LayoutOffset.Empty;
    public LayoutDimension Width;
    public LayoutDockTarget DockTo;

    public LayoutSettings Copy()
    {
        return new LayoutSettings
        {
            Absolute = Absolute,
            Padding = Padding,
            Margin = Margin,
            Docking = Docking,
            Size = Size,
            Offset = Offset,
            Width = Width,
            DockTo = DockTo
        };
    }
}