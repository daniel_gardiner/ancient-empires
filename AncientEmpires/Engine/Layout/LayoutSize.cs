namespace AncientEmpires.Engine.Layout;

public class LayoutSize
{
    public LayoutDimension Width { get; }
    public LayoutDimension Height { get; }
    public bool IsEmpty => Width?.HasValue != true && Height?.HasValue != true;
    public static readonly LayoutSize Empty = new(null, null);

    public LayoutSize(float width, float height, LayoutUnits units = LayoutUnits.Pixels)
    {
        Width = new LayoutDimension(width, units);
        Height = new LayoutDimension(height, units);
    }
        
    public LayoutSize(float width, LayoutDimension height, LayoutUnits units = LayoutUnits.Pixels)
    {
        Width = new LayoutDimension(width, units);
        Height = height;
    }
        
    public LayoutSize(LayoutDimension width, float height, LayoutUnits units = LayoutUnits.Pixels)
    {
        Width = width;
        Height = new LayoutDimension(height, units);
    }
        
    public LayoutSize(LayoutDimension width, LayoutDimension height)
    {
        Width = width;
        Height = height;
    }      
        
    public LayoutSize(LayoutSize size)
    {
        Width = size.Width;
        Height = size.Height;
    }

    public override string ToString() => $"{Width} x {Height}";
}