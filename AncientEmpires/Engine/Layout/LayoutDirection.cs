namespace AncientEmpires.Engine.Layout;

public enum LayoutDirection
{
    Left,
    Right,
    Top,
    Bottom
}