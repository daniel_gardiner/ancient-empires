using System.Numerics;
using AncientEmpires.Engine.Actions;
using AncientEmpires.Engine.Entities;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Map.Picker;
using AncientEmpires.Engine.Render.Components;

namespace AncientEmpires.Engine;

public class EntitySpawner : EngineComponent
{
    protected MapManager MapManager;
    private EntityManager EntityManager;

    public override void OnInitialize()
    {
        base.OnInitialize();

        MapManager = Engine.GetComponent<MapManager>();
        EntityManager = Engine.GetComponent<EntityManager>();
    }

    public void Spawn(Vector2 cursorPosition, List<EntitySpawn> entitySpawnActions)
    {
        foreach (var entitySpawn in entitySpawnActions)
        {
            Spawn(cursorPosition, entitySpawn);
        }
    }

    public void Spawn(Vector2 cursorPosition, EntitySpawn action)
    {
        var result = Engine.GetComponent<MapPicker>().PickFromCursor(cursorPosition.ToVector4());

        if (!result.found)
            return;

        Spawn(result.coordinates, action.EntityType);
    }

    public Entity Spawn(Vector2 coordinates, Entity entity) => Spawn(new Vector4(coordinates.X, coordinates.Y, 0, 0), entity);

    public Entity Spawn(Vector4 position, Entity entity)
    {
        if (!MapManager.Map.IsInsideMap(position, entity.Size))
        {
            ConsoleWriter.WriteLine($"Entity is outside of the map at ({position.X}, {position.Y}) x ({entity.Size.X}, {entity.Size.Y})");
            return null;
        }

        if (!CheckIfEntityCanSpawn(entity, position))
            return null;

        entity.JumpTo(position);
        MapManager.AddToMap(entity);
        EntityManager.Add(entity);
        return entity;
    }

    public bool CheckIfEntityCanSpawn(Entity entity, GamePosition position)
    {
        if (!entity.CanSpawn(position))
        {
            ConsoleWriter.WriteLine($"Entity cannot spawn at ({position.MapSpace.X}, {position.MapSpace.X})");
            return false;
        }

        return true;
    }

    public bool CheckIfEntityCanSpawn(Entity entity, Vector4 position)
    {
        if (!MapManager.Map.IsInsideMap(position, entity.Size))
        {
            ConsoleWriter.WriteLine($"Entity cannot spawn at ({position.X}, {position.X}) position is outside the map");
            return false;
        }

        if (!entity.CanSpawn(position))
        {
            ConsoleWriter.WriteLine($"Entity cannot spawn at ({position.X}, {position.X})");
            return false;
        }

        return true;
    }

    public Entity Spawn(Vector4 position, EntityType entityType)
    {
        var entity = Entity.Create(entityType);
        Spawn(position, entity);
        return entity;
    }

    public void Despawn(Entity entity)
    {
        MapManager.RemoveFromMap(entity);
    }

    private static IEnumerable<(MapTile tile, bool allowed)> CanSpawnOnTiles(EntityType entityType, MapTile[,] previewTiles)
    {
        foreach (var tile in previewTiles.Flatten())
        {
            ConsoleWriter.WriteLine("Update to use CollisionManager EntitySpawner.CanSpawn");
            yield return (tile, true);
            //yield return (tile, allowed: tile.Entities.All(o => o.AllowsSpawn(entityType)));
        }
    }
}