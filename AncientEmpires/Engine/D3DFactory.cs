using System.Runtime.InteropServices;
using AncientEmpires.Engine.Render;
using AncientEmpires.Engine.Render.Components;
using Vortice.Direct3D11;

namespace AncientEmpires.Engine;

public class D3DFactory : EngineComponent
{
    private static ID3D11Buffer _indexBuffer;
    protected ComponentLookup<GraphicsDevice> GraphicsDevice;

    public override void OnInitialize()
    {
        base.OnInitialize();

        GraphicsDevice = new ComponentLookup<GraphicsDevice>();
    }

    public override void OnReleaseResources()
    {
        base.OnReleaseResources();

        Dispose();
    }

    public virtual ID3D11Buffer CreateVertexBuffer(string name, int capacity)
    {
        var sizeOf = Marshal.SizeOf<Vertex>();
        var description = new BufferDescription
        {
            SizeInBytes = sizeOf * capacity,
            Usage = ResourceUsage.Dynamic,
            BindFlags = BindFlags.VertexBuffer,
            CpuAccessFlags = CpuAccessFlags.Write,
            OptionFlags = ResourceOptionFlags.None,
            StructureByteStride = 0
        };
        var buffer = GraphicsDevice.Value.D3DDevice.CreateBuffer(description).Collect();
        if (buffer != null)
            buffer.DebugName = name;

        return buffer;
    }

    public virtual ID3D11Buffer GetNewIndexBuffer(uint[] indices)
    {
        uint[] quadIndexes =
        {
            0, 1, 2,
            2, 3, 0
        };

        var quads = indices.Length / 6;
        for (var i = 0; i < quads; i++)
        {
            var offset = VerticesPerQuad * i;
            var offsetQuadIndexes = new[]
            {
                (uint)(quadIndexes[0] + offset),
                (uint)(quadIndexes[1] + offset),
                (uint)(quadIndexes[2] + offset),
                (uint)(quadIndexes[3] + offset),
                (uint)(quadIndexes[4] + offset),
                (uint)(quadIndexes[5] + offset),
            };
            Array.Copy(offsetQuadIndexes, 0, indices, i * quadIndexes.Length, quadIndexes.Length);
        }

        var sizeInBytes = Marshal.SizeOf<ulong>() * indices.Length;
        var buffer = GraphicsDevice.Value.D3DDevice.CreateBuffer(new BufferDescription
        {
            SizeInBytes = sizeInBytes,
            Usage = ResourceUsage.Dynamic,
            BindFlags = BindFlags.IndexBuffer,
            CpuAccessFlags = CpuAccessFlags.Write,
            OptionFlags = ResourceOptionFlags.None,
            StructureByteStride = 0
        }).Collect();

        var mapped = GraphicsDevice.Value.D3DContext.Map(buffer, MapMode.WriteDiscard);
        indices.AsSpan().CopyTo(mapped.AsSpan<uint>(sizeInBytes));
        GraphicsDevice.Value.D3DDevice.ImmediateContext1.Unmap(buffer);
        return buffer;
    }

    public ID3D11Buffer GetIndexBuffer(string name, bool fresh = false)
    {
        if (_indexBuffer != null && !fresh)
            return _indexBuffer;

        var maxQuads = 100_000;
        var indexes = new uint[IndexesPerQuad * maxQuads];

        _indexBuffer = GetNewIndexBuffer(indexes);
        if (_indexBuffer != null)
            _indexBuffer.DebugName = name;
        return _indexBuffer;
    }

    public virtual ID3D11Buffer CreateConstantBuffer<T>(string name) where T : struct
    {
        return SetName<T>(name, GraphicsDevice.Value.D3DDevice.CreateBuffer(new BufferDescription
        {
            Usage = ResourceUsage.Dynamic,
            BindFlags = BindFlags.ConstantBuffer,
            SizeInBytes = Marshal.SizeOf(typeof(T)),
            CpuAccessFlags = CpuAccessFlags.Write,
            OptionFlags = ResourceOptionFlags.None,
            StructureByteStride = 0
        }).Collect());
    }

    private ID3D11Buffer SetName<T>(string name, ID3D11Buffer buffer) where T : struct
    {
        buffer.DebugName = name;
        return buffer;
    }

    public virtual ID3D11Device1 GetDevice() => GraphicsDevice.Value.D3DDevice;

    public virtual ID3D11BlendState1 CreateBlendState(BlendDescription1 desc1) => GetDevice().CreateBlendState1(desc1).Collect();

    public override void Dispose()
    {
        base.Dispose();

        GameManager.DisposeCollector.RemoveAndDispose(ref _indexBuffer);
    }
}