using System.Diagnostics;
using AncientEmpires.Engine.Render.Components;

namespace AncientEmpires.Engine;

public class ComponentLookup<TComponent> : IComponentLookup
    where TComponent : EngineComponent
{
    private readonly GameEngine _engine;
    private TComponent _component;

    public ComponentLookup()
    {
        _engine = GameContext.Instance.Engine;
        _engine.RegisterComponentLookup(this);
    }

    public TComponent Value
    {
        [DebuggerStepThrough]
        get { return _component ??= _engine.GetComponent<TComponent>(); }
    }

    public static implicit operator TComponent(ComponentLookup<TComponent> lookup)
    {
        return lookup.Value;
    }

    public void Clear()
    {
        _component = null;
    }
}