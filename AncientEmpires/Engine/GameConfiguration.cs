using System.Numerics;
using AncientEmpires.Engine.Render.Components;

namespace AncientEmpires.Engine;

public class GameConfiguration : EngineComponent
{
    public readonly Dictionary<string, float> RenderLayers = new();
    public Vector4 TilePixelSize;
    public Vector4 TileSize;
    public Vector4 TexelSize;
    public Vector4 TilePadding;
    public Size TileBatchDimensions;
    public float TexturePixelSize;
    public int TileMaskTextureIndex = 7;
    public Size MapSize;


    public GameConfiguration()
    {
        RenderLayers.Add(KnownRenderLayers.Map.ToString(), 7);
        RenderLayers.Add(KnownRenderLayers.MapOverlay.ToString(), 6.9f);
        RenderLayers.Add(KnownRenderLayers.Entities.ToString(), 5);
        RenderLayers.Add(KnownRenderLayers.Interface.ToString(), 2);

        TileSize = new Vector4(1, 1, 0, 0);
        TilePixelSize = new Vector4(128, 64, 0, 0);
        TilePadding = new Vector4(0, 0, 0, 0);
        TexelSize = new Vector4(1f / 4096, 1f / 4096, 0, 0);
        TileBatchDimensions = new Size(40, 40);
        TexturePixelSize = 4096;
        MapSize = new Size(200, 200);
    }

    public float GetRenderLayer(KnownRenderLayers layer)
    {
        if (RenderLayers.ContainsKey(layer.ToString()))
            return RenderLayers[layer.ToString()];

        ConsoleWriter.WriteLine($"Render layer {layer} not found in configuration");

        return 5;
    }
}