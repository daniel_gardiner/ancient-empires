namespace AncientEmpires.Engine;

public class GameMetrics
{
    public int LastTick;
    public long Fragments;
    public int MetricDrawCalls;
    public int MetricVerticesDrawn;
    public int MetricVerticesWritten;
    public int MetricQuadsCreated;
    public int MetricFragmentsCreated;
    public int MetricFragmentsUpdated;
}