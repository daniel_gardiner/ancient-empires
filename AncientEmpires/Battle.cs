﻿/*using System;
using System.Drawing;
using AncientEmpires.Engine;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using static AncientEmpires.Globals;

namespace AncientEmpires
{
    internal static class Battle
    {
        public static void Battle_ChangeFrame()
        {
            /*
            for (var bb = 1; bb <= 2; bb++)
            {
                for (var aa = 1; aa <= GeneralData.NumOfTroops[bb]; aa++)
                {
                    if (UnitList[bb, aa].Moving | UnitList[bb, aa].InBattle)
                    {
                        UnitList[bb, aa].Counter = UnitList[bb, aa].Counter + 1;
                        if (UnitList[bb, aa].Counter == UnitList[bb, aa].Period)
                        {
                            UnitList[bb, aa].Counter = 0;
                            UnitList[bb, aa].Frame = UnitList[bb, aa].Frame + 1;
                        }

                        if (UnitList[bb, aa].Frame == 8)
                            UnitList[bb, aa].Frame = 2;
                    }
                }
            }
        #1#
        }

        public static void AssignPosition(int aa, int bb)
        {
            /*
            if (UnitList[bb, aa].Dead == false)
            {
                XPos = (int) Math.Round(UnitList[bb, aa].wCurX);
                YPos = (int) Math.Round(UnitList[bb, aa].wCurY);
                BattleMap[(int) Math.Round(XPos), (int) Math.Round(YPos)].Team = bb;
                BattleMap[(int) Math.Round(XPos), (int) Math.Round(YPos)].Troop = aa;
            }
        #1#
        }

        public static void Create_BattleMap()
        {
            for (cc = 0; cc <= 74; cc++)
            {
                for (ee = 0; ee <= 74; ee++)
                {
                    BattleMap[cc, ee].Texture = 3; //(int) Math.Round(Conversion.Int(VBMath.Rnd() * 4d) + 1d);
                    BattleMap[cc, ee].Team = 0;
                    BattleMap[cc, ee].Troop = 0;
                }
            }
        }

        public static void Draw_BattleMap()
        {
            for (var aa = 1 + GeneralData.MapX; aa <= 15 + GeneralData.MapX; aa++)
            {
                for (var bb = 1 + GeneralData.MapY; bb <= 11 + GeneralData.MapY; bb++)
                    Draw_BattleTile(aa, bb);
            }

            Draw_BattleUnits();
            if (Drag.Draging)
            {
                if (Drag.StartX - Drag.EndX > 0)
                    NP1 = -1;
                if (Drag.StartX - Drag.EndX <= 0)
                    NP1 = 1;
                if (Drag.StartY - Drag.EndY > 0)
                    NP2 = -1;
                if (Drag.StartY - Drag.EndY <= 0)
                    NP2 = 1;
                for (var aa = Drag.StartX;
                    NP1 >= 0
                        ? aa <= Drag.EndX
                        : aa >= Drag.EndX;
                    aa += NP1)
                {
                    for (var bb = Drag.StartY;
                        NP2 >= 0
                            ? bb <= Drag.EndY
                            : bb >= Drag.EndY;
                        bb += NP2)
                        ShowDragTiles(aa, bb);
                }
            }
        }

        public static void Draw_BattleTile(int aa, int bb)
        {
            XPos = (aa - GeneralData.MapX - 1) * 35 + ExtraX;
            YPos = (bb - GeneralData.MapY - 1) * 35 + ExtraY;
            //GameContext.Instance.Engine.Renderer.SurfaceBack.DrawFast((int) Math.Round(XPos), (int) Math.Round(YPos), GameContext.Instance.Engine.Renderer.SurfaceTextures, Textures[1, BattleMap[aa, bb].Texture], DrawFastFlags.SourceColorKey | DrawFastFlags.Wait);
        }

        public static void Draw_BattleUnits()
        {
            for (var bb = 1; bb <= 2; bb++)
            {
                for (var aa = 1; aa <= GeneralData.NumOfTroops[bb]; aa++)
                {
                    /*
                    if (UnitList[bb, aa].Dead != true)
                    {
                        XPos = (int) Math.Round(UnitList[bb, aa].wCurX);
                        YPos = (int) Math.Round(UnitList[bb, aa].wCurY);
                        if ((XPos >= GeneralData.MapX) & (XPos <= GeneralData.MapX + 15) & (YPos >= GeneralData.MapY) & (YPos <= GeneralData.MapY + 11))
                        {
                            TempX = (int) Math.Round((UnitList[bb, aa].wCurX - GeneralData.MapX) * 35m + ExtraX);
                            TempY = (int) Math.Round((UnitList[bb, aa].wCurY - GeneralData.MapY) * 35m + ExtraY);
                            TempRace = UnitList[bb, aa].Race;
                            TempDirection = UnitList[bb, aa].Direction;
                            TempFrame = UnitList[bb, aa].Frame;
                            //GameContext.Instance.Engine.Renderer.SurfaceBack.DrawFast(TempX, TempY - 15, GameContext.Instance.Engine.Renderer.SurfaceUnits, Globals.Sprite[TempRace, 1, TempDirection, TempFrame], DrawFastFlags.SourceColorKey | DrawFastFlags.Wait);
                        }
                    }
                #1#
                }
            }
        }

        public static void Show_Dead()
        {
        }

        public static void Draw_BattleButtons() // Error
        {
            YPos = OptionButtons[1, 1].Top;
            XPos = OptionButtons[1, 1].Left;
            //GameContext.Instance.Engine.Renderer.SurfaceBack.DrawFast((int) Math.Round(XPos), (int) Math.Round(YPos), GameContext.Instance.Engine.Renderer.SurfaceButtons, OptionButtons[OptionButtons[1, 1].Press, 1].Location, DrawFastFlags.NoColorKey | DrawFastFlags.Wait);
            YPos = OptionButtons[1, 3].Top;
            XPos = OptionButtons[1, 3].Left;
//            GameContext.Instance.Engine.Renderer.SurfaceBack.DrawFast((int) Math.Round(XPos), (int) Math.Round(YPos), GameContext.Instance.Engine.Renderer.SurfaceButtons, OptionButtons[OptionButtons[1, 3].Press, 3].Location, DrawFastFlags.NoColorKey | DrawFastFlags.Wait);
            switch (CurrentOptions)
            {
                case mStandard:
                {
                    break;
                }

                case mGameOptions:
                {
                    for (var aa = 1; aa <= 5; aa++)
                    {
                        YPos = OtherButtons[1, aa].Top;
                        XPos = OtherButtons[1, aa].Left;
                        //GameContext.Instance.Engine.Renderer.SurfaceBack.DrawFast((int) Math.Round(XPos), (int) Math.Round(YPos), GameContext.Instance.Engine.Renderer.SurfaceButtons, OtherButtons[OtherButtons[1, aa].Press, aa].Location, DrawFastFlags.NoColorKey | DrawFastFlags.Wait);
                    }

                    break;
                }

                case mUnitOptions:
                {
                    for (var aa = 6; aa <= 8; aa++)
                    {
                        YPos = OtherButtons[1, aa].Top;
                        XPos = OtherButtons[1, aa].Left;
                        switch (aa)
                        {
                            case 6:
                            {
                                FormationButton();
                                break;
                            }

                            case 7:
                            case 8:
                            {
                                StopOrGroupButton(aa);
                                break;
                            }
                        }

                        // GameContext.Instance.Engine.Renderer.SurfaceBack.DrawFast((int) Math.Round(XPos), (int) Math.Round(YPos), GameContext.Instance.Engine.Renderer.SurfaceButtons, OtherButtons[(int) Math.Round(DumbieVar), aa].Location, DrawFastFlags.NoColorKey | DrawFastFlags.Wait);
                    }

                    break;
                }
            }
        }

        public static void StopOrGroupButton(int aa)
        {
            switch (OtherButtons[1, aa].Press)
            {
                case 2:
                {
                    switch (GameContext.Instance.Engine.Input.Current.LeftClick)
                    {
                        case false:
                        {
                            DumbieVar = 1m;
                            OtherButtons[1, aa].Press = 1;
                            break;
                        }

                        case true:
                        {
                            DumbieVar = 2m;
                            break;
                        }
                    }

                    break;
                }

                case 1:
                {
                    DumbieVar = 1m;
                    OtherButtons[1, aa].Press = 1;
                    break;
                }
            }
        }

        public static void FormationButton()
        {
            switch (GeneralData.GroupSelected)
            {
                case var @case when @case != 0:
                {
                    switch (UnitGroup[GeneralData.GroupSelected].Formation)
                    {
                        case true:
                        {
                            DumbieVar = 2m;
                            break;
                        }

                        case false:
                        {
                            DumbieVar = 1m;
                            break;
                        }
                    }

                    break;
                }

                case 0:
                {
                    switch (GeneralData.Formation)
                    {
                        case true:
                        {
                            DumbieVar = 2m;
                            break;
                        }

                        case false:
                        {
                            DumbieVar = 1m;
                            break;
                        }
                    }

                    break;
                }
            }
        }

        public static void AssignDestination()
        {
            if (GeneralData.NumberSelected == 0)
                return;

            if ((MouseX > ExtraX) & (MouseX < 640 - ExtraX))
            {
                if ((MouseY > ExtraY) & (MouseY < 378))
                {
                    if ((int) Math.Round((MouseY - ExtraY) / (decimal) SquareHeight) + GeneralData.MapY <= 0)
                        return;
                    if ((int) Math.Round((MouseX - ExtraX) / (decimal) SquareWidth) + GeneralData.MapX <= 0)
                        return;
                    switch (GeneralData.GroupSelected)
                    {
                        case var @case when @case != 0:
                        {
                            switch (UnitGroup[GeneralData.GroupSelected].Formation)
                            {
                                case true:
                                {
                                    FormationDestination();
                                    break;
                                }

                                case false:
                                {
                                    StandardDestination();
                                    break;
                                }
                            }

                            break;
                        }

                        case var case1 when case1 == 0:
                        {
                            switch (GeneralData.Formation)
                            {
                                case true:
                                {
                                    FormationDestination();
                                    break;
                                }

                                case false:
                                {
                                    StandardDestination();
                                    break;
                                }
                            }

                            break;
                        }
                    }
                }
            }
        }

        public static void FormationDestination()
        {
            /*
            TempTeam = 1;
            XPos = 0m;
            YPos = 0m;
            for (var aa = 1; aa <= GeneralData.NumberSelected; aa++)
            {
                XPos = XPos + UnitList[TempTeam, GeneralData.TroopSelected[aa]].wCurX;
                YPos = YPos + UnitList[TempTeam, GeneralData.TroopSelected[aa]].wCurY;
            }

            XPos = (int) Math.Round(XPos / GeneralData.NumberSelected);
            YPos = (int) Math.Round(YPos / GeneralData.NumberSelected);
            XDis = (int) Math.Round((int) Math.Round((MouseX - ExtraX) / (decimal) SquareWidth) + GeneralData.MapX - XPos);
            YDis = (int) Math.Round((int) Math.Round((MouseY - ExtraY) / (decimal) SquareHeight) + GeneralData.MapY - YPos);
            for (var aa = 1; aa <= GeneralData.NumberSelected; aa++)
            {
                TempTeam = 1;
                TempTroop = GeneralData.TroopSelected[aa];
                UnitList[TempTeam, TempTroop].DestX = (int) Math.Round(UnitList[TempTeam, TempTroop].wCurX + XDis);
                UnitList[TempTeam, TempTroop].DestY = (int) Math.Round(UnitList[TempTeam, TempTroop].wCurY + YDis);
                if (UnitList[TempTeam, TempTroop].InBattle)
                {
                    DisEngageUnit(aa);
                }

                UnitList[TempTeam, TempTroop].Attacking = false;
                UnitList[TempTeam, TempTroop].InBattle = false;
                UnitList[TempTeam, TempTroop].Moving = true;
            }
        #1#
        }

        public static void StandardDestination()
        {
            /*
            for (var aa = 1; aa <= GeneralData.NumberSelected; aa++)
            {
                UnitList[1, GeneralData.TroopSelected[aa]].DestX = (int) Math.Round((MouseX - ExtraX) / (decimal) SquareWidth) + GeneralData.MapX + (aa - 1);
                UnitList[1, GeneralData.TroopSelected[aa]].DestY = (int) Math.Round((MouseY - ExtraY) / (decimal) SquareHeight) + GeneralData.MapY;
                if (UnitList[1, GeneralData.TroopSelected[aa]].InBattle)
                {
                    DisEngageUnit(aa);
                }

                UnitList[1, GeneralData.TroopSelected[aa]].Attacking = false;
                UnitList[1, GeneralData.TroopSelected[aa]].InBattle = false;
                UnitList[1, GeneralData.TroopSelected[aa]].Moving = true;
            }
        #1#
        }

        public static void Choose_Alternate_Destination(int aa, int bb)
        {
            DumbieVar = (decimal) ((float) 3m) + (decimal) 1m;
            //DumbieVar = (decimal) (Conversion.Int(VBMath.Rnd() * (float) 3m) + (float) 1m);
            switch (DumbieVar)
            {
                case 1m:
                {
                    ChooseNewX(aa, bb);
                    break;
                }

                case 2m:
                {
                    UnitList[bb, aa].DestY = UnitList[bb, aa].DestY - 1;
                    ChooseNewX(aa, bb);
                    break;
                }

                case 3m:
                {
                    UnitList[bb, aa].DestY = UnitList[bb, aa].DestY + 1;
                    ChooseNewX(aa, bb);
                    break;
                }
            }
        }

        public static void ChooseNewX(int aa, int bb)
        {
            DumbieVar = (decimal) ((float) 3m) + (decimal) 1m;
            //DumbieVar = (decimal) (Conversion.Int(VBMath.Rnd() * (float) 3m) + (float) 1m);
            switch (DumbieVar)
            {
                case 2m:
                {
                    UnitList[bb, aa].DestX = UnitList[bb, aa].DestX - 1;
                    break;
                }

                case 3m:
                {
                    UnitList[bb, aa].DestX = UnitList[bb, aa].DestX + 1;
                    break;
                }
            }
        }

        public static void Mouse_Press()
        {
            if (MouseY <= ExtraY + 20)
                return;
            if (MouseX <= ExtraX)
                return;
            if (MouseX >= 640 - ExtraX)
                return;

            if (GameContext.Instance.Engine.Input.Current.LeftClick)
            {
                Drag.MousePressed = true;
                if ((MouseY < 380) & (ShiftDown == false))
                    SelectUnit();
                if ((MouseY < 380) & ShiftDown)
                    MultipleSelectUnit(1);
                if (MouseY > 380)
                    Battle_CheckForButtonClick();
                if ((MouseY > 380) & (MouseX > 400))
                    MiniMapClick();
            }

            if (GameContext.Instance.Engine.Input.Current.LeftClick)
            {
                if (GeneralData.NumberSelected != 0)
                {
                    CheckForMoveOrAttack();
                }
            }
        }

        public static void CheckForMoveOrAttack()
        {
            XPos = (int) Math.Round((MouseX - ExtraX) / (decimal) SquareWidth) + GeneralData.MapX;
            YPos = (int) Math.Round((MouseY - ExtraY) / (decimal) SquareWidth) + GeneralData.MapY;
            switch (BattleMap[(int) Math.Round(XPos), (int) Math.Round(YPos)].Team)
            {
                case 0:
                case 1:
                {
                    AssignDestination();
                    break;
                }

                case 2:
                {
                    AssignTarget();
                    break;
                }
            }
        }

        public static void Movement()
        {
            for (var bb = 1; bb <= 2; bb++)
            {
                for (var aa = 1; aa <= GeneralData.NumOfTroops[bb]; aa++)
                {
                    if (UnitList[bb, aa].Attacking)
                    {
                        Move_Dynamic(aa, bb);
                    }
                }

                for (var aa = 1; aa <= GeneralData.NumOfTroops[bb]; aa++)
                    Movement_Calculations(aa, bb);

                for (var aa = 1; aa <= GeneralData.NumOfTroops[bb]; aa++)
                {
                    if (UnitList[bb, aa].Moving)
                    {
                        Move_Unit(aa, bb);
                    }

                    AssignPosition(aa, bb);
                }
            }
        }

        public static void Movement_Calculations(int aa, int bb)
        {
            if (UnitList[bb, aa].wCurX < UnitList[bb, aa].DestX)
            {
                UnitList[bb, aa].wRight = true;
                UnitList[bb, aa].wLeft = false;
            }

            if (UnitList[bb, aa].wCurX > UnitList[bb, aa].DestX)
            {
                UnitList[bb, aa].wRight = false;
                UnitList[bb, aa].wLeft = true;
            }

            if (UnitList[bb, aa].wCurY < UnitList[bb, aa].DestY)
            {
                UnitList[bb, aa].wDown = true;
                UnitList[bb, aa].wUp = false;
            }

            if (UnitList[bb, aa].wCurY > UnitList[bb, aa].DestY)
            {
                UnitList[bb, aa].wDown = false;
                UnitList[bb, aa].wUp = true;
            }

            MoveCheckForXY(aa, bb);

            // REACHED DESIRED X&Y CORDINATES
            if ((UnitList[bb, aa].wCurX == UnitList[bb, aa].DestX) & (UnitList[bb, aa].wCurY == UnitList[bb, aa].DestY))
            {
                switch (UnitList[bb, aa].ReRouting)
                {
                    case true:
                    {
                        UnitList[bb, aa].DestX = UnitList[bb, aa].TempX;
                        UnitList[bb, aa].DestY = UnitList[bb, aa].TempY;
                        UnitList[bb, aa].ReRouting = false;
                        break;
                    }

                    case false:
                    {
                        UnitList[bb, aa].Moving = false;
                        break;
                    }
                }
            }
        }

        public static void SelectUnit()
        {
            if ((MouseX > ExtraX) & (MouseY > ExtraY))
            {
                XPos = (MouseX - ExtraX) / (decimal) SquareWidth + GeneralData.MapX;
                YPos = (MouseY - ExtraY) / (decimal) SquareHeight + GeneralData.MapY;
                for (var aa = 1; aa <= GeneralData.NumOfTroops[1]; aa++)
                {
                    if (UnitList[1, aa].wCurX * 35m + 35m >= MouseX - ExtraX + GeneralData.MapX * 35)
                    {
                        if (UnitList[1, aa].wCurX * 35m <= MouseX - ExtraX + GeneralData.MapX * 35)
                        {
                            if (UnitList[1, aa].wCurY * 35m + 35m > MouseY - ExtraY + GeneralData.MapY * 35)
                            {
                                if (UnitList[1, aa].wCurY * 35m - 15m < MouseY - ExtraY + GeneralData.MapY * 35)
                                {
                                    GeneralData.TeamSelected[1] = 1;
                                    GeneralData.TroopSelected[1] = aa;
                                    GeneralData.NumberSelected = 1;
                                    GeneralData.GroupSelected = 0;
                                    return;
                                }
                            }
                        }
                    }
                }

                GeneralData.NumberSelected = 0;
                GeneralData.TeamSelected[1] = 0;
                GeneralData.TroopSelected[1] = 0;
                GeneralData.GroupSelected = 0;
            }
        }

        public static void MultipleSelectUnit(int bb)
        {
            if ((MouseX > ExtraX) & (MouseY > ExtraY))
            {
                for (var aa = 1; aa <= GeneralData.NumOfTroops[bb]; aa++)
                {
                    if (UnitList[1, aa].wCurX * 35m + 35m >= MouseX - ExtraX + GeneralData.MapX * 35)
                    {
                        if (UnitList[1, aa].wCurX * 35m <= MouseX - ExtraX + GeneralData.MapX * 35)
                        {
                            if (UnitList[1, aa].wCurY * 35m + 35m > MouseY - ExtraY + GeneralData.MapY * 35)
                            {
                                if (UnitList[1, aa].wCurY * 35m - 15m < MouseY - ExtraY + GeneralData.MapY * 35)
                                {
                                    NextTroop = GeneralData.NumberSelected + 1;
                                    GeneralData.TeamSelected[NextTroop] = 1;
                                    GeneralData.TroopSelected[NextTroop] = aa;
                                    GeneralData.NumberSelected = NextTroop;
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }

        public static void ShowSelected()
        {
            if (GeneralData.TeamSelected[1] == 0)
                return;
            for (var aa = 1; aa <= GeneralData.NumberSelected; aa++)
            {
                XPos = UnitList[GeneralData.TeamSelected[aa], GeneralData.TroopSelected[aa]].wCurX * SquareHeight + 55m - GeneralData.MapX * 35;
                YPos = UnitList[GeneralData.TeamSelected[aa], GeneralData.TroopSelected[aa]].wCurY * SquareHeight + 19m - GeneralData.MapY * 35;
                if ((XPos > ExtraX) & (XPos < 640 - ExtraX) & (YPos > ExtraY + 30) & (YPos < 378m))
                {
                    LengthOfBar = (int) Math.Round(UnitList[1, GeneralData.TroopSelected[aa]].Health / (decimal) UnitList[1, GeneralData.TroopSelected[aa]].MaxHealth * 18m);
                    if (LengthOfBar <= 0)
                        return;
                    Bar = new RectangleF(0, 585, Bar.Left + LengthOfBar, 4);
                    //GameContext.Instance.Engine.Renderer.SurfaceBack.DrawFast((int) Math.Round(XPos + 16m), (int) Math.Round(YPos + 39m), GameContext.Instance.Engine.Renderer.SurfaceUnits, Bar, DrawFastFlags.SourceColorKey | DrawFastFlags.Wait);
                    //GameContext.Instance.Engine.Renderer.SurfaceBack.DrawFast((int) Math.Round(XPos + 7m), (int) Math.Round(YPos - 20m), GameContext.Instance.Engine.Renderer.SurfaceUnits, SelectedRect[1], DrawFastFlags.SourceColorKey | DrawFastFlags.Wait);
                }
            }
        }

        public static void ShowCorrectDirection(int aa, int bb)
        {
            if (UnitList[bb, aa].wLeft)
                UnitList[bb, aa].Direction = mLeft;
            if (UnitList[bb, aa].wRight)
                UnitList[bb, aa].Direction = mRight;
            if (UnitList[bb, aa].wUp)
                UnitList[bb, aa].Direction = mUp;
            if (UnitList[bb, aa].wDown)
                UnitList[bb, aa].Direction = mDown;
        }

        public static void DrawBattleMiniMap()
        {
            XPos = MiniX + GeneralData.MapX + 4;
            YPos = MiniY + GeneralData.MapY + 4;
            //GameContext.Instance.Engine.Renderer.SurfaceBack.DrawFast(MiniX, MiniY, GameContext.Instance.Engine.Renderer.SurfaceMiniMap, MiniMapFrame, DrawFastFlags.SourceColorKey | DrawFastFlags.Wait);
            //GameContext.Instance.Engine.Renderer.SurfaceBack.DrawFast((int) Math.Round(XPos), (int) Math.Round(YPos), GameContext.Instance.Engine.Renderer.SurfaceMiniMap, MiniMapFocusBox[GameContext.Instance.Engine.GamePhase], DrawFastFlags.SourceColorKey | DrawFastFlags.Wait);
            for (var bb = 1; bb <= 2; bb++)
            {
                for (var aa = 1; aa <= GeneralData.NumOfTroops[bb]; aa++)
                {
                    if (UnitList[1, aa].Dead != true)
                    {
                        XPos = MiniX + 4 + UnitList[bb, aa].wCurX;
                        YPos = MiniY + 4 + UnitList[bb, aa].wCurY;
                        //GameContext.Instance.Engine.Renderer.SurfaceBack.DrawFast((int) Math.Round(XPos), (int) Math.Round(YPos), GameContext.Instance.Engine.Renderer.SurfaceMiniMap, MiniMapUnit[bb], DrawFastFlags.SourceColorKey | DrawFastFlags.Wait);
                    }
                }

                // Show Which Units Are Selected
                if ((GeneralData.NumberSelected != 0) & (GeneralData.TeamSelected[1] == 1))
                {
                    for (var aa = 1; aa <= GeneralData.NumberSelected; aa++)
                    {
                        if (UnitList[1, GeneralData.TroopSelected[aa]].Dead != true)
                        {
                            XPos = MiniX + 4 + UnitList[1, GeneralData.TroopSelected[aa]].wCurX;
                            YPos = MiniY + 4 + UnitList[1, GeneralData.TroopSelected[aa]].wCurY;
                            //GameContext.Instance.Engine.Renderer.SurfaceBack.DrawFast((int) Math.Round(XPos), (int) Math.Round(YPos), GameContext.Instance.Engine.Renderer.SurfaceMiniMap, MiniMapUnit[3], DrawFastFlags.SourceColorKey | DrawFastFlags.Wait);
                        }
                    }
                }
            }
        }

        public static void MoveCheckForXY(int aa, int bb)
        {
            if (UnitList[bb, aa].wRight)
            {
                if ((int) Math.Round(UnitList[bb, aa].wCurX - 0.3m) == UnitList[bb, aa].DestX)
                {
                    UnitList[bb, aa].wCurX = UnitList[bb, aa].DestX;
                    UnitList[bb, aa].wRight = false;
                    UnitList[bb, aa].wLeft = false;
                }
            }

            if (UnitList[bb, aa].wLeft)
            {
                if ((int) Math.Round(UnitList[bb, aa].wCurX + 0.3m) == UnitList[bb, aa].DestX)
                {
                    UnitList[bb, aa].wCurX = UnitList[bb, aa].DestX;
                    UnitList[bb, aa].wRight = false;
                    UnitList[bb, aa].wLeft = false;
                }
            }

            if (UnitList[bb, aa].wUp)
            {
                if ((int) Math.Round(UnitList[bb, aa].wCurY + 0.3m) == UnitList[bb, aa].DestY)
                {
                    UnitList[bb, aa].wCurY = UnitList[bb, aa].DestY;
                    UnitList[bb, aa].wDown = false;
                    UnitList[bb, aa].wUp = false;
                }
            }

            if (UnitList[bb, aa].wDown)
            {
                if ((int) Math.Round(UnitList[bb, aa].wCurY - 0.3m) == UnitList[bb, aa].DestY)
                {
                    UnitList[bb, aa].wCurY = UnitList[bb, aa].DestY;
                    UnitList[bb, aa].wDown = false;
                    UnitList[bb, aa].wUp = false;
                }
            }
        }

        public static void CallGroup()
        {
            if (UnitGroup[NumberPressed].NumberInGroup <= 0)
                return;

            for (var aa = 1; aa <= UnitGroup[NumberPressed].NumberInGroup; aa++)
            {
                DumbieVar = UnitGroup[NumberPressed].NumberInGroup;
                GeneralData.TeamSelected[aa] = 1;
                if (UnitList[1, aa].Dead != true)
                {
                    GeneralData.TroopSelected[aa] = UnitGroup[NumberPressed].GroupTroop[aa];
                }
                else
                {
                    DumbieVar = DumbieVar - 1m;
                }
            }

            GeneralData.NumberSelected = (int) Math.Round(DumbieVar);
            GeneralData.GroupSelected = NumberPressed;
        }

        public static void CreateGroup()
        {
            if (GeneralData.NumberSelected == 0)
                return;

            for (var aa = 1; aa <= GeneralData.NumberSelected; aa++)
                UnitGroup[NumberPressed].GroupTroop[aa] = GeneralData.TroopSelected[aa];
            UnitGroup[NumberPressed].NumberInGroup = GeneralData.NumberSelected;
            CallGroup();
        }

        /*
        public static void Check_BattleForScroll()
        {
            if (ScrollLeft & GeneralData.MapX > 0)
                GeneralData.MapX = GeneralData.MapX - 1;
            if (ScrollRight & GeneralData.MapX < GeneralData.MapWidth - 15)
                GeneralData.MapX = GeneralData.MapX + 1;
            if (ScrollUp & GeneralData.MapY > 0)
                GeneralData.MapY = GeneralData.MapY - 1;
            if (ScrollDown & GeneralData.MapY < GeneralData.MapWidth - 11)
                GeneralData.MapY = GeneralData.MapY + 1;
        }
        #1#

        public static void MiniMapClick()
        {
            if ((MouseX > MiniX + 3) & (MouseX < MiniX + 80 - 17))
            {
                if ((MouseY > MiniY + 3) & (MouseY < MiniY + 80 - 13))
                {
                    GeneralData.MapX = MouseX - MiniX - 4;
                    GeneralData.MapY = MouseY - MiniY - 4;
                }
            }
        }

        public static void AssignTarget()
        {
            XPos = (int) Math.Round((MouseX - ExtraX) / (decimal) SquareWidth) + GeneralData.MapX;
            YPos = (int) Math.Round((MouseY - ExtraY) / (decimal) SquareWidth) + GeneralData.MapY;

            for (var aa = 1; aa <= GeneralData.NumberSelected; aa++)
            {
                if ((BattleMap[(int) Math.Round(XPos), (int) Math.Round(YPos)].Team != 0) & (BattleMap[(int) Math.Round(XPos), (int) Math.Round(YPos)].Team != 1))
                {
                    UnitList[GeneralData.TeamSelected[aa], GeneralData.TroopSelected[aa]].Moving = true;
                    UnitList[GeneralData.TeamSelected[aa], GeneralData.TroopSelected[aa]].Attacking = true;
                    UnitList[GeneralData.TeamSelected[aa], GeneralData.TroopSelected[aa]].Target = BattleMap[(int) Math.Round(XPos), (int) Math.Round(YPos)].Troop;
                    UnitList[GeneralData.TeamSelected[aa], GeneralData.TroopSelected[aa]].TeamTarget = BattleMap[(int) Math.Round(XPos), (int) Math.Round(YPos)].Team;
                }
            }
        }

        public static void Move_Dynamic(int aa, int bb)
        {
            if (UnitList[bb, aa].ReRouting != true)
            {
                TempTeam = UnitList[bb, aa].TeamTarget;
                TempTroop = UnitList[bb, aa].Target;
                UnitList[bb, aa].DestX = (int) Math.Round(UnitList[TempTeam, TempTroop].wCurX);
                UnitList[bb, aa].DestY = (int) Math.Round(UnitList[TempTeam, TempTroop].wCurY);
            }
        }

        public static void Move_Unit(int aa, int bb)
        {
            TheSpeed = UnitList[bb, aa].Speed;
            ShowCorrectDirection(aa, bb);
            Battle_DetectCollision(aa, bb);
            if (Collision) // Can't Move Unit, Another Is Blocking Its Path
            {
                CurrentErrors.AddOrUpdate(nameof(UnitCollisionException), o => 1, (exception, count) => ++count);
                return;
            }

            BattleMap[(int) Math.Round(UnitList[bb, aa].wCurX), (int) Math.Round(UnitList[bb, aa].wCurY)].Team = 0;
            BattleMap[(int) Math.Round(UnitList[bb, aa].wCurX), (int) Math.Round(UnitList[bb, aa].wCurY)].Troop = 0;
            if (XCollision == false)
            {
                if (UnitList[bb, aa].wLeft)
                    UnitList[bb, aa].wCurX = UnitList[bb, aa].wCurX - TheSpeed;
                if (UnitList[bb, aa].wRight)
                    UnitList[bb, aa].wCurX = UnitList[bb, aa].wCurX + TheSpeed;
            }

            if (YCollision == false)
            {
                if (UnitList[bb, aa].wUp)
                    UnitList[bb, aa].wCurY = UnitList[bb, aa].wCurY - TheSpeed;
                if (UnitList[bb, aa].wDown)
                    UnitList[bb, aa].wCurY = UnitList[bb, aa].wCurY + TheSpeed;
            }

            AssignPosition(aa, bb);
        }

        public static void Battle_DetectCollision(int aa, int bb)
        {
            int ObjectHit;

            Collision = false;

            // X-COLLISION

            UnitCollision = true;
            XPos = UnitList[bb, aa].wCurX;
            YPos = UnitList[bb, aa].wCurY;
            if (UnitList[bb, aa].wLeft)
                XPos = XPos - TheSpeed;
            if (UnitList[bb, aa].wRight)
                XPos = XPos + TheSpeed;
            if (XPos <= 0m)
                return;
            if (YPos <= 0m)
                return;

            // X - Unit Collison Detection
            switch (BattleMap[(int) Math.Round(XPos), (int) Math.Round(YPos)].Team)
            {
                case 0: // No Unit Collision
                {
                    UnitCollision = false;
                    break;
                }

                case var @case when @case == bb: // Collision With Same Team
                {
                    switch (BattleMap[(int) Math.Round(XPos), (int) Math.Round(YPos)].Troop)
                    {
                        case var case1 when case1 == aa: // Collision With Himself
                        {
                            UnitList[bb, aa].Waiting = 0;
                            UnitCollision = false; // Collision With Another From His Team
                            break;
                        }

                        default:
                        {
                            UnitCollision = true;
                            UnitList[bb, aa].Waiting = UnitList[bb, aa].Waiting + 1;
                            if (UnitList[bb, aa].Waiting == 10)
                            {
                                Alternate_Route(aa, bb);
                                UnitList[bb, aa].Waiting = 0;
                            }

                            break;
                        }
                    }

                    break;
                }
                // Collison With Enemy
                case var case2 when (case2 != bb) & (BattleMap[(int) Math.Round(XPos), (int) Math.Round(YPos)].Team != 0):
                {
                    UnitCollision = true;
                    break;
                }
                // Provincal_AttackCheck
            }

            XCollision = false;
            if (UnitCollision)
            {
                XCollision = true;
            }


            // Y-COLLISION

            UnitCollision = true;
            BuildingCollision = true;
            XPos = UnitList[bb, aa].wCurX;
            YPos = UnitList[bb, aa].wCurY;
            if (UnitList[bb, aa].wUp)
                YPos = YPos - TheSpeed;
            if (UnitList[bb, aa].wDown)
                YPos = YPos + TheSpeed;
            if (XPos <= 0m)
                return;
            if (YPos <= 0m)
                return;

            // Y - Unit Collison Detection

            switch (BattleMap[(int) Math.Round(XPos), (int) Math.Round(YPos)].Team)
            {
                case 0: // No Unit Collision
                {
                    UnitCollision = false;
                    break;
                }

                case var case3 when case3 == bb: // Collision With Same Team
                {
                    switch (BattleMap[(int) Math.Round(XPos), (int) Math.Round(YPos)].Troop)
                    {
                        case var case4 when case4 == aa: // Collision With Himself
                        {
                            UnitList[bb, aa].Waiting = 0;
                            UnitCollision = false; // Collision With Another From His Team
                            break;
                        }

                        default:
                        {
                            UnitCollision = true;
                            UnitList[bb, aa].Waiting = UnitList[bb, aa].Waiting + 1;
                            if (UnitList[bb, aa].Waiting == 10)
                            {
                                Alternate_Route(aa, bb);
                                UnitList[bb, aa].Waiting = 0;
                            }

                            break;
                        }
                    }

                    break;
                }
                // Collison With Enemy
                case var case5 when case5 != (bb & Conversions.ToShort(BattleMap[(int) Math.Round(XPos), (int) Math.Round(YPos)].Team != 0)):
                {
                    UnitCollision = true;
                    break;
                }
                // Provincal_AttackCheck
            }

            YCollision = false;
            if (UnitCollision)
            {
                YCollision = true;
            }

            // X and Y Collision (Special Cases)

            /*
            if (XCollision == false & YCollision == false)
            {
                XPos = Leigion[bb, aa].wCurX;
                YPos = Leigion[bb, aa].wCurY;
                if (Leigion[bb, aa].wLeft)
                    XPos = XPos - TheSpeed;
                if (Leigion[bb, aa].wRight)
                    XPos = XPos + TheSpeed;
                if (Leigion[bb, aa].wUp)
                    YPos = YPos - TheSpeed;
                if (Leigion[bb, aa].wDown)
                    YPos = YPos + TheSpeed;
                switch (ProvincalMap[CurrentProvince, (int) Math.Round(XPos), (int) Math.Round(YPos)].StructureType)
                {
                    case EntityType.Farm:
                    {
                        Collision = true;
                        break;
                    }
                    // Provincal_AlternateRoute
                }
            }
        #1#
        }

        public static void Attack_Phase(int aa, int bb)
        {
            Attack_decimalCheck(aa, bb);
            TempTeam = UnitList[bb, aa].TeamTarget;
            TempTroop = UnitList[bb, aa].Target;
            TempDamage = UnitList[bb, aa].Damage;
            VBMath.Randomize();
            ExtraDamage = (int) Math.Round(Conversion.Int(VBMath.Rnd() * (15 - UnitList[bb, aa].Skill)) + (float) 1m);
            VBMath.Randomize();
            ExtraArmour = (int) Math.Round(Conversion.Int(VBMath.Rnd() * (float) 2m) + (float) 1m);
            DamageDone = TempDamage + ExtraArmour - ExtraDamage - UnitList[TempTeam, TempTroop].Armour;
            if (DamageDone < 0)
                DamageDone = 0;
            TempHealth = UnitList[TempTeam, TempTroop].Health;
            TempHealth = TempHealth - DamageDone;
            UnitList[TempTeam, TempTroop].Health = TempHealth;

            if (TempHealth <= 0)
            {
                KillUnit_EndBattle(bb);
                CheckForVictory();
                return;
            }

            MakeEnemyRetaliate(aa, bb);
        }

        public static void Attack_decimalCheck(int aa, int bb)
        {
            TempTeam = UnitList[bb, aa].TeamTarget;
            TempTroop = UnitList[bb, aa].Target;
            XPos = UnitList[TempTeam, TempTroop].wCurX;
            YPos = UnitList[TempTeam, TempTroop].wCurY;
            XDis = (int) Math.Round(XPos - UnitList[bb, aa].wCurX);
            YDis = (int) Math.Round(YPos - UnitList[bb, aa].wCurY);
            if ((XDis <= -1) | (XPos >= 1m))
            {
                UnitList[TempTeam, TempTroop].InBattle = false;
                UnitList[TempTeam, TempTroop].Attacking = false;
                UnitList[TempTeam, TempTroop].Moving = false;
                UnitList[bb, aa].InBattle = false;
                UnitList[bb, aa].Attacking = false;
                UnitList[bb, aa].Moving = false;
                return;
            }

            if ((YDis <= -1) | (YPos >= 1m))
            {
                UnitList[TempTeam, TempTroop].InBattle = false;
                UnitList[TempTeam, TempTroop].Attacking = false;
                UnitList[TempTeam, TempTroop].Moving = false;
                UnitList[bb, aa].InBattle = false;
                UnitList[bb, aa].Attacking = false;
                UnitList[bb, aa].Moving = false;
            }
        }

        public static void MakeEnemyRetaliate(int aa, int bb)
        {
            UnitList[TempTeam, TempTroop].InBattle = true;
            UnitList[TempTeam, TempTroop].TeamTarget = bb;
            UnitList[TempTeam, TempTroop].Target = aa;
            UnitList[TempTeam, TempTroop].Attacking = true;
        }

        public static void Check_For_Battle()
        {
            for (var bb = 1; bb <= 2; bb++)
            {
                for (var aa = 1; aa <= GeneralData.NumOfTroops[bb]; aa++)
                {
                    if (UnitList[bb, aa].InBattle)
                        Attack_Phase(aa, bb);
                }
            }
        }

        public static void KillUnit_EndBattle(int bb)
        {
            switch (bb) // Make The Atatcker Stop Attacking
            {
                case 1:
                {
                    for (var aa = 1; aa <= GeneralData.NumOfTroops[2]; aa++)
                    {
                        if (UnitList[bb, aa].Target == TempTroop)
                        {
                            UnitList[bb, aa].InBattle = false;
                            UnitList[bb, aa].Attacking = false;
                            UnitList[bb, aa].Target = 0;
                            UnitList[bb, aa].TeamTarget = 0;
                        }
                    }

                    break;
                }

                case 2: // Make The Atatcker Stop Attacking
                {
                    for (var aa = 1; aa <= GeneralData.NumOfTroops[1]; aa++)
                    {
                        if (UnitList[bb, aa].Target == TempTroop)
                        {
                            UnitList[bb, aa].InBattle = false;
                            UnitList[bb, aa].Attacking = false;
                            UnitList[bb, aa].Moving = false;
                            UnitList[bb, aa].Target = 0;
                            UnitList[bb, aa].TeamTarget = 0;
                            RemoveFromSelectedList(aa, bb);
                        }
                    }

                    break;
                }
            }

            if ((TempTeam == 1) & (GeneralData.GroupSelected != 0))
            {
                AllDead = true;
                for (var aa = 1; aa <= GeneralData.NumberSelected; aa++)
                {
                    if (UnitList[TempTeam, TempTroop].Dead == false)
                    {
                        AllDead = false;
                    }
                }

                if (AllDead)
                    GeneralData.GroupSelected = 0;
            }

            UnitList[TempTeam, TempTroop].InBattle = false;
            UnitList[TempTeam, TempTroop].Attacking = false;
            UnitList[TempTeam, TempTroop].Moving = false;
            UnitList[TempTeam, TempTroop].Dead = true;
            UnitList[TempTeam, TempTroop].Target = 0;
            UnitList[TempTeam, TempTroop].TeamTarget = 0;
            XPos = (int) Math.Round(UnitList[TempTeam, TempTroop].wCurX);
            YPos = (int) Math.Round(UnitList[TempTeam, TempTroop].wCurY);
            BattleMap[(int) Math.Round(XPos), (int) Math.Round(YPos)].Team = 0;
            BattleMap[(int) Math.Round(XPos), (int) Math.Round(YPos)].Troop = 0;
        }

        public static void Alternate_Route(int aa, int bb)
        {
            int VarDumbie;
            int ExtraDisplacement;
            if (BattleMap[UnitList[bb, aa].DestX, UnitList[bb, aa].DestY].Team == 1)
            {
                Choose_Alternate_Destination(aa, bb);
                return;
            }

            ExtraDisplacement = 2;
            XPos = UnitList[bb, aa].wCurX;
            YPos = UnitList[bb, aa].wCurY;
            UnitList[bb, aa].TempX = UnitList[bb, aa].DestX;
            UnitList[bb, aa].TempY = UnitList[bb, aa].DestY;
            UnitList[bb, aa].ReRouting = true;
            if (UnitList[bb, aa].wLeft)
            {
                VBMath.Randomize();
                VarDumbie = (int) Math.Round(Conversion.Int(VBMath.Rnd() * (float) 2m) + (float) 1m);
                switch (VarDumbie)
                {
                    case 1:
                    {
                        UnitList[bb, aa].DestX = (int) Math.Round(XPos);
                        UnitList[bb, aa].DestY = (int) Math.Round(YPos + ExtraDisplacement);
                        break;
                    }

                    case 2:
                    {
                        UnitList[bb, aa].DestX = (int) Math.Round(XPos);
                        UnitList[bb, aa].DestY = (int) Math.Round(YPos - ExtraDisplacement);
                        break;
                    }
                }

                return;
            }

            if (UnitList[bb, aa].wRight)
            {
                VBMath.Randomize();
                VarDumbie = (int) Math.Round(Conversion.Int(VBMath.Rnd() * (float) 2m) + (float) 1m);
                switch (VarDumbie)
                {
                    case 1:
                    {
                        UnitList[bb, aa].DestX = (int) Math.Round(XPos);
                        UnitList[bb, aa].DestY = (int) Math.Round(YPos + ExtraDisplacement);
                        break;
                    }

                    case 2:
                    {
                        UnitList[bb, aa].DestX = (int) Math.Round(XPos);
                        UnitList[bb, aa].DestY = (int) Math.Round(YPos - ExtraDisplacement);
                        break;
                    }
                }

                return;
            }

            if (UnitList[bb, aa].wUp)
            {
                VBMath.Randomize();
                VarDumbie = (int) Math.Round(Conversion.Int(VBMath.Rnd() * (float) 2m) + (float) 1m);
                switch (VarDumbie)
                {
                    case 1:
                    {
                        UnitList[bb, aa].DestX = (int) Math.Round(XPos + ExtraDisplacement);
                        UnitList[bb, aa].DestY = (int) Math.Round(YPos);
                        break;
                    }

                    case 2:
                    {
                        UnitList[bb, aa].DestX = (int) Math.Round(XPos - ExtraDisplacement);
                        UnitList[bb, aa].DestY = (int) Math.Round(YPos);
                        break;
                    }
                }

                return;
            }

            if (UnitList[bb, aa].wDown)
            {
                VBMath.Randomize();
                VarDumbie = (int) Math.Round(Conversion.Int(VBMath.Rnd() * (float) 2m) + (float) 1m);
                switch (VarDumbie)
                {
                    case 1:
                    {
                        UnitList[bb, aa].DestX = (int) Math.Round(XPos + ExtraDisplacement);
                        UnitList[bb, aa].DestY = (int) Math.Round(YPos);
                        break;
                    }

                    case 2:
                    {
                        UnitList[bb, aa].DestX = (int) Math.Round(XPos - ExtraDisplacement);
                        UnitList[bb, aa].DestY = (int) Math.Round(YPos);
                        break;
                    }
                }
            }
        }

        public static void Battle_AggressionCheck(int aa, int bb)
        {
            Lower1 = (int) Math.Round(UnitList[bb, aa].wCurX) - UnitList[bb, aa].Agressiveness;
            Upper1 = (int) Math.Round(UnitList[bb, aa].wCurX) + UnitList[bb, aa].Agressiveness;
            Lower2 = (int) Math.Round(UnitList[bb, aa].wCurY) - UnitList[bb, aa].Agressiveness;
            Upper2 = (int) Math.Round(UnitList[bb, aa].wCurY) + UnitList[bb, aa].Agressiveness;
            switch (Lower1)
            {
                case var @case when @case < 0:
                {
                    Lower1 = 0;
                    break;
                }

                case var case1 when case1 > 74:
                {
                    Lower1 = 74;
                    break;
                }
            }

            switch (Upper1)
            {
                case var case2 when case2 < 0:
                {
                    Upper1 = 0;
                    break;
                }

                case var case3 when case3 > 74:
                {
                    Upper1 = 74;
                    break;
                }
            }

            switch (Lower2)
            {
                case var case4 when case4 < 0:
                {
                    Lower2 = 0;
                    break;
                }

                case var case5 when case5 > 74:
                {
                    Lower2 = 74;
                    break;
                }
            }

            switch (Upper2)
            {
                case var case6 when case6 < 0:
                {
                    Upper2 = 0;
                    break;
                }

                case var case7 when case7 > 74:
                {
                    Upper2 = 74;
                    break;
                }
            }
        }

        public static void Check_For_Show_Of_Agression()
        {
            for (var bb = 1; bb <= 2; bb++)
            {
                for (var aa = 1; aa <= GeneralData.NumOfTroops[bb]; aa++)
                {
                    if (UnitList[bb, aa].Dead == false)
                    {
                        Battle_AggressionCheck(aa, bb);
                        for (cc = Lower1; cc <= Upper1; cc++)
                        {
                            for (ee = Lower2; ee <= Upper2; ee++)
                            {
                                if ((BattleMap[cc, ee].Team != bb) & (BattleMap[cc, ee].Team != 0))
                                {
                                    if (UnitList[bb, aa].ReRouting == false)
                                    {
                                        UnitList[bb, aa].TeamTarget = BattleMap[cc, ee].Team;
                                        UnitList[bb, aa].Target = BattleMap[cc, ee].Troop;
                                        UnitList[bb, aa].Attacking = true;
                                        UnitList[bb, aa].Moving = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public static object DisEngageUnit(int aa)
        {
            TempTeam = UnitList[GeneralData.TeamSelected[aa], GeneralData.TroopSelected[aa]].TeamTarget;
            TempTroop = UnitList[GeneralData.TeamSelected[aa], GeneralData.TroopSelected[aa]].Target;
            UnitList[TempTeam, TempTroop].InBattle = false;
            // COMPUTOR ACTION BASED ON AGRESSIVENESS
            return null;
        }

        public static void RemoveFromSelectedList(int aa, int bb)
        {
            for (ee = 1; ee <= GeneralData.NumberSelected; ee++)
            {
                if (GeneralData.TroopSelected[ee] == aa)
                {
                    for (cc = ee; cc <= GeneralData.NumberSelected - 1; cc++)
                        GeneralData.TroopSelected[cc] = GeneralData.TroopSelected[cc + 1];
                    GeneralData.TeamSelected[GeneralData.NumberSelected] = 0;
                    GeneralData.TroopSelected[GeneralData.NumberSelected] = 0;
                    GeneralData.NumberSelected = GeneralData.NumberSelected - 1;
                }
            }
        }

        public static void CheckForVictory()
        {
            bool Victory;
            // Dim hdc As Object
            Victory = true;
            for (var aa = 1; aa <= GeneralData.NumOfTroops[1]; aa++)
            {
                if (UnitList[1, aa].Dead == false)
                {
                    Victory = false;
                }
            }

            if (Victory) // Computor Wins :(
            {
                DumbieVar = 0m;
                for (var aa = 1; aa <= GeneralData.NumOfTroops[2]; aa++)
                {
                    if (UnitList[2, aa].Dead != true)
                    {
                        DumbieVar = DumbieVar + 1m;
                    }
                }

                /*
                Leigion[1, GameContext.Instance.Engine.GeneralStats.ComputerLegion].NumberInLeigion = (int) Math.Round(DumbieVar);
                Leigion[1, GameContext.Instance.Engine.GeneralStats.YourLegion].Dead = true;
                Leigion[1, GameContext.Instance.Engine.GeneralStats.YourLegion].Moving = false;
                Leigion[1, GameContext.Instance.Engine.GeneralStats.YourLegion].Attacking = false;
                XPos = (int) Math.Round(Leigion[1, GameContext.Instance.Engine.GeneralStats.YourLegion].wCurX);
                YPos = (int) Math.Round(Leigion[1, GameContext.Instance.Engine.GeneralStats.YourLegion].wCurY);
                #1#
                ProvincalMap[CurrentProvince, (int) Math.Round(XPos), (int) Math.Round(YPos)].Team = 0;
                ProvincalMap[CurrentProvince, (int) Math.Round(XPos), (int) Math.Round(YPos)].Troop = 0;

                throw new Exception("Battle finished");
            }

            Victory = true;
            for (var aa = 1; aa <= GeneralData.NumOfTroops[1]; aa++)
            {
                if (UnitList[2, aa].Dead == false)
                {
                    Victory = false;
                }
            }

            if (Victory) // Human Wins :)
            {
                DumbieVar = 0m;
                for (var aa = 1; aa <= GeneralData.NumOfTroops[2]; aa++)
                {
                    if (UnitList[1, aa].Dead != true)
                    {
                        DumbieVar = DumbieVar + 1m;
                    }
                }

                /*
                Leigion[1, GameContext.Instance.Engine.GeneralStats.YourLegion].NumberInLeigion = (int) Math.Round(DumbieVar);
                Leigion[1, GameContext.Instance.Engine.GeneralStats.ComputerLegion].Dead = true;
                Leigion[1, GameContext.Instance.Engine.GeneralStats.ComputerLegion].Moving = false;
                Leigion[1, GameContext.Instance.Engine.GeneralStats.ComputerLegion].Attacking = false;
                XPos = (int) Math.Round(Leigion[1, GameContext.Instance.Engine.GeneralStats.ComputerLegion].wCurX);
                YPos = (int) Math.Round(Leigion[1, GameContext.Instance.Engine.GeneralStats.ComputerLegion].wCurY);
                #1#
                ProvincalMap[CurrentProvince, (int) Math.Round(XPos), (int) Math.Round(YPos)].Team = 0;
                ProvincalMap[CurrentProvince, (int) Math.Round(XPos), (int) Math.Round(YPos)].Troop = 0;

                throw new Exception("Battle finished");
            }
        }

        public static void Battle_OptionButton(int aa)
        {
            if ((MouseY >= OptionButtons[1, aa].Top) & (MouseY <= OptionButtons[1, aa].Top + OptionButtons[1, aa].Height) & (MouseX >= OptionButtons[1, aa].Left) & (MouseX <= OptionButtons[1, aa].Left + OptionButtons[1, aa].Width))
            {
                switch (OptionButtons[1, aa].Press)
                {
                    case 1:
                    {
                        OptionButtons[1, 1].Press = 1;
                        OptionButtons[1, 3].Press = 1;
                        OptionButtons[1, aa].Press = 2;
                        CurrentOptions = OptionButtons[1, aa].ActivatedOption;
                        break;
                    }

                    case 2:
                    {
                        OptionButtons[1, aa].Press = 1;
                        CurrentOptions = 1;
                        break;
                    }
                }
            }
        }

        public static void Battle_CheckForButtonClick()
        {
            for (var aa = 1; aa <= 3; aa += 2) // Option Click (1 and 3)
                Battle_OptionButton(aa);

            switch (CurrentOptions)
            {
                case mStandard:
                {
                    break;
                }

                case mGameOptions: // Game Settings
                {
                    for (var aa = 1; aa <= 5; aa++)
                    {
                        if ((MouseY >= OtherButtons[1, aa].Top) & (MouseY <= OtherButtons[1, aa].Top + OtherButtons[1, aa].Height) & (MouseX >= OtherButtons[1, aa].Left) & (MouseX <= OtherButtons[1, aa].Left + OtherButtons[1, aa].Width))
                        {
                            switch (OtherButtons[1, aa].Press)
                            {
                                case 1:
                                {
                                    for (cc = 1; cc <= 5; cc++)
                                        OtherButtons[1, cc].Press = 1;
                                    OtherButtons[1, aa].Press = 2;
                                    GameContext.Instance.Engine.ChangeSpeed(aa);
                                    break;
                                }
                            }
                        }
                    }

                    break;
                }

                case mUnitOptions: // Unit Options
                {
                    for (var aa = 6; aa <= 8; aa++)
                        Battle_OtherButton(aa);
                    break;
                }
            }
        }

        public static void Battle_OtherButton(int aa)
        {
            if ((MouseY >= OtherButtons[1, aa].Top) & (MouseY <= OtherButtons[1, aa].Top + OtherButtons[1, aa].Height) & (MouseX >= OtherButtons[1, aa].Left) & (MouseX <= OtherButtons[1, aa].Left + OtherButtons[1, aa].Width))
            {
                switch (OtherButtons[1, aa].Press)
                {
                    case 1:
                    {
                        OtherButtons[1, aa].Press = 2;
                        switch (aa)
                        {
                            case 6: // Formation Button
                            {
                                switch (GeneralData.GroupSelected)
                                {
                                    case var @case when @case != 0:
                                    {
                                        UnitGroup[GeneralData.GroupSelected].Formation = true;
                                        break;
                                    }

                                    case var case1 when case1 == 0:
                                    {
                                        GeneralData.Formation = true;
                                        break;
                                    }
                                }

                                break;
                            }

                            case 7: // Halt Button
                            {
                                OtherButtons[1, 7].Press = 2;
                                StopUnit();
                                break;
                            }

                            case 8: // Create Formation
                            {
                                OtherButtons[1, 8].Press = 2;
                                CreateFormation();
                                break;
                            }
                        }

                        break;
                    }

                    case 2:
                    {
                        OtherButtons[1, aa].Press = 1;
                        switch (aa)
                        {
                            case 6:
                            {
                                switch (GeneralData.GroupSelected)
                                {
                                    case var case2 when case2 != 0:
                                    {
                                        UnitGroup[GeneralData.GroupSelected].Formation = false;
                                        break;
                                    }

                                    case var case3 when case3 == 0:
                                    {
                                        GeneralData.Formation = false;
                                        break;
                                    }
                                }

                                break;
                            }
                        }

                        break;
                    }
                }
            }
        }

        public static void Drag_Select()
        {
            if (Drag.MousePressed == false)
            {
                Drag.Draging = false;
                return;
            }

            if (Drag.MousePressed & (Drag.Draging == false))
            {
                Drag.StartX = (int) Math.Round(XPos);
                Drag.StartY = (int) Math.Round(YPos);
                Drag.EndX = (int) Math.Round(XPos);
                Drag.EndY = (int) Math.Round(YPos);
                Drag.Draging = true;
            }

            if (Drag.Draging)
            {
                XPos = Conversion.Int((MouseX - ExtraX) / (decimal) SquareWidth) + GeneralData.MapX;
                YPos = Conversion.Int((MouseY - ExtraY) / (decimal) SquareHeight) + GeneralData.MapY;
                if ((XPos < 0m) | (YPos < 0m))
                    return;
                if ((XPos > GeneralData.MapWidth - 1) | (YPos > GeneralData.MapHeight - 1))
                    return;
                Drag.EndX = (int) Math.Round(XPos + 1m);
                Drag.EndY = (int) Math.Round(YPos + 1m);
            }
        }

        public static void Drag_Select2()
        {
            if (Drag.StartX - Drag.EndX > 0)
                NP1 = -1;
            if (Drag.StartX - Drag.EndX <= 0)
                NP1 = 1;
            if (Drag.StartY - Drag.EndY > 0)
                NP2 = -1;
            if (Drag.StartY - Drag.EndY <= 0)
                NP2 = 1;
            NextTroop = 0;
            for (var aa = Drag.StartX;
                NP1 >= 0
                    ? aa <= Drag.EndX
                    : aa >= Drag.EndX;
                aa += NP1)
            {
                for (var bb = Drag.StartY;
                    NP2 >= 0
                        ? bb <= Drag.EndY
                        : bb >= Drag.EndY;
                    bb += NP2)
                {
                    if (BattleMap[aa, bb].Team == 1)
                    {
                        NextTroop = NextTroop + 1;
                        GeneralData.TeamSelected[NextTroop] = 1;
                        GeneralData.TroopSelected[NextTroop] = BattleMap[aa, bb].Troop;
                    }
                }
            }

            GeneralData.NumberSelected = NextTroop;
        }

        public static void ShowDragTiles(int aa, int bb)
        {
            XPos = (aa - GeneralData.MapX) * 33 + ExtraX;
            YPos = (bb - GeneralData.MapY) * 34 + ExtraY;
            if (XPos >= GeneralData.MapX)
            {
                if (YPos >= GeneralData.MapY)
                {
                    //GameContext.Instance.Engine.Renderer.SurfaceBack.DrawFast((int) Math.Round(XPos), (int) Math.Round(YPos), GameContext.Instance.Engine.Renderer.SurfaceTextures, Textures[1, mSelect], DrawFastFlags.SourceColorKey | DrawFastFlags.Wait);
                }
            }
        }

        public static void StopUnit()
        {
            for (ee = 1; ee <= GeneralData.NumberSelected; ee++)
            {
                UnitList[1, GeneralData.TroopSelected[ee]].DestX = (int) Math.Round(UnitList[1, GeneralData.TroopSelected[ee]].wCurX);
                UnitList[1, GeneralData.TroopSelected[ee]].DestY = (int) Math.Round(UnitList[1, GeneralData.TroopSelected[ee]].wCurY);
            }
        }

        public static void CreateFormation()
        {
            TempTeam = 1;
            XPos = 0m;
            YPos = 0m;
            for (var aa = 1; aa <= GeneralData.NumberSelected; aa++) // Get Average X and Y Co-ordinates
            {
                XPos = XPos + UnitList[TempTeam, GeneralData.TroopSelected[aa]].wCurX;
                YPos = YPos + UnitList[TempTeam, GeneralData.TroopSelected[aa]].wCurY;
            }

            XPos = (int) Math.Round(XPos / GeneralData.NumberSelected);
            YPos = (int) Math.Round(YPos / GeneralData.NumberSelected);
            for (ee = 1; ee <= GeneralData.NumberSelected; ee++)
            {
                UnitList[TempTeam, GeneralData.TroopSelected[ee]].DestX = (int) Math.Round(XPos - (int) Math.Round(GeneralData.NumberSelected / 2m) + ee);
                UnitList[TempTeam, GeneralData.TroopSelected[ee]].DestY = (int) Math.Round(YPos);
                UnitList[TempTeam, GeneralData.TroopSelected[ee]].Moving = true;
            }
        }

        public static void ChangeBattleIcon()
        {
            if (MouseX <= 0)
            {
                if (GeneralData.IconType == mScrollLeft)
                {
                    GeneralData.IconType = mScrollLeft;
                    Provincal.Load_Icon();
                }

                return;
            }

            if (MouseX >= 635)
            {
                if (GeneralData.IconType == mScrollRight)
                {
                    GeneralData.IconType = mScrollRight;
                    Provincal.Load_Icon();
                }

                return;
            }

            if (MouseY <= 0)
            {
                if (GeneralData.IconType == mScrollUp)
                {
                    GeneralData.IconType = mScrollUp;
                    Provincal.Load_Icon();
                }

                return;
            }

            if (MouseY >= 475)
            {
                if (GeneralData.IconType == mScrollDown)
                {
                    GeneralData.IconType = mScrollDown;
                    Provincal.Load_Icon();
                }

                return;
            }

            if ((MouseX > ExtraX) & (MouseY > ExtraY))
            {
                for (var aa = 1; aa <= GeneralData.NumOfTroops[2]; aa++)
                {
                    if ((UnitList[2, aa].wCurX * 35m + 35m >= MouseX - ExtraX + GeneralData.MapX * 35) & (UnitList[2, aa].wCurX * 35m <= MouseX - ExtraX + GeneralData.MapX * 35) &
                        (UnitList[2, aa].wCurY * 20m + 35m >= MouseY - ExtraY + GeneralData.MapY * 35) & (UnitList[2, aa].wCurY * 35m - 15m <= MouseY - ExtraY + GeneralData.MapY * 35))
                    {
                        Interaction.Beep();
                        if (GeneralData.IconType != mAttackIcon)
                        {
                            GeneralData.IconType = mAttackIcon;
                            Provincal.Load_Icon();
                            return;
                        }

                        return;
                    }
                }
            }

            if ((MouseX > MiniX + 3) & (MouseX < MiniX + 80 - 17))
            {
                if ((MouseY > MiniY + 3) & (MouseY < MiniY + 80 - 13))
                {
                    if (GeneralData.IconType != mMiniMapIcon)
                    {
                        GeneralData.IconType = mMiniMapIcon;
                        Provincal.Load_Icon();
                        return;
                    }

                    return;
                }
            }

            if (MouseY > 378)
            {
                if (GeneralData.IconType != mStandardIcon)
                {
                    GeneralData.IconType = mStandardIcon;
                    Provincal.Load_Icon();
                    return;
                }

                return;
            }

            if (MouseY < 378)
            {
                if (GeneralData.IconType != mMoveIcon)
                {
                    LoadIcon = true;
                    GeneralData.IconType = mMoveIcon;
                    Provincal.Load_Icon();
                }
            }
        }
    }
}*/