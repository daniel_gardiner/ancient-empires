namespace AncientEmpires;

public enum EntityType
{
    None = 0,
    Bulldozer = 10,
    Farm = 101,
    Road = 102,
    House = 103,
    Fountain = 104,
    Garrison = 105,
    Wall = 106,
    RomanLegion = 500,
    GreekLegion = 600,
    TestEntity1 = 9000
}