using System.Runtime.InteropServices;
using AncientEmpires.Engine;
using AncientEmpires.Engine.Render;

namespace AncientEmpires;

public partial class Form : System.Windows.Forms.Form
{
    const int WM_SYSCOMMAND = 0x0112;
    const int SC_MAXIMIZE = 0xF030;
    const int SC_RESTORE = 0xF120;
    const int WM_NCLBUTTONDOWN = 0x00A1;
    const int WM_NCMBUTTONDBLCLK = 0x00A3;

    [DllImport("kernel32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    static extern bool AllocConsole();

    public Form Instance { get; set; }

    public Form()
    {
        InitializeComponent();
    }

    protected override void OnLoad(EventArgs e)
    {
        AllocConsole();
        Instance = this;
        Visible = true;
        Padding = Padding.Empty;
        Margin = Padding.Empty;
        WindowState = FormWindowState.Maximized;
        FormBorderStyle = FormBorderStyle.Sizable;
        //FormBorderStyle = FormBorderStyle.SizableToolWindow;
        ///Size = new Size(1200, 700);
        ResizeRedraw = false;
        Application.DoEvents();

        Focus();

        GameManager.Instance = new GameManager(
            new WinFormsGameWindow(this),
            new GameFileSystem(),
            window => new GraphicsDevice(window));
        GameManager.Instance.StartGame();

        base.OnLoad(e);
    }

    protected override void WndProc(ref Message m)
    {
        base.WndProc(ref m);

        if (m.Msg == WM_NCMBUTTONDBLCLK)
            OnResizeEnd(EventArgs.Empty);

        if (m.Msg != WM_SYSCOMMAND)
            return;

        if (m.WParam == (IntPtr)SC_MAXIMIZE || m.WParam == (IntPtr)SC_RESTORE)
            OnResizeEnd(EventArgs.Empty);
    }

    protected override void OnResizeEnd(EventArgs e)
    {
        if (GameContext.Instance.Engine == null)
            return;

        //ConstrainResize();
        ConsoleWriter.WriteLine($"ClientSize = {ClientSize}");
        Application.DoEvents();
        GameManager.Instance.Resize();

        base.OnResizeEnd(e);
    }

    /*
    private void ConstrainResize()
    {
        var width = ClientSize.Width < 600
            ? 600
            : ClientSize.Width;
        var height = ClientSize.Height < 500
            ? 500
            : ClientSize.Height;
        ClientSize = new Size(width, height);
    }
    */


    protected override void OnKeyDown(KeyEventArgs e)
    {
        switch (e.KeyCode)
        {
            case Keys.F10:
                e.SuppressKeyPress = true;
                return;
        }

        base.OnKeyDown(e);
    }
}