using System.Collections.Concurrent;
using System.Text;
using AncientEmpires.Engine;
using Newtonsoft.Json;

namespace AncientEmpires;

public static class ConsoleWriter
{
    private static BlockingCollection<object> BlockingCollection = new();
    private static long _count;

    static ConsoleWriter()
    {
        Task.Run(() =>
        {
            while (GameContext.Instance.Engine == null)
            {
                Thread.Sleep(0);
            }

            var stringBuilder = new StringBuilder();
            while (GameContext.Instance.Engine.State != GameEngineState.Quiting)
            {
                stringBuilder.Clear();
                while (BlockingCollection.TryTake(out var value))
                {
                    if (value is string stringValue)
                    {
                        stringBuilder.AppendLine(stringValue);
                        //Trace.WriteLine(value);
                    }
                    else
                    {
                        stringBuilder.AppendLine(JsonConvert.SerializeObject(value));
                        //Trace.WriteLine(JsonConvert.SerializeObject(value));
                    }

                    _count++;
                    if (_count > 10)
                    {
                        Console.Write(stringBuilder.ToString());
                        stringBuilder.Clear();
                        _count = 0;
                    }
                }

                _count = 0;
                Console.Write(stringBuilder.ToString());
                Thread.Yield();
            }
        });
    }

    public static void WriteLine(string value)
    {
        if (value == null)
            return;

        if (BlockingCollection.Count > 1000)
        {
            BlockingCollection = new BlockingCollection<object>();
            return;
        }

        BlockingCollection.Add(value);
    }

    public static void WriteLine(object value)
    {
        if (value == null)
            return;

        if (BlockingCollection.Count > 1000)
        {
            BlockingCollection = new BlockingCollection<object>();
            return;
        }

        BlockingCollection.Add(value);
    }

    public static void WriteLineIf(bool condition, string value)
    {
        if (!condition)
            return;

        WriteLine(value);
    }

    public static void WriteLineIfEvery(bool condition, string value, TimeSpan timeSpan, string key)
    {
        if (!condition)
            return;

        if (GameContext.Instance.Engine.Timing.HasBeen(key, timeSpan))
        {
            WriteLine(value);
        }
    }

    public static void WriteLineIfPerSlice(bool condition, string value)
    {
        if (!condition)
            return;

        if (GameContext.Instance.Engine.Timing.IsNewSlice)
        {
            WriteLine(value);
        }
    }
}