﻿

/*namespace AncientEmpires
{
    internal static class Provincal
    {
        private static readonly GameEngine _game = GameContext.Instance.Engine;
        
        public static void ChangeProvincalIcon()
        {
            var entitySpawnActions = GameContext.Instance.Engine.CurrentActions.OfType<EntitySpawn>().ToList();
            
            if (entitySpawnActions.Any(o => o.EntityType != EntityType.Bulldozer))
            {
                if (MouseX <= 0)
                {
                    if (GeneralData.IconType == mScrollLeft)
                    {
                        GeneralData.IconType = mScrollLeft;
                        Load_Icon();
                    }

                    return;
                }

                if (MouseX >= 635)
                {
                    if (GeneralData.IconType == mScrollRight)
                    {
                        GeneralData.IconType = mScrollRight;
                        Load_Icon();
                    }

                    return;
                }

                if (MouseY <= 0)
                {
                    if (GeneralData.IconType == mScrollUp)
                    {
                        GeneralData.IconType = mScrollUp;
                        Load_Icon();
                    }

                    return;
                }

                if (MouseY >= 475)
                {
                    if (GeneralData.IconType == mScrollDown)
                    {
                        GeneralData.IconType = mScrollDown;
                        Load_Icon();
                    }

                    return;
                }

                /*
                if (MouseX > ExtraX & MouseY > ExtraY)
                {
                    for (int aa = 1; aa <= _game.GeneralStats.NumberOfLegion[2]; aa++)
                    {
                        if (Leigion[2, aa].wCurX * 20m + 20m >= MouseX - ExtraX + GeneralData.MapX * 20 & Leigion[2, aa].wCurX * 20m <= MouseX - ExtraX + GeneralData.MapX * 20 &
                            Leigion[2, aa].wCurY * 20m + 15m >= MouseY - ExtraY + GeneralData.MapY * 20 & Leigion[2, aa].wCurY * 20m - 15m <= MouseY - ExtraY + GeneralData.MapY * 20)
                        {
                            if (GeneralData.IconType != mAttackIcon)
                            {
                                GeneralData.IconType = mAttackIcon;
                                Load_Icon();
                                return;
                            }
                            else
                            {
                                return;
                            }
                        }
                    }
                }
                #1#

                if ((MouseX > MiniX + 3) & (MouseX < MiniX + 80 - 17))
                {
                    if ((MouseY > MiniY + 3) & (MouseY < MiniY + 80 - 13))
                    {
                        if (GeneralData.IconType != mMiniMapIcon)
                        {
                            GeneralData.IconType = mMiniMapIcon;
                            Load_Icon();
                            return;
                        }

                        return;
                    }
                }

                if (MouseY > 378)
                {
                    if (GeneralData.IconType != mStandardIcon)
                    {
                        GeneralData.IconType = mStandardIcon;
                        Load_Icon();
                        return;
                    }

                    return;
                }

                if (MouseY < 378)
                {
                    if (GeneralData.IconType != mMoveIcon)
                    {
                        LoadIcon = true;
                        GeneralData.IconType = mMoveIcon;
                        Load_Icon();
                    }
                }
            }
        }

        public static void Load_Icon()
        {
            var entitySpawnActions = GameContext.Instance.Engine.CurrentActions.OfType<EntitySpawn>().ToList();
            
            if (entitySpawnActions.Any(o => o.EntityType == EntityType.Bulldozer))
                GeneralData.IconType = mBulldozeIcon;

            Cursor.Current = GeneralData.IconType switch
            {
                mStandardIcon => new Cursor($"{Application.StartupPath}\\Resources\\Textures\\Cursors\\Standard.cur"),
                mAttackIcon => new Cursor($"{Application.StartupPath}\\Resources\\Textures\\Cursors\\Attack.cur"),
                mMoveIcon => new Cursor($"{Application.StartupPath}\\Resources\\Textures\\Cursors\\Move.cur"),
                mMiniMapIcon => new Cursor($"{Application.StartupPath}\\Resources\\Textures\\Cursors\\MiniMap.cur"),
                mBulldozeIcon => new Cursor($"{Application.StartupPath}\\Resources\\Textures\\Cursors\\Bulldoze.cur"),
                mScrollLeft => new Cursor($"{Application.StartupPath}\\Resources\\Textures\\Cursors\\Left.cur"),
                mScrollRight => new Cursor($"{Application.StartupPath}\\Resources\\Textures\\Cursors\\Right.cur"),
                mScrollUp => new Cursor($"{Application.StartupPath}\\Resources\\Textures\\Cursors\\Up.cur"),
                mScrollDown => new Cursor($"{Application.StartupPath}\\Resources\\Textures\\Cursors\\Down.cur"),
                _ => Cursor.Current
            };
        }

        public static void MousePress_Provincal()
        {
            if (_game.Input.Current.LeftClick)
            {
                if (CurrentOptions == mGameOptions)
                {
                    Provincal_CheckForButtonClick();
                }

                if (MouseY > 300)
                    Provincal_CheckForButtonClick();
            }
        }

        public static void Provincal_AssignBuildingPositions(EntityType entityType, int instanceNumber, int structureType)
        {
            var buildingInstance = BuildingList[1, CurrentProvince, (int) entityType].Instances[instanceNumber];
            var xPos = buildingInstance.Left;
            var yPos = buildingInstance.Top;
            for (var x = xPos; x <= xPos + buildingInstance.ObjectWidth - 1; x++)
            {
                for (var y = yPos; y <= yPos + buildingInstance.ObjectHeight - 1; y++)
                {
                    ProvincalMap[CurrentProvince, x, y].StructureType = entityType;
                    ProvincalMap[CurrentProvince, x, y].StructureNumber = structureType;
                }
            }
        }

        public static void Click_AttackRequest()
        {
            for (cc = 2; cc <= 3; cc++)
            {
                XPos = MenuButton[cc].Left;
                YPos = MenuButton[cc].Top;
                TempX = MenuButton[cc].Width;
                TempY = MenuButton[cc].Height;
                if ((MouseY >= YPos) & (MouseY <= YPos + TempY) & (MouseX >= XPos) & (MouseX <= XPos + TempX))
                {
                    switch (cc)
                    {
                        case 2:
                        {
                            DisableInput = false;
                            SoundManager.Play(1);
                            Provincal_AttackCheck(TempTroop, TempTeam);
                            break;
                        }

                        case 3:
                        {
                            /*
                            Leigion[TempTeam, TempTroop].Attacking = false;
                            Leigion[Leigion[TempTeam, TempTroop].TeamTarget, Leigion[TempTeam, TempTroop].Target].Attacking = false;
                            Leigion[TempTeam, TempTroop].Moving = false;
                            Leigion[Leigion[TempTeam, TempTroop].TeamTarget, Leigion[TempTeam, TempTroop].Target].Moving = false;
                            #1#
                            CurrentOptions = mStandard;
                            DisableInput = false;
                            SoundManager.Play(1);

                            throw new Exception("Run_Province");
                            break;
                        }
                    }
                }
            }
        }

        public static void Click_GarrisonRequest()
        {
            for (cc = 2; cc <= 3; cc++)
            {
                XPos = MenuButton[cc].Left;
                YPos = MenuButton[cc].Top;
                TempX = MenuButton[cc].Width;
                TempY = MenuButton[cc].Height;
                if ((MouseY >= YPos) & (MouseY <= YPos + TempY) & (MouseX >= XPos) & (MouseX <= XPos + TempX))
                {
                    switch (cc)
                    {
                        case 2:
                        {
                            /*
                            Leigion[TempTeam, TempTroop].Attacking = false;
                            Leigion[TempTeam, TempTroop].Moving = false;
                            #1#
                            GeneralData.NumberSelected = 0;
                            DisableInput = false;
                            SoundManager.Play(1);
                            Provincal_GarrisonLegion(TempTeam, TempTroop);
                            break;
                        }

                        case 3:
                        {
                            /*
                            Leigion[TempTeam, TempTroop].Attacking = false;
                            Leigion[TempTeam, TempTroop].Moving = false;
                            #1#
                            CurrentOptions = mStandard;
                            DisableInput = false;
                            SoundManager.Play(1);

                            throw new Exception("Run_Province");
                            break;
                        }
                    }
                }
            }
        }

        public static void Provincal_CheckForButtonClick()
        {
            // Check for Option Click

            for (var aa = 1; aa <= 2; aa++) // Option Click (1 and 3)
                Provincal_OptionButton(aa);

            Provincal_OptionButton(4);

            // Check For Other Click

            switch (CurrentOptions)
            {
                case mAttackRequest:
                {
                    Click_AttackRequest();
                    break;
                }
                case mGarrisonRequest:
                {
                    Click_GarrisonRequest();
                    break;
                }
                case mStandard:
                {
                    break;
                }
            }
        }

        public static void Provincal_OptionButton(int aa)
        {
            if ((MouseY >= OptionButtons[1, aa].Top) & (MouseY <= OptionButtons[1, aa].Top + OptionButtons[1, aa].Height) & (MouseX >= OptionButtons[1, aa].Left) & (MouseX <= OptionButtons[1, aa].Left + OptionButtons[1, aa].Width))
            {
                SoundManager.Play(1);
                switch (OptionButtons[1, aa].Press)
                {
                    case 1:
                    {
                        OptionButtons[1, 1].Press = 1;
                        OptionButtons[1, 2].Press = 1;
                        OptionButtons[1, 4].Press = 1;
                        OptionButtons[1, aa].Press = 2;
                        CurrentOptions = OptionButtons[1, aa].ActivatedOption;
                        break;
                    }

                    case 2:
                    {
                        OptionButtons[1, aa].Press = 1;
                        CurrentOptions = 1;
                        break;
                    }
                }
            }
        }
        
        public static void Provincal_Date()
        {
            if (GameContext.Instance.Engine.Timing.Slice % 100 == 0)
            {
                _game.GeneralStats.Month++;
                Provinclal_MonthlyMatinence();
            }
            
            if (_game.GeneralStats.Month == 13)
            {
                _game.GeneralStats.Month = 1;
                _game.GeneralStats.Year += 1;
                Provinclal_YearlyMatinence();
            }
        }

        public static void Provinclal_MonthlyMatinence()
        {
            return;

            // Natural Foraging
            _game.GeneralStats.Grain = _game.GeneralStats.Grain + 2000m;

            // Increase Population
            _game.GeneralStats.Population = 2000m;
            for (var aa = 1; aa <= BuildingList[1, CurrentProvince, (int) EntityType.House].NumOfType; aa++)
            {
                var buildingType = BuildingList[1, CurrentProvince, (int) EntityType.House];
                var buildingInstance = buildingType.Instances[aa];
                _game.GeneralStats.Population = _game.GeneralStats.Population + buildingInstance.Population;
                buildingInstance.Population = buildingInstance.Population * 1.02m;
                switch (buildingInstance.Population)
                {
                    case var @case when @case >= 500m:
                        buildingInstance.Phase = 3;
                        break;

                    case var case1 when case1 >= 200m:
                        buildingInstance.Phase = 2;
                        break;

                    case var case2 when case2 > 0m:
                        buildingInstance.Phase = 1;
                        break;
                }
            }

            // Feed People
            switch (_game.GeneralStats.FoodRation)
            {
                case mFull:
                {
                    DumbieVar2 = (int) Math.Round(_game.GeneralStats.Population * 1m);
                    break;
                }

                case mHalf:
                {
                    DumbieVar2 = (int) Math.Round(_game.GeneralStats.Population * 0.5m);
                    break;
                }

                case mQuarter:
                {
                    DumbieVar2 = (int) Math.Round(_game.GeneralStats.Population * 0.25m);
                    break;
                }
            }

            _game.GeneralStats.Grain = _game.GeneralStats.Grain - DumbieVar2;

            // Feed Army
            DumbieVar2 = 0m;
            for (var aa = 1; aa <= _game.GeneralStats.NumberOfLegion[1]; aa++)
            {
                /*
                if (Leigion[1, aa].Dead != true)
                {
                    switch (Leigion[1, aa].Garrisoned)
                    {
                        case true:
                        {
                            DumbieVar2 = DumbieVar2 + Leigion[1, aa].NumberInLeigion * 1;
                            break;
                        }

                        case false:
                        {
                            DumbieVar2 = DumbieVar2 + Leigion[1, aa].NumberInLeigion * 2;
                            break;
                        }
                    }
                }
            #1#
            }

            _game.GeneralStats.Grain = _game.GeneralStats.Grain - DumbieVar2;
        }

        public static void Provinclal_YearlyMatinence()
        {
            DumbieVar2 = _game.GeneralStats.Population * _game.GeneralStats.Income / _game.GeneralStats.Tax;
            _game.GeneralStats.Money = _game.GeneralStats.Money + DumbieVar2;
            ArmySize = 0m;
            for (var aa = 1; aa <= _game.GeneralStats.NumberOfLegion[1]; aa++)
            {
                /*
                if (Leigion[1, aa].Dead != true)
                {
                    ArmySize = ArmySize + Leigion[1, aa].NumberInLeigion;
                }
            #1#
            }

            _game.GeneralStats.Money = Conversion.Int(_game.GeneralStats.Money - ArmySize * 128m * 100m);
        }

        public static void Provincal_SelectLeigion()
        {
            if ((MouseX > ExtraX) & (MouseY > ExtraY) & (MouseY < 380))
            {
                for (var aa = 1; aa <= _game.GeneralStats.NumberOfLegion[1]; aa++)
                {
                    /*
                    if (Leigion[1, aa].Dead != true & Leigion[1, aa].Garrisoned != true)
                    {
                        if (Leigion[1, aa].wCurX * 20m >= MouseX - ExtraX - 20 + GeneralData.MapX * 20)
                        {
                            if (Leigion[1, aa].wCurX * 20m <= MouseX - ExtraX + GeneralData.MapX * 20)
                            {
                                if (Leigion[1, aa].wCurY * 20m >= MouseY - ExtraY - 20 + GeneralData.MapY * 20)
                                {
                                    if (Leigion[1, aa].wCurY * 20m - 15m <= MouseY - ExtraY + GeneralData.MapY * 20)
                                    {
                                        GeneralData.TeamSelected[1] = 1;
                                        GeneralData.TroopSelected[1] = aa;
                                        GeneralData.NumberSelected = 1;
                                        GeneralData.GroupSelected = 0;
                                        CurrentOptions = mUnitData;
                                        return;
                                    }
                                }
                            }
                        }
                    }
                #1#
                }

                if (CurrentOptions == mUnitData)
                    CurrentOptions = 0;

                GeneralData.NumberSelected = 0;
                GeneralData.TeamSelected[1] = 0;
                GeneralData.TroopSelected[1] = 0;
                GeneralData.GroupSelected = 0;
            }
        }
        
        public static void Provincal_AssignDestination()
        {
            if (GeneralData.NumberSelected == 0)
                return;
            switch (GeneralData.GroupSelected)
            {
                case var @case when @case != 0:
                {
                    switch (UnitGroup[GeneralData.GroupSelected].Formation)
                    {
                        case true:
                        {
                            Provincal_FormationDestination();
                            break;
                        }

                        case false:
                        {
                            Provincal_StandardDestination();
                            break;
                        }
                    }

                    break;
                }
            }
        }

        public static void Provincal_FormationDestination()
        {
            TempTeam = 1;
            XPos = 0m;
            YPos = 0m;
            /*
            for (int aa = 1; aa <= GeneralData.NumberSelected; aa++)
            {
                XPos = XPos + Leigion[TempTeam, GeneralData.TroopSelected[aa]].wCurX;
                YPos = YPos + Leigion[TempTeam, GeneralData.TroopSelected[aa]].wCurY;
            }
            #1#

            XPos = (int) Math.Round(XPos / GeneralData.NumberSelected);
            YPos = (int) Math.Round(YPos / GeneralData.NumberSelected);
            XDis = (int) Math.Round((int) Math.Round((MouseX - ExtraX) / 20m) + GeneralData.MapX - XPos);
            YDis = (int) Math.Round((int) Math.Round((MouseY - ExtraY) / 20m) + GeneralData.MapY - YPos);
            for (var aa = 1; aa <= GeneralData.NumberSelected; aa++)
            {
                TempTeam = 1;
                TempTroop = GeneralData.TroopSelected[aa];
                /*
                Leigion[TempTeam, TempTroop].DestX[1] = (int) Math.Round(Leigion[TempTeam, TempTroop].wCurX + XDis);
                Leigion[TempTeam, TempTroop].DestY[1] = (int) Math.Round(Leigion[TempTeam, TempTroop].wCurY + YDis);
                Leigion[TempTeam, TempTroop].Attacking = false;
                Leigion[TempTeam, TempTroop].Moving = true;
            #1#
            }
        }

        public static void Provincal_StandardDestination()
        {
            switch (ProvincalMap[CurrentProvince, TrueX, TrueY].StructureType)
            {
                case EntityType.Garrison:
                    for (var aa = 1; aa <= GeneralData.NumberSelected; aa++)
                    {
                        /*
                        Leigion[1, GeneralData.TroopSelected[aa]].DestX[1] = TrueX;
                        Leigion[1, GeneralData.TroopSelected[aa]].DestY[1] = TrueY;
                        Leigion[1, GeneralData.TroopSelected[aa]].Attacking = false;
                        Leigion[1, GeneralData.TroopSelected[aa]].MovingToGarrison = true;
                        Leigion[1, GeneralData.TroopSelected[aa]].Moving = true;
                        Leigion[1, GeneralData.TroopSelected[aa]].FollowingRoad = false;
                    #1#
                    }

                    break;

                default:
                    for (var aa = 1; aa <= GeneralData.NumberSelected; aa++)
                    {
                        /*
                        Leigion[1, GeneralData.TroopSelected[aa]].DestX[1] = TrueX;
                        Leigion[1, GeneralData.TroopSelected[aa]].DestY[1] = TrueY;
                        Leigion[1, GeneralData.TroopSelected[aa]].Attacking = false;
                        Leigion[1, GeneralData.TroopSelected[aa]].Moving = true;
                        Leigion[1, GeneralData.TroopSelected[aa]].MovingToGarrison = false;
                        Leigion[1, GeneralData.TroopSelected[aa]].FollowingRoad = false;
                    #1#
                    }

                    break;
            }
        }

        public static void Provincal_GarrisonLegion(int tempTeam, int tempTroop)
        {
            /*
            Leigion[tempTeam, tempTroop].Garrisoned = true;
            XPos = (int) Math.Round(Leigion[tempTeam, tempTroop].wCurX);
            YPos = (int) Math.Round(Leigion[tempTeam, tempTroop].wCurY);
            #1#
            ProvincalMap[CurrentProvince, (int) Math.Round(XPos), (int) Math.Round(YPos)].Team = 0;
            ProvincalMap[CurrentProvince, (int) Math.Round(XPos), (int) Math.Round(YPos)].Troop = 0;
            CurrentOptions = mStandard;

            throw new Exception("Run_Province");
        }

        public static void Provincal_ShowCorrectDirection(int aa, int bb)
        {

        }
        
        public static CPType[] Movement_CreateCP(int nLeft, int nTop)
        {
            // Looks Kind Inefficent
            var cp = new CPType[5];
            cp[1].x = nLeft; // TopLeft
            cp[1].y = nTop - 1;
            cp[2].x = nLeft + 1; // TopRight
            cp[2].y = nTop - 1;
            cp[3].x = nLeft; // BottomLeft
            cp[3].y = nTop + 1;
            cp[4].x = nLeft + 1; // BottomRight
            cp[4].y = nTop + 1;
            return cp;
        }

        public static bool Movement_StructreCollision(CPType[] cp)
        {
            for (cc = 1; cc <= 4; cc++)
            {
                var mapWidth = 75;
                if (cp[cc].x >= mapWidth || cp[cc].y >= mapWidth)
                    continue;
                if (cp[cc].x <= 0 || cp[cc].y <= 0)
                    continue;

                var mapTile = ProvincalMap[CurrentProvince, cp[cc].x, cp[cc].y];
                if (mapTile.StructureType != EntityType.None)
                {
                    return true;
                }
            }

            return false;
        }

        public static void Provincal_AttackCheck(int aa, int bb)
        {
            /*
            XPos = Leigion[bb, aa].wCurX;
            YPos = Leigion[bb, aa].wCurY;
            if (Leigion[bb, aa].wLeft)
                XPos = XPos - TheSpeed;
            if (Leigion[bb, aa].wRight)
                XPos = XPos + TheSpeed;
            if (Leigion[bb, aa].wUp)
                YPos = YPos - TheSpeed;
            if (Leigion[bb, aa].wDown)
                YPos = YPos + TheSpeed;
            switch (Leigion[bb, aa].Attacking)
            {
                case true: // Is Looking To Engage Enemy
                {
                    switch (ProvincalMap[CurrentProvince, (int) Math.Round(XPos), (int) Math.Round(YPos)].Team)
                    {
                        case var @case when @case == Leigion[bb, aa].TeamTarget:
                        {
                            if (ProvincalMap[CurrentProvince, (int) Math.Round(XPos), (int) Math.Round(YPos)].Troop == Leigion[bb, aa].Target)
                            {
                                Provincal_LaunchBattle(aa, bb); // Prepare For battle!
                            }

                            break;
                        }
                    }

                    break;
                }
            }
        #1#
        }

        public static void Provincal_CheckForShowOfAgression()
        {
            /*
            for (int bb = 1; bb <= 2; bb++)
            {
                for (int aa = 1; aa <= _game.GeneralStats.NumberOfLegion[bb]; aa++)
                {
                    XPos = (int) Math.Round(Leigion[bb, aa].wCurX);
                    YPos = (int) Math.Round(Leigion[bb, aa].wCurY);
                    for (xx = (int) Math.Round(XPos - Leigion[bb, aa].Aggressiveness); xx <= (int) Math.Round(XPos + Leigion[bb, aa].Aggressiveness); xx++)
                    {
                        for (yy = (int) Math.Round(YPos - Leigion[bb, aa].Aggressiveness); yy <= (int) Math.Round(YPos + Leigion[bb, aa].Aggressiveness); yy++)
                        {
                            if (xx >= 0 & yy >= 0 & xx <= 74 & yy <= 74)
                            {
                                if (ProvincalMap[CurrentProvince, xx, yy].Team == 2)
                                {
                                    Leigion[bb, aa].Moving = true;
                                    Leigion[bb, aa].Attacking = true;
                                    Leigion[bb, aa].TeamTarget = 2;
                                    Leigion[bb, aa].Target = ProvincalMap[CurrentProvince, xx, yy].Troop;
                                }
                            }
                        }
                    }
                }
            }
            #1#
        }

        public static void Provincal_AssignTarget()
        {
            if (GeneralData.NumberSelected == 0)
                return;

            if ((MouseX > ExtraX) & (MouseY > ExtraY))
            {
                /*
                for (int bb = 1; bb <= _game.GeneralStats.NumberOfLegion[2]; bb++)
                {
                    for (int aa = 1; aa <= GeneralData.NumberSelected; aa++)
                    {
                        if (Leigion[2, aa].wCurX * 20m + 20m >= MouseX - ExtraX + GeneralData.MapX * 20 & Leigion[2, aa].wCurX * 20m <= MouseX - ExtraX + GeneralData.MapX * 20 &
                            Leigion[2, aa].wCurY * 20m + 15m >= MouseY - ExtraY + GeneralData.MapY * 20 & Leigion[2, aa].wCurY * 20m - 15m <= MouseY - ExtraY + GeneralData.MapY * 20)
                        {
                            Leigion[GeneralData.TeamSelected[aa], GeneralData.TroopSelected[aa]].Moving = true;
                            Leigion[GeneralData.TeamSelected[aa], GeneralData.TroopSelected[aa]].Attacking = true;
                            Leigion[GeneralData.TeamSelected[aa], GeneralData.TroopSelected[aa]].Target = bb;
                            Leigion[GeneralData.TeamSelected[aa], GeneralData.TroopSelected[aa]].TeamTarget = 2;
                        }
                    }
                }
            #1#
            }
        }

        /*
        public static void Provincal_LaunchBattle(int tempTroop, int bb)
        {
            Battle.Create_BattleMap();
            GeneralData.NumberSelected = 0;
            _game.GamePhase = mBattle;
            GeneralData.NumOfTroops[1] = Leigion[1, TempTroop].NumberInLeigion;
            GeneralData.NumOfTroops[2] = Leigion[Leigion[1, TempTroop].TeamTarget, Leigion[bb, TempTroop].Target].NumberInLeigion;
            _game.GeneralStats.YourLegion = TempTroop;
            _game.GeneralStats.ComputerLegion = Leigion[bb, TempTroop].Target;
            for (int aa = 1; aa <= Leigion[1, TempTroop].NumberInLeigion; aa++)
            {
                UnitList[1, aa].wCurX = aa * 3;
                UnitList[1, aa].wCurY = 4m;
                UnitList[1, aa].DestX = (int) Math.Round(UnitList[1, aa].wCurX);
                UnitList[1, aa].DestY = (int) Math.Round(UnitList[1, aa].wCurY);
                UnitList[1, aa].Period = 4;
                UnitList[1, aa].Attacking = false;
                UnitList[1, aa].Moving = false;
                UnitList[1, aa].Race = GeneralData.PlayerRace[1];
                UnitList[1, aa].UnitType = 1;
                UnitList[1, aa].Frame = 1;
                UnitList[1, aa].Direction = 1;
                UnitList[1, aa].MaxHealth = 50;
                UnitList[1, aa].Health = 50;
                UnitList[1, aa].Speed = 0.05m;
                UnitList[1, aa].Armour = 10;
                UnitList[1, aa].Damage = 20;
                BattleMap[aa * 3, 4].Team = 1;
                BattleMap[aa * 3, 4].Troop = aa;
                UnitList[1, aa].Agressiveness = 2;
            }

            for (int aa = 1; aa <= Leigion[Leigion[1, TempTroop].TeamTarget, Leigion[bb, TempTroop].Target].NumberInLeigion; aa++)
            {
                UnitList[2, aa].wCurX = aa * 2 + 5;
                UnitList[2, aa].wCurY = 10m;
                UnitList[2, aa].DestX = (int) Math.Round(UnitList[1, aa].wCurX);
                UnitList[2, aa].DestY = (int) Math.Round(UnitList[1, aa].wCurY);
                UnitList[2, aa].Period = 4;
                UnitList[2, aa].Attacking = false;
                UnitList[2, aa].Moving = false;
                UnitList[2, aa].Race = GeneralData.PlayerRace[2];
                UnitList[2, aa].UnitType = 1;
                UnitList[2, aa].Frame = 1;
                UnitList[2, aa].Direction = 1;
                UnitList[2, aa].MaxHealth = 50;
                UnitList[2, aa].Health = 50;
                UnitList[2, aa].Speed = 0.05m;
                UnitList[2, aa].Armour = 10;
                UnitList[2, aa].Damage = 20;
                UnitList[2, aa].Agressiveness = 2;
                BattleMap[aa * 2 + 10, 20].Team = 2;
                BattleMap[aa * 2 + 10, 20].Troop = aa;
            }

            Module4.Run_Battle();
        }
        #1#

        /*
        public static void Provincal_MoveDynamic(int aa, int bb)
        {
            Leigion[bb, aa].DestX[1] = (int) Math.Round(Leigion[Leigion[bb, aa].TeamTarget, Leigion[bb, aa].Target].wCurX);
            Leigion[bb, aa].DestY[1] = (int) Math.Round(Leigion[Leigion[bb, aa].TeamTarget, Leigion[bb, aa].Target].wCurY);
        }
        #1#

        public static void SelectBuilding()
        {
            if (ProvincalMap[CurrentProvince, TrueX, TrueY].StructureType == EntityType.House)
            {
                TempTroop = ProvincalMap[CurrentProvince, TrueX, TrueY].StructureNumber;
                _game.GeneralStats.BuildingTeam = 1;
                _game.GeneralStats.BuildingType = EntityType.House;
                _game.GeneralStats.BuildingNumber = TempTroop;
                CurrentOptions = mBuildingData;
                for (var aa = 1; aa <= 4; aa++)
                    OptionButtons[1, aa].Press = 1;
            }
        }
    }
}*/