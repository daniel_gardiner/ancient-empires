﻿namespace AncientEmpires;

/*
internal static class SpriteManagerOld
{
    public static void SetSprites()
    {
        TempRect = new RectangleF();
        Bar = new RectangleF();
        for (var spDirection = 1; spDirection <= 4; spDirection++)
        {
            for (var spFrame = 1; spFrame <= 7; spFrame++)
                Sprite[1, 1, spDirection, spFrame] = new RectangleF((spDirection - 1) * 50, (spFrame - 1) * 35, spDirection * 50, spFrame * 35);
        }

        for (var spDirection = 5; spDirection <= 8; spDirection++)
        {
            for (var spFrame = 1; spFrame <= 7; spFrame++)
                Sprite[1, 1, spDirection, spFrame] = new RectangleF((spDirection - 5) * 50, (spFrame - 1) * 35 + 250, (spDirection - 4) * 50, spFrame * 35 + 250);
        }

        for (var spDirection = 1; spDirection <= 4; spDirection++)
        {
            for (var spFrame = 1; spFrame <= 7; spFrame++)
                Sprite[2, 1, spDirection, spFrame] = new RectangleF((spDirection - 1) * 50 + 250, (spFrame - 1) * 35, spDirection * 50 + 250, spFrame * 35);
        }

        for (var spDirection = 5; spDirection <= 8; spDirection++)
        {
            for (var spFrame = 1; spFrame <= 7; spFrame++)
                Sprite[2, 1, spDirection, spFrame] = new RectangleF((spDirection - 5) * 50 + 250, (spFrame - 1) * 35 + 250, (spDirection - 4) * 50 + 250, spFrame * 35 + 250);
        }

        Sprite[1, 1, mDead, 1] = new RectangleF(0, 490, 50, 525);
        Sprite[2, 1, mDead, 1] = new RectangleF(0, 490, 50, 525);
        // Provincal Men
        // Roman

        PopUp[1] = new RectangleF(100, 50, 300, 236);
        PopUp[2] = new RectangleF(100, 350, 300, 500);
        OutPutBox[1] = new RectangleF(0, 216, 90, 240);
        OutPutBox[2] = new RectangleF(100, 250, 220, 274);
        OutPutBox[3] = new RectangleF(0, 240, 51, 265);

        for (var aa = 1; aa <= 5; aa++)
            Labels[aa] = new RectangleF(0, 90 + (aa - 1) * 20, 75, 90 + aa * 20);

        for (var aa = 9; aa <= 10; aa++)
        {
            switch (aa)
            {
                case 9:
                    Labels[aa] = new RectangleF(0, 300 + (aa - 9) * 20, 70, 300 + (aa - 8) * 20);
                    break;

                case 10:
                    Labels[aa] = new RectangleF(0, 300 + (aa - 9) * 20, 85, 300 + (aa - 8) * 20);
                    break;
            }
        }
    }

    public static void Initlize()
    {
        CurrentOptions = mStandard;
        CurrentProvince = 1;
        DrawMiniMap = true;
        DisableInput = false;

        GeneralData.NumberSelected = 0;
        GeneralData.Initialize();
        GeneralData.PlayerRace[1] = mRoman;
        GeneralData.PlayerRace[2] = mGreek;

        GameContext.Instance.Engine.GeneralStats.FoodRation = mFull;
        GameContext.Instance.Engine.GeneralStats.Tax = 10;
        GameContext.Instance.Engine.GeneralStats.Population = 1000m;
        GameContext.Instance.Engine.GeneralStats.Housing = 2000;
        GameContext.Instance.Engine.GeneralStats.Money = 200_000m;
        GameContext.Instance.Engine.GeneralStats.Grain = 10000m;
        GameContext.Instance.Engine.GeneralStats.Income = 100;
        GameContext.Instance.Engine.GeneralStats.Initialize();
        GameContext.Instance.Engine.GeneralStats.NumberOfLegion[1] = 1;
        GameContext.Instance.Engine.GeneralStats.NumberOfLegion[2] = 1;
        
        MonthDB[1] = "January";
        MonthDB[2] = "Febuary";
        MonthDB[3] = "March";
        MonthDB[4] = "April";
        MonthDB[5] = "May";
        MonthDB[6] = "June";
        MonthDB[7] = "July";
        MonthDB[8] = "August";
        MonthDB[9] = "Septemeber";
        MonthDB[10] = "October";
        MonthDB[11] = "Novemeber";
        MonthDB[12] = "December";
    }
}
*/