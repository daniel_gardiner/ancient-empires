using AncientEmpires.Engine;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Layout;
using AncientEmpires.Engine.Render.Interface;

namespace AncientEmpires.Test;

public class TestUiElement : UiElement
{
    public TestUiElement(GameEngine engine, IUiElement parent, GameLayout layout) : base(engine, parent, layout)
    {
    }

    protected override void OnCreateFragment()
    {
        Fragment = new InterfaceMeshFragment().SetZIndex(ZIndex);
    }
}