using System.Numerics;
using AncientEmpires.Engine;
using AncientEmpires.Engine.Entities;
using AncientEmpires.Test.TestEngine;
using FluentAssertions;
using Xunit;

namespace AncientEmpires.Test;

public class LoadSaveTests
{
    private readonly TestGameEngine _engine;
    private readonly TestGameFileSystem _fileSystem;

    public LoadSaveTests()
    {
        _engine = TestGameManager.CreateGame();
        _fileSystem = _engine.GetComponent<TestGameFileSystem>();
        _engine.GetComponent<EntitySpawner>().Spawn(new Vector2(50, 100), Entity.Create(EntityType.RomanLegion));
    }

    [Fact]
    public void Save()
    {
        GameManager.Instance.StartGame();
        GameManager.Instance.SaveGame();

        _fileSystem.ListSavedGames().Should().ContainSingle();
    }

    [Fact(Skip = "TODO")]
    public void Load()
    {
        GameManager.Instance.LoadGame(_engine);
    }
}