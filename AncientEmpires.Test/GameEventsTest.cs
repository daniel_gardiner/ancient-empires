using System;
using AncientEmpires.Engine.Events;
using AncientEmpires.Test.TestEngine;
using FluentAssertions;
using Xunit;

namespace AncientEmpires.Test;

public class GameEventsTest
{
    private readonly GameEvents _gameEvents;
    private TestEngineComponent _engineComponent;
    private TestGameEngine _engine;

    public GameEventsTest()
    {
        _engine = TestGameManager.CreateGame();
        _engineComponent = new TestEngineComponent(_engine);
        _gameEvents = new GameEvents();
    }

    [Fact]
    public void Enqueue()
    {
        _gameEvents.Enqueue(new TestGameEvent());
        _gameEvents.Events.Count.Should().Be(1);
    }

    [Fact]
    public void OnUpdate()
    {
        _gameEvents.Enqueue(new TestGameEvent());
        _gameEvents.OnNextFrame(Single.Epsilon);
        _gameEvents.Events.Count.Should().Be(0);
    }

    [Fact]
    public void OnUpdate_AsyncEvent()
    {
        _gameEvents.Enqueue(new TestGameEvent());
        _gameEvents.OnNextFrame(Single.Epsilon);
        _gameEvents.Events.Count.Should().Be(0);
    }
}