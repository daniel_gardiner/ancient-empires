using System;
using System.IO;
using AncientEmpires.Engine.Render;

namespace AncientEmpires.Test.TestEngine;

public class TestJetBrainsFont : Font
{
    public TestJetBrainsFont()
    {
        try
        {
            File.WriteAllText("JetBrains.fnt", Resources.JetBrains_fnt);
            Resources.JetBrains_png.Save("JetBrains.png");
        }
        catch (Exception)
        {
        }

        Texture = new TestTexture(Engine, this, "JetBrains.png");
        ReadConfiguration("JetBrains.fnt");
    }
}