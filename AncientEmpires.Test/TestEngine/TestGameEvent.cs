using System.Collections.Concurrent;
using System.Threading;
using AncientEmpires.Engine;
using AncientEmpires.Engine.Events;

namespace AncientEmpires.Test.TestEngine;

public class TestGameEvent : GameEvent
{
    public override void OnRun(GameEngine engine, ConcurrentQueue<GameEvent> gameEvents)
    {
    }
}

public class TestAsyncGameEvent : AsyncGameEvent
{
    public override void OnRun(GameEngine engine, ConcurrentQueue<GameEvent> gameEvents)
    {
        Thread.Sleep(1000);
    }

    public override void OnComplete(GameEngine engine, ConcurrentQueue<GameEvent> gameEvents)
    {
        ConsoleWriter.WriteLine("TestAsyncGameEvent:OnComplete");
    }
}