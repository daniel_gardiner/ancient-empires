﻿using AncientEmpires.Engine;
using AncientEmpires.Engine.Render;
using Vortice.Direct2D1;
using Vortice.Direct3D11;
using Vortice.DXGI;

namespace AncientEmpires.Test.TestEngine;

public class TestRenderBuffers : RenderBuffers
{
    public TestRenderBuffers(GameEngine engine, GraphicsDevice graphicsDevice, GraphicsPresentation graphicsPresentation) : base(engine, graphicsDevice, graphicsPresentation)
    {
    }

    public override void OnCreateComponents()
    {
    }

    public override void OnInitialize()
    {
    }

    public override void OnEngineReady()
    {
    }

    public override RenderTarget CreateTextureRenderTarget(string name)
    {
        return new TestRenderTarget();
    }

    public override ID2D1Bitmap1 CreateD2DRenderTarget(ID3D11Texture2D surface) => null;
    public override ID2D1Bitmap1 CreateD2DRenderTarget(IDXGISurface surface) => null;
}