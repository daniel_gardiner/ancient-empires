using AncientEmpires.Engine;
using AncientEmpires.Engine.Render;

namespace AncientEmpires.Test.TestEngine;

public class TestTexture : Texture
{
    public TestTexture(GameEngine engine, string filePath) : this(engine, null, filePath)
    {
    }

    public TestTexture(GameEngine engine, Font font, string filePath) : base(engine, font, filePath)
    {
    }
}