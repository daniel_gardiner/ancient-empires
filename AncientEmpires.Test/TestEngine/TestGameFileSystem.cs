﻿using System.IO.Abstractions;
using System.IO.Abstractions.TestingHelpers;
using AncientEmpires.Engine;

namespace AncientEmpires.Test.TestEngine;

public class TestGameFileSystem : GameFileSystem
{
    public override void OnInitialize()
    {
        FileSystem = new MockFileSystem();
    }
    
    public IFileSystem GetFileSystem() => FileSystem;
}