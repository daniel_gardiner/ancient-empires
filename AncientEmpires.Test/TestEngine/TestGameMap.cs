using System.Numerics;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Map.Picker;
using AncientEmpires.Engine.Map.Rendering;
using AncientEmpires.Engine.Render;

namespace AncientEmpires.Test.TestEngine;

public class TestGameMap : GameMap
{
    public TestGameMap()
    {
        TileSize = new Vector4(128, 64, 0, 0);
        PositionStrategy = new IsometricPositionStrategy();
    }

    public override void OnInitialize()
    {
        Tiles = new MapTile[(int)Size.X, (int)Size.Y];
        for (var x = 0; x < Size.X; x++)
        {
            for (var y = 0; y < Size.Y; y++)
            {
                Tiles[x, y] = new MapTile(new Vector4(x, y, 0, 0), Sprite.Empty, Vector4.Zero);
            }
        }
            
        base.OnInitialize();
    }
}