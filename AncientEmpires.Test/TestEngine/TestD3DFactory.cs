using AncientEmpires.Engine;
using Vortice.Direct3D11;

namespace AncientEmpires.Test.TestEngine;

public class TestD3DFactory : D3DFactory
{
    public TestD3DFactory()
    {
    }

    public override ID3D11BlendState1 CreateBlendState(BlendDescription1 desc1) => null;
    public override ID3D11Buffer CreateConstantBuffer<T>(string name) => null;
    public override ID3D11Buffer CreateVertexBuffer(string s, int capacity) => null;
    public override ID3D11Buffer GetNewIndexBuffer(uint[] indices) => null;
}