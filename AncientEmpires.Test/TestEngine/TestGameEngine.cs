using System;
using System.Collections.Generic;
using AncientEmpires.Engine;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Metrics;
using AncientEmpires.Engine.Render;
using AncientEmpires.Engine.Render.Components;
using AncientEmpires.Engine.Render.Util;

namespace AncientEmpires.Test.TestEngine;

public sealed class TestGameEngine : GameEngine
{
    private readonly Dictionary<Type, EngineComponent> _componentSubstitutions = new();

    public override void Register()
    {
        Timing = AddComponent(new TestTiming());
        MetricsRoot = new KnownMetrics(this);
        Configuration = new GameConfiguration();
        ReplaceComponent<CollisionLayer>(new TestCollisionLayer());
        ReplaceComponent<GraphicsPresentation>(new TestGraphicsPresentation(Engine, null));
        ReplaceComponent<GraphicsDevice>(new TestGraphicsDevice(GameContext.Instance.Manager.Window));
        ReplaceComponent<GameMap>(new TestGameMap());
        ReplaceComponent<D3DFactory>(new TestD3DFactory());
        ReplaceComponent<GameTextures>(new TestGameTextures());
        ReplaceComponent<FontManager>(new TestFontManager())
            .Fonts.Add(KnownFont.JetBrains, new TestJetBrainsFont());
        
        base.Register();
    }

    public override TComponent ReplaceComponent<TComponent>(TComponent component)
    {
        if (ComponentCache.ContainsKey(typeof(TComponent)))
            ComponentCache.Remove(typeof(TComponent));

        if (TryGetComponent<TComponent>() != null)
        {
            RemoveComponent<TComponent>();
            AddComponent(component);
        }

        if (_componentSubstitutions.ContainsKey(typeof(TComponent)))
            _componentSubstitutions.Remove(typeof(TComponent));

        _componentSubstitutions.Add(typeof(TComponent), component);
        return component;
    }
    
    public override TComponent PrepareComponent<TComponent>(TComponent component)
    {
        return _componentSubstitutions.TryGetValue(typeof(TComponent), out var substitute)
            ? (TComponent)base.PrepareComponent(substitute)
            : base.PrepareComponent(component);
    }
}