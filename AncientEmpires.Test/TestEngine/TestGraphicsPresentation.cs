﻿using AncientEmpires.Engine;
using AncientEmpires.Engine.Render;

namespace AncientEmpires.Test.TestEngine;

public class TestGraphicsPresentation : GraphicsPresentation
{
    public TestGraphicsPresentation(GameEngine engine, GraphicsDevice graphicsDevice) : base(graphicsDevice)
    {
    }

    public override void OnCreateComponents()
    {
        AddComponent(new TestRenderBuffers(Engine, GraphicsDevice, this));
    }

    public override void OnInitialize()
    {
    }
}