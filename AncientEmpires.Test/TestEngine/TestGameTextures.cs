using System;
using System.Collections.Concurrent;
using AncientEmpires.Engine.Render;
using Vortice.Direct3D11;

namespace AncientEmpires.Test.TestEngine;

public class TestGameTextures : GameTextures
{
    public TestGameTextures()
    {
        TextureCache = new ConcurrentDictionary<string, Texture>
        {
            ["Terrain.dds"] = new TestTexture(Engine, "Terrain.dds"),
            ["Interface.dds"] = new TestTexture(Engine, "Interface.dds"),
            ["Romans.dds"] = new TestTexture(Engine, "Romans.dds"),
            ["TerrainTileMask.dds"] = new TestTexture(Engine, "TerrainTileMask.dds"),
            ["JetBrains.png"] = new TestTexture(Engine, "JetBrains.png"),
        };
    }

    public override ID3D11ShaderResourceView1[] GetAllTextureResources() => Array.Empty<ID3D11ShaderResourceView1>();

    protected override void LoadTextures(params string[] textureFiles)
    {
    }

    protected override void InitializeTextures()
    {
            
    }
}