using System.Drawing;
using AncientEmpires.Engine;

namespace AncientEmpires.Test.TestEngine;

public class TestGameManager : GameManager
{
    public TestGameManager(IGameWindow window) : base(
        window,
        new TestGameFileSystem(), gameWindow => new TestGraphicsDevice(gameWindow))
    {
        IsTestMode = true;
    }

    public override void NewGame()
    {
    }

    public override void StartGame()
    {
    }

    public static TestGameEngine CreateGame(Size? screenSize = null)
    {
        screenSize ??= new Size(2400, 1200);
        var testGameWindow = new TestGameWindow(screenSize);
        var testGameEngine = new TestGameEngine();
        Instance = new TestGameManager(testGameWindow);
        CurrentGame = testGameEngine;
        GameContext.Set(CurrentGame, Instance);
        testGameEngine.InitializeEngine();
        return testGameEngine;
    }
}