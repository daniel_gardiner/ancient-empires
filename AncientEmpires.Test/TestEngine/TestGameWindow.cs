﻿using System;
using System.Drawing;
using System.Numerics;
using System.Windows.Forms;
using AncientEmpires.Engine;

namespace AncientEmpires.Test.TestEngine;

public class TestGameWindow : IGameWindow
{
    private readonly Size? _screenSize;
    private string Text;

    public TestGameWindow(Size? screenSize)
    {
        _screenSize = screenSize;
    }

    public bool HasFocus { get; }
    public IntPtr Handle { get; }
    public Control Control { get; }

    public void SetText(string text)
    {
        Text = text;
    }

    public Vector2 GetSurfaceSize() => new(_screenSize?.Width ?? 2400, _screenSize?.Height ?? 1200);

    public Rectangle GetBounds() => new(10, 10, (int)(10 + GetSurfaceSize().X), (int)(10 + GetSurfaceSize().Y));

    public Vector4 MakeCursorRelative(int x, int y) => new(20, 20, 0, 0);
}