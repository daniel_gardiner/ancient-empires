using AncientEmpires.Engine;
using AncientEmpires.Engine.Render.Components;

namespace AncientEmpires.Test.TestEngine;

public class TestTiming : Timing
{
    public TestTiming()
    {
        SecondFraction = 0.5f;
        Tick = 100;
        Frame = 10;
        Slice = 1;
    }

    //public override bool IsNewFrame(EngineComponent component) => true;
    //public override bool IsNewFrame(EngineComponent component, out float timeDelta) => (timeDelta = 1f) > 0;
    /*
    public override bool IsNewSlice(EngineComponent component) => true;
    */
    public override bool IsNewTick(EngineComponent component) => true;
}