using AncientEmpires.Engine;
using AncientEmpires.Engine.Render;
using AncientEmpires.Engine.Render.Helpers;
using Vortice.Mathematics;

namespace AncientEmpires.Test.TestEngine;

public class TestGraphicsDevice : GraphicsDevice
{
    public TestGraphicsDevice(IGameWindow window) : base(window)
    {
    }

    public override void OnCreateComponents()
    {
        base.OnCreateComponents();
    }

    public override Viewport GetViewport()
    {
        return new Viewport(0, 0, 2400, 1200);
    }

    public override SamplerStateCollection CreateSamplerCollection() => new TestSamplerStateCollection(null);

    public override void OnInitialize()
    {
    }
}