﻿using AncientEmpires.Engine.Render.Helpers;
using Vortice.Direct3D11;

namespace AncientEmpires.Test.TestEngine;

public class TestSamplerStateCollection : SamplerStateCollection
{
    public TestSamplerStateCollection(ID3D11Device1 device) : base(device)
    {
    }
}