﻿using AncientEmpires.Engine.Render;

namespace AncientEmpires.Test.TestEngine;

public class TestRenderTarget : RenderTarget
{
    public TestRenderTarget()
    {
    }

    public override void OnCreateComponents()
    {
    }

    public override void OnInitialize()
    {
    }

    public override void OnEngineReady()
    {
    }
}