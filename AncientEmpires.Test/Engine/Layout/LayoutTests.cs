using System.Drawing;
using System.Numerics;
using AncientEmpires.Engine.Layout;
using AncientEmpires.Test.TestEngine;
using FluentAssertions;
using Xunit;
using static AncientEmpires.Engine.Layout.LayoutDirection;
using static AncientEmpires.Engine.Layout.LayoutUnits;

namespace AncientEmpires.Test.Engine.Layout;

public class LayoutTests
{
    private TestGameEngine _engine;
    private readonly GameLayout _parentLayout;
    private GameLayout _layout;
    private readonly GameLayout _childLayout;
    private readonly GameLayout _siblingLayout;

    public LayoutTests()
    {
        _engine = TestGameManager.CreateGame();
        _parentLayout = new GameLayout(_engine, null);
        _siblingLayout = new GameLayout(_engine, _parentLayout);
        _childLayout = new GameLayout(_engine, _layout);
        _layout = new GameLayout(_engine, _parentLayout);
    }

    [Fact]
    public void Null_parent_defaults_to_screen_size()
    {
        _engine.ScreenSize.Should().Be(new Vector2(2400, 1200));
        new GameLayout(_engine, null).OuterBounds.Should().Be(new RectangleF(0, 0, 2400, 1200));
        new GameLayout(_engine, null).InnerBounds.Should().Be(new RectangleF(0, 0, 2400, 1200));
    }

    [Fact]
    public void EmptyLayout_parent_defaults_to_screen_size()
    {
        _engine.ScreenSize.Should().Be(new Vector2(2400, 1200));
        new GameLayout(_engine, GameLayout.Empty).OuterBounds.Should().Be(new RectangleF(0, 0, 2400, 1200));
        new GameLayout(_engine, GameLayout.Empty).InnerBounds.Should().Be(new RectangleF(0, 0, 2400, 1200));
    }

    [Fact]
    public void Defaults_to_parent_bounds()
    {
        _layout.OuterBounds.Should().Be(_parentLayout.OuterBounds);
        _layout.InnerBounds.Should().Be(_parentLayout.InnerBounds);
    }

    [Fact]
    public void Size()
    {
        _layout.Size(new LayoutSize(100, 200)).OuterBounds.Should().Be(new RectangleF(0, 0, 100, 200));
        _layout.Size(new LayoutSize(100, 200)).InnerBounds.Should().Be(new RectangleF(0, 0, 100, 200));
    }

    [Theory,
     InlineData(Left, 0, 0, 100, 200),
     InlineData(Top, 0, 0, 100, 200),
     InlineData(Right, 2400 - 100, 0, 100, 200),
     InlineData(Bottom, 0, 1200 - 200, 100, 200)]
    public void Docking_with_size(LayoutDirection dock, float expectedLeft, float expectedTop, float expectedRight, float expectedBottom)
    {
        _layout.Size(new LayoutSize(100, 200)).Dock(dock).OuterBounds.Should().Be(new RectangleF(expectedLeft, expectedTop, expectedRight, expectedBottom));
        _layout.Size(new LayoutSize(100, 200)).Dock(dock).InnerBounds.Should().Be(new RectangleF(expectedLeft, expectedTop, expectedRight, expectedBottom));
    }

    [Theory,
     InlineData(Left, 50, 20, 300, 200),
     InlineData(Top, 50, 20, 300, 200),
     InlineData(Right, 2400 - 350, 20, 300, 200),
     InlineData(Bottom, 50, 1200 - 220, 300, 200)]
    public void Docking_with_size_and_padding_OuterBounds(LayoutDirection dock, float expectedLeft, float expectedTop, float expectedRight, float expectedBottom)
    {
        _parentLayout.Padding(50, 20);
        _layout.Size(new LayoutSize(300, 200)).Dock(dock).Padding(25, Pixels).OuterBounds.Should().Be(new RectangleF(expectedLeft, expectedTop, expectedRight, expectedBottom));
    }

    [Theory,
     InlineData(Left, 75, 45, 250, 150),
     InlineData(Top, 75, 45, 250, 150),
     InlineData(Right, 2075, 45, 250, 150),
     InlineData(Bottom, 75, 1005, 250, 150)]
    public void Docking_with_size_and_padding_InnerBounds(LayoutDirection dock, float expectedLeft, float expectedTop, float expectedRight, float expectedBottom)
    {
        _parentLayout.Padding(50, 20);
        _layout.Size(new LayoutSize(300, 200)).Dock(dock).Padding(25, Pixels).InnerBounds.Should().Be(new RectangleF(expectedLeft, expectedTop, expectedRight, expectedBottom));
    }

    [Theory,
     InlineData(Left, Top, 0, 0, 300, 200),
     InlineData(Bottom, Right, 2400 - 300, 1200 - 200, 300, 200)]
    public void Docking_size_with_multiple_directions(LayoutDirection dock1, LayoutDirection dock2, float expectedLeft, float expectedTop, float expectedRight, float expectedBottom)
    {
        _layout.Size(new LayoutSize(300, 200)).Dock(dock1, dock2).OuterBounds.Should().Be(new RectangleF(expectedLeft, expectedTop, expectedRight, expectedBottom));
    }

    [Fact]
    public void Fill()
    {
        _layout.Fill().OuterBounds.Should().Be(_parentLayout.OuterBounds);
    }

    [Fact]
    public void Margin_OuterBounds()
    {
        _parentLayout.Size(new LayoutSize(500, 500));
        _layout.Fill().Margin(50, 30).OuterBounds.Should().Be(new RectangleF(50, 30, 400, 440));
    }

    [Fact]
    public void Margin_InnerBounds()
    {
        _parentLayout.Size(new LayoutSize(500, 500));
        _layout.Fill().Margin(50, 30).InnerBounds.Should().Be(new RectangleF(50, 30, 400, 440));
    }

    [Fact]
    public void Parent_padding()
    {
        _parentLayout.Size(new LayoutSize(500, 500)).Padding(50, 20);
        _layout.Fill().OuterBounds.Should().Be(new RectangleF(50, 20, 400, 460));
    }

    [Fact]
    public void Padding_OuterBounds()
    {
        _parentLayout.Size(new LayoutSize(500, 500));
        _layout.Fill().Padding(50, 20).OuterBounds.Should().Be(new RectangleF(0, 0, 500, 500));
    }

    [Fact]
    public void Padding_InnerBounds()
    {
        _parentLayout.Size(new LayoutSize(500, 500));
        _layout.Fill().Padding(50, 20).InnerBounds.Should().Be(new RectangleF(50, 20, 400, 460));
    }

    [Fact]
    public void Width()
    {
        _parentLayout.Size(new LayoutSize(500, 500));
        var parent = _parentLayout.OuterBounds;
        var expected = new RectangleF(parent.Left, parent.Top, 300, parent.Bottom);
        _layout.Dock(Left, Top, Bottom).Width(300).OuterBounds.Should().Be(expected);
    }

    [Fact]
    public void Width_percent()
    {
        _parentLayout.Size(new LayoutSize(500, 500));
        var parent = _parentLayout.OuterBounds;
        var expected = new RectangleF(parent.Left, parent.Top, 100, parent.Bottom);
        _layout.Dock(Left, Top, Bottom).Width(20, Percent).OuterBounds.Should().Be(expected);
    }

    [Fact]
    public void Width_percent_with_padding()
    {
        _parentLayout.Size(new LayoutSize(500, 500)).Padding(50, Pixels);
        _layout.Dock(Left, Top, Bottom).Width(20, Percent).OuterBounds.Should().Be(new RectangleF(50, 50, 80, 400));
    }

    [Fact]
    public void DockTo_left()
    {
        _parentLayout.Size(new LayoutSize(1000, 500)).Padding(50, 20);
        _siblingLayout.Dock(Top, Bottom, Left).Width(200);
        _layout.Dock(Top, Bottom).DockTo(_siblingLayout, Left).Width(400);
        _layout.OuterBounds.Should().Be(new RectangleF(250, 20, 400, 460));
    }

    [Fact]
    public void DockTo_left_with_margin()
    {
        _parentLayout.Size(new LayoutSize(1000, 500)).Padding(50, 20);
        _siblingLayout.Dock(Top, Bottom, Left).Width(200);
        _layout.Dock(Top, Bottom).DockTo(_siblingLayout, Left, 10).Width(400);
        _layout.OuterBounds.Should().Be(new RectangleF(260, 20, 400, 460));
    }

    [Fact]
    public void DockTo_right()
    {
        _parentLayout.Size(new LayoutSize(1000, 500)).Padding(50, 20);
        _siblingLayout.Dock(Top, Bottom, Right).Width(200);
        _layout.Dock(Top, Bottom).DockTo(_siblingLayout, Right).Width(400);
        _layout.OuterBounds.Should().Be(new RectangleF(350, 20, 400, 460));
    }

    [Fact(Skip = "TODO")]
    public void DockTo_bottom()
    {
        _parentLayout.Size(new LayoutSize(1000, 500)).Padding(50, 20);
        _siblingLayout.Dock(Top, Left).Size(200, 300);
        _layout.Dock(Bottom).DockTo(_siblingLayout, Bottom).Size(300, 400);
        _layout.OuterBounds.Should().Be(new RectangleF(50, 320, 350, 720));
    }

    /*
    [Fact()]
    public void Absolute_bottom()
    {
        _layout.Absolute(new LayoutOffset(40, 100), new LayoutSize(500, 500)).OuterBounds.Should().Be(new RectangleF(100, 40, 540, 600));
    }
*/
}