using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Render.Map;
using AncientEmpires.Test.TestEngine;
using Xunit;

namespace AncientEmpires.Test.Engine.Map;

public class GameMapRendererTests
{
    private readonly TestGameEngine _engine;
    private readonly MapLayer _mapLayer;
    private readonly GameMap _map;

    public GameMapRendererTests()
    {
        _engine = TestGameManager.CreateGame();
        _mapLayer = _engine.GetComponent<GameMap>().GetComponent<MapLayer>();
        _map = _engine.GetComponent<GameMap>();
    }

    [Fact]
    public void BuildMap()
    {
        var mapTiles = _map.Tiles;
        //_mapRenderer.BuildMap(in mapTiles);
    }
}