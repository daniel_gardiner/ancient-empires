using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Render.Map;
using AncientEmpires.Test.TestEngine;
using Xunit;

namespace AncientEmpires.Test.Engine.Map;

public class GameMapGeneratorTests
{
    private readonly TestGameEngine _engine;
    private readonly GameMapGenerator _mapGenerator;
    private readonly GameMap _map;

    public GameMapGeneratorTests()
    {
        _engine = TestGameManager.CreateGame();
        _mapGenerator = _engine.GetComponent<GameMap>().GetComponent<GameMapGenerator>();
        _map = _engine.GetComponent<GameMap>();
    }

    [Fact]
    public void BuildMap()
    {

    }
}