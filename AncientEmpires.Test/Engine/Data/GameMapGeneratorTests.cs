using System.Numerics;
using AncientEmpires.Engine;
using AncientEmpires.Engine.Data;
using AncientEmpires.Engine.Entities;
using AncientEmpires.Test.TestEngine;
using FluentAssertions;
using Xunit;
using Xunit.Abstractions;

namespace AncientEmpires.Test.Engine.Data;

public class EntityLookupTests
{
    private readonly ITestOutputHelper _output;
    private readonly TestGameEngine _engine;
    private readonly EntityLookup _entityLookup;

    public EntityLookupTests(ITestOutputHelper output)
    {
        _output = output;
        _engine = TestGameManager.CreateGame();
        _engine.GetComponent<TestGameMap>().OnEngineReady();
        _entityLookup = _engine.RegisterComponent<EntityLookup>();
    }

    private Entity CreateEntity(Vector4 position)
    {
        var entity = Entity.Create(EntityType.RomanLegion);
        entity.JumpTo(GamePosition.FromMapSpace(position));
        return entity;
    }

    [Fact]
    public void Insert_adds_to_map_and_world_trees()
    {
        _entityLookup.Insert(CreateEntity(new Vector4(98, 98, 0, 0)));
        _entityLookup.MapTree.Count().Should().Be(1);
        _entityLookup.WorldTree.Count().Should().Be(1);
    }

    [Fact]
    public void Insert_multiple_entities()
    {
        var entities = new[]
        {
            CreateEntity(new Vector4(98, 98, 0, 0)),
            CreateEntity(new Vector4(97, 98, 0, 0)),
            CreateEntity(new Vector4(96, 98, 0, 0)),
            CreateEntity(new Vector4(95, 98, 0, 0)),
            CreateEntity(new Vector4(98, 97, 0, 0)),
            CreateEntity(new Vector4(97, 97, 0, 0)),
            CreateEntity(new Vector4(96, 97, 0, 0)),
            CreateEntity(new Vector4(95, 97, 0, 0)),
            CreateEntity(new Vector4(88, 88, 0, 0)),
            CreateEntity(new Vector4(78, 78, 0, 0)),
            CreateEntity(new Vector4(68, 68, 0, 0)),
            CreateEntity(new Vector4(58, 58, 0, 0)),
        };

        foreach (var entity in entities)
        {
            _entityLookup.Insert(entity);
        }

        _entityLookup.MapTree.Count().Should().Be(entities.Length);
        _entityLookup.WorldTree.Count().Should().Be(entities.Length);
        _output.WriteLine("_entityLookup.MapTree.GetGrid().Length = " + _entityLookup.MapTree.GetGrid().Length);
        _output.WriteLine("_entityLookup.WorldTree.GetGrid().Length = " + _entityLookup.WorldTree.GetGrid().Length);

        foreach (var grid in _entityLookup.WorldTree.GetGrid())
        {
            var screenSpaceTL = _engine.Camera.WorldSpaceToScreenSpaceFlipped(new Vector4(grid.Left, grid.Top, 0, 0));
            var screenSpaceBL = _engine.Camera.WorldSpaceToScreenSpaceFlipped(new Vector4(grid.X + grid.Width, grid.Y + grid.Height, 0, 0));
            _output.WriteLine("{0}x{1} {2}x{3} | {4}x{5} {6}x{7}",
                grid.Left,
                grid.Top,
                grid.Width,
                grid.Height,
                screenSpaceTL.X,
                screenSpaceTL.Y,
                screenSpaceBL.X,
                screenSpaceBL.Y);
        }
    }
}