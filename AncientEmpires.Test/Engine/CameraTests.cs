using System;
using System.Drawing;
using System.Numerics;
using AncientEmpires.Engine.Map;
using AncientEmpires.Engine.Map.Rendering;
using AncientEmpires.Engine.Render.Map;
using AncientEmpires.Test.TestEngine;
using FluentAssertions;
using Xunit;

namespace AncientEmpires.Test.Engine;

public class CameraTests
{
    private readonly TestGameEngine _engine;
    private readonly GameMapGenerator _mapGenerator;
    private readonly GameMap _map;
    private readonly IsometricPositionStrategy _positionStrategy;
    private readonly Vector2 _halfScreen;

    public CameraTests()
    {
        _engine = TestGameManager.CreateGame(new Size(2400, 1200));
        _mapGenerator = _engine.GetComponent<GameMap>().GetComponent<GameMapGenerator>();
        _map = _engine.GetComponent<GameMap>();
        _positionStrategy = _engine.GetComponent<IsometricPositionStrategy>();
        _halfScreen = _engine.ScreenSize / 2;
    }

    [Fact]
    public void WorldSpaceToScreenSpace()
    {
        var worldSpace = new Vector4(0, 0, 0, 0);
        var screenSpace = new Vector4(_engine.ScreenSize.X / 2, _engine.ScreenSize.Y / 2, 0, 0);
        _engine.Camera.WorldSpaceToScreenSpace(worldSpace).Should().Be(screenSpace);
    }

    [Fact]
    public void WorldSpaceToScreenSpace_with_offsets()
    {
        var screenCenter = new Vector4(_engine.ScreenSize.X / 2, _engine.ScreenSize.Y / 2, 0, 0);
        _engine.Camera.WorldSpaceToScreenSpace(new Vector4(-100, 100, 0, 0)).Should().Be(screenCenter + new Vector4(-100, -100, 0, 0));
        _engine.Camera.WorldSpaceToScreenSpace(new Vector4(-100, -100, 0, 0)).Should().Be(screenCenter + new Vector4(-100, 100, 0, 0));
        _engine.Camera.WorldSpaceToScreenSpace(new Vector4(100, 100, 0, 0)).Should().Be(screenCenter + new Vector4(100, -100, 0, 0));
        _engine.Camera.WorldSpaceToScreenSpace(new Vector4(100, -100, 0, 0)).Should().Be(screenCenter + new Vector4(100, 100, 0, 0));
    }

    [Fact]
    public void ScreenSpaceToWorldSpace()
    {
        var screenSpace = new Vector4(_engine.ScreenSize.X / 2, _engine.ScreenSize.Y / 2, 0, 0);
        _engine.Camera.ScreenSpaceToWorldSpace(screenSpace).Should().Be(_engine.Camera.Unproject(screenSpace));
    }

    [Fact]
    public void ScreenSpaceToMapSpace()
    {
        var screenSpace = new Vector4(_engine.ScreenSize.X / 2, _engine.ScreenSize.Y / 2, 0, 0);
        var position = _engine.Camera.ScreenSpaceToMapSpace(screenSpace);
        var rounded = new Vector4((float)Math.Floor(position.X), (float)Math.Floor(position.Y), 0, 0);
        rounded.Should().Be(new Vector4(100, 100, 0, 0));
    }

    [Fact]
    public void MapSpaceToScreenSpace()
    {
        var mapSpace = new Vector4(100, 100, 0, 0);
        var screenSpace = _engine.Camera.MapSpaceToScreenSpace(mapSpace);
        screenSpace.Should().Be(new Vector4(_engine.ScreenSize.X / 2, _engine.ScreenSize.Y / 2, 0, 0));
    }

    [Fact]
    public void MapSpaceToWorldSpace()
    {
        var mapSpace = new Vector4(100, 100, 0, 0);
        _engine.Camera.MapSpaceToWorldSpace(mapSpace).Should().Be(new Vector4(0, 0, 0, 0));
    }

    [Fact]
    public void MapSpaceToWorldSpace_with_offsets()
    {
        var mapSpace = new Vector4(100, 100, 0, 0);
        var expected = new Vector4(0, 0, 0, 0);

        _engine.Camera.MapSpaceToWorldSpace(mapSpace + new Vector4(-1, -1, 0, 0)).Should().Be(expected + new Vector4(0, 64, 0, 0));
        _engine.Camera.MapSpaceToWorldSpace(mapSpace + new Vector4(1, 1, 0, 0)).Should().Be(expected + new Vector4(0, -64, 0, 0));
        _engine.Camera.MapSpaceToWorldSpace(mapSpace + new Vector4(1, 0, 0, 0)).Should().Be(expected + new Vector4(64, -32, 0, 0));
        _engine.Camera.MapSpaceToWorldSpace(mapSpace + new Vector4(-1, 0, 0, 0)).Should().Be(expected + new Vector4(-64, 32, 0, 0));
        _engine.Camera.MapSpaceToWorldSpace(mapSpace + new Vector4(0, 1, 0, 0)).Should().Be(expected + new Vector4(-64, -32, 0, 0));
        _engine.Camera.MapSpaceToWorldSpace(mapSpace + new Vector4(0, -1, 0, 0)).Should().Be(expected + new Vector4(64, 32, 0, 0));
    }

    [Fact]
    public void MapSpaceToScreenSpace_with_scrolling()
    {
        _engine.Camera.ScrollBy(200, 0);

        var mapSpace = new Vector4(100, 100, 0, 0);
        var screenSpace = _engine.Camera.MapSpaceToScreenSpace(mapSpace);
        screenSpace.Should().Be(new Vector4(_engine.ScreenSize.X / 2 - 200, _engine.ScreenSize.Y / 2, 0, 0));

        _engine.Camera.ScrollBy(-400, 0);
        screenSpace = _engine.Camera.MapSpaceToScreenSpace(mapSpace);
        screenSpace.Should().Be(new Vector4(_engine.ScreenSize.X / 2 + 200, _engine.ScreenSize.Y / 2, 0, 0));
    }

    [Fact]
    public void ScreenSpaceToMapSpace_with_offsets()
    {
        var screenSpace = new Vector4(_engine.ScreenSize.X / 2, _engine.ScreenSize.Y / 2, 0, 0);
        screenSpace += new Vector4(128f, 64f, 0, 0);
        var position = _engine.Camera.ScreenSpaceToMapSpace(screenSpace);
        var rounded = new Vector4((float)Math.Floor(position.X), (float)Math.Floor(position.Y), 0, 0);
        rounded.Should().Be(new Vector4(102, 100, 0, 0));
    }

    [Fact]
    public void ScreenSpaceToWorldSpace_center_is_0_0()
    {
        var screenSpace = new Vector4(_halfScreen.X, _halfScreen.Y, 0, 0);
        _engine.Camera.ScreenSpaceToWorldSpace(screenSpace).Should().Be(new Vector4(0, 0, 0, 0));
    }

    [Fact]
    public void ScreenSpaceToWorldSpace_with_offsets()
    {
        var screenSpace = new Vector4(_halfScreen.X, _halfScreen.Y, 0, 0) + new Vector4(150, 90, 0, 0);
        var actual = _engine.Camera.ScreenSpaceToWorldSpace(screenSpace);
        var vector2 = actual - new Vector4(150, -90, 0, 0);
        vector2.Length().Should().BeLessThan(0.1f);

        screenSpace = new Vector4(_halfScreen.X, _halfScreen.Y, 0, 0) - new Vector4(150, 90, 0, 0);
        actual = _engine.Camera.ScreenSpaceToWorldSpace(screenSpace);
        vector2 = actual - new Vector4(-150, 90, 0, 0);
        vector2.Length().Should().BeLessThan(0.1f);
    }

    [Fact]
    public void MapSpaceToGameSpace()
    {
        var screenSpace = new Vector4(_halfScreen.X, _halfScreen.Y, 0, 0) + new Vector4(150, 90, 0, 0);
        var actual = _engine.Camera.MapSpaceToGameSpace(in screenSpace);
        var vector2 = actual - new Vector4(150, -90, 0, 0);
        vector2.Length().Should().BeLessThan(0.1f);

        screenSpace = new Vector4(_halfScreen.X, _halfScreen.Y, 0, 0) - new Vector4(150, 90, 0, 0);
        actual = _engine.Camera.MapSpaceToGameSpace(in screenSpace);
        vector2 = actual - new Vector4(-150, 90, 0, 0);
        vector2.Length().Should().BeLessThan(0.1f);
    }

    [Fact]
    public void WorldSpaceToGameSpace()
    {
        var worldSpace = new Vector4(0, 0, 0, 0);
        _engine.Camera.WorldSpaceToGameSpace(in worldSpace).Should().Be(new Vector4(100, 100, 0, 0));
    }
}