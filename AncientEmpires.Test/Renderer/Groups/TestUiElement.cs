using AncientEmpires.Engine;
using AncientEmpires.Engine.Interface;
using AncientEmpires.Engine.Layout;
using AncientEmpires.Engine.Render.Interface;

namespace AncientEmpires.Test.Renderer.Groups;

public class TestUiElement : UiElement
{
    public TestUiElement(GameEngine engine, IUiElement parent, GameLayout layout) : base(engine, parent, layout)
    {
    }

    protected override void OnCreateFragment()
    {
        Fragment = RegisterComponent<InterfaceMeshFragment>().SetZIndex(ZIndex);
    }
}