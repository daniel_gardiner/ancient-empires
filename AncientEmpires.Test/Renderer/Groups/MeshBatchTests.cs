using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using AncientEmpires.Engine.Entities;
using AncientEmpires.Engine.Render;
using AncientEmpires.Engine.Render.Entities;
using AncientEmpires.Engine.Render.Groups;
using AncientEmpires.Engine.Render.Text;
using AncientEmpires.Test.TestEngine;
using FluentAssertions;
using Xunit;

namespace AncientEmpires.Test.Renderer.Groups;

public class MeshBatchTests
{
    private readonly GameMeshBatch _batch;
    private readonly TestGameEngine _engine;
    private readonly TestEngineComponent _parent;
    private readonly Entity _entity;
    private int entityCounter;
    private readonly TestEntityRender _entityRenderer;
    private GameMeshBatches _gameMeshBatches;

    public MeshBatchTests()
    {
        _engine = TestGameManager.CreateGame();
        _parent = new TestEngineComponent(null);
        _gameMeshBatches = _engine.RegisterComponent(new GameMeshBatches(new TiledMeshBatchStrategy(_engine, true)));
        _batch = _gameMeshBatches.RegisterNewBatch(new GameMeshBatch());
        _batch.AddComponent(new DebugTextLayer());
        _entityRenderer = new TestEntityRender();
        _entity = Entity.Create(_engine, entityCounter++, EntityType.RomanLegion);
        _entity.Register();
        _entity.OnNextFrame(0);
    }

    [Fact]
    public void AddMesh()
    {
        var meshFragment = new EntityMeshFragment(_entity);
        _batch.AddFragment(meshFragment);
    }

    [Fact]
    public void AddFragment_adding_one_fragment_multiple_times()
    {
        var fragment = CreateFragment(new Vector4(199, 198, 0, 0));
        var fragment1 = _batch.AddFragment(fragment);
        fragment.Position = new Vector4(100, 100, 0, 0);
        _batch.AddFragment(fragment);
        fragment.Position = new Vector4(50, 100, 0, 0);
        _batch.AddFragment(fragment);

        _batch.AllocationLookup.Count.Should().Be(1);
        _batch.AllocationLookup[fragment1.Key].Offset.Should().Be(0);
    }

    [Fact]
    public void AddFragment_y_axis_adding_multiple_fragments_should_be_sorted_by_depth()
    {
        var fragment1 = _batch.AddFragment(CreateFragment(new Vector4(199, 195, 0, 0)));
        var fragment2 = _batch.AddFragment(CreateFragment(new Vector4(199, 196, 0, 0)));
        var fragment3 = _batch.AddFragment(CreateFragment(new Vector4(199, 197, 0, 0)));
        var fragment4 = _batch.AddFragment(CreateFragment(new Vector4(199, 198, 0, 0)));

        var fragments = _batch.Fragments.Cast<EntityMeshFragment>().ToArray();
        fragments[0].Entity.Should().Be(fragment1.Entity);
        fragments[1].Entity.Should().Be(fragment2.Entity);
        fragments[2].Entity.Should().Be(fragment3.Entity);
        fragments[3].Entity.Should().Be(fragment4.Entity);

        var vertices = _batch.Fragments.SelectMany(o => o.Quads.SelectMany(o => o.AllVertices)).ToArray();
        new Span<Vertex>(vertices, 0, 4).ToArray().Should().BeEquivalentTo(fragment1.Quads.First().AllVertices);
        new Span<Vertex>(vertices, 4, 4).ToArray().Should().BeEquivalentTo(fragment2.Quads.First().AllVertices);
        new Span<Vertex>(vertices, 8, 4).ToArray().Should().BeEquivalentTo(fragment3.Quads.First().AllVertices);
        new Span<Vertex>(vertices, 12, 4).ToArray().Should().BeEquivalentTo(fragment4.Quads.First().AllVertices);
    }

    [Fact]
    public void AddFragment_x_axis_adding_multiple_fragments_should_be_sorted_by_depth()
    {
        var fragment1 = _batch.AddFragment(CreateFragment(new Vector4(195, 199, 0, 0)));
        var fragment2 = _batch.AddFragment(CreateFragment(new Vector4(196, 199, 0, 0)));
        var fragment3 = _batch.AddFragment(CreateFragment(new Vector4(197, 199, 0, 0)));
        var fragment4 = _batch.AddFragment(CreateFragment(new Vector4(198, 199, 0, 0)));

        var fragments = _batch.Fragments.Cast<EntityMeshFragment>().ToArray();
        fragments[0].Entity.Should().Be(fragment1.Entity);
        fragments[1].Entity.Should().Be(fragment2.Entity);
        fragments[2].Entity.Should().Be(fragment3.Entity);
        fragments[3].Entity.Should().Be(fragment4.Entity);
    }

    [Fact]
    public void AddFragment_x_axis_adding_multiple_fragments_should_be_sorted_by_depth_long()
    {
        var addedFragments = new List<EntityMeshFragment>();
        for (var x = 0; x < 199; x++) addedFragments.Add(_batch.AddFragment(CreateFragment(new Vector4(x, 199, 0, 0))));

        var fragments = _batch.Fragments.Cast<EntityMeshFragment>().ToArray();
        fragments.Should().ContainInOrder(addedFragments);
        
        _batch.Clear();
        addedFragments.Clear();
        for (var x = 0; x < 199; x++) addedFragments.Add(_batch.AddFragment(CreateFragment(new Vector4(x, 99, 0, 0))));
        for (var x = 0; x < 199; x++) addedFragments.Add(_batch.AddFragment(CreateFragment(new Vector4(x, 100, 0, 0))));

        fragments = _batch.Fragments.Cast<EntityMeshFragment>().ToArray();
        fragments.Should().ContainInOrder(addedFragments);
    }
    
    [Fact]
    public void AddFragment_y_axis_adding_multiple_fragments_should_be_sorted_by_depth_long()
    {
        var addedFragments = new List<EntityMeshFragment>();
        for (int x = 0; x < 199; x++)
        {
            addedFragments.Add(_batch.AddFragment(CreateFragment(new Vector4(x, 199, 0, 0))));
        }

        var fragments = _batch.Fragments.Cast<EntityMeshFragment>().ToArray();
        fragments.Should().ContainInOrder(addedFragments);
    }

    [Fact]
    public void AddFragment_insert_descending_depth_inserts()
    {
        var fragment1 = _batch.AddFragment(CreateFragment(new Vector4(199, 199, 0, 0)));
        var fragment2 = _batch.AddFragment(CreateFragment(new Vector4(199, 198, 0, 0)));
        var fragment3 = _batch.AddFragment(CreateFragment(new Vector4(199, 197, 0, 0)));
        var fragment4 = _batch.AddFragment(CreateFragment(new Vector4(199, 196, 0, 0)));

        var fragments = _batch.Fragments.Cast<EntityMeshFragment>().ToArray();
        fragments[0].Entity.Should().Be(fragment4.Entity);
        fragments[1].Entity.Should().Be(fragment3.Entity);
        fragments[2].Entity.Should().Be(fragment2.Entity);
        fragments[3].Entity.Should().Be(fragment1.Entity);

        _batch.VertexCount.Should().Be(16);
        _batch.Fragments.Count().Should().Be(4);
        var vertices = _batch.Fragments.SelectMany(o => o.Quads.SelectMany(o => o.AllVertices)).ToArray();
        new Span<Vertex>(vertices, 0, 4).ToArray().Should().BeEquivalentTo(fragment4.Quads.First().AllVertices);
        new Span<Vertex>(vertices, 4, 4).ToArray().Should().BeEquivalentTo(fragment3.Quads.First().AllVertices);
        new Span<Vertex>(vertices, 8, 4).ToArray().Should().BeEquivalentTo(fragment2.Quads.First().AllVertices);
        new Span<Vertex>(vertices, 12, 4).ToArray().Should().BeEquivalentTo(fragment1.Quads.First().AllVertices);
    }

    [Fact]
    public void AddFragment_xy_axis_adding_multiple_fragments_should_be_sorted_by_depth()
    {
        var fragment1 = _batch.AddFragment(CreateFragment(new Vector4(197, 197, 0, 0)));
        var fragment2 = _batch.AddFragment(CreateFragment(new Vector4(198, 197, 0, 0)));
        var fragment3 = _batch.AddFragment(CreateFragment(new Vector4(197, 198, 0, 0)));
        var fragment4 = _batch.AddFragment(CreateFragment(new Vector4(198, 198, 0, 0)));

        var fragments = _batch.Fragments.Cast<EntityMeshFragment>().ToArray();
        fragments[0].Entity.Should().Be(fragment1.Entity);
        fragments[1].Entity.Should().Be(fragment2.Entity);
        fragments[2].Entity.Should().Be(fragment3.Entity);
        fragments[3].Entity.Should().Be(fragment4.Entity);
    }

    [Fact]
    public void AddFragment_update_reduce_depth_should_move_fragment_to_end()
    {
        var fragment1 = _batch.AddFragment(CreateFragment(new Vector4(199, 198, 0, 0)));
        var fragment2 = _batch.AddFragment(CreateFragment(new Vector4(199, 197, 0, 0)));
        var fragment3 = _batch.AddFragment(CreateFragment(new Vector4(199, 196, 0, 0)));
        var fragment4 = _batch.AddFragment(CreateFragment(new Vector4(199, 195, 0, 0)));
        UpdatePosition(fragment4, new Vector4(199, 199, 0, 0));

        _batch.AddFragment(fragment4);

        var fragments = _batch.Fragments.Cast<EntityMeshFragment>().ToArray();
        fragments[0].Entity.Should().Be(fragment3.Entity);
        fragments[1].Entity.Should().Be(fragment2.Entity);
        fragments[2].Entity.Should().Be(fragment1.Entity);
        fragments[3].Entity.Should().Be(fragment4.Entity);

        _batch.VertexCount.Should().Be(16);
        _batch.Fragments.Count().Should().Be(4);
        _batch.Fragments.First().Should().Be(fragment3);
        var vertices = _batch.Fragments.SelectMany(o => o.Quads.SelectMany(o => o.AllVertices)).ToArray();
        new Span<Vertex>(vertices, 0, 4).ToArray().Should().BeEquivalentTo(fragment3.Quads.First().AllVertices);
        new Span<Vertex>(vertices, 4, 4).ToArray().Should().BeEquivalentTo(fragment2.Quads.First().AllVertices);
        new Span<Vertex>(vertices, 8, 4).ToArray().Should().BeEquivalentTo(fragment1.Quads.First().AllVertices);
        new Span<Vertex>(vertices, 12, 4).ToArray().Should().BeEquivalentTo(fragment4.Quads.First().AllVertices);
    }

    [Fact]
    public void AddFragment_update_mesh_increase_depth_should_move_fragment_to_start()
    {
        var fragment1 = _batch.AddFragment(CreateFragment(new Vector4(199, 198, 0, 0)));
        var fragment2 = _batch.AddFragment(CreateFragment(new Vector4(199, 197, 0, 0)));
        var fragment3 = _batch.AddFragment(CreateFragment(new Vector4(199, 196, 0, 0)));
        var fragment4 = _batch.AddFragment(CreateFragment(new Vector4(199, 199, 0, 0)));
        UpdatePosition(fragment4, new Vector4(199, 195, 0, 0));

        _batch.AddFragment(fragment4);

        var fragments = _batch.Fragments.Cast<EntityMeshFragment>().ToArray();
        fragments[0].Entity.Should().Be(fragment4.Entity);
        fragments[1].Entity.Should().Be(fragment3.Entity);
        fragments[2].Entity.Should().Be(fragment2.Entity);
        fragments[3].Entity.Should().Be(fragment1.Entity);

        _batch.VertexCount.Should().Be(16);
        _batch.Fragments.Count().Should().Be(4);
        _batch.Fragments.ElementAt(0).Should().Be(fragment4);
        var vertices = _batch.Fragments.SelectMany(o => o.Quads.SelectMany(o => o.AllVertices)).ToArray();
        new Span<Vertex>(vertices, 0, 4).ToArray().Should().BeEquivalentTo(fragment4.Quads.First().AllVertices);
        new Span<Vertex>(vertices, 4, 4).ToArray().Should().BeEquivalentTo(fragment3.Quads.First().AllVertices);
        new Span<Vertex>(vertices, 8, 4).ToArray().Should().BeEquivalentTo(fragment2.Quads.First().AllVertices);
        new Span<Vertex>(vertices, 12, 4).ToArray().Should().BeEquivalentTo(fragment1.Quads.First().AllVertices);
    }

    [Fact]
    public void AddFragment_updating_existing_fragment()
    {
        var fragment1 = _batch.AddFragment(CreateFragment(new Vector4(197, 197, 0, 0)));
        var fragment2 = _batch.AddFragment(CreateFragment(new Vector4(198, 197, 0, 0)));

        _batch.Fragments.OfType<EntityMeshFragment>().ElementAt(0).Entity.Should().Be(fragment1.Entity);
        _batch.Fragments.OfType<EntityMeshFragment>().ElementAt(1).Entity.Should().Be(fragment2.Entity);

        UpdatePosition(fragment1, new Vector4(197, 197, 0, 0));
        _batch.AddFragment(fragment1);
        UpdatePosition(fragment1, new Vector4(197, 197, 0, 0));
        _batch.AddFragment(fragment1);

        _batch.Fragments.OfType<EntityMeshFragment>().ElementAt(0).Entity.Should().Be(fragment1.Entity);
        _batch.Fragments.OfType<EntityMeshFragment>().ElementAt(1).Entity.Should().Be(fragment2.Entity);
    }

    private static void UpdatePosition(EntityMeshFragment fragment4, Vector4 position)
    {
        fragment4.Position = position;
        fragment4.Entity.Position = position;
        fragment4.Entity.OnNextFrame(1);
        fragment4.OnUpdateDepth();
        fragment4.OnUpdateFragment(1);
    }

    private EntityMeshFragment CreateFragment(Vector4 position)
    {
        var entity = Entity.Create(_engine, entityCounter++, EntityType.RomanLegion);
        entity.Position = position;
        entity.Fragment = new EntityMeshFragment(entity);
        entity.Fragment.OnUpdateFragment(1);
        entity.OnNextFrame(1);
        return entity.Fragment;
    }
}