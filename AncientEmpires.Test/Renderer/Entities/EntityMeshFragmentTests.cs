using System.Numerics;
using AncientEmpires.Engine;
using AncientEmpires.Engine.Entities;
using AncientEmpires.Engine.Map.Rendering;
using AncientEmpires.Engine.Render.Entities;
using AncientEmpires.Test.TestEngine;
using FluentAssertions;
using Xunit;

namespace AncientEmpires.Test.Renderer.Entities;

public class EntityMeshFragmentTests
{
    private readonly TestGameEngine _engine;
    private TestEngineComponent _parent;
    private Entity _entity;
    private IsometricPositionStrategy _positionStrategy;
    private readonly Vector4 _mapDimensions;
    private readonly Vector4 _tileSize;

    public EntityMeshFragmentTests()
    {
        _engine = TestGameManager.CreateGame();
        _parent = new TestEngineComponent(null);
        _mapDimensions = new Vector4(200, 200, 0, 0);
        _tileSize = new Vector4(128, 64, 0, 0);
        _positionStrategy = new IsometricPositionStrategy();
        _entity = CreateEntity();
    }

    [Fact]
    public void Depth()
    {
        CreateFragment(new Vector4(0, 0, 0, 0)).Depth.Should().BeInRange(4, 5);
        CreateFragment(new Vector4(50, 50, 0, 0)).Depth.Should().BeInRange(4, 5);
        CreateFragment(new Vector4(199, 199, 0, 0)).Depth.Should().BeInRange(4, 5);
    }

    private EntityMeshFragment CreateFragment(Vector4 position)
    {
        _entity.Position = position;
        var fragment = new EntityMeshFragment(_entity);
        fragment.ZIndex = GameContext.Instance.Engine.Configuration.GetRenderLayer(KnownRenderLayers.Entities);
        fragment.OnUpdateDepth();
        return fragment;
    }

    private Entity CreateEntity()
    {
        var entity = Entity.Create(EntityType.RomanLegion);
        entity.MarkDirty();
        return entity;
    }
}